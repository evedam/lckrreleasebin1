//****************************************************************************************************
var loadingParams = 
{
	mediaConstraints: {video: true, audio: true},
	offerOptions: {},
	peerConnectionConfig: {rtcpMuxPolicy: "require", iceServers: [], bundlePolicy: "max-bundle"},
	peerConnectionConstraints: {optional: []},
};

var p_VidCtrl = null;
var p_Msg = null;

var b_ImgReceived = false;

var p_TabCtrl = new TabCtrl();

//****************************************************************************************************
function initialize() 
{
	if (document.visibilityState === 'prerender') 
	{
		document.addEventListener('visibilitychange', onVisibilityChange);
		return;
	}

	p_VidCtrl = new VidCtrl(loadingParams);
}

//****************************************************************************************************
function onVisibilityChange() 
{
	if (document.visibilityState === 'prerender') 
	{
		return;
	}

	document.removeEventListener('visibilitychange', onVisibilityChange);
	initialize();
}

initialize();

//****************************************************************************************************
//****************************************************************************************************
var s_To = null;
var ui_ImgReqTime = 0;

//****************************************************************************************************
function register(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel)
{
	var pMsg = new Msg(p_VidCtrl);
	pMsg.register(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel, function(uiId, bRet, sMsg)
	{
		console.log("id = " + uiId + " ret = " + bRet + " msg = " + sMsg);	

		if (bRet)
		{
			console.log("registation done");
			alert("user registration successful");
		}
		else
		{
			alert("unable to register user " + sMsg);
		}
	}.bind(this));
}

//****************************************************************************************************
function sendContactReq(sTo)
{
	p_Msg.sendContactReq(121, "", sTo);
}

//****************************************************************************************************
function sendCamReq(sTo)
{
	var sSesId = "session_id_" + (new Date()).getTime();
	p_Msg.sendCamReq(122, "", sSesId, sTo);
}

//****************************************************************************************************
function sendMsg()
{
	var sTo = document.getElementById("txt_msg_to").value;
	var sMsg = document.getElementById("txt_msg").value;
	p_Msg.sendMsg(123, "", sTo, "{\"val\":\"" + sMsg + "\"}");
}

//****************************************************************************************************
function onClickButton(sId, fncOnClick, bConnectRequired)
{
	console.log("onClickButton id = " + sId);

	var pCmd = document.getElementById(sId);
	if (pCmd == null || pCmd == undefined)
	{
		console.log("unable to find element by id (" + sId + ")");
		return;
	}

	pCmd.onclick = function()
	{
		pCmd.disabled = true;
		setTimeout(function(){ pCmd.disabled = false; }, 1000);	

		if (bConnectRequired && s_To == null)
			console.log("please connect to peer");
		else
			return fncOnClick();
	};
};

//****************************************************************************************************
function onSeekChange(sId, fncOnChange, bConnectRequired)
{
	console.log("onSeekChange id = " + sId);

	var pSeek = document.getElementById(sId);
	if (pSeek == null || pSeek == undefined)
	{
		console.log("unable to find element by id (" + sId + ")");
		return;
	}

	pSeek.onchange = function()
	{
		var iVal = pSeek.value;
		pSeek.disabled = true;
		setTimeout(function(){ pSeek.disabled = false; }, 1000);	

		if (bConnectRequired && s_To == null)
			console.log("please connect to peer");
		else
			fncOnChange(iVal);
	};
};

//****************************************************************************************************
function initMsg()
{
	p_Msg = new Msg(p_VidCtrl);
}

//****************************************************************************************************
function initDefaultMsgCallback()
{
	p_Msg.setOnContactReq(function(uiId, sFrom)
	{
		addCustomer(sFrom);
	}.bind(this));

	p_Msg.setOnCamReq(function(uiId, sFrom, sSesId)
	{
		if (window.confirm("Connection request from " + sFrom + ". Do you want to connect ?"))
		{
			transformUiToConnected();
			p_Msg.sendCamRes(uiId, "", sSesId, sFrom, true);
			this.p_VidCtrl.connect(sFrom, sSesId);
			s_To = sFrom;
		}
		else
			console.log("connection not confirmed");

	}.bind(this));

	p_Msg.setOnCamRes(function(iId, sFrom, sSesId, bAccept)
	{
		console.log("id = " + iId + " from = " + sFrom + " ses_id = " + sSesId);

		if (bAccept == true)
		{
			transformUiToConnected();
			s_To = sFrom;
			p_VidCtrl.connect(sFrom, sSesId);
		}
		else
			console.log("access denied");
	}.bind(this));

	p_Msg.setOnGetGpsRes(function(iId, sFrom, dLat, dLng)
	{
		addGpsInfo(sFrom, dLat, dLng);
	}.bind(this));

	p_Msg.setOnFileStart(function(sFrom, uiId, uiLen, sMsg)
	{
		console.log("on image receive start from = " + sFrom + " id = " + uiId + " len = " + uiLen + " msg = " + sMsg);
		addImgReceiveProgressbar(sFrom, uiId);
	}.bind(this));

	p_Msg.setOnFileProgress(function(sFrom, uiId, uiProgress, sMsg)
	{
		console.log("on image progress from = " + sFrom + " id = " + uiId  + " progress = " + uiProgress + " msg = " + sMsg);
		setImgReceiveProgress(sFrom, uiId, uiProgress);
		
	}.bind(this));

	p_Msg.setOnFile(function(sFrom, uiId, sFile, sMsg)
	{
		var uiTime = new Date().getTime() - ui_ImgReqTime;
		console.log("on image from " + sFrom + " receive time = " + uiTime + " msg = " + sMsg);

		addImg('data:image/jpeg;base64,' + btoa(sFile));

		b_ImgReceived = true;
		
		setTimeout(function(){ rmImgReceiveProgressDiv(sFrom, uiId)}, 1000);
	}.bind(this));

	p_Msg.setOnLocalHangup(function(sSesId)
	{
		console.log("local hangup ses_id = " + sSesId);
		transformUiToDefault();
	}.bind(this));

	p_Msg.setOnRemoteHangup(function(sSesId)
	{
		console.log("remote hangup ses_id = " + sSesId);
		transformUiToDefault();
	}.bind(this));
	
	p_Msg.setOnUsrAdd(function(sId, sState)
	{
		console.log("add user id = " + sId);	
		setUsrState(sId, UsrState.Connect);
	}.bind(this));

	p_Msg.setOnUsrConnect(function(sId)
	{
		console.log("on usr connected");
		setUsrState(sId, UsrState.Connect);
	}.bind(this));

	p_Msg.setOnUsrDisconnect(function(sId)
	{
		console.log("on usr disconnect");	
		setUsrState(sId, UsrState.Disconnect);
	}.bind(this));

	p_Msg.setOnUsrLogout(function(sId)
	{
		console.log("on usr remove");
		rmCustomer(sId);
	});
	
	p_Msg.setOnUsrLst(function(uiId, pMstUsr)
	{
		console.log("on lst users id = " + uiId + " lst_usr = " + JSON.stringify(pMstUsr));

		for (var sId in pMstUsr)
		{
			var pUsr = pMstUsr[sId];
			console.log("adding user " + sId + " state = " + pUsr.state);
			setUsrState(sId, UsrState.toState(pUsr.state));
		}
			
	}.bind(this));

	p_Msg.setOnLstApk(function(pData)
	{
		console.log("on lst apk " + JSON.stringify(pData));

		var pSelApk = document.getElementById("sel_apk_id");
		if (pSelApk == null)
			return;

		pSelApk.innerHTML = "";
		var iLen = pData.length;
		for (var iIdx = 0; iIdx < iLen; iIdx++)
		{
			var pApk = pData[iIdx];
			var pOpt = document.createElement("option");	
			pOpt.text = pApk.desc;
			pOpt.value = pApk.id; 
			pSelApk.add(pOpt);
		}
	});

	p_Msg.setOnLstApkInstalled(function(iId, sFrom, aPkg)
	{
		console.log("on lst apk installed id = " + iId + " from = " + sFrom + " aPkg = " + JSON.stringify(aPkg));

		var pSelApk = document.getElementById("tab_apk_query_sel_lst_apk");
		pSelApk.innerHTML = "";
		var iLen = aPkg.length;
		for (var iIdx = 0; iIdx < iLen; iIdx++)
		{
			var pApk = aPkg[iIdx];
			var pOpt = document.createElement("option");	
			pOpt.text = pApk.pkg;
			pOpt.value = pApk.pkg; 
			pSelApk.add(pOpt);
		}
	});

	// modules callbacks
	
	// ModFile
	
	p_Msg.getModFile().setOnDir(function(uiId, sFrom, pRes)
	{
		console.log("on dir id = " + uiId + " from = " + sFrom + " res = " + JSON.stringify(pRes));	
	}.bind(this));

	p_Msg.getModFile().setOnFile(function(uiId, sFrom, sFile, sMsg)
	{
		console.log("on file id = " + uiId + " from = " + sFrom + " file_len = " + sFile.length + " msg = " + sMsg);
	}.bind(this));

}

//****************************************************************************************************
function initCallback()
{ 
	initMsg();
	initDefaultMsgCallback();

	var i_CamZoom = 10;

	onClickButton("cmd_take_pic", function()
	{
		ui_ImgReqTime = new Date().getTime();
		p_Msg.takePic(200, s_To);
	}, true); 

	onClickButton("cmd_flash_auto", function()
	{
		p_Msg.flashAuto(201, s_To);
	}, true);

	onClickButton("cmd_flash_on", function()
	{	p_Msg.flashOn(202, s_To);
	}, true);

	onClickButton("cmd_flash_off", function()
	{
		p_Msg.flashOff(203, s_To);	
	}, true);

	onClickButton("cmd_flash_torch", function()
	{
		p_Msg.flashTorch(204, s_To);	
	}, true);

	onClickButton("cmd_cam_zoom_in", function()
	{
		i_CamZoom += 10;
		if (i_CamZoom > 100)
			i_CamZoom = 100;

		p_Msg.camZoom(210, s_To, i_CamZoom);
	}, true);

	onClickButton("cmd_cam_zoom_out", function()
	{
		i_CamZoom -= 10;
		if (i_CamZoom < 0)
			i_CamZoom = 0;

		p_Msg.camZoom(211, s_To, i_CamZoom);
	}, true);

	onClickButton("cmd_img_download", function()
	{
		p_Msg.downloadFile();
	}, true);

	onClickButton("cmd_usr_refresh", function()
	{
		refreshLstCustomer();
	}, false);

	onClickButton("cmd_cls_gps_info", function()
	{
		clsGpsInfo();
	}, false);

	onClickButton("cmd_logoff", function()
	{
		logoff();
		return false;
	}, false);

	onSeekChange("seek_img_size", function(iSize)
	{
		p_Msg.imgSize(212, s_To, iSize);
	}, true);

	onSeekChange("seek_img_quality", function(iQuality)
	{
		p_Msg.imgQuality(213, s_To, iQuality);	
	}, true); 
}

//****************************************************************************************************
function initUi()
{
	p_VidCtrl.initUi();
	transformUiToDefault();

	document.getElementById("txt_new_customer").onkeypress = function(pEvt)
	{
		if (pEvt.keyCode == 13)
			document.getElementById("cmd_add_customer").focus();
	};

	refreshLstCustomer();

	/*for (var iIdx = 0; iIdx < 5; iIdx ++)
		addCustomer("id_" + iIdx);
	
	for (var iIdx = 0; iIdx < 4; iIdx ++)
		addImg("img/" + (iIdx + 1) + ".jpg");
	*/ 

	p_TabCtrl.init
	(
		[
			{id:"tab_home", id_cmd:"tab_cmd_home"}, 
			{id:"tab_call", id_cmd:"tab_cmd_call"}, 
			{id:"tab_apk_add", id_cmd:"tab_cmd_apk_add"}, 
			{id:"tab_apk_install", id_cmd:"tab_cmd_apk_install"}, 
			{id:"tab_apk_query", id_cmd:"tab_cmd_apk_query"}, 
			{id:"tab_apk_rm", id_cmd:"tab_cmd_apk_rm"}, 
			{id:"tab_msg", id_cmd:"tab_cmd_msg"},
			{id:"tab_file", id_cmd:"tab_cmd_file"}
		],
		0
	);
}


//****************************************************************************************************
// customer list
//****************************************************************************************************

//****************************************************************************************************
function addCustomer(sId)
{
	var sDivId = getCustomerDivId(sId);
	var sIdImgState = getCustomerStateImgId(sId);

	if (document.getElementById(sDivId) != null)
	{
		console.log("customer allredy in the ui");
		return;
	}

	var sDis = sId;
	if (sDis.length < 5)
		sDis = "Customer " + sDis;

	var sHtml = "<div href=\"#\" class=\"list-group-item active\">" +
				"<span class=\"spanStyle1\">" + sDis + " </span>" +
				"<a>" +
					"<span class=\"spanStyle2\"><img id=\"" + sIdImgState + "\" width=\"30\" src=\"img/online.png\"></span> " +
				"</a>" +
				"<a href=\"#\" onclick=\"rmCustomer('" + sId + "'); return false;\">" +
					"<span class=\"spanStyle2\"><img width=\"30\" src=\"img/reject.png\"></span>" +
				"</a>" +
				"<a href=\"#\" onclick=\"rebootDevice('" + sId + "'); return false;\"" +
					"<button class=\"spanStyle2\"><img width=\"30\">Restart Device</button>" +
				"</a>" +
				"<a class=\"\" href=\"#\" onclick=\"sendCamReq('" + sId + "'); return false;\">" +
					"<span class=\"spanStyle2\"><img width=\"30\" src=\"img/call2.png\"></span> " +
				"</a>" +
			"</div>";

	var pDiv = document.createElement("div");
	pDiv.id = sDivId;
	pDiv.innerHTML = sHtml;

	if (document.getElementById("lst_customer") == null)
	{
		console.log("unable to fild lst_customer");
		return;
	}

	document.getElementById("lst_customer").appendChild(pDiv);	
}

//****************************************************************************************************
function addNewCustomer()
{
	var sId = document.getElementById('txt_new_customer').value;

	if (isValidCustomerId(sId))
		addCustomer(sId);
	else
		window.alert("please enter valid customer id (" + sId + ")");
}

//****************************************************************************************************
function setUsrState(sId, eState)
{
	addCustomer(sId);

	var sIdImgState = getCustomerStateImgId(sId);
	var sImg = null;

	console.log("state = " + eState);
	
	if (eState == UsrState.Connect)
		sImg = "img/online.png"; 
	else
		sImg = "img/offline.png";	
	
	document.getElementById(sIdImgState).src = sImg; 
}

//****************************************************************************************************
function isValidCustomerId(sId)
{
	return /^\w+$/.test(sId); 
}

//****************************************************************************************************
function rebootDevice(sId)
{
	var pStatusBar = document.getElementById("status_bar");
	pStatusBar.innerHTML = "selecting user " + sId + " ...";
	setTimeout(function()
	{
		pStatusBar.innerHTML = "";
	}, 2000);

	p_Msg.rebootOs(7000, sId);
}

//****************************************************************************************************
function rmCustomer(sId)
{
	var sDivId = getCustomerDivId(sId);	
	var pDiv = document.getElementById(sDivId);
	document.getElementById("lst_customer").removeChild(pDiv);
} 

//****************************************************************************************************
function getCustomerDivId(sId)
{
	return "customer_div_" + sId;
}

//****************************************************************************************************
function getCustomerStateImgId(sId)
{
	return "customer_div_state_img_" + sId;
} 

//****************************************************************************************************
function addImg(sSrc)
{ 
	var sHtml = 
			"<a href=\"" + sSrc + "\" data-lightbox=\"img_received\">" +
				"<img class=\"img-responsive portfolio-item\" src=\"" + sSrc + "\" alt=\"\">" +
			"</a>";
           
	var pDiv = document.createElement("div");
	pDiv.className = "col-sm-3 col-xs-6";
	pDiv.innerHTML = sHtml;
	var pLstImg = document.getElementById("lst_img");
	if (pLstImg == null)
	{
		console.log("no lst_img");
		return;
	}

	pLstImg.className = "row";
	pLstImg.appendChild(pDiv);
}

//****************************************************************************************************
// image receive progress bar
//****************************************************************************************************
function addImgReceiveProgressbar(sFrom, sId)
{
	var pParent = document.getElementById("div_img_receive_progress");
	var sIdDiv = getImgReceiveProgressDivId(sFrom, sId);
	var sIdProgress = getImgReceiveProgressId(sFrom, sId);
	var sIdTxt = getImgReceiveProgressTxtId(sFrom, sId);

	var pDiv = document.createElement("div");
	pDiv.className = "progress";	
	pDiv.style = "height:15px;";
	pDiv.id = sIdDiv;
	pDiv.innerHTML = "<div id = \"" + sIdProgress + "\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:0%\"> <span id = " + sIdTxt + ">0%test</span> </div>";

	if (pParent)
		pParent.appendChild(pDiv);
	else
		console.log("no div_img_receive_progress");
}

//****************************************************************************************************
function setImgReceiveProgress(sFrom, sId, uiProgress)
{
	var pProgress = document.getElementById(getImgReceiveProgressId(sFrom, sId));	
	if (pProgress == null)
		return;

	pProgress.style = "width:" + uiProgress + "%";

	var pTxt = document.getElementById(getImgReceiveProgressTxtId(sFrom, sId));
	pTxt.innerHTML = uiProgress + "%";
}

//****************************************************************************************************
function rmImgReceiveProgressDiv(sFrom, sId)
{
	var pParent = document.getElementById("div_img_receive_progress");
	if (pParent == null)
	{
		console.log("no div_img_receive_progress");
		return;
	}

	var pDiv = document.getElementById(getImgReceiveProgressDivId(sFrom, sId));
	pParent.removeChild(pDiv);
}

//****************************************************************************************************
function getImgReceiveProgressDivId(sFrom, sId)
{
	return "div_img_receive_progress_div_" + sFrom + "_" + sId;
}

//****************************************************************************************************
function getImgReceiveProgressId(sFrom, sId)
{
	return "div_img_receive_progress_" + sFrom + "_" + sId;
}

//****************************************************************************************************
function getImgReceiveProgressTxtId(sFrom, sId)
{
	return "div_img_receive_progress_txt_" + sFrom + "_" + sId;
}

//****************************************************************************************************
// change ui mode conncted and default 
//****************************************************************************************************
function transformUiToDefault()
{
	var pImgVid = document.getElementById("img_def_vid");
	pImgVid.className = "";

	if (b_ImgReceived)
		pImgVid.width = "400";

	var pVid = document.getElementById("videos");
	pVid.className = "hidden";
}

//****************************************************************************************************
function transformUiToConnected()
{ 
	var pImgVid = document.getElementById("img_def_vid");
	pImgVid.className = "hidden";

	var pVid = document.getElementById("videos");
	pVid.className = "";
}

//****************************************************************************************************
// user info handling
//****************************************************************************************************
function refreshLstCustomer()
{
	document.getElementById("lst_customer").innerHTML = "";
	p_Msg.usrGetLst(6000, "", "", 0, 0, true);
}

//****************************************************************************************************
// logoff 
//****************************************************************************************************
function logoff()
{
	p_Msg.logoff();
	location.reload();
}

//****************************************************************************************************
// gps info
//****************************************************************************************************
function addGpsInfo(sUsr, dLat, dLng)
{
	var pDiv = document.createElement("tr"); 
	pDiv.innerHTML = "<td>" + sUsr + "</td><td>" + dLat + "</td><td>" + dLng + "</td>";
	var pLst = document.getElementById("lst_gps_info");
	pLst.insertBefore(pDiv, pLst.childNodes[0]);
}

//****************************************************************************************************
function clsGpsInfo()
{
	document.getElementById("lst_gps_info").innerHTML = "";
}

//****************************************************************************************************
function addApk(pEvt)
{
	var pFile = document.getElementById("cmd_add_apk_file").files[0];
	var sDesc = document.getElementById("txt_apk_desc").value;

	if (pFile == null)
	{
		alert("please select apk file to be upload");
		return;
	}

	if (sDesc == null || sDesc == "")
	{
		alert("please enter apk description");
		return;
	}

	p_Msg.addApk(123, "", pFile, sDesc, function(iProgress)
	{
		console.log("apk file uploading progress " + iProgress);

		if (iProgress == 100)
			alert("adding apk has been completed");
	});	
}

//****************************************************************************************************
function getLstApk()
{
	p_Msg.getLstApk(125, "");
}

//****************************************************************************************************
function installApk()
{
	var sInstallTo = document.getElementById("txt_apk_install_to").value;
	var sApkId = document.getElementById("sel_apk_id").value;
	p_Msg.installApk(124, "", sInstallTo, 60, sApkId);	
}

//****************************************************************************************************
function getLstInstalledApk()
{
	var sInstallTo = document.getElementById("txt_apk_query_to").value;
	p_Msg.sendGetApkLst(125, "", sInstallTo, 60);	
}

//****************************************************************************************************
function apkRm()
{
	var sTo = document.getElementById("txt_apk_rm_to").value;
	var sPkg = document.getElementById("txt_apk_rm_pkg").value;
	p_Msg.apkRm(126, "", sTo, 0, sPkg);	
}

// File module

//****************************************************************************************************
function fileAdd()
{
	var pFile = document.getElementById("file_upload").files[0];	
	var sFilePath = document.getElementById("file_upload").value;
	var sTo = document.getElementById("txt_file_usr").value;
	var sDest = document.getElementById("file_dest").value;

	if (pFile == null)
	{
		console.log("please select a file to upliad");
		alert("please select a file to upload");
	}

	p_Msg.getModFile().add(sTo, pFile, sFilePath, sDest, function(iProgress)
	{
		console.log("add file progress " + iProgress);
	});
}

//****************************************************************************************************
function fileRefresh()
{
	var sFile = document.getElementById("txt_file_path").value;
	var sTo = document.getElementById("txt_file_usr").value;
	
	p_Msg.getModFile().get(sTo, sFile);
}

//****************************************************************************************************
function fileMv()
{
	var sTo = document.getElementById("txt_file_usr").value;
	var sSrc = document.getElementById("txt_file_src").value;
	var sDest = document.getElementById("txt_file_dest").value;
	var bOverwrite = false;

	p_Msg.getModFile().mv(sTo, sSrc, sDest, bOverwrite);
}

//****************************************************************************************************
function fileRm()
{
	var sTo = document.getElementById("txt_file_usr").value;
	var sFile = document.getElementById("file_txt_rm").value;
	
	p_Msg.getModFile().rm(sTo, sFile);
}

//****************************************************************************************************
// ui 
//****************************************************************************************************
function selTab(iTab)
{
	p_TabCtrl.select(iTab);
}
