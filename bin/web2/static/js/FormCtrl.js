var app = angular.module("app", []);

app.controller('ctrl', function($scope, $http) 
{
	//****************************************************************************************************
	$scope.login = function()
	{
		var bValid = true;

		var sUsr = $scope.validateTextField("txt_usr");
		if (sUsr == null)
			bValid = false;

		var sPwd = $scope.validateTextField("txt_pwd");
		if (sPwd == null)
			bValid = false;

		if (!bValid)
			return;
		
		p_VidCtrl.login
		(
			sUsr, sPwd, 
			function()
			{
				console.log("login success");	

				var bIsBasic = getUrlVal("is_basic"); 
				var sUrl = "sec/connect?is_basic=" + bIsBasic;
				console.log("connect get url = " + sUrl);

				$http.get(sUrl).then(function(pRes)
				{
					document.getElementById("div_ctrl").innerHTML = pRes.data;		
					document.getElementById("cmd_logoff").innerHTML = "Logout " + sUsr;
					initCallback();
					initUi();
				});
			}.bind(this), 
			function(sErr)
			{
				console.log("login failed [" + sErr + "]");	
				document.getElementById("div_login_info").innerHTML = 
					"<font color=\"red\">Please enter valid user name and password</font>";
			}.bind(this)
		);
	}

	//****************************************************************************************************
	$scope.register = function()
	{
		var bValid = true;
		var sUsr = $scope.validateTextField("txt_usr");
		if (sUsr == null)
			bValid = false;	

		var sGrp = $scope.validateTextField("sel_grp");
		if (sGrp == null)
			bValid = false;

		var sType =$scope.validateTextField("sel_type");
		if (sType == null)
			bValid = false;

		var sName =$scope.validateTextField("txt_name");
		if (sName == null)
			bValid = false;

		var sEmail = $scope.validateTextField("txt_email", /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		if (sEmail == null)
			bValid = false;

		var sTel = $scope.validateTextField("txt_tel", /^[0-9]{10,10}$/);
		if (sTel == null)
			bValid = false;

		var regPwd = /^[a-zA-Z0-9]+$/;
		var pTxtPwd1 = document.getElementById("txt_pwd1");
		var sPwd1 = pTxtPwd1.value;	
		var pTxtPwd2 = document.getElementById("txt_pwd2");
		var sPwd2 = pTxtPwd2.value;
		if (regPwd.test(sPwd1) && regPwd.test(sPwd2) && sPwd1 == sPwd2)
		{
			pTxtPwd1.style.borderColor = "green";
			pTxtPwd2.style.borderColor = "green";
		}
		else
		{
			pTxtPwd1.style.borderColor = "red";
			pTxtPwd2.style.borderColor = "red";
			bValid = false;
		}

		if (!bValid)
			return;
		
		register(100, sUsr, sPwd1, sGrp, sType, sName, sEmail, sTel);
	}

	//****************************************************************************************************
	$scope.registerForm = function()
	{
		console.log("loading register form");	

		var bIsBasic = getUrlVal("is_basic"); 
		var sUrl = "sec/register?is_basic=" + bIsBasic;
		console.log("connect get url = " + sUrl);

		$http.get(sUrl).then(function(pRes)
		{
			document.getElementById("div_ctrl").innerHTML = pRes.data;		
		});
		
	};

	//****************************************************************************************************
	$scope.validateTextField = function(sId, regName)
	{
		console.log("validate text field id = " + sId);
		var pField = document.getElementById(sId);
		var sVal = pField.value;

		if (typeof regName == 'undefined')
			regName = /^[a-zA-Z0-9]+$/;

		if (regName.test(sVal))
		{
			pField.style.borderColor = "green";
		}
		else
		{
			pField.style.borderColor = "red";
			return null;
		}

		return sVal;
	};
});

