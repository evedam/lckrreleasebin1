
//****************************************************************************************************
function getUrlVal(name)
{
	if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
		return decodeURIComponent(name[1]);

	return null;
}

//****************************************************************************************************
function isSet(val)
{
	return typeof val !== 'undefined' && val != null && val != "";
}

//****************************************************************************************************
function isEmpty(val)
{
	return !isSet(val);
}

//****************************************************************************************************
function isAlNum(str)
{
	return /^[A-Za-z\d]+$/.test(str)
}

//****************************************************************************************************
function isAlNumSpace(str)
{
	return /^[A-Za-z\d\s]+$/.test(str)
}

//****************************************************************************************************
function getFileExt(sFile)
{
	var sExt = "";
	var aTok = getFileName(sFile).split(".");
	var iLen = aTok.length;

	for (var iIdx = iLen - 1; iIdx >= 1; iIdx--)
	{
		var sTok = aTok[iIdx];
		sExt = sTok + sExt;
	}

	return sExt;
}

//****************************************************************************************************
function getFileName(sFile)
{
	if (!isSet(sFile))
		return "";

	var sExt = "";
	var aTok = sFile.split(/[\\\/]/);
	var iLen = aTok.length;

	if (iLen == 0 || iLen == 1)
		return sFile;
		
	return aTok[iLen - 1];
}

//****************************************************************************************************
function showSuccessNotification(text)
{
		new PNotify({
                                  title: 'Success',
                                  text: text, 
                                  type: 'success',
                                  styling: 'bootstrap3'
                              });

}

//****************************************************************************************************
var isFullScreen = false;
//****************************************************************************************************
function fullScreen(element) 
{
	console.log("enter full screen");

	var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || 
				element.mozRequestFullScreen || element.msRequestFullScreen;

	if (requestMethod) 
	{ 
		requestMethod.call(element);
	} 
	else if (typeof window.ActiveXObject !== "undefined") 
	{ 
		var wscript = new ActiveXObject("WScript.Shell");

		if (wscript !== null) 
		{
			wscript.SendKeys("{F11}");
		}
	}

	isFullScreen = true;
}

//****************************************************************************************************
function exitFullscreen()
{
	console.log("exit full screen");

	if (document.cancelFullScreen) 
	{  
		document.cancelFullScreen();  
	} 
	else if (document.mozCancelFullScreen) 
	{  
		document.mozCancelFullScreen();  
	}
	else if (document.webkitCancelFullScreen) 
	{  
		document.webkitCancelFullScreen();  
	}  
	else
		console.log("no fnc exit fullscreen");

	isFullScreen = false;
}

//****************************************************************************************************
function toggleFullScreen()
{
	if (!isFullScreen)
		fullScreen(document.body);
	else
		exitFullscreen();
}

//****************************************************************************************************
function fileDownload(pBlob, sName)
{
	console.log("downloading len = " + pBlob.length + " file " + sName);

	var pUrl = window.URL.createObjectURL(pBlob);
	var pA = document.createElement("a");
	pA.href = pUrl;
	pA.download = sName;
	pA.click();
	window.URL.revokeObjectURL(pUrl);
}

//****************************************************************************************************
function stringDownload(sStr, sName)
{
	let aStr = [];
	let iLen = sStr.length;
	for (let i = 0; i < iLen; i++)
		aStr.push(sStr.charAt(i));
	let pBlob = new Blob(aStr);

	fileDownload(pBlob, sName);
}

//****************************************************************************************************
function downloadAsZip(sFile, sName)
{
	var pZip = new JSZip();

	console.log("file len = " + sFile.length + " name = " + sName);
	pZip.file(sName, sFile, {binary:true});			

	console.log("generating zip file ...");
	pZip.generateAsync({type:"blob"}).then(function(pBlob)
	{
		console.log("zip file has been generated");
		fileDownload(pBlob, sName + ".zip");
	}.bind(this));
}

//****************************************************************************************************
function toArray(pBufData)
{
	var aData = [];
	for (var pData in pBufData)
		aData.push(pData);
	return aData;
}

//****************************************************************************************************
function secToDate(seconds)
{
	var pDate = new Date(1970, 0, 1); 
	pDate.setSeconds(seconds);
	return pDate;
}


//****************************************************************************************************
function secToDateStr(seconds)
{
	var pDate = secToDate(seconds + 19800);	
	return (pDate.getMonth() + 1) + "/" + pDate.getDate() + "/" + pDate.getFullYear() + " " + 
		pDate.getHours() + ":" + pDate.getMinutes() + ":" + pDate.getSeconds();
}
