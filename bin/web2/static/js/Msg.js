
//****************************************************************************************************
var Msg = function(pVidCtrl)
{
	pVidCtrl.setOnReq(this.onReq.bind(this));
	pVidCtrl.setOnRes(this.onRes.bind(this))
	pVidCtrl.setOnCmdRes(this.onCmdRes.bind(this));
	pVidCtrl.setOnGetRes(this.onGetRes.bind(this));
	pVidCtrl.setOnUsrInfo(this.onUsrInfo.bind(this));
	pVidCtrl.setOnFileStart(this.onFileStart.bind(this));
	pVidCtrl.setOnFileProgress(this.onFileProgress.bind(this))
	pVidCtrl.setOnFile(this.onFile.bind(this));
	pVidCtrl.setOnLocalHangup(this.onLocalHangup.bind(this));
	pVidCtrl.setOnRemoteHangup(this.onRemoteHangup.bind(this));
	pVidCtrl.setOnLstApk(this.onLstApk.bind(this));
	pVidCtrl.setOnHttp(this.onHttp.bind(this));

	this.p_VidCtrl = pVidCtrl;
	this.fnc_onCamReq = null;
	this.fnc_onContactReq = null;
	this.fnc_onCamRes = null;

	this.fnc_onCmdRes = null;

	this.fnc_onGetGpsRes = null;
	this.fnc_onApkLst = null;

	this.fnc_onFileStart = null;
	this.fnc_onFileProgress = null;
	this.fnc_onFile = null;

	this.fnc_onLocalHangup = null;
	this.fnc_onRemoteHangup = null;

	this.fnc_onUsrAdd = null;
	this.fnc_onUsrConnect = null;
	this.fnc_onUsrDisconnect = null;
	this.fnc_onUsrLogout = null;
	this.fnc_onUsrLst = null;

	this.fnc_onLstApk = null;

	this.fnc_onHttp = null;

	this.a_Mod = [];
	this.a_Mod.push(new ModUsr(this));
	this.a_Mod.push(new ModFile(this));
	this.a_Mod.push(new ModScreenShare(this));
	this.a_Mod.push(new ModScript(this));
	this.a_Mod.push(new ModDeviceState(this));
	this.a_Mod.push(new ModLoc(this));
};

//****************************************************************************************************
Msg.prototype.register = function(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel, fncRet) 
{
	this.p_VidCtrl.register(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel, fncRet);
};

//****************************************************************************************************
Msg.prototype.setLUsr = function(uiId, sUsr, sLUsr, bAdd)
{
	this.p_VidCtrl.setLUsr(uiId, sUsr, sLUsr, bAdd);
};

//****************************************************************************************************
Msg.prototype.sendMsg = function(uiId, sSesId, sTo, sMsg)
{
	this.p_VidCtrl.sendMsg(uiId, sSesId, sTo, sMsg);
};

//****************************************************************************************************
Msg.prototype.sendGet = function(uiId, sSesId, sTo, pMsg)
{
	this.p_VidCtrl.sendGet(uiId, sSesId, sTo, JSON.stringify(pMsg));
};

//****************************************************************************************************
Msg.prototype.sendCmd = function(uiId, sSesId, sTo, pCmd)
{
	this.p_VidCtrl.sendCmd(uiId, sSesId, sTo, JSON.stringify(pCmd));
};

//****************************************************************************************************
Msg.prototype.sendCmdRes = function(uiId, sTo, pRes)
{
	this.p_VidCtrl.sendCmdRes(uiId, sTo, pRes);
};

//****************************************************************************************************
Msg.prototype.sendFile = function(sTo, sSesId, pFile, sMsg, fncProgress)
{
	console.log("send file to = " + sTo + " ses_id = " + sSesId + " msg = " + JSON.stringify(sMsg));
	this.p_VidCtrl.sendFile(sTo, sSesId, pFile, JSON.stringify(sMsg), fncProgress);
};

//****************************************************************************************************
Msg.prototype.sendCamReq = function(uiId, sSesId, sNewSesId, sTo)	
{
	this.p_VidCtrl.sendReq(uiId, sSesId, sTo, JSON.stringify({type:"cam", ses_id:sNewSesId}));
};

//****************************************************************************************************
Msg.prototype.sendContactReq = function(uiId, sSesId, sTo)
{
	this.p_VidCtrl.sendReq(uiId, sSesId, sTo, JSON.stringify({type:"contact"}));		
};

//****************************************************************************************************
Msg.prototype.sendCamRes = function(uiId, sSesId, sNewSesId, sTo, bAccept)
{
	this.p_VidCtrl.sendRes(uiId, sSesId, sTo, JSON.stringify({type:"cam", ret:(bAccept ? "true" : "false"), ses_id:sNewSesId}));
};

//****************************************************************************************************
Msg.prototype.sendGetGps = function(uiId, sSesId, sTo)
{
	this.sendGet(uiId, sSesId, sTo, {type:"gps"});
};

//****************************************************************************************************
Msg.prototype.sendGetApkLst = function(uiId, sSesId, sTo)
{
	this.sendGet(uiId, sSesId, sTo, {type:"apk_lst"});
};

//****************************************************************************************************
Msg.prototype.addSes = function(sSesId)
{
	this.p_VidCtrl.addSes(sSesId);	
};

//****************************************************************************************************
Msg.prototype.usrGetLst = function(uiId, sType, sState, uiIdx, uiCnt, bSubscripe)
{
	this.p_VidCtrl.usrGetLst(uiId, sType, sState, uiIdx, uiCnt, bSubscripe);
};

//****************************************************************************************************
Msg.prototype.takePic = function(uiId, sTo)
{
	this.sendCmd(uiId, null, sTo, {type:"img"});
};

//****************************************************************************************************
Msg.prototype.flash = function(uiId, sTo, sMode)
{
	this.sendCmd(uiId, null, sTo, {type:"flash", mode:sMode});
};

//****************************************************************************************************
Msg.prototype.flashAuto = function(uiId, sTo)
{
	this.flash(uiId, sTo, "auto");
};

//****************************************************************************************************
Msg.prototype.flashOn = function(uiId, sTo)
{
	this.flash(uiId, sTo, "on");
};

//****************************************************************************************************
Msg.prototype.flashOff = function(uiId, sTo)
{
	this.flash(uiId, sTo, "off");
};

//****************************************************************************************************
Msg.prototype.flashTorch = function(uiId, sTo)
{
	this.flash(uiId, sTo, "torch");
};

//****************************************************************************************************
Msg.prototype.camZoom = function(uiId, sTo, iVal)
{
	this.sendCmd(uiId, null, sTo, {type:"cam_zoom", val:iVal});
};

//****************************************************************************************************
Msg.prototype.imgSize = function(uiId, sTo, iPercentage)
{
	this.sendCmd(uiId, null, sTo, {type:"img_size", percentage:iPercentage});
};

//****************************************************************************************************
Msg.prototype.imgQuality = function(uiId, sTo, iPercentage)
{
	this.sendCmd(uiId, null, sTo, {type:"img_quality", percentage:iPercentage});
};

//****************************************************************************************************
Msg.prototype.rebootOs = function(uiId, sTo)
{
	this.sendCmd(uiId, null, sTo, {type:"reboot_os"});
};

//****************************************************************************************************
// apk service 
//****************************************************************************************************

//****************************************************************************************************
Msg.prototype.addApk = function(uiId, sSesId, pFile, sDesc, fncProgress)
{
	this.p_VidCtrl.addApk(uiId, sSesId, pFile, sDesc, fncProgress);
};

//****************************************************************************************************
Msg.prototype.getLstApk = function(uiId, sSesId)
{
	this.p_VidCtrl.getLstApk(uiId, sSesId);
};

//****************************************************************************************************
Msg.prototype.installApk = function(uiId, sSesId, sTo, uiExpireTime, sApk, bOpen)
{
	this.sendCmd(uiId, sSesId, sTo, {type:"apk_install", apk:sApk, open:bOpen}); 
};

//****************************************************************************************************
Msg.prototype.apkRm = function(uiId, sSesId, sTo, uiExpireTime, sPkg)
{
	this.sendCmd(uiId, sSesId, sTo, {type:"apk_rm", pkg:sPkg}); 
};

//****************************************************************************************************
// http 
//****************************************************************************************************
Msg.prototype.http = function(uiId, sSrv, sPath, pData)
{
	this.p_VidCtrl.http(uiId, sSrv, sPath, pData);
};

//****************************************************************************************************
Msg.prototype.downloadFile = function()
{
	this.p_VidCtrl.downloadFile();
};

//****************************************************************************************************
Msg.prototype.logoff = function()
{
	this.p_VidCtrl.logoff();
};

//****************************************************************************************************
Msg.prototype.onReq = function(uiId, sFrom, sReq)
{
	console.log("id = " + uiId + " from = " + sFrom + " data = " + sReq);
	var pReq = JSON.parse(sReq);

	if (pReq.type == "cam")
	{
		var sSesId = pReq.ses_id;
		this.fnc_onCamReq(uiId, sFrom, sSesId);
	}
	else if (pReq.type == "contact")
	{
		this.fnc_onContactReq(uiId, sFrom);
	}
	else
	{
		console.log("invalid request type");
	}

};

//****************************************************************************************************
Msg.prototype.onRes = function(iId, sFrom, sRes)
{
	console.log("id = " + iId + " from = " + sFrom + " data = " + sRes);
	var pRes = JSON.parse(sRes);

	if (pRes.type == "cam")
	{
		var sSesId = pRes.ses_id;

		if (pRes.ret == "true")
		{
			this.fnc_onCamRes(iId, sFrom, sSesId, true);
		}
		else
		{
			console.log("access denied");
			this.fnc_onCamRes(iId, sFrom, sSesId, false);
		}
	}
	else
	{
		console.log("invalid response type");
	}
};

//****************************************************************************************************
Msg.prototype.onCmdRes = function(iId, sFrom, sRes)
{
	console.log("id = " + iId + " from = " + sFrom + " data = " + sRes);
	var pRes = JSON.parse(sRes);
	var bProcess = false;	

	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];

		if (pMod.onCmdRes(iId, sFrom, pRes))		
		{
			console.log("cmd res processed by " + pMod.getName() + " module");
			bProcess = true;
			break;
		}
	}

	if (!bProcess)
	{
		if (this.fnc_onCmdRes != null)
			this.fnc_onCmdRes(iId, sFrom, sRes);	
		else
			console.log("no fnc_onCmdRes");
	}
	else
		console.log("cmd response has already been handled");
};

//****************************************************************************************************
Msg.prototype.onGetRes = function(iId, sFrom, sRes)
{
	console.log("id = " + iId + " from = " + sFrom + " data = " + sRes);
	
	var pRes = JSON.parse(sRes);
	var bProcess = false;	

	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];

		if (pMod.onGetRes(iId, sFrom, pRes))		
		{
			console.log("get res processed by " + pMod.getName() + " module");
			bProcess = true;
			break;
		}
	}
	
	if (bProcess)
	{
		console.log("get response has been handled by a module");
	}
	else if (pRes.type == "gps")
	{
		if (this.fnc_onGetGpsRes != null)
			this.fnc_onGetGpsRes(iId, sFrom, pRes.lat, pRes.lng);	
		else
			console.log("no gps callback");
	}
	else if (pRes.type == "apk_lst")
	{
		if (this.fnc_onLstApkInstalled != null)
			this.fnc_onLstApkInstalled(iId, sFrom, pRes.apk_lst);
		else
			console.log("no apk list callback");
	}
	else
		console.log("unknown get response " + sRes);
};

//****************************************************************************************************
Msg.prototype.onUsrInfo = function(uiId, sMsg)
{
	console.log("onUsrInfo id = " + uiId + " msg = " + sMsg);
	var pMsg = JSON.parse(sMsg); 

	switch (pMsg.type)
	{
	case "add":
		if (this.fnc_onUsrAdd != null)
			this.fnc_onUsrAdd(pMsg.usr);
		else
			console.log("no user add callbck");
		break;
	case "connect":
		if (this.fnc_onUsrConnect != null)
			this.fnc_onUsrConnect(pMsg.usr);
		else
			console.log("no user connect callback");
		break;
	case "disconnect":
		if (this.fnc_onUsrDisconnect != null)
			this.fnc_onUsrDisconnect(pMsg.usr);
		else
			console.log("no user disconnect callback");
		break;
	case "logout":
		if (this.fnc_onUsrLogout != null)
			this.fnc_onUsrLogout(pMsg.usr);
		else
			console.log("no user rm callback");
		break;
	case "lst_usr":
		if (this.fnc_onUsrLst != null)
			this.fnc_onUsrLst(uiId, pMsg.lst_usr, pMsg.usr_type);
		else
			console.log("no user list callback");
		break;
	default:
		console.log("unhandled user info (" + sMsg + ")");
	}
};

//****************************************************************************************************
Msg.prototype.onFileStart = function(sFrom, uiId, uiLen, sMsg)
{
	console.log("on file receive start from = " + sFrom + " id = " + uiId + " len = " + uiLen + " msg = " + sMsg);

	var pMsg = JSON.parse(sMsg);
	var bProcess = false;
	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];
		
		if (pMod.onFileStart(sFrom, uiId, uiLen, pMsg))
		{
			console.log("on file start processed by " + pMod.getName() + " module");	
			bProcess = true;
			break;
		}
	}

	if (!bProcess)
		this.fnc_onFileStart(sFrom, uiId, uiLen, sMsg);
};

//****************************************************************************************************
Msg.prototype.onFileProgress = function(sFrom, uiId, uiProgress, sMsg)
{
	console.log("on file progress from = " + sFrom + " id = " + uiId  + " progress = " + uiProgress + " msg = " + sMsg);

	var pMsg = JSON.parse(sMsg);
	var bProcess = false;
	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];
		
		if (pMod.onFileProgress(sFrom, uiId, uiProgress, pMsg))
		{
			console.log("on file progress processed by " + pMod.getName() + " module");	
			bProcess = true;
			break;
		}
	}

	if (!bProcess)
		this.fnc_onFileProgress(sFrom, uiId, uiProgress, sMsg);
};

//****************************************************************************************************
Msg.prototype.onFile = function(sFrom, uiId, sFile, sMsg)
{
	console.log("on file reseived done sFrom = " + sFrom + " id = " + uiId + " msg = " + sMsg);

	var pMsg = JSON.parse(sMsg);
	var bProcess = false;
	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];
		
		if (pMod.onFile(sFrom, uiId, sFile, pMsg))
		{
			console.log("on file processed by " + pMod.getName() + " module");	
			bProcess = true;
			break;
		}
	}
	
	if (!bProcess)
		this.fnc_onFile(sFrom, uiId, sFile, sMsg);
};

//****************************************************************************************************
Msg.prototype.onLocalHangup = function(sSesId)
{
	this.fnc_onLocalHangup(sSesId);
};

//****************************************************************************************************
Msg.prototype.onRemoteHangup = function(sSesId)
{
	this.fnc_onRemoteHangup(sSesId);
};

//****************************************************************************************************
Msg.prototype.onLstApk = function(pData)
{
	if (this.fnc_onLstApk != null)
		this.fnc_onLstApk(pData);
	else
		console.log("no on lst apk callback");
};

//****************************************************************************************************
Msg.prototype.onHttp = function(pData)
{
	console.log("http res = " + JSON.stringify(pData));

	var bProcess = false;	

	var iCntMod = this.a_Mod.length;
	for (var iMod = 0; iMod < iCntMod; iMod++)
	{
		pMod = this.a_Mod[iMod];

		if (pMod.onHttp(pData))		
		{
			console.log("cmd res processed by " + pMod.getName() + " module");
			bProcess = true;
			break;
		}
	}

	if (bProcess)
	{
		console.log("http response already been handled");
	}
	else
	{
		if (this.fnc_onHttp != null)
			this.fnc_onHttp(pData);
		else 
			console.log("no on http callback");
	}
};

//****************************************************************************************************
Msg.prototype.setOnCamReq = function(fncOnCamReq)
{
	this.fnc_onCamReq = fncOnCamReq;
};

//****************************************************************************************************
Msg.prototype.setOnContactReq = function(fncOnContactReq)
{
	this.fnc_onContactReq = fncOnContactReq;
};

//****************************************************************************************************
Msg.prototype.setOnCamRes = function(fncOnCamRes)
{
	this.fnc_onCamRes = fncOnCamRes;
};

//****************************************************************************************************
Msg.prototype.setOnCmdRes = function(fncOnCmdRes)
{
	this.fnc_onCmdRes = fncOnCmdRes;
};

//****************************************************************************************************
Msg.prototype.setOnGetGpsRes = function(fncOnGps)
{
	this.fnc_onGetGpsRes = fncOnGps;
};

//****************************************************************************************************
Msg.prototype.setOnApkLst = function(fncOnApkLst)
{
	this.fnc_onApkLst = fncOnApkLst;
};

//****************************************************************************************************
Msg.prototype.setOnFileStart = function(fncOnFileStart)
{
	this.fnc_onFileStart = fncOnFileStart;
}; 

//****************************************************************************************************
Msg.prototype.setOnFileProgress = function(fncOnFileProgress)
{
	this.fnc_onFileProgress = fncOnFileProgress;
};

//****************************************************************************************************
Msg.prototype.setOnFile = function(fncOnFile)
{
	this.fnc_onFile = fncOnFile;
};

//****************************************************************************************************
Msg.prototype.setOnLocalHangup = function(fncOnHangup)
{
	this.fnc_onLocalHangup = fncOnHangup;
};

//****************************************************************************************************
Msg.prototype.setOnRemoteHangup = function(fncOnRemoteHangup)
{
	this.fnc_onRemoteHangup = fncOnRemoteHangup;
};

//****************************************************************************************************
Msg.prototype.setOnUsrAdd = function(fncOnAdd)
{
	this.fnc_onUsrAdd = fncOnAdd;
};

//****************************************************************************************************
Msg.prototype.setOnUsrConnect = function(fncOnConnect)
{
	this.fnc_onUsrConnect = fncOnConnect;
};

//****************************************************************************************************
Msg.prototype.setOnUsrDisconnect = function(fncOnDisconnect)
{
	this.fnc_onUsrDisconnect = fncOnDisconnect;
};

//****************************************************************************************************
Msg.prototype.setOnUsrLogout = function(fncOnLogout)
{
	this.fnc_onUsrLogout = fncOnLogout;
};

//****************************************************************************************************
Msg.prototype.setOnUsrLst = function(fncOnLst)
{
	this.fnc_onUsrLst = fncOnLst;
};

//****************************************************************************************************
Msg.prototype.setOnLstApk = function(fncOnLstApk)
{
	this.fnc_onLstApk = fncOnLstApk;
};

//****************************************************************************************************
Msg.prototype.setOnHttp = function(fncOnHttp)
{
	this.fnc_onHttp = fncOnHttp;
};

//****************************************************************************************************
Msg.prototype.setOnLstApkInstalled = function(fncOnLstApk)
{
	this.fnc_onLstApkInstalled = fncOnLstApk;
};

// Modules

//****************************************************************************************************
Msg.prototype.getModUsr = function()
{
	return this.a_Mod[0];
};

//****************************************************************************************************
Msg.prototype.getModFile = function()
{
	return this.a_Mod[1];
};

//****************************************************************************************************
Msg.prototype.getModScreenShare = function()
{
	return this.a_Mod[2];
};

//****************************************************************************************************
Msg.prototype.getModScript = function()
{
	return this.a_Mod[3];
};

//****************************************************************************************************
Msg.prototype.getModDeviceState = function()
{
	return this.a_Mod[4];
};

//****************************************************************************************************
Msg.prototype.getModLoc = function()
{
	return this.a_Mod[5];
};

