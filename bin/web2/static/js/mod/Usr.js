//****************************************************************************************************
var ModUsr = function(pMsg)
{
	this.p_Msg = pMsg; 

	this.service = "usr";
	this.ui_IdHttp = 200;
	this.ui_IdHttpSetUid = ++this.ui_IdHttp;
	this.ui_IdHttpSetName = ++this.ui_IdHttp;

	this.fn_OnSetUidRet = null;
	this.fn_OnSetNameRet = null;
};

//****************************************************************************************************
ModUsr.prototype.getName = function()
{
	return "ModUsr";
};

//****************************************************************************************************
ModUsr.prototype.setUid = function(sId, sUid, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid id");
		return;
	}

	if (!isSet(sUid))
	{
		console.log("please enter valid name");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret function");
		return;	
	}

	this.setOnSetUidRet(fnRet);

	var pData = {uid:sUid};
	this.p_Msg.http(this.ui_IdHttpSetUid, this.service, "/v1/set_uid/" + sId, pData);
	console.log("data = " + JSON.stringify(pData));
};

//****************************************************************************************************
ModUsr.prototype.setName = function(sId, sName, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid id");
		return;
	}

	if (!isSet(sName))
	{
		console.log("please enter valid name");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret function");
		return;	
	}

	this.setOnSetNameRet(fnRet);

	var pData = {name:sName};
	this.p_Msg.http(this.ui_IdHttpSetName, this.service, "/v1/set_name/" + sId, pData);
	console.log("data = " + JSON.stringify(pData));
};

// Callbacks from msg 

//****************************************************************************************************
ModUsr.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModUsr.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModUsr.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	return false;
};

//****************************************************************************************************
ModUsr.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModUsr.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModUsr.prototype.onHttp = function(pData)
{
	var iId = pData.i_Id;
	var bRet = true;
	console.log("onHttp id = " + iId);

	switch (iId)
	{ 
		case this.ui_IdHttpSetUid:
		{
			console.log("set uid ret " + JSON.stringify(pData));
			if (this.fn_OnSetUidRet != null)	
				this.fn_OnSetUidRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpSetName:
		{
			console.log("set name ret " + JSON.stringify(pData));
			if (this.fn_OnSetNameRet != null)	
				this.fn_OnSetNameRet(pData.p_Msg);
		}
		break;
		default:
			bRet = false;
	}

	return bRet;
};

//****************************************************************************************************
ModUsr.prototype.setOnSetUidRet = function(fnRet)
{
	this.fn_OnSetUidRet = fnRet;
};

//****************************************************************************************************
ModUsr.prototype.setOnSetNameRet = function(fnRet)
{
	this.fn_OnSetNameRet = fnRet;
};
