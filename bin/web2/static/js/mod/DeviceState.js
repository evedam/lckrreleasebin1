
//****************************************************************************************************
var ModDeviceState = function(pMsg)
{
	this.p_Msg = pMsg; 

	this.service = "device";
	this.ui_IdHttp = 100;
	this.ui_IdHttpAddState = ++this.ui_IdHttp;
	this.ui_IdHttpSetState = ++this.ui_IdHttp;
	this.ui_IdHttpGetStateL = ++this.ui_IdHttp;
	this.ui_IdHttpRmState = ++this.ui_IdHttp;

	this.ui_IdHttpAddDevice = ++this.ui_IdHttp;
	this.ui_IdHttpSetDevice = ++this.ui_IdHttp;
	this.ui_IdHttpGetDeviceL = ++this.ui_IdHttp;
	this.ui_IdHttpRmDevice = ++this.ui_IdHttp;

	this.fn_OnAddStateRet = null;
	this.fn_OnSetStateRet = null;
	this.fn_OnLState = null;
	this.fn_OnRmStateRet = null;

	this.fn_OnAddDeviceRet = null;
	this.fn_OnSetDeviceRet = null;
	this.fn_OnLDevice = null;
	this.fn_OnRmDeviceRet = null;
};

//****************************************************************************************************
ModDeviceState.prototype.getName = function()
{
	return "ModDeviceState";
};

//****************************************************************************************************
ModDeviceState.prototype.addState = function(pState, fnRet)
{
	if (!isSet(pState))
	{
		console.log("please enter valid state");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret function");
		return;	
	}

	this.setOnAddStateRet(fnRet);

	this.p_Msg.http(this.ui_IdHttpAddState, this.service, "/v1/state/add", pState);
	console.log("state = " + JSON.stringify(pState));
};

//****************************************************************************************************
ModDeviceState.prototype.setState = function(pState, fnRet)
{
	if (!isSet(pState))
	{
		console.log("please enter valid state");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret function");
		return;	
	}

	this.setOnSetStateRet(fnRet);

	this.p_Msg.http(this.ui_IdHttpSetState, this.service, "/v1/state/set", pState);
	console.log("state = " + JSON.stringify(pState));
};

//****************************************************************************************************
ModDeviceState.prototype.getLState = function()
{
	this.p_Msg.http(this.ui_IdHttpGetStateL, this.service, "/v1/state/getl", {});
};

//****************************************************************************************************
ModDeviceState.prototype.rmState = function(sId, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid id");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid return function");
		return;
	}

	this.setOnRmStateRet(fnRet);

	var pState = {id:sId};
	this.p_Msg.http(this.ui_IdHttpSetState, this.service, "/v1/state/rm", pState);
	console.log("state = " + JSON.stringify(pState));
};

//****************************************************************************************************
ModDeviceState.prototype.addDevice = function(sId, aState, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid device id");
		return;
	}

	if (!isSet(aState))
	{
		console.log("please enter valid state list");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret");
		return;
	}
	
	this.setOnAddDeviceRet(fnRet);

	var pDevice = {id:sId, lstate:aState};
	this.p_Msg.http(this.ui_IdHttpAddDevice, this.service, "/v1/device/add", pDevice);
	console.log("device = " + JSON.stringify(pDevice));
};

//****************************************************************************************************
ModDeviceState.prototype.setDevice = function(sId, aState, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid device id");
		return;
	}

	if (!isSet(aState))
	{
		console.log("please enter valid state list");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret");
		return;
	}
	
	this.setOnSetDeviceRet(fnRet);

	var pDevice = {id:sId, lstate:aState};
	this.p_Msg.http(this.ui_IdHttpAddDevice, this.service, "/v1/device/set", pDevice);
	console.log("device = " + JSON.stringify(pDevice));
};

//****************************************************************************************************
ModDeviceState.prototype.getLDevice = function()
{
	console.log("get ldevice");
	this.p_Msg.http(this.ui_IdHttpGetDeviceL, this.service, "/v1/device/getl", {});
};

//****************************************************************************************************
ModDeviceState.prototype.rmDevice = function(sId, fnRet)
{
	if (!isSet(sId))
	{
		console.log("please enter valid device id");
		return;
	}

	if (!isSet(fnRet))
	{
		console.log("please enter valid ret function");
		return;
	}

	this.fn_OnRmDeviceRet = fnRet;

	var pDevice = {id:sId};
	this.p_Msg.http(this.ui_IdHttpAddDevice, this.service, "/v1/device/rm", pDevice);
	console.log("device = " + JSON.stringify(pDevice));
};

//****************************************************************************************************
ModDeviceState.prototype.sendState = function(sTo, sState)
{
	if (!isSet(sTo))	
	{
		console.log("please enter valid destination user");
		return;
	}

	if (!isSet(sState))
	{
		console.log("please enter valid state");
		return;
	}

	this.p_Msg.sendCmd(50000, null, sTo, {type:"state", state:sState});
};

//****************************************************************************************************
ModDeviceState.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModDeviceState.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModDeviceState.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	return false;
};

//****************************************************************************************************
ModDeviceState.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModDeviceState.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModDeviceState.prototype.onHttp = function(pData)
{
	var iId = pData.i_Id;
	var bRet = true;
	console.log("onHttp id = " + iId);

	switch (iId)
	{ 
		case this.ui_IdHttpAddState:
		{
			console.log("add state ret " + JSON.stringify(pData));
			if (this.fn_OnAddStateRet != null)	
				this.fn_OnAddStateRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpSetState:
		{
			console.log("set state ret " + JSON.stringify(pData));
			if (this.fn_OnSetStateRet != null)	
				this.fn_OnSetStateRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpGetStateL: 
		{
			console.log("lst state " + JSON.stringify(pData));
			if (this.fn_OnLState != null)
				this.fn_OnLState(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpRmState:
		{
			console.log("rm state ret " + JSON.stringify(pData));
			if (this.fn_OnRmStateRet != null)
				this.fn_OnRmStateRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpAddDevice: 
		{
			console.log("add ret " + JSON.stringify(pData));
			if (this.fn_OnAddDeviceRet != null)
				this.fn_OnAddDeviceRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpSetDevice:
		{
			console.log("set device ret " + JSON.stringify(pData));
			if (this.fn_OnSetDeviceRet != null)	
				this.fn_OnSetDeviceRet(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpGetDeviceL:
		{
			console.log("on device list " + JSON.stringify(pData));
			if (this.fn_OnLDevice != null)
				this.fn_OnLDevice(pData.p_Msg);
		}
		break;
		case this.ui_IdHttpRmDevice: 
		{
			console.log("on rm device ret " + JSON.stringify(pData));
			if (this.fn_OnRmDeviceRet != null)
				this.fn_OnRmDeviceRet(pData.p_Msg);
		}
		break;
		default:
			bRet = false;
	}

	return bRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnAddStateRet = function(fnRet)
{
	this.fn_OnAddStateRet = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnSetStateRet = function(fnRet)
{
	this.fn_OnSetStateRet = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnLState = function(fnRet)
{
	this.fn_OnLState = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnRmStateRet = function(fnRet)
{
	this.fn_OnRmStateRet = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnAddDeviceRet = function(fnRet)
{
	this.fn_OnAddDeviceRet = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnSetDeviceRet = function(fnRet)
{
	this.fn_OnAddDeviceRet = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnLDevice = function(fnRet)
{
	this.fn_OnLDevice = fnRet;
};

//****************************************************************************************************
ModDeviceState.prototype.setOnRmDeviceRet = function(fnRet)
{
	this.fn_OnRmDeviceRet = fnRet;
};
