function TabCtrl()
{
	//****************************************************************************************************
	this.init = function(aoId, iSel)	
	{
		this.ao_Id = aoId;
		this.select(iSel);
	};

	//****************************************************************************************************
	this.select = function(iSel)
	{
		var iLen = this.ao_Id.length;

		for (var iIdx = 0; iIdx < iLen; iIdx++)
		{
			var pItem = this.ao_Id[iIdx];

			if (iIdx == iSel)
			{
				this.setClass(pItem.id, "");
				this.setClass(pItem.id_cmd, "active");
			}
			else
			{
				this.setClass(pItem.id, "hidden");
				this.setClass(pItem.id_cmd, "");
			}
		}
	};

	//****************************************************************************************************
	this.setClass = function(sId, sClass)
	{
		console.log("set class id = " + sId + " class = " + sClass);
	
		document.getElementById(sId).className = sClass;
	};

	//****************************************************************************************************
	this.ao_Id = null;
}
