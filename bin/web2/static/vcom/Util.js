
'use strict';

//****************************************************************************************************
//function $(selector) 
//{
//	return document.querySelector(selector);
//}

//****************************************************************************************************
function getEle(selector)
{
	return document.querySelector(selector);
}

//****************************************************************************************************
function queryStringToDictionary(queryString) 
{
	var pairs = queryString.slice(1).split('&');

	var result = {};
	pairs.forEach(function(pair) 
	{
		if (pair) 
		{
			pair = pair.split('=');
			if (pair[0]) 
			{
				result[pair[0]] = decodeURIComponent(pair[1] || '');
			}
		}
	});

	return result;
}

//****************************************************************************************************
function sendAsyncUrlRequest(method, url, body) 
{
	return sendUrlRequest(method, url, true, body);
}

//****************************************************************************************************
function sendUrlRequest(method, url, async, body) 
{
	return new Promise(function(resolve, reject) 
	{
		var xhr;
		var reportResults = function() 
		{
			if (xhr.status !== 200) 
			{
				reject(
				Error('Status=' + xhr.status + ', response=' + xhr.responseText));
				return;
			}

			resolve(xhr.responseText);
		};

		xhr = new XMLHttpRequest();
		if (async) 
		{
			xhr.onreadystatechange = function() 
			{
				if (xhr.readyState !== 4) 
				{
					return;
				}

				reportResults();
			};
		}

		console.log("request url = " + url + " method = " + method);
		xhr.open(method, url, async);
		xhr.send(body);

		if (!async) 
		{
			reportResults();
		}
	});
}

//****************************************************************************************************
// Returns a list of ICE servers after requesting it from the ICE server
// provider.
// Example response (iceServerRequestResponse) from the ICE server provider
// containing two TURN servers and one STUN server:
// {
//   lifetimeDuration: '43200.000s',
//   iceServers: [
//     {
//       urls: ['turn:1.2.3.4:19305', 'turn:1.2.3.5:19305'],
//       username: 'username',
//       credential: 'credential'
//     },
//     {
//       urls: ['stun:stun.example.com:19302']
//     }
//   ]
// }
function requestIceServers(iceServerRequestUrl, iceTransports) 
{
	console.log("ice server request url " + iceServerRequestUrl + " transports = " + iceTransports);

	return new Promise(function(resolve, reject) 
	{
		sendAsyncUrlRequest('GET', iceServerRequestUrl).then(function(sRes) 
		{
			var iceServerRequestResponse = parseJSON(sRes);

			if (!iceServerRequestResponse) 
			{
				reject(Error('Error parsing response JSON: ' + sRes));
				return;
			}

			if (iceTransports !== '') 
			{
				filterIceServersUrls(iceServerRequestResponse, iceTransports);
			}

			trace('Retrieved ICE server information. ' + sRes);
			resolve(iceServerRequestResponse.iceServers);

		}).catch(function(error) 
		{
			reject(Error('ICE server request error: ' + error.message));
			return;
		});
	});
}

//****************************************************************************************************
function parseJSON(json) 
{
	try 
	{
		return JSON.parse(json);
	} 
	catch (e) 
	{
		trace('Error parsing json: (' + json + ')');
	}
	return null;
}

//****************************************************************************************************
// Filter a peerConnection config to only contain ice servers with
// transport=|protocol|.
function filterIceServersUrls(config, protocol) 
{
	var transport = 'transport=' + protocol;
	var newIceServers = [];

	for (var i = 0; i < config.iceServers.length; ++i) 
	{
		var iceServer = config.iceServers[i];
		var newUrls = [];

		for (var j = 0; j < iceServer.urls.length; ++j) 
		{
			var url = iceServer.urls[j];
			if (url.indexOf(transport) !== -1) 
			{
				newUrls.push(url);
			} 
			else if (url.indexOf('?transport=') === -1) 
			{
				newUrls.push(url + '?' + transport);
			}
		}

		if (newUrls.length !== 0) 
		{
			iceServer.urls = newUrls;
			newIceServers.push(iceServer);
		}
	}

	config.iceServers = newIceServers;
}

//****************************************************************************************************
// Start shims for fullscreen
function setUpFullScreen() 
{
	document.cancelFullScreen = document.webkitCancelFullScreen ||
	document.mozCancelFullScreen || document.cancelFullScreen;

	document.body.requestFullScreen = document.body.webkitRequestFullScreen ||
	document.body.mozRequestFullScreen || document.body.requestFullScreen;

	document.onfullscreenchange = document.onfullscreenchange || document.onwebkitfullscreenchange || document.onmozfullscreenchange;
}

//****************************************************************************************************
function isFullScreen() 
{
	return !!(document.webkitIsFullScreen || document.mozFullScreen || document.isFullScreen); 
}

//****************************************************************************************************
function fullScreenElement() 
{
	return document.webkitFullScreenElement || document.webkitCurrentFullScreenElement || 
		document.mozFullScreenElement || document.fullScreenElement;
}

//****************************************************************************************************
function randomString(strLength) 
{
	var result = [];
	strLength = strLength || 5;
	var charSet = '0123456789';

	while (strLength--) 
	{
		result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
	}

	return result.join('');
}

