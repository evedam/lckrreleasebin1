(function(global) 
{
	System.config(
	{
		transpiler: 'ts',
		typescriptOptions: 
		{
			"target": "es5",
			"module": "commonjs",
			"moduleResolution": "node",
			"sourceMap": true,
			"emitDecoratorMetadata": true,
			"experimentalDecorators": true,
			"removeComments": false,
			"noImplicitAny": true,
			"suppressImplicitAnyIndexErrors": true,
			"typeRoots": 
			[
				"../../node_modules/@types/"
			]
		},
		meta: 
		{
			'typescript': 
			{
				"exports": "ts"
			}
		},
		paths: 
		{
			'npm:': 'https://unpkg.com/',
			'node:': 'mdm/node_modules/'
		},
		map: 
		{
			app: 'mdm',

			'@angular/core': 'node:@angular/core/bundles/core.umd.js',
			'@angular/common': 'node:@angular/common/bundles/common.umd.js',
			'@angular/compiler': 'node:@angular/compiler/bundles/compiler.umd.js',
			'@angular/platform-browser': 'node:@angular/platform-browser/bundles/platform-browser.umd.js',
			'@angular/platform-browser-dynamic': 'node:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
			'@angular/http': 'node:@angular/http/bundles/http.umd.js',
			'@angular/router': 'node:@angular/router/bundles/router.umd.js',
			'@angular/forms': 'node:@angular/forms/bundles/forms.umd.js',
			'@angular/upgrade': 'node:@angular/upgrade/bundles/upgrade.umd.js',

			'rxjs': 'node:rxjs',
			'angular-in-memory-web-api': 'node:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
			'ts': 'node:plugin-typescript/lib/plugin.js',
			'typescript': 'node:typescript/lib/typescript.js',

			'angular2-datatable': 'node:angular2-datatable',
			'ngx-tooltip':'npm:ngx-tooltip@0.0.9/index.js',
			'angular2-google-maps':'npm:angular2-google-maps/core',
			'lodash': 'node:lodash/lodash.js'

		},
		packages: 
		{
			app: 
			{
				main: './main.ts',
				defaultExtension: 'ts'
			}, 
			'@angular/core': 
			{
				defaultExtension: 'js'
			},
			'@angular/common':
			{
				defaultExtension: 'js'
			},
			'@angular/compiler': 
			{
				defaultExtension: 'js'
			},
			'@angular/platform-browser':
			{
				defaultExtension: 'js'
			},
			'@angular/platform-browser-dynamic':
			{
				defaultExtension: 'js'
			},
			'@angular/http':
			{
				defaultExtension: 'js'
			},
			'@angular/router':
			{
				defaultExtension: 'js'
			},
			'@angular/forms':
			{
				defaultExtension: 'js'
			},
			'@angular/upgrade':
			{
				defaultExtension: 'js'
			},

			'angular-in-memory-web-api':
			{
				defaultExtension: 'js'
			},
			'ts': 
			{
				defaultExtension: 'js'
			},
			'typescript':
			{
				defaultExtension: 'js'
			},
			rxjs: 
			{
				defaultExtension: 'js'
			},
			'angular2-datatable': 
			{
				main: 'index.js',
				defaultExtension: 'js'
			},
			"ngx-tooltip": 
			{ 
				defaultExtension: "js" 
			},
			"angular2-google-maps": 
			{ 
				main: 'index.js',
				defaultExtension: "js" 
			},
			'lodash':
			{
				defaultExtension: 'js'
			} 
		}
	});

	if (!global.noBootstrap) 	
	{
		bootstrap();
	}

	function bootstrap() 
	{
		System.set(System.normalizeSync('mdm/main.ts'), System.newModule({}));

		Promise.all(
		[
			System.import('@angular/platform-browser-dynamic'),
			System.import('mdm/app.module')
		])
		.then(function(imports) 
		{
			var platform = imports[0];
			var app = imports[1];
			platform.platformBrowserDynamic().bootstrapModule(app.AppModule);
		})
		.catch(function(err) 
		{
			console.error(err);
		});
	}

})(this);
