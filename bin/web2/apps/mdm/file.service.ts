
import { Injectable } from '@angular/core';

import { User } from './user';
import { File } from './file';

@Injectable()
export class FileService
{
	//****************************************************************************************************
	init() : void
	{
		p_Msg.getModFile().setOnDir(function(uiId, sFrom, pRes)
		{
			console.log("on dir id = " + uiId + " from = " + sFrom + " res = " + JSON.stringify(pRes));	
			
			var files = [];
			const len = pRes.lst_file.length;
			for (var idx = 0; idx < len; idx++)
			{
				var data = pRes.lst_file[idx];
				var file = new File(data.path, File.toType(data.type), data.size, data.hidden == "true");
				files.push(file);
			}
			
			this.setFile(sFrom, pRes.path, files);

		}.bind(this));

		p_Msg.getModFile().setOnFileStart(function(sFrom, uiId, uiLen, pMsg)
		{
			console.log("on file start id = " + uiId + " from = " + sFrom + " len = " + uiLen + " msg = " + JSON.stringify(pMsg));
		}.bind(this));

		p_Msg.getModFile().setOnFileProgress(function(sFrom, uiId, iProgress, pMsg)
		{
			console.log("on file progress id = " + uiId + " from = " + sFrom + " progress = " + iProgress + 
				" msg = " + JSON.stringify(pMsg));

			this.fileDownloadProgress = iProgress;
		}.bind(this));	

		p_Msg.getModFile().setOnFile(function(uiId, sFrom, sFile, pMsg)
		{
			console.log("msg = " + pMsg);
			console.log("on file id = " + uiId + " from = " + sFrom + " file_len = " + sFile.length + " msg = " + JSON.stringify(pMsg));
			console.log("path = " + pMsg.msg.path);

			var fileName = getFileName(pMsg.msg.path);
			if (!isSet(fileName))
				fileName = "file";

			var ext = getFileExt(fileName);
			if (!isSet(ext))
				ext = "png";

			console.log("downloading file " + fileName + " ext = " + ext);
			var blob = new Blob([sFile], {type: 'application/octet-binary'});
			fileDownload(blob, fileName);

			this.fileDownloadProgress = 0;
		}.bind(this));
	}

	//****************************************************************************************************
	get(usr: string, path: string, cache: boolean) : void
	{
		if (!isSet(usr))
		{
			console.log("please enter valid user name");
			return;
		}

		if (!isSet(path))
		{
			console.log("please enter valid file path");
			return;
		}

		if (cache && isSet(this.files[usr]) && this.files[usr][path])
		{
			console.log("file in cache " + path + " for " + usr);
			return;
		}

		this.setFile(usr, path, []);
		p_Msg.getModFile().get(usr, path);
	}

	//****************************************************************************************************
	setFile(usr, path, files)
	{
		var data = this.files[usr]; 
		if (!isSet(data))
			data = {};

		data[path] = files;
		this.files[usr] = data;
	}

	//****************************************************************************************************
	getFileDownloadProgress() : void
	{
		return this.fileDownloadProgress;
	}

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
		if (this.getCntUsr() > 0)
		{
			this.get(usr, "/", true);
		}
	}

	//****************************************************************************************************
	incCntUsr() : void
	{
		this.cntUsr++;
	}

	//****************************************************************************************************
	decCntUsr() : void
	{
		this.cntUsr--;
	}

	//****************************************************************************************************
	getCntUsr() : number
	{
		return this.cntUsr;
	}

	//****************************************************************************************************
	files = {};
	cntUsr: number = 0;
	fileDownloadProgress: number = 0;
}
