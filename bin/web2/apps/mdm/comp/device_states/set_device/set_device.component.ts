
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pkg } from '../../../pkg';
import { State } from '../../../state';
import { Script } from '../../../script';
import { DeviceStateService } from '../../../device_state.service';
import { ApkService } from '../../../apk.service';
import { CfgService } from '../../../cfg.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_set_device',
	templateUrl: './set_device.component.html'
})
export class SetDeviceComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private deviceStateService: DeviceStateService,
		private cfg: CfgService,
		private apkService: ApkService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.deviceStateService.getLState(false);
	}

	//****************************************************************************************************
	ngOnDestroy() : void
	{
	} 

	//****************************************************************************************************
	onSelState(id: string) : void	
	{
		console.log("id = " + id);
		this.state = id;	
		console.log("selected state = " + JSON.stringify(this.state));
	}

	//****************************************************************************************************
	setDevice() : void	
	{
		this.tried = true;
		this.processing = true;
		this.error = "";
		this.processStatus = "";

		if (!isSet(this.cfg.getUsrSel()))
		{
			console.log("please select user");
			alert("please select a user");
			return;
		}

		if (!isSet(this.state))
		{
			alert("select a state");
			console.log("please select state");
			return;
		}

		this.deviceStateService.setDevice(this.cfg.getUsrSel().getId(), [this.state], function(pRet)
		{
			console.log("ret = " + JSON.stringify(pRet));
			this.processing = false;

			if (pRet.ret == "false")
			{
				console.log("adding new device");
				this.deviceStateService.addDevice(this.cfg.getUsrSel().getId(), [this.state], function(pRet)
				{
					console.log("ret = " + JSON.stringify(pRet));
				}.bind(this));
			}
		}.bind(this)); 
	}

	newState: State = new State();	
	tried: boolean = false;
	processStatus: string = "";
	processing: boolean = false;
	state: string = "";
}

