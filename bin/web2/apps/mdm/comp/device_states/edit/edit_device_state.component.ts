
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pkg } from '../../../pkg';
import { State } from '../../../state';
import { Script } from '../../../script';
import { DeviceStateService } from '../../../device_state.service';
import { ApkService } from '../../../apk.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_edit_device_state',
	templateUrl: './edit_device_state.component.html'
})
export class EditDeviceStateComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private deviceStateService: DeviceStateService,
		private apkService: ApkService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.getLApk(false);
		this.getLState(false);
	}

	//****************************************************************************************************
	onSelId(id: string) : void	
	{
		console.log("id = " + id);
		this.state = id;	
		console.log("selected state = " + JSON.stringify(this.getState()));
	}

	//****************************************************************************************************
	onSelPkgApk(apk: string, pkg: Pkg) : void
	{
		console.log("apk = " + apk);
		console.log("pkg = " + JSON.stringify(pkg));
	
		this.apkService.getApk(apk).then(function(pApk)
		{
			pkg.apk = pApk.id;
			pkg.pkg = pApk.pkg.pkg;
			pkg.versionCode = pApk.pkg.versionCode;
			pkg.versionName = pApk.pkg.versionName;	
		}.bind(this)); 
	}
	
	//****************************************************************************************************
	ngOnDestroy() : void
	{
	}

	//****************************************************************************************************
	getLApk(fromServer: boolean) : void
	{
		this.apkService.getApks(fromServer);
	}

	//****************************************************************************************************
	addState() : void	
	{
		for (var idx = 0; idx < 3; idx++)
		{
			var id = "" + idx;
			var pState = 
			{
				"id":"state" + id,
				"desc":"state " + id + " description",
				"lpkg":
				[
					{"apk":"xxxxx" + id + "1","pkg":"xxx.xxx.xxx" + id + "1","ver_code":id, "ver_name":"" + id + ".0","state":"none","type":"none"},
					{"apk":"xxxxx" + id + "2","pkg":"xxx.xxx.xxx" + id + "2","ver_code":id, "ver_name": id + ".2","state":"none","type":"none"}
				],
				"lpkg_rm":
				[
					{"apk":"xxxxx" + id + "3","pkg":"xxx.xxx.xxx" + id + "3","state":"none","type":"none"},
					{"apk":"xxxxx" + id + "4","pkg":"xxx.xxx.xxx" + id + "4","state":"none","type":"none"}
				],
				"st":"active"
			};

			this.deviceStateService.addState(pState, function(pData)
			{
				console.log("data = " + JSON.stringify(pData));
			}.bind(this));
		}
	}

	//****************************************************************************************************
	setState() : void
	{
		var state = new State();
		state.setId("state1");
		state.setDesc("new state description 1");
		state.setSt("active");
		
		for (var idx = 0; idx < 10; idx++)
		{
			var pkg = new Pkg();
			pkg.setApk("apk1" + idx);			
			pkg.setPkg("pkg1" + idx);
			pkg.setVersionCode("1" + idx);
			pkg.setVersionName("1.2.2_" + idx);
			state.addPkg(pkg);
		}

		for (var idx = 0; idx < 3; idx++)
		{
			var pkg = new Pkg();
			pkg.setApk("apk2" + idx);
			pkg.setPkg("pkg2" + idx);	
			pkg.setVersionCode("2" + idx);
			pkg.setVersionName("1.2.2_" + idx);
			state.addPkgRm(pkg);
		}

		this.deviceStateService.setState(state.getData(), function(pData)
		{
			console.log("ret = " + JSON.stringify(pData));
		}.bind(this));
	}

	//****************************************************************************************************
	private save() : void
	{
		this.tryToSave = true;

		var sRet = this.getState().isValid();
		if (isSet(sRet))	
		{
			console.log("invalid state " + sRet);
			return;
		}

		this.deviceStateService.setState(this.getState().getData(), function(pData)
		{
			console.log("ret = " + JSON.stringify(pData));
		}.bind(this));
	}

	//****************************************************************************************************
	getLState(fromServer: boolean) : void
	{
		this.deviceStateService.getLState(fromServer);
	}	

	//****************************************************************************************************
	rmState(id: string) : void
	{
		this.deviceStateService.rmState(id, function(pData)
		{
			console.log("data = " + JSON.stringify(pData));
		}.bind(this));	
	} 

	//****************************************************************************************************
	private addPkg() : void
	{
		this.getState().addPkg(new Pkg());	

		console.log("state = " + JSON.stringify(this.getState()));
	}

	//****************************************************************************************************
	private addPkgRm() : void
	{
		this.getState().addPkgRm(new Pkg());
	}
	
	//****************************************************************************************************
	private addScript() : void
	{
		this.getState().addScript(new Script());
	}

	//****************************************************************************************************
	private getState() : State
	{
		var state = this.deviceStateService.getState(this.state);
		if (!isSet(state))
			state = new State();
		return state;
	}

	private state: string;
	private tryToSave: boolean = false;
}

