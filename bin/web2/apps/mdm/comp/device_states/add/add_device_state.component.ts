
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pkg } from '../../../pkg';
import { State } from '../../../state';
import { Script } from '../../../script';
import { DeviceStateService } from '../../../device_state.service';
import { ApkService } from '../../../apk.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_add_device_state',
	templateUrl: './add_device_state.component.html'
})
export class AddDeviceStateComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private deviceStateService: DeviceStateService,
		private apkService: ApkService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
	}

	//****************************************************************************************************
	ngOnDestroy() : void
	{
	} 

	//****************************************************************************************************
	addState() : void	
	{
		this.tried = true;
		this.processing = true;
		this.error = "";
		this.processStatus = "";

		if (this.validateId(this.newState.id) != " " || this.validateDescription(this.newState.desc) != " ")
		{
			console.log("please enter valid fields");
			this.processing = false;
			return;
		}

		this.deviceStateService.addState(this.newState, function(pData)
		{
			console.log("data = " + JSON.stringify(pData));
			this.processing = false;

			if (pData.ret == "true")
			{
				console.log("success");
				this.processStatus = "success";
			}
			else
			{
				this.processStatus = "failed";
				this.error = pData.msg;
			}
		}.bind(this));
	}

	//****************************************************************************************************
	validateId(id)
	{
		let ret = " ";

		if (this.tried != true) 
			return ret; 

		console.log("validate id " + id + " ret = " + isAlNum(id));

		if (isSet(id) != true)
			ret = "Plese enter valid id";

		if (isAlNum(id) != true)
			ret = "Id can only contains letter and numbers only";

		return ret;
	}

	//****************************************************************************************************
	validateDescription(desc)
	{
		let ret = " ";
		console.log("validate description " + desc);

		if (this.tried != true) 
			return ret; 

		if (isSet(desc) != true)
			ret = "Plese enter valid description";

		if (isAlNumSpace(desc) != true)
			ret = "Description can only contains letter, numbers and spaces only";

		return ret;
	}

	newState: State = new State();	
	tried: boolean = false;
	processStatus: string = "";
	processing: boolean = false;
}

