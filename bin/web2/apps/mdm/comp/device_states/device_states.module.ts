import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { DeviceStatesComponent }   from './device_states.component';
import { AddDeviceStateComponent }   from './add/add_device_state.component';
import { EditDeviceStateComponent }   from './edit/edit_device_state.component';
import { SendDeviceStateComponent } from './send/send_device_state.component';
import { SetDeviceComponent } from './set_device/set_device.component';

@NgModule(
{
	imports:      
	[ 
		CommonModule, 
		FormsModule
	],
	declarations: 
	[ 
		DeviceStatesComponent,
		AddDeviceStateComponent,
		EditDeviceStateComponent,
		SendDeviceStateComponent,
		SetDeviceComponent
	],
	exports: 
	[
		DeviceStatesComponent,
		AddDeviceStateComponent,
		EditDeviceStateComponent,
		SendDeviceStateComponent,
		SetDeviceComponent
	]
})

export class DeviceStatesModule { }

