
import { Component, OnInit } from '@angular/core';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';
import { LocService } from '../../loc.service.ts';
import { UserService } from '../../user.service.ts';
import { AgmCoreModule } from 'angular2-google-maps/core';


@Component
({
	moduleId: module.id,
	selector: 'comp_loc',
	styles: [` .sebm-google-map-container { height: 600px; } `],
	templateUrl: 'loc.component.html'
})
export class LocComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private viewService: ViewService,
		private cfg: CfgService,
		private locService: LocService,
		private userService: UserService,
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.progress = 0;
		
		this.setTimeout();
	} 

	//****************************************************************************************************
	setTimeout() : void
	{
		this.refresh();

		setTimeout(
		()=> 
		{
			this.setTimeout();
		}, 30000);
	}

	//****************************************************************************************************
	refresh() : void
	{
		this.locService.getCurrentL();
	}

	//****************************************************************************************************
	clickedMarker(label: string, index: number) 
	{
		console.log(`clicked the marker: ${label || index}`)
	} 

	//****************************************************************************************************
	getName(imei: string)
	{
		let usr = this.userService.getUser(imei);
		if (!isSet(usr))
			return imei;

		return usr.getName();
	}

	//****************************************************************************************************
	mapClicked($event: MouseEvent) 
	{
		let marker = (
		{
			lat: $event.coords.lat,
			lng: $event.coords.lng
		});
		console.log("marker = " + JSON.stringify(marker));
	}

	//****************************************************************************************************
	markerDragEnd(m: marker, $event: MouseEvent) 
	{
		console.log('dragEnd', m, $event);
	} 

	zoom: number = 8;
	lat: number = 6.9077693;
	lng: number = 79.922475;
}

