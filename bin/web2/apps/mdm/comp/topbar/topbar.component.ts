
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { UserService } from '../../user.service.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_topbar',
	templateUrl: 'topbar.component.html'
})
export class TopbarComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private usrService: UserService,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit() : void 
	{
	}

	//****************************************************************************************************
	getUsrStatusImg() : string
	{
		if (this.cfg.isMultiSelUsr()) 
			return this.imgUsr; 

		if (this.cfg.getUsrSel() != null && this.cfg.getUsrSel().getState() == 'connected')
			return this.imgOnlineUsr;

		return this.imgOfflineUsr;
	}

	//****************************************************************************************************
	getUsrInfo() : string
	{
		if (this.cfg.isMultiSelUsr())
			return "Multiple users have been selected";
			
		if (this.cfg.getUsrSel() == null) 
			return "Device Not Selected";

		return this.cfg.getUsrSel().getName() + " IMEI : " + this.cfg.getUsrSel().getDevice().getImei();
	}

	//****************************************************************************************************
	logOff() : void
	{
		window.location.reload();
		this.cfg.setUsr(null);
		this.viewService.showAll(false);
	}

	path: string = "mdm/comp/topbar/";
	imgOnlineUsr = this.path + "img/online.png"; 
	imgUsr = this.path + "img/user.png"; 
	imgOfflineUsr = this.path + "img/offline.png"; 
	usrImgWidth = 25;
}

