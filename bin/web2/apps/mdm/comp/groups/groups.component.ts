
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { UserService } from '../../user.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_groups',
	templateUrl: 'groups.component.html',
	styleUrls: [ 'groups.component.css' ]
})
export class GroupsComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private userService: UserService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.userService.getGroups(false);
	}
	
	//****************************************************************************************************
	ngOnDestroy() : void
	{
	}

	//****************************************************************************************************
	add() : void	
	{
		this.userService.setLUsr(123, "continental", "[\"device1\", \"device2\", \"device3\", \"device4\", \"device5\"]", false);
		this.userService.setLUsr(123, "group1", "[\"device1\", \"device4\", \"device5\"]", false);
		this.userService.setLUsr(123, "group2", "[\"device1\", \"device2\", \"device3\", \"device5\"]", false);
	}
}

