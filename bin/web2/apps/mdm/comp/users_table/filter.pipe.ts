import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

import { User } from '../../user.ts';

@Pipe(
{
	name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform 
{
	transform(array: any[], filterUser: User): any 
	{
		if (filterUser) 
		{
			return _.filter(array, row=>
				row.id.toUpperCase().indexOf(filterUser.getId().toUpperCase()) > -1 && 
				row.uid.toUpperCase().indexOf(filterUser.getUid().toUpperCase()) > -1 && 
				row.name.toUpperCase().indexOf(filterUser.getName().toUpperCase()) > -1 && 
				row.type.toUpperCase().indexOf(filterUser.getType().toUpperCase()) > -1 && 
				row.state.toUpperCase().indexOf(filterUser.getState().toUpperCase()) > -1 && 
				row.getDevice().getImei().toUpperCase().indexOf(filterUser.getDevice().getImei().toUpperCase()) > -1 &&
				row.getDevice().getModel().toUpperCase().indexOf(filterUser.getDevice().getModel().toUpperCase()) > -1);
		}

		return array;
	}
}
