import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";
import { HttpModule } from "@angular/http";

import { DemoComponent }   from './users_table.component';
import { DataFilterPipe }   from './filter.pipe';

@NgModule(
{
	imports:      
	[ 
		CommonModule, 
		DataTableModule, 
		FormsModule,
		HttpModule
	],
	declarations: 
	[ 
		DemoComponent, 
		DataFilterPipe 
	],
	exports: 
	[
		DemoComponent
	]
})

export class UsersTableModule { }

