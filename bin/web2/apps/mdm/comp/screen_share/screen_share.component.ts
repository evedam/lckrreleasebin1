
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { CmdService } from '../../cmd.service';
import { ScreenShareService } from '../../screen_share.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_screen_share',
	templateUrl: 'screen_share.component.html'
})
export class ScreenShareComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private cmdService: CmdService,
		private screenShareService: ScreenShareService
	) 
	{ 
		this.to = null;
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		
	}

	//****************************************************************************************************
	ngOnDestroy() : void
	{
		console.log("on destroy");
		
		if (this.to != null)	
			this.stop();

		this.to = null;
	}

	//****************************************************************************************************
	start() : void
	{
		if (this.cfg.getUsrSel() == null)
		{
			alert("please select a destination user");
			return;
		}

		this.to = new User();
		this.to.setId(this.cfg.getUsrSel().getId());
		this.screenShareService.start(this.to.getId());
	}

	//****************************************************************************************************
	stop() : void
	{
		this.screenShareService.stop(this.to.getId());

	}

	//****************************************************************************************************
	onClick(event) : void
	{ 
		let width = document.getElementById("screen_share_screen").offsetWidth;
		let height = document.getElementById("screen_share_screen").offsetHeight;

		console.log("x = " + event.offsetX + " y = " + event.offsetY + " width = " + width + " height = " + height);

		let x = event.offsetX / width;
		let y = event.offsetY / height;
		this.screenShareService.click(this.to.getId(), x, y);
	}

	to: string;
}

