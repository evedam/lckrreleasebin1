
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_sidebar',
	templateUrl: 'sidebar.component.html'
})
export class SidebarComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		initSidebar();
	}

	//****************************************************************************************************
	toggleFullScreen() : void
	{
		toggleFullScreen(document.body);
	}
}

