
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_menu',
	templateUrl: 'menu.component.html'
})
export class MenuComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
	}
}

