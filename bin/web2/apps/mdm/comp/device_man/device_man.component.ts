
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_device_man',
	templateUrl: 'device_man.component.html'
})
export class DeviceManComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit() : void 
	{
	}

	//****************************************************************************************************
	restart() : void
	{
		if (this.cfg.getUsrSel() == null)
		{
			console.log("please select a user to reboot");
			alert("Please select a user to reboot.");
			return;
		}

		p_Msg.rebootOs(7000, this.cfg.getUsrSel().getId());
	}
}

