
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { FileService } from '../../file.service';
import { File } from '../../file';

@Component
({
	moduleId: module.id,
	selector: 'comp_files',
	templateUrl: 'files.component.html',
	styleUrls: [ 'files.component.css' ]
})
export class FilesComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private fileService: FileService
	) 
	{ 
		this.pathCur = "/storage/emulated/0";
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.fileService.incCntUsr();
	}
	
	//****************************************************************************************************
	ngOnDestroy() : void
	{
		this.fileService.decCntUsr();
	}

	//****************************************************************************************************
	add()
	{
		var usr = this.cfg.getUsrSel();
		if (usr == null)
		{
			alert("please select a user");
			return;
		}

		if (this.getPathCur() == null)
		{
			alert("please navigate to a path before add a file");
			return;
		}

		var dest = this.getPathCur() + "/" + getFileName(this.filePathToAdd); 
		console.log("usr = " + usr.getId() + " file = " + this.fileToAdd + " file_path = " + this.filePathToAdd + " dest = " + dest);

		p_Msg.getModFile().add(usr.getId(), this.fileToAdd, this.filePathToAdd, dest, function(iProgress)
		{
			console.log("add file progress " + iProgress);
			this.progressAdd = iProgress;
		}.bind(this));
	}

	//****************************************************************************************************
	go(path: string, cache: boolean) : void
	{
		if (this.cfg.getUsrSel() == null)
		{
			alert("please select a user");
			return;
		}

		this.pathCur = path;
		this.fileService.get(this.cfg.getUsrSel().getId(), path, cache);
	}

	//****************************************************************************************************
	open(file: File) : void
	{
		if (file.getType() == File.Type.Dir)
		{
			this.go(file.getPath(), true);
			return;
		}

		console.log("openning file " + file.getPath() + " ... ");
		this.fileService.get(this.cfg.getUsrSel().getId(), file.getPath(), false);
	}

	//****************************************************************************************************
	select(file: File) : void
	{
		console.log("select file " + file.getPath());
		this.selectedFile = file;
	}

	//****************************************************************************************************
	setTooltipFile(file: File)
	{
		console.log("set tooltip file");
		this.tooltipFile = file;
	}

	//****************************************************************************************************
	getPathCur() : string
	{
		if (!isSet(this.pathCur))	
			return "";

		return this.pathCur[this.pathCur.length - 1] == '/' && this.pathCur.length != 1 ? 
			this.pathCur.substring(0, this.pathCur.length - 1) : this.pathCur; 
	}

	//****************************************************************************************************
	onFileToAdd(file, path) : void 
	{
		this.fileToAdd = file;
		this.filePathToAdd = path;
		console.log("select file = " + this.fileToAdd + " path = " + this.filePathToAdd);	
	}

	//****************************************************************************************************
	isFile(file: File) : boolean
	{
		return file.getType() == File.Type.File;
	}

	//****************************************************************************************************
	getFileImg(file) : void
	{
		return file.getType() == File.Type.Dir ? this.imgDir : this.imgFile;
	}

	//****************************************************************************************************
	getFileName(file: File, len: number) : string
	{
		return file.getName().length > len ? file.getName().substring(0, len - 2) + ".." : file.getName(); 
	}

	path: string = "mdm/comp/files/";
	imgFile = this.path + "img/file.png"; 
	imgDir = this.path + "img/dir.png"; 
	fileImgWidth = 50;

	pathCur: string;
	triedAdd: boolean;
	progressAdd: number = 0;
	fileToAdd: string;
	filePathToAdd: string;
	
	selectedFile: File;
	tooltipFile: File;
}

