import { Component, OnInit } from '@angular/core';

import { User } from '../../user.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_call',
	templateUrl: 'call.component.html',
})

export class CallComponent implements OnInit 
{
	//****************************************************************************************************
	constructor(private cfg: CfgService) 
	{
	
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{ 
		initWindowButtons();
	}

	//****************************************************************************************************
	call() : void
	{
		if (this.cfg.getUsrSel() == null)
		{
			alert("please select a user to connect");
			return;
		}

		var sSesId = "session_id_" + (new Date()).getTime();
		p_Msg.sendCamReq(122, "", sSesId, this.cfg.getUsrSel().getId());
	}
}
