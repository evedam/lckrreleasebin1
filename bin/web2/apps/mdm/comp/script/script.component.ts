
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { ScriptService } from '../../script.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_script',
	templateUrl: 'script.component.html'
})
export class ScriptComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private scriptService: ScriptService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit() : void 
	{
	}

	//****************************************************************************************************
	run() : void
	{
		this.tried = true;

		if (this.cfg.getCntUsrSel() == 0)
		{
			alert("please select user");
			return;
		}

		if (!this.isValidScript())
			return;
		
		var mUsrSel = this.cfg.getMUsrSel();
		for (var usr in mUsrSel)
			this.scriptService.run(mUsrSel[usr].getId(), this.script);
	}

	//****************************************************************************************************
	isValidScript() : boolean
	{
		return isSet(this.script);
	}

	//****************************************************************************************************
	onKey(code) : void
	{
		console.log("on key " + code);

		switch (code)
		{
		case 13:
			this.run();
			break;
		}
	}

	tried: boolean;
	script: string;
}

