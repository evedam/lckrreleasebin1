import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";
import { HttpModule } from "@angular/http";

import { ReportDeviceListComponent }   from './device_list.component';
import { DataFilterPipe }   from './filter.pipe';

@NgModule(
{
	imports:      
	[ 
		CommonModule, 
		DataTableModule, 
		FormsModule,
		HttpModule
	],
	declarations: 
	[ 
		ReportDeviceListComponent, 
		DataFilterPipe 
	],
	exports: 
	[
		ReportDeviceListComponent
	]
})

export class ReportDeviceListModule { }

