
import { Pkg } from './pkg';
import { Script } from './script';

export class State 
{
	//****************************************************************************************************
	constructor()
	{
		this.setId("");
		this.setDesc("");
		this.setLock(false);
		this.lpkg = [];	
		this.lpkgRm = [];
		this.lscript = [];
	}
	
	//****************************************************************************************************
	public load(data) : void
	{
		this.setId(data.id);
		this.setDesc(data.desc);
		this.setSt(data.st);
		this.setLock(data.is_lock == "true");

		if (isSet(data.lpkg))
		{
			var len = data.lpkg.length;
			for (var idx = 0; idx < len; idx++)
			{
				var pkg = new Pkg();
				pkg.load(data.lpkg[idx]);
				this.lpkg.push(pkg);
			}
		}
		
		if (isSet(data.lpkg_rm))
		{	
			len = data.lpkg_rm.length;
			for (var idx = 0; idx < len; idx++)
			{
				var pkg = new Pkg();
				pkg.load(data.lpkg_rm[idx]);
				this.lpkgRm.push(pkg);
			}
		}

		if (isSet(data.lscript))
		{
			len = data.lscript.length;
			for (var idx = 0; idx < len; idx++)
			{
				var script = new Script();
				script.load(data.lscript[idx]);
				this.lscript.push(script);
			}
		}
	}

	//****************************************************************************************************
	public getData()
	{
		var data = {};
		data.id = this.getId();
		data.desc = this.getDesc();
		data.st = this.getSt();
		data.is_lock = this.isLock() ? "true" : "false";
		
		var len = this.lpkg.length;
		data.lpkg = [];
		for (var idx = 0; idx < len; idx++)	
			data.lpkg.push(this.lpkg[idx].getData());	

		len = this.lpkgRm.length;
		data.lpkg_rm = [];
		for (var idx = 0; idx < len; idx++)
			data.lpkg_rm.push(this.lpkgRm[idx].getData());

		len = this.lscript.length;
		data.lscript = [];
		for (var idx = 0; idx < len; idx++)
			data.lscript.push(this.lscript[idx]);

		return data;
	}

	//****************************************************************************************************
	public isValid() : string 
	{
		var sRet = "";

		if (!isSet(this.getId()))
			sRet += "{msg:'invalid id'}";

		if (!isSet(this.getDesc()))
			sRet += "{msg:'invalid description'}";

		if (!isSet(this.getSt()))
			sRet += "{msg:'invalid state'}";

		var len = this.lpkg.length;
		for (var idx = 0; idx < len; idx++)
		{
			var pkg = this.lpkg[idx];

			if (!isSet(pkg.getPkg()))
				sRet += "{msg:'invalid package', sec:'pkg', idx:" + idx + "}";

			if (!isSet(pkg.getApk()))
				sRet += "{msg:'invalid apk', sec:'pkg', idx:" + idx + "}";

			if (!isSet(pkg.getVersionCode()))
				sRet += "{msg:'invalid version code', sec:'pkg', idx:" + idx + "}";

			if (!isSet(pkg.getVersionName()))
				sRet += "{msg:'invalid version name', sec:'pkg', idx:" + idx + "}";
		}

		var len = this.lpkgRm.length;
		for (var idx = 0; idx < len; idx++)
		{
			var pkg = this.lpkgRm[idx];

			if (!isSet(pkg.getPkg()))
				sRet += "{msg:'invalid package', sec:'pkg rm', idx:" + idx + "}";
		}

		var len = this.lscript.length;
		for (var idx = 0; idx < len; idx++)
		{
			var pkg = this.lscript[idx];

			if (!isSet(pkg.getBash()))
				sRet += "{msg:'invalid bash script', sec:'script', idx:" + idx + "}";
		}

		return sRet;
	}

	private setId(id: string) : void
	{
		this.id = id;
	}

	public getId() : string
	{
		return this.id;
	}

	private setDesc(desc: string)
	{
		this.desc = desc;
	}

	public getDesc() : string
	{
		return this.desc;
	}

	public setSt(st: string) : void
	{
		this.st = st;
	}

	public getSt() : string
	{
		return this.st;
	}
	
	public setLock(lock: boolean) : void
	{
		this.lock = lock;
	}

	public isLock() : boolean
	{
		return this.lock;
	}

	public addPkg(pkg: Pkg) : void
	{
		this.lpkg.push(pkg);
	}

	public getAPkg() : []
	{
		return this.lpkg;
	}

	public addPkgRm(pkg: Pkg) : void
	{
		this.lpkgRm.push(pkg);
	}

	public getAPkgRm() : []
	{
		return this.lpkgRm;
	}

	public addScript(script: Script) : void
	{
		this.lscript.push(script);
	}

	public getAScript() : []
	{
		return this.lscript;
	}

	id: string;
	desc: string;	
	st: string = "none";
	lock: boolean = false;
	lpkg: Pkg[];
	lpkgRm: Pkg[];
	lscript: Script[];
}
