
import { Injectable } from '@angular/core';

import {UserService } from './user.service';
import { User } from './user';
import { File } from './file';

@Injectable()
export class ScriptService
{
	//****************************************************************************************************
	constructor(private usrService: UserService)
	{

	}

	//****************************************************************************************************
	init() : void
	{
		p_Msg.getModScript().setOnRet(function(uiId, sFrom, pRes)
		{
			console.log("on run script res id = " + uiId + " from = " + sFrom + " res = " + JSON.stringify(pRes));	

			pRes.usr = this.usrService.getDeviceSync(sFrom);	
			this.out.unshift(pRes);
			
		}.bind(this));
	}

	//****************************************************************************************************
	run(usr: string, script: string) : void
	{
		if (!isSet(usr))
		{
			console.log("please enter valid user name");
			return;
		}

		if (!isSet(script))
		{
			console.log("please enter valid script");
			return;
		}

		p_Msg.getModScript().run(usr, script);
	}

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
	}

	//****************************************************************************************************
	out: [] = [];
}
