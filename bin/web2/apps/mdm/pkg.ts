export class Pkg 
{
	constructor()
	{
	}

	public load(pkg) : void
	{
		this.setPkg(pkg.pkg);
		this.setApk(pkg.apk);
		this.setVersionCode(pkg.ver_code);
		this.setVersionName(pkg.ver_name);
	} 

	public getData() 
	{
		var data = {};
		data.pkg = this.getPkg();
		data.apk = this.getApk();
		data.ver_code = this.getVersionCode();
		data.ver_name = this.getVersionName();
		return data;
	}

	setPkg(pkg: string) : void
	{
		this.pkg  = pkg;
	}

	getPkg() : string
	{
		return this.pkg;
	}

	setApk(apk: string) : void
	{
		this.apk = apk;	
	}

	getApk() : string
	{
		return this.apk;
	}

	setVersionCode(code: number) : void
	{
		this.versionCode = code;
	}

	getVersionCode() : number
	{
		return this.versionCode;
	}

	setVersionName(name: string) : void
	{
		this.versionName = name;
	}

	getVersionName() : string
	{
		return this.versionName;
	}

	pkg: string;
	apk: string;
	versionCode: number;
	versionName: string;
}
