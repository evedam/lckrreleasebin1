
import { Pkg } from './pkg';

export class Apk
{
	constructor(id: string, desc: string, state: string)
	{
		this.setId(id);
		this.setDesc(desc);
		this.setState(state);
	}

	load(apk) : void
	{
		this.setId(apk.id);
		this.setDesc(apk.desc);
		this.setState(apk.state);

		this.pkg = new Pkg();
		this.pkg.load(apk);
	} 

	setId(sId : string) : void
	{
		this.id = sId;
	}

	getId() : string
	{
		return this.id;
	}

	setDesc(desc: string) : void
	{
		this.desc = desc;
	}

	getDesc() : string
	{
		return this.desc;
	}

	setState(state: string) : void
	{
		this.state = state;
	} 

	getState() : string
	{
		return this.state;
	}

	id: string;
	pkg: Pkg;
	desc: string;
	state: string;
}
