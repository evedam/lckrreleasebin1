
import { Injectable } from '@angular/core';

import { User } from './user';
import { Apk } from './apk';
import { Pkg } from './pkg';
import { UserService } from './user.service';
import { PkgService } from './pkg.service';
import { CmdService } from './cmd.service';
import { LocService } from './loc.service';
import { Device } from './device';

@Injectable()
export class CfgService 
{
	//****************************************************************************************************
	constructor(private usrService: UserService, private pkgService: PkgService, private cmdService: CmdService,
			private locService: LocService)
	{
		this.usr = null;
		this.usrSel = {};
		
		this.services.push(this.pkgService);
		this.services.push(this.cmdService);
		this.services.push(this.locService);
	}

	//****************************************************************************************************
	isLogged() : boolean
	{
		return this.usr != null; 
	}

	//****************************************************************************************************
	setUsr(usr: User) : void
	{
		console.log("set user " + JSON.stringify(usr));
		this.usr = usr;
	}

	//****************************************************************************************************
	getUsr() : User
	{
		return this.usr;
	}
	
	//****************************************************************************************************
	setUsrSel(usr: User) : void
	{
		this.usrSel = {};
		this.usrSel[usr.getId()] = usr;

		var len = this.services.length;
		for (var idx = 0; idx < len; idx++)
		{
			this.services[idx].onSetUsrSel(usr);
		}
	}

	//****************************************************************************************************
	getUsrSel() : User
	{
		var keys = Object.keys(this.usrSel);
		if (keys.length != 1)
			return null;

		return this.usrSel[keys[0]];
	}	

	//****************************************************************************************************
	addUsrSel(usr: Usr) : void
	{
		this.usrSel[usr.getId()] = usr;
	}

	//****************************************************************************************************
	getMUsrSel() : {} 
	{
		return this.usrSel;
	}

	//****************************************************************************************************
	isSelUsr(usr: User) : boolean
	{
		return isSet(this.usrSel[usr.getId()]);
	}

	//****************************************************************************************************
	isMultiSelUsr() : boolean
	{
		var keys = Object.keys(this.usrSel);
		return keys.length > 1;
	}

	//****************************************************************************************************
	getCntUsrSel() : boolean
	{
		var keys = Object.keys(this.usrSel);
		return keys.length;
	}

	//****************************************************************************************************
	setApkSel(apk: Apk) : void
	{
		this.apkSel = apk;
	}

	//****************************************************************************************************
	getApkSel() : Apk
	{
		return this.apkSel;
	}

	//****************************************************************************************************
	setPkgSel(apk: Apk) : void
	{
		this.apkSel = apk;
	}

	//****************************************************************************************************
	getPkgSel() : Pkg
	{
		return this.pkgSel;
	}

	usr: User;
	usrSel: {};
	apkSel: Apk;
	pkgSel: Pkg;
	services = [];
}
