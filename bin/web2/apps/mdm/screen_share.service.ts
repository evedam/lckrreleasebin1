import { Injectable } from '@angular/core';

import { User } from './User.ts';

@Injectable()
export class ScreenShareService
{
	//****************************************************************************************************
	start(sTo) : void
	{
		var uiId = 5000;
		console.log("start screen share id = " + uiId + " to = " + sTo);
		p_Msg.getModScreenShare().shareScreen(uiId, sTo);
	};

	//****************************************************************************************************
	stop(sTo)
	{
		var uiId = 5001;
		console.log("stop share screen id = " + uiId + " to = " + sTo);
		p_Msg.getModScreenShare().stopShareScreen(uiId, sTo);

		setTimeout(function()
		{
			this.lastlyReceivedImg = null;	
			console.log("clear last image");
		}.bind(this), 5000);
	};

	//****************************************************************************************************
	click(sTo, iX, iY) : void
	{
		var uiId = 5002;
		console.log("start screen share id = " + uiId + " to = " + sTo);
		p_Msg.getModScreenShare().click(uiId, sTo, iX, iY);
	};

	//****************************************************************************************************
	init() : void
	{
		p_Msg.getModScreenShare().setOnScreenImgStart(function(sFrom, uiId, uiLen, pMsg)
		{
			console.log("on screen img start id = " + uiId + " from = " + sFrom + " len = " + uiLen + " msg = " + JSON.stringify(pMsg));
		}.bind(this));

		p_Msg.getModScreenShare().setOnScreenImgProgress(function(sFrom, uiId, iProgress, pMsg)
		{
			console.log("on screen img progress id = " + uiId + " from = " + sFrom + " progress = " + iProgress + 
				" msg = " + JSON.stringify(pMsg));
		}.bind(this));	

		p_Msg.getModScreenShare().setOnScreenImg(function(uiId, sFrom, sFile, sMsg)
		{
			console.log("on screen img id = " + uiId + " from = " + sFrom + " file_len = " + sFile.length + " msg = " + sMsg);

			this.lastlyReceivedImg = 'data:image/jpeg;base64,' + btoa(sFile);
		}.bind(this));
	}
	
	//****************************************************************************************************
	getLastlyReceivedImg() : string
	{
		return this.lastlyReceivedImg;	
	}

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
	}

	//****************************************************************************************************
	callbacks = [];
	noticedTime: number = 0;
	lastlyReceivedImg: string = null;
}
