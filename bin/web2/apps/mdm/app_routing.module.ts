
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard }		from './auth_guard.service.ts';
import { LoginComponent } 	from './comp/login/login.component.ts';
import { AdminComponent }   from './comp/admin/admin.component.ts';
import { UseresComponent }      from './comp/users/users.component.ts';
import { UserDetailComponent }  from './comp/user_detail/user_detail.component.ts';

const a_Routes : Routes = 
[
	{ 
		path: '', 
		redirectTo: '/admin', 
		pathMatch: 'full' 
	},
	{ 
		path: 'login', 
		component: LoginComponent 
	},
	{ 
		path: 'admin',  
		component: AdminComponent, 
		canActivate: [AuthGuard]
	},
	{ 
		path: 'detail/:id', 
		component: UserDetailComponent,
		canActivate: [AuthGuard]
	},
	{ 
		path: 'users',     
		component: UseresComponent,
		canActivate: [AuthGuard]
	}
];

@NgModule
({
	imports: [ RouterModule.forRoot(a_Routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule
{

}
