
const Log = require("../Cmn/Log"); 
const Comm = require("../Comm");
const Util = require("../Cmn/Util");
const AuthService = require("../Service/Auth.js");
const UsrService = require("../Service/Usr.js");

module.exports = 
{
	//****************************************************************************************************
	init: function(pApp, pAppInt)
	{
		pAppInt.post("/v1/usr/info", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, "usr_info", null);
		});	
	},

	//****************************************************************************************************
	initSocket: function(pSocket)
	{
		pSocket.on("usr_add", function(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel)
		{
			Log.d("usr_add id = " + uiId + " usr = " + sUsr + " pwd = " + sPwd + " grp = " + sGrp + 
				" type = " + sType + " name = " + sName + " email = " + sEmail + " tel = " + sTel);

			AuthService.add(sUsr, sPwd, function(pRet)
			{
				if (pRet.ret == "true")
				{
					UsrService.add(sUsr, sGrp, sType, sName, sEmail, sTel, function(pRet)
					{
						pSocket.emit("usr_add_ret", {id:uiId, ret:pRet.ret, msg:pRet.msg});
					});
				}
				else
				{
					Log.d("unable to register user in auth service " + JSON.stringify(pRet));
					pSocket.emit("usr_add_ret", {id:uiId, ret:"false", msg:pRet.msg});
				}
			});
		});

		pSocket.on("usr_set_lusr", function(uiId, sDestUsr, sLUsr, bAdd)
		{
			Log.d("usr_set_lusr id = " + uiId + " sLUsr = " + sLUsr + " add = " + bAdd);

			if (Util.isSet(pSocket.getUsr))
			{
				Log.d("usr_set_lusr usr = (" + pSocket.getUsr().getUserName() + ") id = " + uiId);

				var sUsr = pSocket.getUsr().getUserName();
				UsrService.setLUsr(uiId, sDestUsr, sLUsr, bAdd, function(pData)
				{
					Comm.sendData("usr_set_lusr_ret", null, "", uiId, "", "_usr_srv", sUsr, JSON.stringify(pData), 60);
				}.bind(this));
			}
			else
			{
				Log.d("login required to get lst usr");

				Comm.emit(pSocket, "usr_set_lusr_ret", {ret:false, msg:"login required"});
			}
		});

		pSocket.on("usr_get_lst", function(uiId, sType, sState, uiIdx, uiCnt, bSubscripe, bIdOnly)
		{ 
			if (Util.isSet(pSocket.getUsr))
			{
				Log.d("usr_get_lst usr = (" + pSocket.getUsr().getUserName() + ") id = " + uiId);

				var sUsr = pSocket.getUsr().getUserName();
				UsrService.getLst(uiId, sUsr, sType, sState, uiIdx, uiCnt, bSubscripe, bIdOnly, function(pData)
				{
					pData.usr_type = sType;
					Comm.sendData("usr_info", null, "", uiId, "", "_usr_srv", sUsr, JSON.stringify(pData), 60);
				}.bind(this));
			}
			else
			{
				Log.d("login required to get lst usr");

				Comm.emit(pSocket, "usr_get_lst_ret", "false", {msg:"login required"});
			}
		});
	}
};
