
var Fs = require("fs");

const Log = require("../Cmn/Log");
const Msg = require("../Msg/Api");
const Util = require("../Cmn/Util");
const Usr = require("../Usr");
const ApkService = require("../Service/Apk");
const AuthService = require("../Service/Auth");
const Comm = require("../Comm");
const Iconv = require("iconv-lite");

module.exports = 
{ 
	//****************************************************************************************************
	init: function(pApp, pAppInt)
	{
	},

	//****************************************************************************************************
	initSocket: function(pSocket)
	{
		pSocket.on("apk_install", function(uiId, sSesId, sTo, uiExpireTime, sApk)
		{
			if (Util.isSet(pSocket.getUsr))
			{
				var sUsr = pSocket.getUsr().getUserName();
				ApkService.install(uiId, sSesId, sUsr, sTo, sApk, function(pRet)
				{
					Log.d("ApkService.install ret = " + JSON.stringify(pRet));
					Comm.emit(pSocket, "install_apk_ret", uiId, pRet);
				}.bind(this));
			}
			else
			{
				Comm.emit(pSocket, "install_apk_ret", uiId, {ret:"false", msg:"you shoud login to install apk."});
				Log.d("login required to install apk.");
			}

			Log.d("on install_apk id = (" + uiId + ") ses_id = (" + sSesId + ") to = " + sTo + 
				" expire_time = " + uiExpireTime + " apk = " + sApk);
		});

		pSocket.on("apk_file_start", function(uiId, sSesId, uiLen, uiPktSize, sMsg, sFileData)
		{
			Log.d("start apk file id = " + uiId + " ses_id = " + sSesId + " length = " + uiLen + 
				" packet_size = " + uiPktSize + " msg = " + sMsg + " file_data = " + sFileData);

			if (!(pSocket.getUsr))
			{
				Comm.emit(pSocket, "apk_file_start_ret", -1, {id:uiId, msg:"login required"});
				return;
			}

			if (isNaN(uiLen))
			{
				Comm.emit(pSocket, "apk_file_start_ret", -1, {id:uiId, msg:"invalid file length"});
				return;
			}

			if (isNaN(uiPktSize))		
			{
				Comm.emit(pSocket, "apk_file_start_ret", -1, {id:uiId, msg:"invalid packet size"});
				return;
			}
			
			var sUsr = pSocket.getUsr().getUserName();
			ApkService.startFile(uiId, sUsr, sSesId, uiLen, uiPktSize, sMsg, sFileData, function(pData)
			{
				console.log("mod::Apk data = " + JSON.stringify(pData));
				Msg.send("apk_file_start_ret", "", uiId, sSesId, "__apk_server", sUsr, {usr_data:sFileData});
			});
		});

		pSocket.on("apk_file_bin", function(uiId, sSesId, uiIdx, sData, uiLen)
		{
			Log.d("apk file bin id = (" + uiId + ") session_id = (" + sSesId + ") " + 
				"index = (" + uiIdx + ") data_length = (" + uiLen + ") (" + sData.length + ") )");	

			if (!Util.isSet(pSocket.getUsr))
			{
				Log.d("socket user not have been set");
				Comm.emit(pSocket, "apk_file_bin_ret", -1, {msg:"login require to add apk file"});
				return;
			}

			var sUsr = pSocket.getUsr().getUserName();

			ApkService.fileBin(uiId, sUsr, sSesId, uiIdx, sData, uiLen);
		});

		pSocket.on("apk_get_lst", function(uiId, sSesId)
		{
			if (!Util.isSet(pSocket.getUsr))
			{
				Log.d("socket user not have been set");
				Comm.emit(pSocket, "apk_get_lst_ret", -1, {msg:"login require to get list of apks"});
				return;
			}

			var sUsr = pSocket.getUsr().getUserName();

			ApkService.getLstApk(sUsr, function(pData)
			{
				Msg.send("apk_lst", "", uiId, sSesId, "__apk_server", sUsr, pData, 60);
			});
		});
	}
}
