
const Cfg = require("../Service/Cfg");
const Log = require("../Cmn/Log");
const Util = require("../Cmn/Util");
const Comm = require("../Comm");

const SERVER_NAME = "_cfg1_server";

module.exports = 
{
	init: function(pApp, pAppInt)
	{ 
	},
	
	//****************************************************************************************************
	initSocket: function(pSocket)
	{
		pSocket.on("cfg_start", function(uiId, sSesId, uiExpireTime)
		{
			Comm.auth(pSocket, "cfg_start", "cfg_start_ret", 
			function()
			{
				start(pSocket.getUsr().getUserName(), uiId, sSesId, uiExpireTime);				
			}.bind(this));
		});

		pSocket.on("cfg_cls", function(uiId, sSesId, uiExpireTime)
		{
			Comm.auth(pSocket, "cfg_cls", "cfg_cls_ret", 
			function()
			{
				cls(pSocket.getUsr().getUserName(), uiId, sSesId, uiExpireTime);
			}.bind(this));
		});	

		pSocket.on("cfg_turn", function(uiId, sSesId, uiExpireTime)
		{
			Comm.auth(pSocket, "cfg_turn", "cfg_turn_ret", 
			function()
			{
				turn(pSocket.getUsr().getUserName(), uiId, sSesId, uiExpireTime);
			}.bind(this))
		});
	}
};

//****************************************************************************************************
function start(sUsr, uiId, sSesId, uiExpireTime)
{
	Log.d("Cfg::start usr = (" + sUsr + ") id = (" + uiId + ") ses_id = ("  + sSesId + ") expire_time = (" + uiExpireTime + ")");

	Cfg.start(sSesId, function(pData)
	{
		Comm.sendData("cfg_start_ret", null, null, uiId, "", SERVER_NAME, sUsr, pData, uiExpireTime);
	}.bind(this));
}

//****************************************************************************************************
function cls(sUsr, uiId, sSesId, uiExpireTime)
{
	Log.d("Cfg::cls usr = (" + sUsr + ") id = (" + uiId + ") ses_id = ("  + sSesId + ") expire_time = (" + uiExpireTime + ")");

	Cfg.cls(sSesId, function(pData)
	{
		Comm.sendData("cfg_cls_ret", null, null, uiId, sSesId, SERVER_NAME, sUsr, pData, uiExpireTime);
	}.bind(this))
}

//****************************************************************************************************
function turn(sUsr, uiId, sSesId, uiExpireTime) 
{
	Log.d("Cfg::turn usr = (" + sUsr + ") id = (" + uiId + ") ses_id = ("  + sSesId + ") expire_time = (" + uiExpireTime + ")");
	
	Cfg.turn(function(pData)
	{
		Comm.sendData("cfg_turn_ret", null, null, uiId, sSesId, SERVER_NAME, sUsr, pData, uiExpireTime);
	}.bind(this));
}

