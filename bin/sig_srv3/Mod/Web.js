
var Fs = require("fs");

const Log = require("../Cmn/Log");
const Util = require("../Cmn/Util");
const Config = require("../Config");

module.exports = 
{ 
	//****************************************************************************************************
	init: function(pApp, pAppInt)
	{
		//****************************************************************************************************
		pApp.all("/v1/web/index", function(pReq, pRes)
		{
			Log.d("loading index page");
			Util.redirect(pReq, pRes, Config.getUrlWeb());
		});

		//****************************************************************************************************
		pApp.all("/v1/web/*", function(pReq, pRes)
		{ 
			Log.d("post request url = " + pReq.url);		
			Util.redirect(pReq, pRes, Config.getUrlWeb() + pReq.url.replace("/v1/web", ""));
		}); 
	},

	//****************************************************************************************************
	initSocket: function(pSocket)
	{

	}
}
