
var Fs = require("fs");

const Log = require("../Cmn/Log");
const Util = require("../Cmn/Util");
const Usr = require("../Usr");
const UsrService = require("../Service/Usr");
const DeviceService = require("../Service/Device");
const LocService = require("../Service/Loc");
const AuthService = require("../Service/Auth");
const Comm = require("../Comm");
const Iconv = require("iconv-lite");
const Msg = require('../Msg/Msg');
const Config = require("../Config");

module.exports = 
{ 
	//****************************************************************************************************
	init: function(pApp, pAppInt)
	{ 
		//****************************************************************************************************
		pApp.all("/v1/device/*", function(pReq, pRes)
		{ 
			let sUrl = Config.getUrlDevice() + "/v1/device" + pReq.url.replace("/v1/device", "");
			Log.d("post request url = " + pReq.url + " url = " + sUrl);		
			Util.redirect(pReq, pRes, sUrl);
		}); 

		//****************************************************************************************************
		pApp.all("/v1/state/*", function(pReq, pRes)
		{ 
			let sUrl = Config.getUrlDevice() + "/v1/state" + pReq.url.replace("/v1/state", "");
			Log.d("post request url = " + pReq.url + " url = " + sUrl);		
			Util.redirect(pReq, pRes, sUrl);
		}); 
	},

	//****************************************************************************************************
	initSocket: function(pSocket)
	{
		pSocket.on("http", function(uiId, sSrv, sPath, sData)
		{
			if (Util.isSet(pSocket.getUsr))
			{
				var sUsr = pSocket.getUsr().getUserName();

				switch (sSrv)
				{
				case 'usr':
					UsrService.post(sPath, sData, function(pRet)
					{
						var pMsg = new Msg.Msg(); 
						pMsg.init("http", "", uiId, "", UsrService.getName(), pRet, 30);

						Log.d("http ret = " + JSON.stringify(pMsg));
						Comm.emitObj(pSocket, "http", pMsg);
					}.bind(this));
					break;

				case 'device': 
					DeviceService.post(sPath, sData, function(pRet)
					{
						var pMsg = new Msg.Msg(); 
						pMsg.init("http", "", uiId, "", DeviceService.getName(), pRet, 30);

						Log.d("http ret = " + JSON.stringify(pMsg));
						Comm.emitObj(pSocket, "http", pMsg);
					}.bind(this));
					break; 
				case 'loc': 
					LocService.post(sPath, sData, function(pRet)
					{
						var pMsg = new Msg.Msg(); 
						pMsg.init("http", "", uiId, "", LocService.getName(), pRet, 30);

						Log.d("http ret = " + JSON.stringify(pMsg));
						Comm.emitObj(pSocket, "http", pMsg);
					}.bind(this));
					break;
				default:
					Log.d("invalid service name " + sSrv);
				}

			}
			else
			{
				Comm.emit(pSocket, "http", uiId, {ret:"false", msg:"you shoud login to send a http request."});
				Log.d("login required to send http request.");
			}

			Log.d("on http request id = (" + uiId + ") srv = " + sSrv + " path = (" + sPath + ") data = (" + sData + ")");
		});
	}
}
