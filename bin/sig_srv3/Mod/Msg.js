
var Fs = require("fs");

const Config = require("../Config");
const Log = require("../Cmn/Log");
const Msg = require("../Msg/Api");
const MsgMeta = require("../Msg/Meta");
const Util = require("../Cmn/Util");
const Usr = require("../Usr");
const MsgService = require("../Service/Msg");
const UsrService = require("../Service/Usr");
const AuthService = require("../Service/Auth");
const Comm = require("../Comm");
const Iconv = require("iconv-lite");

module.exports = 
{ 
	//****************************************************************************************************
	init: function(pApp, pAppInt)
	{
		//****************************************************************************************************
		pAppInt.post("/v1/send/msg", function(pReq, pRes)
		{ 
			Comm.send(pReq, pRes, MsgMeta.getMsg(), MsgMeta.getSendMsgRet()); 
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/sig", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getSig(), MsgMeta.getSendSigRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/req", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getReq(), MsgMeta.getSendReqRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/res", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getRes(), MsgMeta.getSendResRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/cmd", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getCmd(), MsgMeta.getSendCmdRet());
		});
		
		//****************************************************************************************************
		pAppInt.post("/v1/send/cmd_res", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getCmdRes(), MsgMeta.getSendCmdResRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/get", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getGet(), MsgMeta.getSendGetRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/get_res", function(pReq, pRes)
		{
			Comm.send(pReq, pRes, MsgMeta.getGetRes(), MsgMeta.getSendGetResRet());
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/start_file", function(pReq, pRes)
		{
			var sBody = [];
			pReq.on("data", function(sBuf)
			{
				sBody.push(sBuf);
			}).on("end", function()
			{
				sBody = Buffer.concat(sBody).toString();
				try
				{
					Log.d("body = (" + sBody + ")");

					var pData = JSON.parse(sBody);  
					var sSysId = pData.sys_id;
					var iFileId = pData.id;
					var sSesId = pData.ses_id;
					var sUsr = pData.usr;
					var sTo = pData.to;
					var uiLen = pData.len;
					var uiPktSize = pData.pkt_size;
					var pMsg = pData.msg; 
					var pUsrData = pData.usr_data;
					var bTigerRet = pData.tiger_ret;

					Log.d("file_id = " + iFileId + " ses_id = " + sSesId + " usr = " + sUsr + " to = " + sTo + 
						" len = " + uiLen + " pkt_size = " + uiPktSize + " msg = " + pMsg + " tiger_ret = " + bTigerRet);

					Msg.send(MsgMeta.getStartFile(), sSysId, iFileId, sSesId, sUsr, sTo, 
							{id:iFileId, len:uiLen, pkt_size:uiPktSize, msg:pMsg});

					if (!sUsr.startsWith("__") && bTigerRet != false && bTigerRet != "false")
					{	
						Msg.send(MsgMeta.getSendStartFileRet(), "", iFileId, sSesId, "__sig_srv", sUsr, 
								{id:iFileId, usr_data:pUsrData});
					}

					Util.writeJsonRes(pRes, {state:"true"});
				}
				catch (pExe)
				{
					Log.e("start file exception [" + pExe + "]");
					Util.writeJsonResErr(pRes, {state:"false", msg:"start file exception [" + pExe + "]"});
				}
			});
		});

		//****************************************************************************************************
		pAppInt.post("/v1/send/file_bin/:id/:ses_id/:usr/:to/:idx/:expire_time/:tiger_ret", function(pReq, pRes)
		{
			var sBody = [];
			pReq.on("data", function(sData)
			{
				sBody.push(sData);
			}).on("end", function()
			{
				sBody = Iconv.decode(Buffer.concat(sBody), "ISO-8859-1");
				Log.d("body len = " + sBody.length);
					
				try
				{
					var sSysId = pReq.params.sys_id;
					var uiId = pReq.params.id; 
					var sSesId = pReq.params.ses_id; 
					var sUsr = pReq.params.usr; 
					var sTo = pReq.params.to; 
					var uiIdx = pReq.params.idx; 
					var sData = sBody; 
					var uiLen = sBody.length;
					var uiExpireTime = pReq.params.expire_time;
					var bTigerRet = pReq.params.tiger_ret; 

					Log.d("sys_id = " + sSysId + " file_id = " + uiId + " ses_id = " + sSesId + 
						" usr = " + sUsr + " to = " + sTo + " idx = " + uiIdx + " len = " + uiLen + 
						" data_len = " + sData.length + " body_length = " + sBody.length + 
						" expire_time = " + uiExpireTime + " tiger_ret = " + bTigerRet);

					Msg.send(MsgMeta.getFileBin(), sSysId, uiId, sSesId, sUsr, sTo, 
						{id:uiId, idx:uiIdx, data:sData, len:uiLen}, uiExpireTime);
				
					Util.writeJsonRes(pRes, {state:"true"});
				}
				catch (pEx)
				{
					Log.e("exception [" + pExe + "]");
					Util.writeJsonResErr(pRes, {state:"false", msg:"send/file_bin::exception [" + pExe  + "]"});
				}
			});
		}); 
	},

	//****************************************************************************************************
	initSocket: function(pSocket)
	{
		pSocket.on(MsgMeta.getSendMsg(), function(uiId, sSesId, sTo, sMsg)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendMsg(sUsr, sTo, sSesId, uiId, sMsg);	
				Comm.emit(pSocket, MsgMeta.getSendMsgRet(), uiId, {ret:"false", msg:"success in sig_srv"});
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendMsgRet(), uiId, {ret:"false", msg:"destination socket not logged"});
				Log.d("unable to send message. socket not autorized.");
			}

			Log.d("on message id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + "\n");	
		});

		pSocket.on(MsgMeta.getSendSig(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendSig(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendSigRet(), uiId, {ret:"false", msg:"unable to send signal. socket not autorized"});
				Log.d("unable to send message. socket not autorized.");
			}

			Log.d("on signal id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") msg = " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendReq(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendReq(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendReqRet(), uiId, {ret:"false", msg:"unable to send signal. socket not autorized."});
				Log.d("unable to send message. socket not autorized.");
			}

			Log.d("on send_req id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendRes(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendRes(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendResRet(), uiId, {ret:"false", msg:"unable to send signal. socket not autorized."});
				Log.d("unable to send message. socket not autorized.");
			}

			Log.d("on send_res id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		}); 

		pSocket.on(MsgMeta.getSendCmd(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendCmd(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendCmdRet(), uiId, {ret:"false", msg:"unable to send signal. socket not autorized."});
				Log.d("unable to send command. socket not autorized.");
			}

			Log.d("on send_cmd id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendCmdRes(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendCmdRes(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendCmdResRet(), uiId, {ret:"false", 
						msg:"unable to send send cmd ret. socket not autorized."});
				Log.d("unable to send cmd ret. socket not authorized.");
			}

			Log.d("on send_cmd_res id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendGet(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)	
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendGet(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendGetRet(), uiId, {ret:"false", msg:"socket not authorized"});
				Log.d("unable to send get. socket not authorized");
			}
		
			Log.d("on send_get id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendGetRes(), function(uiId, sSesId, sTo, sMsg, uiExpireTime)
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				MsgService.sendGetRes(sUsr, sTo, sSesId, uiId, sMsg, uiExpireTime);
			}
			else
			{
				Comm.emit(pSocket, MsgMeta.getSendGetResRet(), uiId, {ret:"false", msg:"socket not authorized"});
				Log.d("unable to send get_res. socket not authorized");
			}	

			Log.d("on send_get_res id = (" + uiId + ") ses_id = (" + sSesId + ") to = (" + sTo + ") " + sMsg + 
				" expire_time = " + uiExpireTime + "\n");	
		});

		pSocket.on(MsgMeta.getSendStartFile(), function(uiId, sTo, sSesId, uiLen, uiPktSize, sMsg, sFileData)
		{
			Log.d("start sending file id = " + uiId + " to = " + sTo + " ses_id = " + sSesId + " length = " + uiLen + 
					" packet_size = " + uiPktSize + " msg = " + sMsg + " data = " + sFileData);

			if (!hasAcs(pSocket))
			{
				Comm.emit(pSocket, MsgMeta.getSendStartFileRet(), {id:uiId, ret:"false", msg:"login required"});
				return;
			}

			if (isNaN(uiLen))
			{
				Comm.emit(pSocket, MsgMeta.getSendStartFileRet(), {id:uiId, ret:"false", msg:"invalid file length"});
				return;
			}

			if (isNaN(uiPktSize))		
			{
				Comm.emit(pSocket, MsgMeta.getSendStartFileRet(), {id:uiId, ret:"false", msg:"invalid packet size"});
				return;
			}
			
			var sUsr = pSocket.getUsr().getUserName();
			MsgService.startSendFile(uiId, sUsr, sTo, sSesId, uiLen, uiPktSize, sMsg, sFileData);
		});

		pSocket.on(MsgMeta.getSendFileBin(), function(sTo, uiId, sSesId, uiIdx, sData, uiLen, uiExpireTime)
		{
			Log.d("send file bin id = (" + uiId + ") session_id = (" + sSesId + ") to = (" + sTo + ") " + 
				"index = (" + uiIdx + ") data_length = (" + uiLen + ") (" + sData.length + ") expire_time = (" + uiExpireTime + ")");	
			if (!hasAcs(pSocket))
			{
				Log.d("socket user not have been set");
				Comm.emit(pSocket, MsgMeta.getErr(), -1, {msg:"login require to send file"});
				return;
			}

			var sUsr = pSocket.getUsr().getUserName();

			MsgService.sendFileBin(sUsr, sTo, sSesId, uiId, uiIdx, sData, uiLen, uiExpireTime, false);
		});

		pSocket.on(MsgMeta.getAddSes(), function(sSesId)
		{
			if (hasAcs(pSocket))
			{
				Log.d("session add " + sSesId + " to " + pSocket.getUsr().getUserName());	
				Msg.addSes(pSocket.getUsr().getUserName(), sSesId);
			}
			else
			{
				Log.d("unable to add session. login required.");
				Comm.emit(pSocket, MsgMeta.getErr(), {msg:"login require to add session"});
			}
		});

		pSocket.on(MsgMeta.getDone(), function(sId, sState, sNote)
		{
			if (!hasAcs(pSocket))
			{
				Log.d("socket user not have been set to done message");
				Comm.emit(pSocket, MsgMeta.getErr(), -1, {msg:"login require to done message"});
				return;
			}

			var sUsr = pSocket.getUsr().getUserName();
			MsgService.done(sUsr, sId, sState, sNote);
		});

		pSocket.on(MsgMeta.getLogin(), function(sUsr, sPwd, pDevice)
		{
			Log.d("user login user_name = (" + sUsr + ") password = (" + sPwd + ") device = " + JSON.stringify(pDevice));

			try
			{
				AuthService.login(sUsr, sPwd, function(bRet, sMsg)
				{
					if (bRet)
					{
						UsrService.get(1, sUsr, sUsr, function(pUserData)
						{
							Log.d("UsrService.get use data = " + JSON.stringify(pUserData));
							Comm.emit(pSocket, MsgMeta.getLoginRet(), 
									{ret:"true", type:pUserData.type, grp:pUserData.grp, name:pUserData.name});

							UsrService.connect(sUsr, pUserData.type, pDevice);
							UsrService.setDevice(sUsr, pDevice);
							MsgService.connectUsr(sUsr);
							Msg.connectUsr(sUsr, pSocket);
						
							var pUsr = new Usr.Usr();
							pUsr.setUserName(sUsr);
							pUsr.setGrp(pUserData.grp);
							pUsr.setType(pUserData.type);
							pUsr.setDevice(pDevice);
							pSocket.p_Usr = pUsr; 
							pSocket.getUsr = function() { return this.p_Usr; };
							
						}.bind(this));

					}
					else
						Comm.emit(pSocket, MsgMeta.getLoginRet(), {ret:"false", msg:"invalid user_name or password"});
				});

			}
			catch (pExe)
			{
				Log.d("login failed exception [" + pExe + "]");
			}
		});

		pSocket.on("ping_msg", function(uiId)
		{
			var sUsr = Util.isSet(pSocket.getUsr) ? pSocket.getUsr().getUserName() : "null";
			Log.d("mod::Msg ping " + sUsr);

			if (hasAcs(pSocket))
			{
				Log.d("mod::Msg ping ok usr  = " + sUsr);

				if (Config.isReconnectWithPing())
				{
					var pUsr = pSocket.getUsr();
					UsrService.connect(sUsr, pUsr.getType(), pUsr.getDevice());
					MsgService.connectUsr(sUsr);
					Msg.connectUsr(sUsr, pSocket);
				}

				Comm.emitObj(pSocket, "ping_ret", {i_Id:uiId, p_Msg:{ret:"true"}});
			}
			else
			{
				Log.d("mod::Msg ping ok usr  = " + sUsr);
				Comm.emitObj(pSocket, "ping_ret", {i_Id:uiId, p_Msg:{ret:"false", msg:"login required"}});
			}
		}.bind(this));

		pSocket.on(MsgMeta.getLogoff(), function()
		{
			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				Log.d("Mod::Msg log off usr = (" + sUsr + ")");
				Msg.rmUsr(sUsr);
				UsrService.logout(sUsr);
				MsgService.disconnectUsr(sUsr);
			}

			pSocket.disconnect();
		});

		pSocket.on(MsgMeta.getDisconnect(), function()
		{
			Log.d("disconnect socket");

			if (hasAcs(pSocket))
			{
				var sUsr = pSocket.getUsr().getUserName();
				Log.d("disconnect user (" + sUsr + ")");
				Msg.disconnectUsr(sUsr);
				UsrService.disconnect(sUsr);	
				MsgService.disconnectUsr(sUsr);
			}
		}); 
	}
}

//****************************************************************************************************
function hasAcs(pSocket)
{
	return Util.isSet(pSocket.getUsr) && Msg.isConnected(pSocket);
}
