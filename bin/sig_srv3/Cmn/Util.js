
const Log = require("./Log");
const Http = require("http");
const Url = require("url");
const Iconv = require("iconv-lite");

module.exports = 
{
	//****************************************************************************************************
	isEmpty: function(sStr)
	{
		return sStr == null || sStr == undefined || sStr == "";
	},

	//****************************************************************************************************
	isSet: function(pVar)
	{
		return typeof pVar != 'undefined' && pVar != null && pVar != "";
	},

	//****************************************************************************************************
	http: function(sUrl, sMethod, pBuf, sContentType, bJson, fnRet)
	{
		var pUrl = Url.parse(sUrl);
		var sHost = pUrl.hostname;	
		var iPort = pUrl.port;
		var sPath = pUrl.pathname; 
		
		if (bJson == null || typeof bJson == undefined)
			bJson == true;
		
		var pOptions = 
		{
			host: sHost,
			path: sPath,
			port: '' + iPort + '',
			headers: 
				{
					'content-type': this.isSet(sContentType) ? sContentType : "", 
					'content-length':'' + (pBuf == null ? 0 : pBuf.length) + ''
				},
			method:sMethod
		};

		process.stdout.write("option = " + JSON.stringify(pOptions));

		var pCallback = function(pRes) 
		{
			var aOut = [];
			pRes.on('data', function (sChunk) 
			{
				aOut.push(sChunk);
			}.bind(this));

			pRes.on('end', function () 
			{
				Log.d("headers (" + JSON.stringify(pRes.headers) + ")");
				var oMHeader = this.toLowerKey(pRes.headers);
				var sContentType = oMHeader["content-type"]; 
				Log.d("content type = " + sContentType);
				var pBuf = Buffer.concat(aOut);

				if (sContentType.toUpperCase().startsWith("application/json".toUpperCase()))
				{
					Log.d("converting to json url = " + sUrl + " response = (" + pBuf.toString() + ")");
					//process.stdout.write("cmn::Utils::http " + pBuf.toString());

					if (bJson)
						fnRet(JSON.parse(pBuf.toString()), sContentType);
					else
						fnRet(pBuf.toString(), sContentType);
				}
				else
					fnRet(pBuf, sContentType);	
			}.bind(this));
		}.bind(this);
	
		try
		{
			var pReq = Http.request(pOptions, pCallback);
			if (pBuf != null)
				pReq.write(pBuf);
			pReq.end();
		}
		catch (pExe)
		{
			Log.e("cmn::Util::http exception [" + pExe + "]");
			throw "cmn::Util::http exception [" + pExe + "]";
		}
	},

	//****************************************************************************************************
	post: function(sUrl, pBuf, fnRet)
	{
		process.stdout.write("cmn::Util::post url = (" + sUrl + ") data = (" + JSON.stringify(pBuf) + ")");
		this.http(sUrl, "POST", JSON.stringify(pBuf), "application/json", true, fnRet);
	},

	//****************************************************************************************************
	postBin: function(sUrl, pBuf, fnRet)
	{
		this.http(sUrl, "POST", pBuf, "application/binary", true, fnRet);
	},

	//****************************************************************************************************
	get: function(sUrl, pBuf, fnRet)
	{
		process.stdout.write("cmn::Util::get url = (" + sUrl + ") data = (" + JSON.stringify(pBuf) + ")");
		this.http(sUrl, "GET", JSON.stringify(pBuf), "application/json", true, fnRet);
	},

	//****************************************************************************************************
	redirect: function(pReq, pRes, sUrl)
	{
		var sContentType = pReq.get('content-type');
		var sMethod = pReq.method;
		var sOptParam = null;

		for (var sVarName in pReq.query)
		{
			if (pReq.query.hasOwnProperty(sVarName))
			{
				if (sOptParam != null)	
					sOptParam = "?";

				sOptParam += sVarName + "=" + pReq.query[sVarName]
			}
		}
		
		Log.d("opt params = " + sOptParam);
		Log.d("content type = " + sContentType);
		Log.d("method = " + sMethod);		
		
		var sBody = [];
		pReq.on("data", function(sData)
		{
			sBody.push(sData);
		}.bind(this)).on("end", function()
		{
			var pBuf = Buffer.concat(sBody);
			this.http(sUrl, sMethod, pBuf, sContentType, false, function(pResBuf, sContentType)
			{
				Log.d("redirect responst content type = " + sContentType + " length = " + pResBuf.length);
				pRes.writeHead(200, {"content-type": sContentType});
				pRes.end(pResBuf);
			});	
		}.bind(this));	
	},

	//****************************************************************************************************
	toLowerKey: function(pMVal)
	{
		var pMRet = {};
		for (var sKey in pMVal)
		{
			if (pMVal.hasOwnProperty(sKey))
			{
				pMRet[sKey.toLowerCase()] = pMVal[sKey]; 	
			}
		}

		return pMRet;
	},
		
	//****************************************************************************************************
	writeJsonRes: function(pRes, oRes)
	{
		pRes.setHeader('Content-Type', 'application/json');
		pRes.status(200).end(JSON.stringify(oRes));
		Log.d("writeJsonRes(" + JSON.stringify(oRes) + ")");
	},
	
	//****************************************************************************************************
	writeJsonResErr: function(pRes, oRes)
	{
		pRes.setHeader('Content-Type', 'application/json');
		pRes.status(400).end(JSON.stringify(oRes));
	},

	//****************************************************************************************************
	isArr: function(pVar)
	{
		return Object.prototype.toString.call(pVar) === '[object Array]';
	}
};

