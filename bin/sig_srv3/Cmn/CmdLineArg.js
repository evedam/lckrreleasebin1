
const Util = require("./Util"); 

module.exports = 
{
	//****************************************************************************************************
	init: function()
	{
		process.argv.forEach((sVal, iIdx) => 
		{
			if (sVal.indexOf("=") > -1)
			{
				var asTok = sVal.split("=");
				if (asTok.length = 2)
				{
					var sKey = asTok[0];
					var sValue = asTok[1];

					if (sKey.length != 0 && sValue.length != 0)
						this.p_Arg[asTok[0]] = asTok[1];
					else
						process.stdout.write("incomplete argument (" + sVal + ")\n");
				}
				else
					process.stdout.write("incomplete argument (" + sVal + ")\n");
			}
			else
				this.p_Arg[sVal] = " ";
		});
	},

	//****************************************************************************************************
	isSet: function(sId)
	{
		return Util.isSet(this.p_Arg[sId]);	
	},

	//****************************************************************************************************
	isBool: function(sId)
	{
		if (!this.isSet(sId))
			return false;

		var sVal = this.get(sId); 
		return sVal == null || sVal == "" || sVal == "true" || sVal == "false";
	},

	//****************************************************************************************************
	hasVal: function(sId)
	{
		return !Util.isEmpty(this.p_Arg[sId]);
	},

	//****************************************************************************************************
	get: function(sId)
	{
		if (!this.isSet(sId))
			return null;
	
		return this.p_Arg[sId];
	},

	//****************************************************************************************************
	getBool: function(sId)
	{
		if (!this.isSet(sId))
			return false;

		var sVal = this.get(sId); 
		if (sVal != null)
			sVal = sVal.trim();

		return sVal == null || sVal == "" || sVal == "true";
	},

	p_Arg: {}
};
