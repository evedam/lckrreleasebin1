
const Util = require("../Cmn/Util");
const Config = require("../Config");
const Log = require("../Cmn/Log");

module.exports =
{
	getName: function()
	{
		return "__usr_srv";
	},

	//****************************************************************************************************
	post: function(sPath, pData, fncRet)	
	{
		Log.d("mod::usr::post path = " + sPath + " data = " + JSON.stringify(pData));
		var pMsg = pData; 

		Util.post(getUrlFromPath(sPath), pMsg, fncRet);
	},

	//****************************************************************************************************
	add: function(sUsr, sGrp, sType, sName, sEmail, sTel, fncRet)
	{
		var pMsg = {grp:sGrp, type:sType, name:sName, email:sEmail, tel:sTel};
		Util.post(getUrlAdd(sUsr), pMsg, fncRet);
	},

	//****************************************************************************************************
	setLUsr: function(uiId, sUsr, sLUsr, bAdd, fncRet)
	{
		var pLUsr = JSON.parse("{\"lst\":" + sLUsr + "}");
		
		console.log("src::setLUsr id = " + uiId + " usr = " + sUsr + " sLUsr = " + sLUsr.lst + " add = " + bAdd);
		var pMsg = {id:uiId, lusr:pLUsr.lst, add:bAdd};	
		Util.post(getUrlSetLUsr(sUsr), pMsg, fncRet);
	},

	//****************************************************************************************************
	connect: function(sUsr, sType, pDevice)	
	{
		var pMsg = {type:sType, state:"connected", device:pDevice};
		post(getUrlConnect(sUsr), pMsg);
	},

	//****************************************************************************************************
	disconnect: function(sUsr)	
	{
		post(getUrlDisconnect(sUsr), {});
	}, 

	//****************************************************************************************************
	logout: function(sUsr)	
	{
		post(getUrlLogout(sUsr), {});
	}, 

	//****************************************************************************************************
	getLst: function(uiId, sUsr, sType, sState, uiIdx, uiCnt, bSubscripe, bIdOnly, fncRet)	
	{
		var pMsg = {id:uiId, type:sType, state:sState, idx:uiIdx, cnt:uiCnt, subscripe:bSubscripe, id_only:bIdOnly};
		Log.d("getLst msg = " + JSON.stringify(pMsg));	

		Util.post(getUrlGetLst(sUsr), pMsg, fncRet);
	},

	//****************************************************************************************************
	get: function(uiId, sUsr, sFilterUsr, fncRet)
	{
		var pMsg = {id:uiId, usr:sUsr};
		Log.d("get msg = " + JSON.stringify(pMsg));	

		Util.post(getUrlGetLst(sUsr), pMsg, function(pLstUsr)
		{
			Log.d("Usr::get response " + JSON.stringify(pLstUsr));
			fncRet(Util.isSet(pLstUsr.lst_usr) ? pLstUsr.lst_usr[sFilterUsr] : null);
		}.bind(this));
	},

	//****************************************************************************************************
	setDevice: function(sUsr, pDevice)
	{
		Log.d("Service::Usr::setDevice device = " + pDevice);
		post(getUrlSetDevice(sUsr), pDevice);
	}
};


//****************************************************************************************************
function post(sUrl, pMsg)
{
	Util.post(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg post response data = " + JSON.stringify(pData));
	});
}

//****************************************************************************************************
function getUrl()
{
	return Config.getUrlUsr(); 
}

//****************************************************************************************************
function getUrlFromPath(sPath)
{
	return getUrl() + sPath;
}

//****************************************************************************************************
function getUrlAdd(sUsr)
{
	return getUrl() + "/v1/add/" + sUsr;
}

//****************************************************************************************************
function getUrlSetLUsr(sUsr)
{
	return getUrl() + "/v1/set_lusr/" + sUsr;
}

//****************************************************************************************************
function getUrlConnect(sUsr)
{
	return getUrl() + "/v1/connect/" + sUsr;
}

//****************************************************************************************************
function getUrlDisconnect(sUsr)
{
	return getUrl() + "/v1/disconnect/" + sUsr; 
}

//****************************************************************************************************
function getUrlLogout(sUsr)
{ 
	return getUrl() + "/v1/logout/" + sUsr; 
}

//****************************************************************************************************
function getUrlGetLst(sUsr)
{ 
	return getUrl() + "/v1/get_lst/" + sUsr; 
}

//****************************************************************************************************
function getUrlSetDevice(sUsr)
{
	return getUrl() + "/v1/set_device/" + sUsr; 
}

