
const Util = require("../Cmn/Util");
const Config = require("../Config");
const Log = require("../Cmn/Log");

module.exports =
{
	//****************************************************************************************************
	add: function(sUsr, sPwd, fncRet)
	{
		var pMsg = {usr:sUsr, pwd:sPwd};
		Util.post(getUrlAdd(sUsr), pMsg, fncRet);
	},

	//****************************************************************************************************
	login: function(sUsr, sPwd, fncRet)
	{
		if (!(/^[A-Za-z0-9_]+$/.test(sUsr)))
			fncRet(false, "invalid user name");

		if (!(/^[A-Za-z0-9_]+$/.test(sPwd) && sPwd != null && sPwd != undefined))
			fncRet(false, "invalid password");	

		var pMsg = {usr:sUsr, pwd:sPwd};
		Util.post(getUrlLogin(sUsr), pMsg, function(pRet)
		{
			if (pRet.ret == "true")
				fncRet(true, "");
			else
				fncRet(false, pRet.msg);
		});
	}
};


//****************************************************************************************************
function post(sUrl, pMsg)
{
	Util.post(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg add_data responst data = " + pData);
	});
}

//****************************************************************************************************
function getUrl()
{
	return Config.getUrlAuth(); 
}

//****************************************************************************************************
function getUrlAdd()
{
	return getUrl() + "/v1/usr/register";
}

//****************************************************************************************************
function getUrlLogin()
{
	return getUrl() + "/v1/usr/login";
}
