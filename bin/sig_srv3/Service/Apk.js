
const Util = require("../Cmn/Util");
const Log = require("../Cmn/Log");
const Config = require("../Config");
const Iconv = require("iconv-lite");

module.exports =
{
	//****************************************************************************************************
	install: function(uiId, sSesId, sUsr, sTo, sApk, fncRet)	
	{
		var pMsg = {id:uiId, ses_id:sSesId, usr:sUsr, to:sTo, apk:sApk};

		Util.post(getUrlInstall(), pMsg, fncRet);
	},

	//****************************************************************************************************
	startFile: function(uiId, sUsr, sSesId, uiLen, uiPktSize, pMsg, pUsrData, fncRet)
	{
		var pMsg = {id:uiId, ses_id:sSesId, len:uiLen, pkt_size:uiPktSize, usr:sUsr, usr_data:pUsrData, msg:pMsg};

		Util.post(getUrlStartFile(), pMsg, fncRet);
	},

	//****************************************************************************************************
	fileBin: function(uiId, sUsr, sSesId, uiIdx, sData, uiLen)
	{
		if (!Util.isSet(sSesId))
			sSesId = "null";
	
		var sUrl = getUrlSendBin() + "/" + uiId + "/" + sSesId + "/" + sUsr + "/" + uiIdx;
		Log.d("file_bin url = (" + sUrl + ")");
	
		var pBuf = Iconv.encode(sData, "ISO-8859-1"); 
		postBin(sUrl, pBuf);
	},

	//****************************************************************************************************
	connectUsr: function(sUsr)
	{
		var pMsg = {usr:sUsr, state:"connected"};
		post(getUrlUsrState(), pMsg);
	},

	//****************************************************************************************************
	disconnectUsr: function(sUsr)
	{
		var pMsg = {usr:sUsr, state:"disconnected"};
		post(getUrlUsrState(), pMsg);
	},
	
	//****************************************************************************************************
	getLstApk: function(sUsr, fncRet)
	{
		var pMsg = {usr:sUsr};
		Util.post(getUrlGetLstApk(), pMsg, function(pRet)
		{
			fncRet(pRet.data);
		});
	}
};

//****************************************************************************************************
function post(sUrl, pMsg)
{
	Util.post(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg post responst data = " + JSON.stringify(pData));
	});
}

//****************************************************************************************************
function postBin(sUrl, pMsg)
{
	Util.postBin(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg post responst data = " + JSON.stringify(pData));
	});
}

//****************************************************************************************************
function getUrl()
{
	return Config.getUrlApk(); 
}

//****************************************************************************************************
function getUrlInstall()
{ 
	return getUrl() + "/v1/apk/install"; 
}

//****************************************************************************************************
function getUrlStartFile()
{
	return getUrl() + "/v1/file/add/start";
}

//****************************************************************************************************
function getUrlSendBin()
{
	return getUrl() + "/v1/file/add/bin";
}

//****************************************************************************************************
function getUrlUsrState()
{
	return getUrl() + "/v1/usr/state";
}

//****************************************************************************************************
function getUrlGetLstApk()
{
	return getUrl() + "/v1/apk/get/lst";
}
