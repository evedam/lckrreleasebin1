
const Util = require("../Cmn/Util");
const Log = require("../Cmn/Log");
const Config = require("../Config");
const Iconv = require("iconv-lite");

module.exports =
{
	//****************************************************************************************************
	sendMsg: function(sUsr, sTo, sSesId, uiId, pMsg)
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg};

		post(getUrlMsg(), pMsg);
	},

	//****************************************************************************************************
	sendSig: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlSig(), pMsg);
	},

	//****************************************************************************************************
	sendReq: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlReq(), pMsg);
	}, 

	//****************************************************************************************************
	sendRes: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlRes(), pMsg);
	}, 

	//****************************************************************************************************
	sendCmd: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlCmd(), pMsg);
	},

	//****************************************************************************************************
	sendCmdRes: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlCmdRes(), pMsg);
	},

	//****************************************************************************************************
	sendGet: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};

		post(getUrlGet(), pMsg);
	},

	//****************************************************************************************************
	sendGetRes: function(sUsr, sTo, sSesId, uiId, pMsg, uiExpireTime)	
	{ 
		var pMsg = {usr:sUsr, to:sTo, ses_id:sSesId, id:uiId, msg:pMsg, expire_time:uiExpireTime};
			
		post(getUrlGetRes(), pMsg);
	},

	//****************************************************************************************************
	startSendFile: function(uiId, sUsr, sTo, sSesId, uiLen, uiPktSize, pMsg, pUsrData)
	{
		var pMsg = {id:uiId, to:sTo, ses_id:sSesId, len:uiLen, pkt_size:uiPktSize, usr:sUsr, usr_data:pUsrData, msg:pMsg};

		post(getUrlStartFile(), pMsg);
	},

	//****************************************************************************************************
	sendFileBin: function(sUsr, sTo, sSesId, uiId, uiIdx, sData, uiLen, uiExpireTime, bTigerRet)
	{
		var sUrl = getUrlSendBin() + "/" + uiId + "/" + sSesId + "/" + sUsr + "/" + sTo + "/" + uiIdx + "/" + uiExpireTime + 
				"/" + (bTigerRet ? "true" : "false");

		Log.d("file_bin url = (" + sUrl + ")");
	
		var pBuf = Iconv.encode(sData, "ISO-8859-1"); 
		postBin(sUrl, pBuf);
	},

	//****************************************************************************************************
	done: function(sUsr, sId, sState, sNote)
	{
		var pMsg = {usr:sUsr, id:sId, state:sState, note:sNote};
		post(getUrlDone(), pMsg);
	},
	
	//****************************************************************************************************
	connectUsr: function(sUsr)
	{
		var pMsg = {usr:sUsr, state:"connected"};
		post(getUrlUsrState(), pMsg);
	},

	//****************************************************************************************************
	disconnectUsr: function(sUsr)
	{
		var pMsg = {usr:sUsr, state:"disconnected"};
		post(getUrlUsrState(), pMsg);
	}
};

//****************************************************************************************************
function post(sUrl, pMsg)
{
	Util.post(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg http post data responst data = " + JSON.stringify(pData));
	});
}

//****************************************************************************************************
function postBin(sUrl, pMsg)
{
	Util.postBin(sUrl, pMsg, function(pData)
	{
		Log.d("Service::Msg http post data responst data = " + JSON.stringify(pData));
	});
}

//****************************************************************************************************
function getUrl()
{
	return Config.getUrlMsg(); 
}

//****************************************************************************************************
function getUrlMsg()
{ 
	return getUrl() + "/v1/msg/send";
}

//****************************************************************************************************
function getUrlSig()
{
	return getUrl() + "/v1/sig/send";
}

//****************************************************************************************************
function getUrlReq()
{
	return getUrl() + "/v1/req/send"; 
}

//****************************************************************************************************
function getUrlRes()
{ 
	return getUrl() + "/v1/res/send"; 
}

//****************************************************************************************************
function getUrlCmd()
{ 
	return getUrl() + "/v1/cmd/send"; 
}

//****************************************************************************************************
function getUrlCmdRes()
{ 
	return getUrl() + "/v1/cmd/res/send"; 
}

//****************************************************************************************************
function getUrlGet()
{
	return getUrl() + "/v1/get/send";
}

//****************************************************************************************************
function getUrlGetRes()
{
	return getUrl() + "/v1/get/res/send";
}

//****************************************************************************************************
function getUrlStartFile()
{
	return getUrl() + "/v1/file/start";
}

//****************************************************************************************************
function getUrlSendBin()
{
	return getUrl() + "/v1/file/send_bin";
}

//****************************************************************************************************
function getUrlDone()
{
	return getUrl() + "/v1/done";
}

//****************************************************************************************************
function getUrlUsrState()
{
	return getUrl() + "/v1/usr/state";
}
