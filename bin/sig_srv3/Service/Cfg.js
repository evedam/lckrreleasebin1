
const Util = require("../Cmn/Util");
const Config = require("../Config");

module.exports = 
{ 
	start: function(sId, fncRet)
	{
		post(getUrlStart() + "/" + sId, "", fncRet);
	},
	
	cls: function(sId, fncRet)
	{
		post(getUrlCls() + "/" + sId, "", fncRet);
	},

	turn: function(fncRet)
	{
		get(getUrlTurn(), "", fncRet);
	},

	msg: function(sId, sMsg, fnRet)
	{
		post(getUrlMsg() + "/" + sId, sMsg, fnRet);
	}
};

//****************************************************************************************************
function post(sUrl, pMsg, fncRet)
{
	Util.post(sUrl, pMsg, fncRet);
}

//****************************************************************************************************
function get(sUrl, pMsg, fncRet)
{
	Util.get(sUrl, pMsg, fncRet);
}

//****************************************************************************************************
function getUrl()
{
	return Config.getUrlCfg(); 
}

//****************************************************************************************************
function getUrlStart()
{
	return getUrl() + "/v1/start";
}

//****************************************************************************************************
function getUrlCls()
{
	return getUrl() + "/v1/cls";
}

//****************************************************************************************************
function getUrlTurn()
{
	return getUrl() + "/v1/turn"
}

//****************************************************************************************************
function getUrlMsg()
{
	return getUrl() + "/v1/msg";
}
