
const Util = require("../Cmn/Util");
const Log = require("../Cmn/Log");
const Config = require("../Config");

module.exports =
{
	//****************************************************************************************************
	post: function(sPath, pData, fncRet)	
	{
		var pMsg = pData; 

		Util.post(getUrl(sPath), pMsg, fncRet);
	},
	
	getName: function()
	{
		return "__loc_srv";
	}
};

//****************************************************************************************************
function getUrlSrv()
{
	return Config.getUrlLoc(); 
}

//****************************************************************************************************
function getUrl(sPath)
{ 
	return getUrlSrv() + sPath; 
}
