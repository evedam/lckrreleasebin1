
function Client(sUrl, sUsr, sPwd)
{
	//****************************************************************************************************
	var State = 
	{
		Init:1,
		Connected:2,
		Logged:3,
		Err:4
	};

	//****************************************************************************************************
	function Msg(sAct, pData)
	{
		this.setAct 	= function(sAct) 	{ this.s_Act = sAct;		};		
		this.getAct 	= function()		{ return this.s_Act;		};
		this.setData	= function(aData)	{ this.a_Data = aData;		};
		this.getData	= function()		{ return this.a_Data;		};
		this.getCntData = function()		{ return this.a_Data.length;	};
		this.getData	= function(iIdx)	{ return this.a_Data[iIdx];	};

		this.setAct(sAct);
		this.setData(pData); 
	}

	//****************************************************************************************************
	function LocalFile(sTo, sSesId, sData, sMsg, fnOnProgress)
	{
		this.setTo = function(sTo)			{ this.s_To = sTo; 			}	
		this.getTo = function()				{ return this.s_To;			}
		this.setSesId = function(sSesId)		{ this.s_SesId = sSesId;		}
		this.getSesId = function()			{ return this.s_SesId;			}
		this.setData = function(sData)			{ this.s_Data = sData; 			} 
		this.getData = function() 			{ return this.s_Data;			}
		this.getLen = function()			{ return this.s_Data.length;		}
		this.setMsg = function(sMsg)			{ this.s_Msg = sMsg; 			}
		this.getMsg = function()			{ return this.s_Msg; 			}
		this.setOnProgress = function(fnOnProgress)	{ this.fn_OnProgress = fnOnProgress;	}
		this.getOnProgress = function()			{ return this.fn_OnProgress;		}
		this.setPktSize = function(uiPktSize)		{ this.ui_PktSize = uiPktSize;		}
		this.getPktSize = function()			{ return this.ui_PktSize;		}

		this.setTo(sTo);
		this.setSesId(sSesId);
		this.setData(sData);
		this.setMsg(sMsg);
		this.setOnProgress(fnOnProgress);
		this.setPktSize(1024);		
	}

	//****************************************************************************************************
	function RmtFile(uiId, uiLen, uiPktSize, sMsg)
	{
		this.isDone = function()
		{
			return (((this.ui_Len + this.ui_PktSize - 1) / this.ui_PktSize) | 0) == this.ui_Idx;	
		};

		this.setData = function(uiIdx, sData)
		{
			this.ui_Idx ++;
			this.a_Data[uiIdx] = sData;	
		};

		this.getData = function()
		{
			var sData = "";
			var uiLen = this.a_Data.length;
			for (var uiIdx = 0; uiIdx < uiLen; uiIdx ++)
				sData += this.a_Data[uiIdx];	
			return sData;
		};

		this.setId = function(uiId)		{ this.ui_Id = uiId;		};
		this.getId = function()			{ return this.ui_Id;		};
		this.setLen = function(uiLen)		{ this.ui_Len = uiLen;		};
		this.getLen = function()		{ return this.ui_Len;		};
		this.setPktSize = function(uiSize)	{ this.ui_PktSize = uiSize;	};
		this.getPktSize = function()		{ return this.ui_PktSize;	};
		this.setMsg = function(sMsg)		{ this.s_Msg = sMsg;		};
		this.getMsg = function()		{ return this.s_Msg;		};

		this.setId(uiId);
		this.setLen(uiLen);
		this.setPktSize(uiPktSize);
		this.setMsg(sMsg);

		this.a_Data = [];
		this.ui_Idx = 0;
	}

	//****************************************************************************************************
	var s_UserName = sUsr;
	var s_Password = sPwd;
	var s_Url = sUrl;
	var p_Socket = io(s_Url);	
	var s_FileContent;
	var e_State = State.Init;
	var q_Msg = [];
	var a_LocalFile = [];
	var m_RmtFile = {};

	var fn_OnMsg = null;
	var fn_OnMsgRet = null;
	var fn_OnSig = null;
	var fn_OnSigRet = null;
	var fn_OnFileStart = null;
	var fn_OnFileProgress = null;
	var fn_OnFile = null;

	//****************************************************************************************************
	this.addSes = function(sSesId)
	{
		console.log("add session");
		send("add_ses", [sSesId]);
	};

	//****************************************************************************************************
	this.logoff = function()
	{
		send("logoff");
	};

	//****************************************************************************************************
	this.sendMsg = function(uiId, sSesId, sTo, pMsg)
	{
		console.log("sending message id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + " msg = " + JSON.stringify(pMsg));
		send("send_msg", [uiId, sSesId, sTo, pMsg]);
	};

	//****************************************************************************************************
	this.sendSig = function(uiId, sSesId, sTo, pMsg)
	{
		console.log("sending signal id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + " msg = " + JSON.stringify(pMsg));
		send("send_sig", [uiId, sSesId, sTo, pMsg]);
	};

	//****************************************************************************************************
	this.setOnMsg = function(fnOnMsg)
	{
		fn_OnMsg = fnOnMsg;
	};

	//****************************************************************************************************
	this.setOnMsgRet = function(fnOnMsgRet)
	{
		fn_OnMsgRet = fnOnMsgRet;	
	};

	//****************************************************************************************************
	this.setOnSig = function(fnOnSig)
	{
		fn_OnSig = fnOnSig;
	};

	//****************************************************************************************************
	this.setOnSigRet = function(fnOnSigRet)
	{
		fn_OnSigRet = fnOnSigRet;
	};

	//****************************************************************************************************
	function sendMsgQueue()
	{
		while (q_Msg.length > 0 && e_State == State.Logged)	
		{
			var pMsg = q_Msg.shift();	

			switch (pMsg.getCntData())
			{
			case 0:
				p_Socket.emit(pMsg.getAct());
				break;
			case 1:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0));
				break;
			case 2:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1)); break;
			case 3:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2));
				break;
			case 4:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3));
				break;
			case 5:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4));
				break;
			case 6:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5));
				break;
			case 7:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6));
				break;
			case 8:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7));
				break;
			case 9:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8));
				break;
			case 10:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9));
				break;
			case 11:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10));
				break;
			case 12:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10),
					pMsg.getData(11));
				break;
			case 13:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10),
					pMsg.getData(11), pMsg.getData(12));
				break;
			}
		}
	}

	//****************************************************************************************************
	function queue(pMsg)
	{
		q_Msg.push(pMsg);	
		sendMsgQueue();
	}
	
	//****************************************************************************************************
	function send(sAct, aData)
	{
		queue(new Msg(sAct, aData == undefined ? [] : aData));
	}

	//****************************************************************************************************
	function login()
	{
		p_Socket.emit("login", s_UserName, s_Password);
	}

	//****************************************************************************************************
	p_Socket.on("connect", function()
	{
		console.log("on connect ");
		console.log("Stete = " + State.Connected);
		
		e_State = State.Connected;

		if (s_UserName != "" && s_UserName != null)
			login();
	});

	//****************************************************************************************************
	p_Socket.on("disconnect", function()
	{
		console.log("on disconnect");
		e_State = State.Init;
	});

	//****************************************************************************************************
	p_Socket.on("login_ret", function(bRet, sMsg)
	{
		console.log("login response (" + bRet + ") message = (" + sMsg + ")");	
		if (bRet)
		{
			e_State = State.Logged;
			sendMsgQueue();
		}
	});

	//****************************************************************************************************
	p_Socket.on("msg", function(pMsg)
	{
		console.log("on message " + JSON.stringify(pMsg));
		fn_OnMsg(pMsg.p_Msg);
	});

	p_Socket.on("sig", function(pMsg)
	{
		console.log("on signal " + JSON.stringify(pMsg));
		fn_OnSig(pMsg.p_Msg);
	});

	//****************************************************************************************************
	p_Socket.on("error", function(sMsg)
	{
		console.log("on error [" + sMsg + "]");
		e_State = State.Err;
	});

	//****************************************************************************************************
	p_Socket.on("err", function(sMsg)
	{
		console.log("on error [" + sMsg + "]");	
	});

	//Send file

	//****************************************************************************************************
	p_Socket.on("send_start_file_ret", function(pData)
	{
		var pMsg = pData.p_Msg;
		console.log("start send file bin id = " + pMsg.id + " usr_data = " + pMsg.usr_data + " json = " + JSON.stringify(pData));	
		
		if (pMsg.id >= 0)
			continueSendFile(pMsg.id, pMsg.usr_data);
		else
			console.log("unable to start send file [" + pData.err + "]");

	});

	//****************************************************************************************************
	function sendFileBinary(sTo, uiFileId, iIdx, sData, uiLen)
	{
		send("send_file_bin", [sTo, uiFileId, "session_id", iIdx, sData, uiLen]);

		console.log("sending binary to (" + sTo + ")" + iIdx + " length = " + sData.length);
	}

	//****************************************************************************************************
	function startSendFile(pFileData, uiIdx)
	{
		send("send_start_file", [pFileData.getTo(), pFileData.getSesId(), pFileData.getLen(), pFileData.getPktSize(), 
			pFileData.getMsg(), uiIdx]);
		console.log("start sending file");
	}

	//****************************************************************************************************
	this.sendFile = function(sTo, sSesId, pFile, sMsg, fnOnProgress)
	{
		if (!isSet(sTo))
			throw "invalid to user name";
		
		if (!isSet(pFile))
			throw "invalid pFile";	

		if (!isSet(sMsg))
			sMsg = "";

		if (!isSet(fnOnProgress))
			throw "invalid onProgress function";

		var pReader = new FileReader();
		pReader.onload = function(pE)
		{
			var sContent = pE.target.result;
			var pData = new LocalFile(sTo, sSesId, sContent, sMsg, fnOnProgress);
			a_LocalFile.push(pData);	
			startSendFile(pData, a_LocalFile.length - 1); 
		}

		pReader.readAsBinaryString(pFile);
	};

	//****************************************************************************************************
	function continueSendFile(uiFileId, iIdxFileData)
	{
		var pFileData = a_LocalFile[iIdxFileData];
		var sFileContent = pFileData.getData();
		var fnOnProgress = pFileData.getOnProgress();
		var uiLen = sFileContent.length;
		var uiPktSize = pFileData.getPktSize();
		var sTo = pFileData.getTo();

		for (var iIdx = 0, iCnt = 0; iIdx < uiLen; iIdx += uiPktSize, iCnt++)
		{
			var sData = sFileContent.substring(iIdx, iIdx + uiPktSize);
			sendFileBinary(sTo, uiFileId, iCnt, sData, sData.length);
		
			iProgress = 100 * (((iCnt + 1) * uiPktSize) / uiLen);
			fnOnProgress(iProgress | 0);
		} 
	}

	// Receive file

	//****************************************************************************************************
	p_Socket.on("start_file", function(pMsg)
	{
		console.log("start file " + pMsg + " str = (" + JSON.stringify(pMsg) + ")");

		var pFileData = pMsg.p_Msg;
		var uiFileId = pFileData.id;
		var uiLen = pFileData.len;
		var uiPktSize = pFileData.pkt_size;
		var sMsg = pFileData.msg;

		m_RmtFile[uiFileId] = new RmtFile(uiFileId, uiLen, uiPktSize, sMsg);	
		
		fn_OnFileStart(uiFileId, uiLen, uiPktSize);

		console.log("start file id = " + uiFileId + " len = " + uiLen + " pkt_size = " + uiPktSize + " message = " + sMsg);
	});

	//****************************************************************************************************
	p_Socket.on("file_bin", function(pMsg)
	{
		var pData = pMsg.p_Msg;
		var uiFileId = pData.id;
		var uiIdx = pData.idx;
		var uiLen = pData.len;

		var pRmtFile = m_RmtFile[uiFileId];	
		pRmtFile.setData(uiIdx, pData.data);

		var iProgress = (100 * (uiIdx + 1) / (((pRmtFile.getLen() + pRmtFile.getPktSize() - 1) / pRmtFile.getPktSize()) | 0) | 0); 
		getOnFileProgress()(uiFileId, iProgress);

		console.log("on file bin fild_id = " + uiFileId + " idx = " + uiIdx + " len = " + uiLen + " progress = " + iProgress); 

		if (pRmtFile.isDone())
		{
			console.log("file receive complete id = " + pRmtFile.getId());
			fn_OnFile(pRmtFile.getId(), pRmtFile.getData(), pRmtFile.getMsg());		
		}
	});

	//****************************************************************************************************
	this.setOnFileStart = function(fnOnFileStart)
	{
		fn_OnFileStart = fnOnFileStart;	
	};

	//****************************************************************************************************
	this.setOnFileProgress = function(fnOnFileProgress)
	{
		fn_OnFileProgress = fnOnFileProgress;
	};

	function getOnFileProgress()
	{
		return fn_OnFileProgress;
	}

	//****************************************************************************************************
	this.setOnFile = function(fnOnFile)
	{
		fn_OnFile = fnOnFile;
	};
}
