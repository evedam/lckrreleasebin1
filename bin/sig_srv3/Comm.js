
var Msg = require("./Msg/Api");
var Util = require("./Cmn/Util");
var Log = require("./Cmn/Log");

module.exports =
{
	//****************************************************************************************************
	send: function(pReq, pRes, sAct, sActRet)
	{ 
		Log.d("send act = " + sAct + " act_ret = " + sActRet);

		var sBody = [];
		pReq.on("data", function(sBuf)
		{
			sBody.push(sBuf);
		}).on("end", function()
		{
			sBody = Buffer.concat(sBody).toString();
			Log.d("send (" + sBody + ")");

			try
			{
				var pData = JSON.parse(sBody);  
				var sSysId = pData.sys_id;
				var uiId = pData.id;
				var sSesId = pData.ses_id;
				var sUsr = pData.usr;
				var sTo = pData.to;
				var pMsg = pData.msg; 
				var uiExpireTime = isNaN(pData.expire_time) ? 0 : pData.expire_time;

				this.sendData(sAct, sActRet, sSysId, uiId, sSesId, sUsr, sTo, pMsg, uiExpireTime);

				Util.writeJsonRes(pRes, {state:"true"});
			}
			catch (pExe)
			{
				Log.e("send msg exception [" + pExe + "]");
				Util.writeJsonResErr(pRes, {state:"false", msg:"send msg exception [" + pExe + "]"});
			}
		}.bind(this));
	},

	//****************************************************************************************************
	sendData: function(sAct, sActRet, sSysId, uiId, sSesId, sUsr, sTo, pMsg, uiExpireTime)
	{ 
		Log.d("act = " + sAct + " act_ret = " + sActRet + " sys_id = " + sSysId + " id = " + uiId + " ses_id = " + sSesId + " usr = " + 
			sUsr + " to = " + sTo + " msg = " + pMsg + " expire_time = " + uiExpireTime);

		Msg.send(sAct, sSysId, uiId, sSesId, sUsr, sTo, pMsg, uiExpireTime);

		if (sActRet != null)
			Msg.send(sActRet, uiId, "", sSesId, "server", sUsr, JSON.stringify({sent:true}), uiExpireTime);

		Msg.print(); 
	},

	//****************************************************************************************************
	emit: function(pSocket, sAct, iId, pData)
	{
		try
		{
			var sData = JSON.stringify(pData == undefined ? {} : pData);
			Log.d("emit(" + sAct + ", " + iId + ", " + sData + ")");
			pSocket.emit(sAct, iId, sData);
		}
		catch (pExe)
		{
			Log.e("exception emit [" + pExe + "]");
		}
	},

	//****************************************************************************************************
	emitObj: function(pSocket, sAct, pData)
	{
		try
		{
			Log.d("sendObj act = " + sAct + " data = " + JSON.stringify(pData));	
			pSocket.emit(sAct, pData);
		}
		catch (pExe)
		{
			Log.e("comm::sendObj exception [" + pExe + "]");
		}
	},

	//****************************************************************************************************
	auth: function(pSocket, sAct, sActRet, fncOk)
	{
		if (Util.isSet(pSocket.getUsr))
		{
			fncOk();
		}
		else
		{
			Comm.emit(pSocket, sActRet, uiId, {ret:"false", msg:"unable to send (" + sAct + "). socket not autorized"});
			Log.d("unable to send (" + sAct + "). socket not autorized.");
		}
	}
};
