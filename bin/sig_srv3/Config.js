
const Log = require("./Cmn/Log");
const CmdLineArg = require("./Cmn/CmdLineArg");

module.exports = 
{
	//****************************************************************************************************
	init: function()
	{
		CmdLineArg.init();

		if (!this.validate())
			process.exit(1);

		this.i_Port = CmdLineArg.get("--port");
		this.i_PortHttp = CmdLineArg.get("--port-http");
		this.i_SocketPingTimeout = parseInt(CmdLineArg.get("--socket-ping-timeout"));
		this.i_SocketPingInterval = parseInt(CmdLineArg.get("--socket-ping-interval"));
		this.s_UrlMsg = CmdLineArg.get("--url-msg");
		this.s_UrlCfg = CmdLineArg.get("--url-cfg");
		this.s_UrlUsr = CmdLineArg.get("--url-usr");
		this.s_UrlWeb = CmdLineArg.get("--url-web");
		this.s_UrlAuth = CmdLineArg.get("--url-auth");
		this.s_UrlApk = CmdLineArg.get("--url-apk");
		this.s_UrlDevice = CmdLineArg.get("--url-device");
		this.s_UrlLoc = CmdLineArg.get("--url-loc");
		this.b_Secure = CmdLineArg.getBool("--secure"); 
		this.b_QueueMsg = CmdLineArg.getBool("--queue-msg");
		this.b_ReconnectWithPing = CmdLineArg.getBool("--reconnect-with-ping");

		this.print();
	},

	//****************************************************************************************************
	getDir: function()
	{
		return "Data/"
	},

	//****************************************************************************************************
	getDirUsr: function(sUsr)
	{
		return this.getDir() + sUsr + "/";
	},	

	//****************************************************************************************************
	getNewFile: function(sUsr, sId)
	{
		var sFile = this.getDirUsr(sUsr) + this.getNewFileName(sUsr, sId);
		Log.d("file = " + sFile);
		return sFile; 
	},

	//****************************************************************************************************
	getNewFileName: function(sUsr, sId)	
	{
		return "file_" + sUsr + "_" + (new Date().getTime()) + "_" + sId;
	},
	
	//****************************************************************************************************
	getFileId: function(sUsr, sId)
	{
		return "file_" + sUsr + "_" + sId;
	},
	
	//****************************************************************************************************
	validate: function()
	{
		if (!CmdLineArg.hasVal("--port"))
		{
			Log.d("please enter port (--port)");	
			return false;
		}

		if (!CmdLineArg.hasVal("--port-http"))
		{
			Log.d("please enter http port (--port-http)");	
			return false;
		}

		if (!CmdLineArg.hasVal("--socket-ping-timeout"))
		{
			Log.d("please enter socket ping timout (--socket-ping-timeout)");
			return false;
		}

		if (!CmdLineArg.hasVal("--socket-ping-interval"))
		{
			Log.d("please enter socket ping interval (--socket-ping-interval)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-msg"))
		{
			Log.d("please enter message url (--url-msg)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-cfg"))
		{
			Log.d("please enter config url (--url-cfg)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-usr"))
		{
			Log.d("please enter usr_srv url (--url-usr)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-web"))
		{
			Log.d("please enter web_srv url (--url-web)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-auth"))
		{
			Log.d("please enter auth service url (--url-auth)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-apk"))
		{
			Log.d("please enter apk service url (--url-apk)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-device"))
		{
			Log.d("please enter valid device service url (--url-device)");
			return false;
		}

		if (!CmdLineArg.hasVal("--url-loc"))
		{
			Log.d("please enter valid location service url (--url-loc)");
			return false;
		}

		if (CmdLineArg.isSet("--secure") && !CmdLineArg.isBool("--secure"))
		{
			Log.d("please enter valid value for --secure argument");
			return false;
		}

		if (CmdLineArg.isSet("--reconnect-with-ping") && !CmdLineArg.isBool("--reconnect-with-ping"))
		{
			Log.d("please enter valid value for --reconnect-with-ping argument");
			return false;
		}

		return true;
	},

	//****************************************************************************************************
	getPort: function()
	{
		return this.i_Port;
	},
	
	//****************************************************************************************************
	getPortHttp : function()
	{
		return this.i_PortHttp;
	},

	//****************************************************************************************************
	getSocketPingTimeout: function()
	{
		return this.i_SocketPingTimeout;
	},

	//****************************************************************************************************
	getSocketPingInterval: function()
	{
		return this.i_SocketPingInterval;
	},

	//****************************************************************************************************
	getUrlMsg : function()
	{
		return this.s_UrlMsg;
	},

	//****************************************************************************************************
	getUrlCfg: function()
	{
		return this.s_UrlCfg;
	},

	//****************************************************************************************************
	getUrlUsr: function()
	{
		return this.s_UrlUsr;
	},

	//****************************************************************************************************
	getUrlWeb: function()
	{
		return this.s_UrlWeb;
	},

	//****************************************************************************************************
	getUrlAuth: function()
	{
		return this.s_UrlAuth;
	},

	//****************************************************************************************************
	getUrlApk: function()
	{
		return this.s_UrlApk;
	},

	//****************************************************************************************************
	getUrlDevice: function()
	{
		return this.s_UrlDevice;
	},

	//****************************************************************************************************
	getUrlLoc: function()
	{
		return this.s_UrlLoc;
	},

	//****************************************************************************************************
	isSecure: function()
	{
		return this.b_Secure;
	},

	//****************************************************************************************************
	isQueueMsg: function()
	{
		return this.b_QueueMsg;
	},

	//****************************************************************************************************
	isReconnectWithPing: function()
	{
		return this.b_ReconnectWithPing;
	},

	//****************************************************************************************************
	print: function()
	{
		Log.d("port = " + this.i_Port);
		Log.d("port http = " + this.i_PortHttp);
		Log.d("socket ping timeout = " + this.i_SocketPingTimeout);
		Log.d("socket pint interval = " + this.i_SocketPingInterval);
		Log.d("url msg = " + this.s_UrlMsg);
		Log.d("url cfg = " + this.s_UrlCfg);
		Log.d("url usr = " + this.s_UrlUsr);
		Log.d("url web = " + this.s_UrlWeb);
		Log.d("url auth = " + this.s_UrlAuth);
		Log.d("url apk = " + this.s_UrlApk);
		Log.d("url device = " + this.s_UrlDevice);
		Log.d("url loc = " + this.s_UrlLoc);
		Log.d("secure = " + this.b_Secure);
		Log.d("queue msg = " + this.b_QueueMsg);
		Log.d("reconnect with ping = " + this.b_ReconnectWithPing);
	},

	//****************************************************************************************************
	i_Port:0,
	i_PortHttp:0,
	s_UrlMsg: "",
	sUrlCfg: ""
};
