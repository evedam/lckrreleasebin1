var Config = require("./Config");
Config.init();

const SocketIo = require("socket.io");
const Express = require("express");
const App = Express();
const AppHttp = Express();
const b_Https = Config.isSecure();
const Https = b_Https ? require("https") : require("http");
const Http = require("http");
const Fs = require("fs");
const Logger = require("morgan");
const Path = require("path");

var Log = require("./Cmn/Log");
var Util = require("./Cmn/Util");
var Usr = require("./Usr");
var Msg = require("./Mod/Msg");
var MsgMeta = require("./Msg/Meta");
var Cfg = require("./Mod/Cfg");
var ModUsr = require("./Mod/Usr"); 
var ModWeb = require("./Mod/Web");
var ModApk = require("./Mod/Apk");
var ModHttp = require("./Mod/Http");

App.use(Logger("dev"));
AppHttp.use(Logger("dev"));

App.use(Express.static(Path.join(__dirname, "static")));

//****************************************************************************************************
App.all("/", function(pReq, pRes)
{
	Util.writeJsonRes(pRes, {version:"2.0"});	
});

Msg.init(App, AppHttp);
Cfg.init(App, AppHttp);
ModUsr.init(App, AppHttp);
ModWeb.init(App, AppHttp);
ModApk.init(App, AppHttp);
ModHttp.init(App, AppHttp);

App.all("*", function(pReq, pRes)
{
	Util.writeJsonRes(pRes, {server:"SigSrv2", state:"false", msg:"please enter valid request", url:pReq.url});	
});

//****************************************************************************************************
var Server = null;
if (b_Https)
{
	Server = Https.createServer
	(
		{
			key:Fs.readFileSync("/etc/apache2/ssl/key.pem"),
			cert:Fs.readFileSync("/etc/apache2/ssl/cert.pem")
		},
		App
	);
}
else
	Server = Https.createServer(App);

//****************************************************************************************************
var ServerHttp = Http.createServer(AppHttp);

//****************************************************************************************************
var Io = SocketIo(Server, {pingTimeout:Config.getSocketPingTimeout(), pingInterval:Config.getSocketPingInterval(), upgradeTimeout:10000});

//****************************************************************************************************
Io.on(MsgMeta.getConnect(), function(pSocket)
{
	Log.d("connected socket " + pSocket.id);
	Msg.initSocket(pSocket); 
	Cfg.initSocket(pSocket);
	ModUsr.initSocket(pSocket);
	ModWeb.initSocket(pSocket);
	ModApk.initSocket(pSocket);
	ModHttp.initSocket(pSocket);
});

//****************************************************************************************************
Server.listen(Config.getPort(), function()	
{
	Log.d("start listen port = " + Config.getPort() + " secure = " + b_Https);
});

//****************************************************************************************************
ServerHttp.listen(Config.getPortHttp(), function()
{
	Log.d("start listen http port = " + Config.getPortHttp());
})

//****************************************************************************************************
process.on("uncaughtException", function (pErr) 
{
	Log.e("uncaughtException [" + JSON.stringify(pErr) + "]"); 

	if (pErr.stack) 
	{
		console.log('\nStacktrace:')
		console.log('====================')
		console.log(pErr.stack);
	}
}); 

