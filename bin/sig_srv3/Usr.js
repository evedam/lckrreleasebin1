
var Log = require("./Cmn/Log");
var Util = require("./Cmn/Util");

module.exports =
{
	//****************************************************************************************************
	Usr: function()
	{
		this.setUserName = function(sUsr)
		{
			this.s_Usr = sUsr;
		};		

		this.getUserName = function()
		{
			return this.s_Usr;
		};

		this.setGrp = function(sGrp)
		{
			this.s_Grp = sGrp;
		};

		this.getGrp = function()
		{
			return this.s_Grp;
		};

		this.setType = function(sType)
		{
			this.s_Type = sType;
		};

		this.getType = function()
		{
			return this.s_Type;
		};

		this.setDevice = function(pDevice)
		{
			this.p_Device = pDevice;
		};

		this.getDevice = function()
		{
			return this.p_Device;
		};
	},

	//****************************************************************************************************
	getUsr: function(sUsr)
	{
		var pUsr = getUsr(sUsr);

		if (!Util.isSet(pUsr))
		{
			pUsr = new this.Usr();
			pUsr.setUserName(sUsr);
			this.addUsr(sUsr, pUsr);
		}

		return pUsr;
	},

	//****************************************************************************************************
	addUsr: function(sUsr, pUsr)
	{
		this.m_IdUsr[sUsr] = pUsr;
	},

	//****************************************************************************************************
	getUsr: function(sUsr)
	{
		return this.m_IdUsr[sUsr];
	},

	//****************************************************************************************************
	print: function()
	{
		for (var pUsr in this.m_IdUsr)
		{
			Log.d("user " + pUsr.getUserName());
		}
	},

	//****************************************************************************************************
	m_IdUsr: {}
};
