
var Log = require("../Cmn/Log");
var Util = require("../Cmn/Util");
var Meta = require("./Meta");
var Usr = require("./Usr");
var Msg = require("./Msg");

module.exports = 
{ 
	//****************************************************************************************************
	send: function(sAct, sSysId, iId, sSesId, sFrom, sTo, pData, uiExpireTime)
	{
		if (Util.isEmpty(sFrom))	
			throw "invalid from_user name (" + sFrom + ")";

		if (Util.isEmpty(sTo))
			throw "invalid to_user name (" + sTo + ")";

		pUsr = this.getUsr(sTo);

		Log.d("sending message act = (" + sAct + ") sys_id = (" + sSysId + ") id = (" + iId + ") ses_id = (" + sSesId + ")" +
			" from = (" + sFrom + ") to = (" + sTo + ") message = (" + pData + ") expire_time = (" + uiExpireTime + ")");

		if (!Util.isSet(pUsr))
		{
			pUsr = new Usr.Usr();
			pUsr.init(sTo);
			this.addUsr(sTo, pUsr);
		}

		var pMsg = new Msg.Msg(); 
		pMsg.init(sAct, sSysId, iId, sSesId, sFrom, pData, uiExpireTime);
		pUsr.sendMsg(pMsg);
	},

	//****************************************************************************************************
	addSes: function(sUsr, sSesId)
	{
		pUsr = this.getUsr(sUsr);		

		if (!Util.isSet(pUsr))
			throw "invalid user name (" + sUsr + ")";

		pUsr.addSes(sSesId);
	},

	//****************************************************************************************************
	addUsr: function(sUsr, pUsr)
	{
		this.m_IdUsr[sUsr] = pUsr;		
	},

	//****************************************************************************************************
	connectUsr: function(sUsr, pSocket)
	{
		if (Util.isEmpty(sUsr))	
			throw "invalid user name (" + sUsr + ")";

		pUsr = this.getUsr(sUsr); 

		if (!Util.isSet(pUsr))
		{
			pUsr = new Usr.Usr();
			pUsr.init(sUsr);
			this.addUsr(sUsr, pUsr);

			Log.d("new client connected (" + sUsr + ")");
		}
		else
			Log.d("reconnect client (" + sUsr + ")");

		this.m_SocketUsr[sUsr] = pSocket.id;
		pUsr.connect(pSocket);
	},

	//****************************************************************************************************
	isConnected: function(pSocket)
	{
		if (!Util.isSet(pSocket.getUsr))
			return false;	

		var pUsr = pSocket.getUsr();
		var sSocketId = this.m_SocketUsr[pUsr.getUserName()];

		if (!Util.isSet(sSocketId))
			return false;

		return pSocket.id == sSocketId; 
	},

	//****************************************************************************************************
	disconnectUsr: function(sUsr)
	{
		if (this.getUsr(sUsr) == null)
		{
			Log.d("Msg::Api::disconnectUsr() :: no user ");
			return;
		}

		this.getUsr(sUsr).disconnect(null);	
	},

	//****************************************************************************************************
	rmUsr: function(sUsr)
	{
		this.addUsr(sUsr, null);
	},

	//****************************************************************************************************
	getUsr: function(sUsr)
	{
		return this.m_IdUsr[sUsr];
	},
	
	//****************************************************************************************************
	isUsr: function(sUsr)
	{
		return this.getUsr(sUsr) != null;
	},

	//****************************************************************************************************
	print: function()
	{
		Log.d("messages");
		Log.d("{");

		Log.d("\tUsers");
		Log.d("\t{");
		for (var sUsr in this.m_IdUsr)
		{
			if (!this.m_IdUsr.hasOwnProperty(sUsr))
				continue;

			pUsr = this.m_IdUsr[sUsr];
			if (pUsr == null)
				continue;

			pUsr.print(2);
		}

		Log.d("\t}");
		Log.d("}");
	},

	//****************************************************************************************************
	m_IdUsr: {},
	m_SocketUsr: {}
};
