
const Log = require("../Cmn/Log");
const Util = require("../Cmn/Util");
const Meta = require("./Meta");
const Config = require("../Config");

module.exports =
{
	//****************************************************************************************************
	Usr: function()
	{
		this.init = function(sUsr)	
		{
			this.setUserName(sUsr);	
			this.setSocket(null);
		};

		this.sendMsg = function(pMsg)
		{
			if (Config.isQueueMsg())
			{
				Log.d("Msg::Usr::sendMsg queue message " + pMsg.getId());
				this.a_Msg.push(pMsg);	
				this.sendMsgQueue();
			}
			else
			{
				Log.d("Msg::Usr::sendMsg send message directly " + pMsg.getId());
				this.sendSocket(pMsg);
			}
		};

		this.connect = function(pSocket)
		{
			this.setSocket(pSocket);
			this.sendMsgQueue();
		};

		this.disconnect = function()
		{
			this.setSocket(null);
			this.clsSes();
		};

		this.sendSocket = function(pMsg)
		{
			if (this.getSocket() == null)
			{
				Log.d("sendSocket getSocket()==null");
				return false;
			}

			try
			{
				this.getSocket().emit(pMsg.getAct(), pMsg);		
			}
			catch (pExe)
			{
				Log.d("Msg::Msg send socket exception [" + pExe.message + "]");
				return false;
			}

			return true;
		};

		this.sendMsgQueue = function()
		{
			if (this.getSocket() == null)
				return;

			var iCntSent = 0;

			for (var iIdx = 0; iIdx < this.a_Msg.length; iIdx ++)			
			{
				pMsg = this.a_Msg[iIdx];
				
				var bDone = true;

				if (pMsg.isExpired())
				{
					iCntSent++;
					Log.d("message has been expired " + JSON.stringify(pMsg));
				}
				else
				{
					Log.d("has session " + pMsg.getSesId() + " ret = " + this.hasSes(pMsg.getSesId()));

					if (this.hasSes(pMsg.getSesId()))
					{
						if (this.sendSocket(pMsg))
							iCntSent++;
						else
							break;
					}
					else
					{
						bDone = false;

						Log.d("no session to send message " + pMsg.getSesId());
					}
				}

				if (bDone)
				{
					this.a_Msg.splice(iIdx, 1);
					iIdx--;
				}
			}
		};

		this.addSes = function(sSes)
		{
			Log.d("Msg::Usr::addSes " + sSes);

			this.m_Ses[sSes] = true;
			this.sendMsgQueue();
		};

		this.hasSes = function(sSesId)
		{
			if (Util.isEmpty(sSesId) || sSesId == "NULL" || sSesId == "null")
				return true;

			return this.m_Ses[sSesId] == true;	
		};

		this.rmSes = function(sSesId)
		{
			for (var iIdx = this.a_Msg.length - 1; iIdx >= 0; iIdx --)
			{
				pMsg = this.m_Msg[iIdx];

				if (pMsg.getSesId() == sSesId)
				{
					this.a_Msg.splice(iIdx, 1);	
					Log.d("remove session. delete msg " + JSON.strinify(pMsg));
				}
			}
		};

		this.clsSes = function()
		{
			this.m_Ses = {};	
		};

		this.setUserName = function(sUsr)
		{
			this.s_UserName = sUsr;
		};
		
		this.getUserName = function()
		{
			return this.s_UserName;
		};

		this.setSocket = function(pSocket)
		{
			Log.d("set socket user = " + this.getUserName() + " socket = " + pSocket);
			this.p_Socket = pSocket;
		};

		this.getSocket = function()
		{
			return this.p_Socket;
		};

		this.print = function(iTab)
		{
			Log.t(iTab, "user ");	
			Log.t(iTab, "{");
			iTab++;
			{
				Log.t(iTab, "user_name : " + this.getUserName());	
				Log.t(iTab, "message_queue");

				Log.t(iTab, "{");
				for (var iIdx = 0; iIdx < this.a_Msg.length; iIdx ++)
					logMsg(iTab + 1, this.a_Msg[iIdx]); 
				Log.t(iTab, "}");

				Log.t(iTab, "sessions : " + JSON.stringify(this.m_Ses));
			}
			iTab--;
			Log.t(iTab, "}");
		}	

		this.a_Msg = [];
		this.m_Ses = {};
	},
};

//****************************************************************************************************
function logMsg(iTab, pMsg)
{
	if (pMsg.getAct() == Meta.getFileBin())	
	{
		Log.t(iTab, 
			"{" + 
				"act = " + pMsg.getAct() + " id = " + pMsg.getId() + " ses_id = " + pMsg.getSesId() + 
				" from = " + pMsg.getFrom() + " time = " + pMsg.getTime() + " expire_time = " + pMsg.getExpireTime() + 
				"msg {id = " + pMsg.getMsg().id + " idx = " + pMsg.getMsg().idx + " actual_len" + pMsg.getMsg().data.length + 
				" len = " + pMsg.getMsg().len + "}" +
			"}");	
	}
	else
		Log.t(iTab, JSON.stringify(pMsg));
}
