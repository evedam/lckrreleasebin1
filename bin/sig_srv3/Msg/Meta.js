
module.exports =
{
	getConnect: function()
	{
		return "connection";
	},

	getDisconnect: function()
	{
		return "disconnect"
	},

	getMsg: function()
	{
		return "msg";
	},

	getSendMsg: function()
	{
		return "send_msg";
	},

	getSendMsgRet: function()
	{
		return "send_msg_ret";
	},

	getSig: function()
	{
		return "sig";
	},

	getSendSig: function()
	{
		return "send_sig";
	},

	getSendSigRet: function()
	{
		return "send_sig_ret";
	},

	getReq: function()
	{
		return "req";
	},

	getSendReq: function()
	{
		return "send_req";
	},

	getSendReqRet: function()
	{
		return "send_req_ret";
	},

	getRes: function()
	{
		return "res";
	},

	getSendRes: function()
	{
		return "send_res";
	},

	getSendResRet: function()
	{
		return "send_res_ret";
	},

	getCmd: function()
	{
		return "cmd";	
	},
	
	getSendCmd: function()
	{
		return "send_cmd";
	},

	getSendCmdRet: function()
	{
		return "send_cmd_ret";
	},

	getCmdRes: function()
	{
		return "cmd_res";	
	},
	
	getSendCmdRes: function()
	{
		return "send_cmd_res";
	},

	getSendCmdResRet: function()
	{
		return "send_cmd_res_ret";
	},

	getGet: function()
	{
		return "get";
	},

	getSendCmdRet: function()
	{
		return "send_get";
	},
		
	getSendGet: function()
	{
		return "send_get";
	},

	getSendGetRet: function()
	{
		return "send_get_ret";
	},

	getGetRes: function()
	{
		return "get_res";
	},

	getSendGetRes: function()
	{
		return "send_get_res";
	},

	getSendGetResRet: function()
	{
		return "send_get_res_ret";
	},

	getSendStartFile: function()
	{
		return "send_start_file";	
	},

	getStartFile: function()
	{
		return "start_file";
	},

	getSendStartFileRet: function()
	{
		return "send_start_file_ret"
	},

	getSendFileBin: function()
	{
		return "send_file_bin";
	},

	getFileBin: function()
	{
		return "file_bin";
	},

	getRegister: function()
	{
		return "register";
	},

	getLogin: function()
	{
		return "login";
	},

	getLogoff: function()
	{
		return "logoff";
	},

	getLoginRet: function()
	{
		return "login_ret";
	},

	getLogoff: function()
	{
		return "logoff";
	},

	getAddSes: function()
	{
		return "add_ses";
	},
	
	getDone: function()
	{
		return "done";
	},

	getErr: function()
	{
		return "err";	
	},
};
