
module.exports = 
{
	Msg: function()
	{
		this.init = function(sAct, sSysId, iId, sSesId, sFrom, pMsg, uiExpireTime)
		{
			this.setAct(sAct);
			this.setSysId(sSysId);
			this.setId(iId);	
			this.setSesId(sSesId);
			this.setFrom(sFrom);
			this.setMsg(pMsg);
			this.setTime(new Date().getTime());
			this.setExpireTime(uiExpireTime);
		};

		this.isExpired = function()
		{
			if (this.getExpireTime() == 0)
				return false;

			return (new Date().getTime() - this.getTime()) > this.getExpireTime() * 1000;	
		};

		this.setAct = function(sAct)
		{
			this.s_Act = sAct;
		};

		this.getAct = function()
		{
			return this.s_Act;
		};

		this.setSysId = function(sSysId)
		{
			this.s_SysId = sSysId;
		};

		this.getSysId = function()
		{
			return this.s_SysId;
		};

		this.setId = function(iId)
		{
			this.i_Id = iId;
		};

		this.getId = function()
		{
			return this.i_Id;
		};
		
		this.setSesId = function(sId)
		{
			this.s_SesId = sId;
		};

		this.getSesId = function()
		{
			return this.s_SesId;
		};

		this.setFrom = function(sFrom)
		{
			this.s_From = sFrom;
		};

		this.getFrom = function()
		{
			return this.s_From;
		};

		this.setMsg = function(pMsg)	
		{
			this.p_Msg = pMsg;	
		};

		this.getMsg = function()
		{
			return this.p_Msg;
		};

		this.setTime = function(iTime)
		{
			this.i_Time = iTime;
		};

		this.getTime = function()
		{
			return this.i_Time;
		};

		this.setExpireTime = function(iTime)
		{
			this.i_ExpireTime = iTime; 
		};

		this.getExpireTime = function()
		{
			return this.i_ExpireTime;
		};
	}
};
