
const Config = require("./Config");
const Util = require("./Cmn/Util");
const Log = require("./Cmn/Log");

module.exports = 
{
	init: function(pApp)
	{
		pApp.post("/cfg/start/:id", function(pReq, pRes)
		{
			start(pReq.params.id, pRes);
		});

		pApp.all("/cfg/start/:id", function(pReq, pRes)
		{
			methodNotSupport(pReq, pRes, "POST only");	
		});
	
		pApp.post("/cfg/cls/:id", function(pReq, pRes)
		{
			cls(pReq.params.id, pRes);	
		})

		pApp.post("/cfg/cls/:id", function(pReq, pRes)
		{
			methodNotSupport(pReq, pRes, "POST only");	
		});

		pApp.get("/cfg/turn", function(pReq, pRes)
		{
			turn(pRes);
		});

		pApp.all("/cfg/turn", function(pReq, pRes)
		{
			methodNotSupport(pReq, pRes, "GET only");	
		});
	}
};

//****************************************************************************************************
function start(sId, pRes)
{
	apiPost("v1/cfg/start/" + sId, pRes);
}

//****************************************************************************************************
function cls(sId, pRes)
{
	apiPost("v1/cfg/cls/" + sId, pRes);
}

//****************************************************************************************************
function turn(pRes)
{ 
	apiGet("v1/cfg/turn", pRes);
}

//****************************************************************************************************
function apiPost(sUrl, pRes)
{
	var sUrl = Config.getUrlApi() + "/" + sUrl; 
	Log.d("Cfg::apiPost url = " + sUrl);

	Util.post(sUrl, "", function(pData)
	{
		Util.writeJsonRes(pRes, pData);
	});

}

//****************************************************************************************************
function apiGet(sUrl, pRes)
{
	var sUrl = Config.getUrlApi() + "/" + sUrl; 
	Log.d("Cfg::apiGet url = " + sUrl);

	Util.get(sUrl, "", function(pData)
	{
		Util.writeJsonRes(pRes, pData);
	});

}

//****************************************************************************************************
function methodNotSupport(pReq, pRes, sMethodReq)
{
	var oRes = { server:"web1", state:"false", url:pReq.url, msg:"method not support", method:pReq.method, method_req:sMethodReq };
	Util.writeJsonResErr(pRes, oRes);
}

