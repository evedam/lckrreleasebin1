
const Config = require("./Config");
Config.init();

const b_Secure = Config.isSecure(); 
const Http = b_Secure ? require("https") : require("http");
const Express = require("express");
const Logger = require("morgan");
const Path = require("path");
const Fs = require("fs");

const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");
const Cfg = require("./Cfg");

App = Express();

App.use(Logger("dev"));

App.use(Express.static(Path.join(__dirname, "static")));
App.use(Express.static(Path.join(__dirname, "ui/theme_02")));
//TestCode remove after add user registation to new ui
App.use(Express.static(Path.join(__dirname, "ui/theme_01")));
App.use(Express.static(Path.join(__dirname, "apps")));

var iPort = Config.getPort();

//****************************************************************************************************
App.all("/sec/connect", function(pReq, pRes)
{
	var bIsBasic = pReq.query.is_basic == "true";
	if (bIsBasic)
		pRes.render("sec/connect.ejs", {});
	else
		pRes.render("ui/sec/connect.ejs", {});
});

//****************************************************************************************************
App.all("/register", function(pReq, pRes)
{
	var bIsBasic = pReq.query.is_basic == "true";
	pRes.render("index.ejs", {page:"register", is_basic:bIsBasic, url_api:Config.getUrlApi()});
});

//****************************************************************************************************
App.all("/", function(pReq, pRes)
{
	var bIsBasic = pReq.query.is_basic == "true";
	pRes.render("index.ejs", {page:"login", is_basic:bIsBasic, url_api:Config.getUrlApi()});
});

//****************************************************************************************************
App.all("/mdm", function(pReq, pRes)
{
	pRes.render("mdm/index.ejs", {url_api:Config.getUrlApi(), page:"login"});
});

//****************************************************************************************************
App.all("/mdm/beta", function(pReq, pRes)
{
	pRes.render("mdm/index_beta.ejs", {url_api:Config.getUrlApi(), page:"login"});
});

//****************************************************************************************************
App.all("/mdm/*", function(pReq, pRes)
{
	pRes.render("mdm/index.ejs", {url_api:Config.getUrlApi(), page:"login"});
});

Cfg.init(App);

//****************************************************************************************************
App.all("*", function(pReq, pRes)
{
	Log.dv("invalid request " + pReq.url);
	Util.writeJsonErrRes(pRes, 404, {server:"web", state:"false", msg:"please enter valid request", url:pReq.url}) 
});

//****************************************************************************************************
if (b_Secure)
{
	Http.createServer
		(
			{
				key:Fs.readFileSync("/etc/apache2/ssl/key.pem"),
				cert:Fs.readFileSync("/etc/apache2/ssl/cert.pem")
			},
			App
	).listen(iPort);
}
else
{
	Http.createServer(App).listen(iPort);
}

Log.d("listen " + (b_Secure ? "secure" : "non-secure") + " " + iPort + " ....");

//TestCode remove after get rial ssl certificates 
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
