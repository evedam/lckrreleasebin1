
const CmdLineArg = require("./Cmn/CmdLineArg");
const Log = require("./Cmn/Log");

module.exports = 
{
	//****************************************************************************************************
	init: function()
	{
		CmdLineArg.init();

		if (!this.validate())
			process.exit(1);

		this.i_Port = CmdLineArg.get("--port");
		this.s_UrlApi = CmdLineArg.get("--url-api");
		this.b_Secure = CmdLineArg.isSet("--secure");
	},
	
	//****************************************************************************************************
	validate: function()
	{
		if (!CmdLineArg.hasVal("--port"))
		{
			Log.d("please enter port (--port)");	
			return false;
		}

		if (!CmdLineArg.hasVal("--url-api"))
		{
			Log.d("please enter api gateway url (--url-api)");
			return false;
		}

		return true;
	},

	//****************************************************************************************************
	getPort: function()
	{
		return this.i_Port;
	},
	
	//****************************************************************************************************
	getUrlApi: function()
	{
		return this.s_UrlApi;
	},

	//****************************************************************************************************
	isSecure: function()
	{
		return this.b_Secure;
	},

	//****************************************************************************************************
	i_Port:0,
	s_UrlApi: ""	
};

