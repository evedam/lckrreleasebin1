
//****************************************************************************************************
var ModFile = function(pMsg)
{
	this.p_Msg = pMsg; 
	
	this.fn_OnDir = null;
	this.fn_OnFileStart = null;
	this.fn_OnFileProgress = null;
	this.fn_OnFile = null;
};

//****************************************************************************************************
ModFile.prototype.getName = function()
{
	return "ModFile";
};

//****************************************************************************************************
ModFile.prototype.add = function(sUsr, pFile, sFilePath, sDest, fncProgress)
{
	console.log("ModFile::add usr = " + sUsr + " file path = " + sFilePath + " dest = " + sDest);
	var sExt = getFileExt(sFilePath); 
	this.p_Msg.sendFile(sUsr, "", pFile, {type:"file", dest:sDest, ext:sExt}, fncProgress);	
};

//****************************************************************************************************
ModFile.prototype.get = function(sUsr, sFile)
{
	this.p_Msg.sendGet(2000, "", sUsr, {type:"file", file:sFile});
};

//****************************************************************************************************
ModFile.prototype.mv = function(sUsr, sSrc, sDest, bOverwrite)
{
	this.p_Msg.sendCmd(2001, "", sUsr, {type:"file_mv", src:sSrc, dest:sDest, overwrite:bOverwrite});
};

//****************************************************************************************************
ModFile.prototype.rm = function(sUsr, sFile)
{
	this.p_Msg.sendCmd(2002, "", sUsr, {type:"file_rm", file:sFile});
};

//****************************************************************************************************
ModFile.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModFile.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	var bRet = true;
	console.log("ModFile::onGetRes id = " + uiId + " from = " + sFrom + " res = " + JSON.stringify(pRes));

	if (pRes.type == "file")
	{
		console.log("file response " + JSON.stringify(pRes));	

		if (this.fn_OnDir != null)
			this.fn_OnDir(uiId, sFrom, pRes);
		else
			console.log("no dir callback");
	}
	else
		bRet = false;

	return bRet;
};

//****************************************************************************************************
ModFile.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	if (pMsg.type != "file")
		return false;	

	if (this.fn_OnFileStart != null)
	{
		this.fn_OnFileStart(sFrom, uiId, uiLen);
	}
	else
		consoelg.log("no file start callback");

	return true;
};

//****************************************************************************************************
ModFile.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	if (pMsg.type != "file")
		return false;	

	if (this.fn_OnFileProgress)	
	{
		this.fn_OnFileProgress(sFrom, uiId, iProgress, pMsg);
	}
	else
		console.log("no file progress callback");

	return true;
};

//****************************************************************************************************
ModFile.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	if (pMsg.type != "file")
		return false;	

	if (this.fn_OnFile != null)
	{
		this.fn_OnFile(sFrom, uiId, sFile, pMsg);	
	}
	else
		console.log("no on file calllback");

	return true;
};

//****************************************************************************************************
ModFile.prototype.onHttp = function(pData)
{
	return false;
};

//****************************************************************************************************
ModFile.prototype.setOnDir = function(fnOnDir)
{
	this.fn_OnDir = fnOnDir;
};

//****************************************************************************************************
ModFile.prototype.setOnFileStart = function(fnOnFileStart)
{
	this.fn_OnFileStart = fnOnFileStart;
};

//****************************************************************************************************
ModFile.prototype.setOnFileProgress = function(fnOnFileProgress)
{
	this.fn_OnFileProgress = fnOnFileProgress;
};

//****************************************************************************************************
ModFile.prototype.setOnFile = function(fnOnFile)
{
	this.fn_OnFile = fnOnFile;
};

