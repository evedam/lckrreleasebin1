
//****************************************************************************************************
var ModLoc = function(pMsg)
{
	this.p_Msg = pMsg; 

	this.service = "loc";
	this.ui_IdHttp = 300;
	this.ui_IdHttpGetL = ++this.ui_IdHttp;
	this.ui_IdHttpGetCurrentL = ++this.ui_IdHttp;

	this.fn_OnLLoc = null;
};

//****************************************************************************************************
ModLoc.prototype.getName = function()
{
	return "ModLoc";
};

//****************************************************************************************************
ModLoc.prototype.getLLoc = function()
{
	this.p_Msg.http(this.ui_IdHttpGetL, this.service, "/v1/loc/get_l", {});
};

//****************************************************************************************************
ModLoc.prototype.getCurrentL = function()
{
	this.p_Msg.http(this.ui_IdHttpGetCurrentL, this.service, "/v1/loc/get_current_l", {});
};

// Callbacks 
//****************************************************************************************************
ModLoc.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModLoc.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModLoc.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	return false;
};

//****************************************************************************************************
ModLoc.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModLoc.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModLoc.prototype.onHttp = function(pData)
{
	var iId = pData.i_Id;
	var bRet = true;
	console.log("onHttp id = " + iId);

	switch (iId)
	{ 
		case this.ui_IdHttpGetL:
		{
			console.log("get list of locations" + JSON.stringify(pData));
			if (this.fn_OnLLoc != null)	
				this.fn_OnLLoc(pData.p_Msg);
		}
		break; 
		case this.ui_IdHttpGetCurrentL:
		{
			console.log("get list of current locations" + JSON.stringify(pData));
			if (this.fn_OnCurrentL != null)	
				this.fn_OnCurrentL(pData.p_Msg);
			else
				console.log("no get current list callback");
		}
		break;
		default:
			bRet = false;
	}

	return bRet;
};

//****************************************************************************************************
ModLoc.prototype.setOnLLoc = function(fnRet)
{
	this.fn_OnLLoc = fnRet;
};

//****************************************************************************************************
ModLoc.prototype.setOnCurrentL = function(fnRet)
{
	this.fn_OnCurrentL = fnRet;
};
