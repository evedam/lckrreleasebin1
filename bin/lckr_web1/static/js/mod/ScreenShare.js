
//****************************************************************************************************
var ModScreenShare = function(pMsg)
{
	this.p_Msg = pMsg; 
	
	this.fn_OnScreenImgStart = null;
	this.fn_OnScreenImgProgress = null;
	this.fn_OnScreenImg = null;
};

//****************************************************************************************************
ModScreenShare.prototype.getName = function()
{
	return "ModScreenShare";
};

//****************************************************************************************************
ModScreenShare.prototype.shareScreen = function(uiId, sTo)
{
	this.p_Msg.sendCmd(uiId, null, sTo, {type:"share_screen"});
};

//****************************************************************************************************
ModScreenShare.prototype.stopShareScreen = function(uiId, sTo)
{
	this.p_Msg.sendCmd(uiId, null, sTo, {type:"stop_share_screen"});
};

//****************************************************************************************************
ModScreenShare.prototype.click = function(uiId, sTo, iX, iY)
{
	this.p_Msg.sendCmd(uiId, null, sTo, {type:"click", point:{x:iX, y:iY}});
};

//****************************************************************************************************
ModScreenShare.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModScreenShare.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	var bRet = true;
	console.log("ModScreenShare::onGetRes id = " + uiId + " from = " + sFrom + " res = " + JSON.stringify(pRes));

	bRet = false;
	return bRet;
};

//****************************************************************************************************
ModScreenShare.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	if (pMsg.type != "screen_share_img")
		return false;	

	if (this.fn_OnScreenImgStart != null)
	{
		this.fn_OnScreenImgStart(sFrom, uiId, uiLen);
	}
	else
		console.log("no file start callback");

	return true;
};

//****************************************************************************************************
ModScreenShare.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	if (pMsg.type != "screen_share_img")
		return false;	

	if (this.fn_OnScreenImgProgress)	
	{
		this.fn_OnScreenImgProgress(sFrom, uiId, iProgress, pMsg);
	}
	else
		console.log("no file progress callback");

	return true;
};

//****************************************************************************************************
ModScreenShare.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	if (pMsg.type != "screen_share_img")
		return false;	

	if (this.fn_OnScreenImg != null)
	{
		this.fn_OnScreenImg(sFrom, uiId, sFile, pMsg);	
	}
	else
		console.log("no on file calllback");

	return true;
};

//****************************************************************************************************
ModScreenShare.prototype.onHttp = function(pData)
{
	return false;
};

//****************************************************************************************************
ModScreenShare.prototype.setOnDir = function(fnOnDir)
{
	this.fn_OnDir = fnOnDir;
};

//****************************************************************************************************
ModScreenShare.prototype.setOnScreenImgStart = function(fnOnScreenImgStart)
{
	this.fn_OnScreenImgStart = fnOnScreenImgStart;
};

//****************************************************************************************************
ModScreenShare.prototype.setOnScreenImgProgress = function(fnOnScreenImgProgress)
{
	this.fn_OnScreenImgProgress = fnOnScreenImgProgress;
};

//****************************************************************************************************
ModScreenShare.prototype.setOnScreenImg = function(fnOnScreenImg)
{
	this.fn_OnScreenImg = fnOnScreenImg;
};

