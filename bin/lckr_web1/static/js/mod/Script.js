
//****************************************************************************************************
var ModScript = function(pMsg)
{
	this.p_Msg = pMsg; 
	
	this.fn_OnRet = null;
};

//****************************************************************************************************
ModScript.prototype.getName = function()
{
	return "ModScript";
};

//****************************************************************************************************
ModScript.prototype.run  = function(sUsr, sScript)
{
	this.p_Msg.sendCmd(4000, "", sUsr, {type:"script", script:sScript});
};

//****************************************************************************************************
ModScript.prototype.onCmdRes = function(uiId, sFrom, pRes)
{
	var bRet = true;
	var sType = pRes.type;
	console.log("type = " + sType);
	if (sType == "script")
	{
		if (this.fn_OnRet != null)
			this.fn_OnRet(uiId, sFrom, pRes);
		else
			console.log("no on ret callback");
	}
	else 
		bRet = false;

	return bRet;
};

//****************************************************************************************************
ModScript.prototype.onGetRes = function(uiId, sFrom, pRes)
{
	return false;
};

//****************************************************************************************************
ModScript.prototype.onFileStart = function(sFrom, uiId, uiLen, pMsg)
{
	return false;
};

//****************************************************************************************************
ModScript.prototype.onFileProgress = function(sFrom, uiId, iProgress, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModScript.prototype.onFile = function(sFrom, uiId, sFile, pMsg)
{
	return false;	
};

//****************************************************************************************************
ModScript.prototype.onHttp = function(pData)
{
	return false;
};

//****************************************************************************************************
ModScript.prototype.setOnRet = function(fnOnRet)
{
	this.fn_OnRet = fnOnRet;
};

