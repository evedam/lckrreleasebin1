
UsrState = 
{
	Init: 0,
	Connect: 1,
	Disconnect: 2,

	//****************************************************************************************************
	toState: function(sState)
	{
		if (sState == "connected")
			return this.Connect;
		else if (sState == "disconnected")
			return this.Disconnect;
		else
			return this.Init;
	},

	toStr: function(state)
	{
		if (state == 2)
			return "disconnected";

		if (state == 1)
			return "connected";

		return "init";
	}
};
