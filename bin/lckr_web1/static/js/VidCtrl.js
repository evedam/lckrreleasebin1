'use strict';

//****************************************************************************************************
var Ui = 
{
	videosDiv: '#videos',
	localVideo: '#local_video',
	remoteVideo: '#remote_video',
	callIcons: '#call_icons',
	cmdMuteAudio: '#cmd_mute_audio',
	cmdMuteVideo: '#cmd_mute_video',
	cmdHangup: '#cmd_hangup',
	cmdRecord: '#cmd_record',
	imgCmdRecord: '#img_cmd_record'
};

//****************************************************************************************************
var VidCtrl = function(loadingParams) 
{
	trace('Initializing; ses_id =' + loadingParams.ses_id + '.');

	this.loadingParams_ = loadingParams;
	this.loadUrlParams_();

	this.localStream_ = null;
	this.remoteVideoResetTimer_ = null; 
	this.p_Socket = null;
	this.a_File = [];

	this.p_RemoteStream = null;
	this.p_Recorder = null;
	this.b_Recording = false;

	this.fnc_onReq = null;
	this.fnc_onRes = null;
	this.fnc_onCmdRes = null;
	this.fnc_onGetRes = null;
	this.fnc_onUsrInfo = null;
	this.fnc_onFileStart = null;
	this.fnc_onFileProgress = null;
	this.fnc_onFile = null;
	this.fnc_onLocalHangup = null;
	this.fnc_onRemoteHangup = null;

	this.fnc_onLstApk = null;
	
	this.fnc_onHttp = null;
};

//****************************************************************************************************
VidCtrl.prototype.createCall_ = function() 
{
	var sTo = this.getTo(); 
	this.call_ = new Call(this.loadingParams_, sTo, this.sendSig.bind(this), this.addSes.bind(this), 
		this.cfgStart.bind(this), this.cfgCls.bind(this), this.cfgTurn.bind(this));

	this.call_.onremotehangup = this.onRemoteHangup_.bind(this);
	this.call_.onremotesdpset = this.onRemoteSdpSet_.bind(this);
	this.call_.onremotestreamadded = this.onRemoteStreamAdded_.bind(this);
	this.call_.onlocalstreamadded = this.onLocalStreamAdded_.bind(this);

	this.call_.onsignalingstatechange = this.logPeerState.bind(this);
	this.call_.oniceconnectionstatechange = this.logPeerState.bind(this);
	this.call_.onnewicecandidate = this.logPeerState.bind(this);

	this.call_.onerror = this.displayError_.bind(this);
	this.call_.onstatusmessage = this.displayStatus_.bind(this);
	this.call_.oncallerstarted = this.onCallStart.bind(this);
};

//****************************************************************************************************
VidCtrl.prototype.finishCallSetup_ = function(sSesId) 
{
	this.call_.start(sSesId);

	window.onmousemove = this.showCtrl.bind(this);

	getEle(Ui.cmdMuteAudio).onclick = this.toggleAudioMute_.bind(this);
	getEle(Ui.cmdMuteVideo).onclick = this.toggleVideoMute_.bind(this);
	getEle(Ui.cmdHangup).onclick = this.hangup.bind(this);
	getEle(Ui.cmdRecord).onclick = this.record.bind(this);

	window.onbeforeunload = function() 
	{
		this.call_.hangup(false);
	}.bind(this);
};

//****************************************************************************************************
VidCtrl.prototype.hangup = function() 
{
	trace('Hanging up.');
	//this.hide_(this.callIcons);
	this.displayStatus_('Hanging up');
	this.transitionToDone_();

	this.call_.hangup(true);

	if (this.fnc_onLocalHangup != null)
		this.fnc_onLocalHangup(this.getSesId());
	else
		console.log("no on local hangup callback");

	this.call_ = null;
};

//****************************************************************************************************
VidCtrl.prototype.record = function()
{
	if (this.p_RemoteStream == null)
	{
		console.log("this.p_RemoteStream == null");
		return;
	}	

	if (!this.b_Recording)
	{
		var pVideoConfig = 
		{
			type: 'video'
		};

		this.p_Recorder = RecordRTC(this.p_RemoteStream, pVideoConfig);
		this.p_Recorder.startRecording();
		this.b_Recording = true;

		getEle(Ui.imgCmdRecord).src = "img/recording.png";
		
		console.log("start recording");
	}
	else
	{
		this.p_Recorder.stopRecording(this.saveRecording.bind(this));	
		this.b_Recording = false;

		getEle(Ui.imgCmdRecord).src = "img/record.png";
		
		console.log("stop recording");
	}
};

//****************************************************************************************************
VidCtrl.prototype.sendMsg = function(uiId, sSesId, sTo, sMsg)
{
	this.getSocket().sendMsg(uiId, sSesId, sTo, sMsg);
};

//****************************************************************************************************
VidCtrl.prototype.sendSig = function(uiId, sTo, sMsg)
{
	this.getSocket().sendSig(uiId, this.getSesId(), sTo, sMsg, 30);
};

//****************************************************************************************************
VidCtrl.prototype.addSes = function()
{
	this.getSocket().addSes(this.getSesId());
};

//****************************************************************************************************
VidCtrl.prototype.cfgStart = function(sSesId)
{
	this.getSocket().cfgStart(501, sSesId, 60);
};

//****************************************************************************************************
VidCtrl.prototype.cfgCls = function(sSesId)
{
	this.getSocket().cfgCls(502, sSesId);
};

//****************************************************************************************************
VidCtrl.prototype.cfgTurn = function()
{
	this.getSocket().cfgTurn(503);
};

//****************************************************************************************************
VidCtrl.prototype.usrGetLst = function(uiId, sType, sState, uiIdx, uiCnt, bSubscripe)
{
	this.getSocket().usrGetLst(uiId, sType, sState, uiIdx, uiCnt, bSubscripe);
};

//****************************************************************************************************
VidCtrl.prototype.onRemoteHangup_ = function() 
{
	this.displayStatus_('The remote side hung up.');
	this.transitionToWaiting_();

	this.call_.onRemoteHangup();

	if (this.fnc_onRemoteHangup != null)
		this.fnc_onRemoteHangup(this.getSesId());
	else
		console.log("no remote hangup callback");
};

//****************************************************************************************************
VidCtrl.prototype.onRemoteSdpSet_ = function(hasRemoteVideo) 
{
	if (hasRemoteVideo) 
	{
		trace('Waiting for remote video.');
		this.waitForRemoteVideo_();
	} 
	else 
	{
		trace('No remote video stream; not waiting for media to arrive.');
		this.transitionToActive_();
	}
};

//****************************************************************************************************
VidCtrl.prototype.waitForRemoteVideo_ = function() 
{
	if (getEle(Ui.remoteVideo).readyState >= 2) 
	{  
		trace('Remote video started; currentTime: ' +
		getEle(Ui.remoteVideo).currentTime);
		this.transitionToActive_();
	} 
	else 
		getEle(Ui.remoteVideo).oncanplay = this.waitForRemoteVideo_.bind(this);
};

//****************************************************************************************************
VidCtrl.prototype.onRemoteStreamAdded_ = function(pStream) 
{
	trace('Remote stream added.');
	attachMediaStream(getEle(Ui.remoteVideo), pStream);

	this.b_Recording = false;
	this.p_RemoteStream = pStream;

	if (this.remoteVideoResetTimer_) 
	{
		clearTimeout(this.remoteVideoResetTimer_);
		this.remoteVideoResetTimer_ = null;
	}
};

//****************************************************************************************************
VidCtrl.prototype.onLocalStreamAdded_ = function(pStream) 
{
	trace('User has granted access to local media.');
	this.localStream_ = pStream;

	this.attachLocalStream_();
};

//****************************************************************************************************
VidCtrl.prototype.attachLocalStream_ = function() 
{
	trace('Attaching local stream.');

	// Call the polyfill wrapper to attach the media stream to this element.
	attachMediaStream(getEle(Ui.localVideo), this.localStream_);

	this.displayStatus_('');
	this.activate_(getEle(Ui.localVideo));
	this.show_(getEle(Ui.callIcons));

	if (this.localStream_.getVideoTracks().length === 0) 
	{
		this.hide_(getEle(Ui.cmdMuteVideo));
	}

	if (this.localStream_.getAudioTracks().length === 0) 
	{
		this.hide_(getEle(Ui.cmdMuteAudio));
	}
};

//****************************************************************************************************
VidCtrl.prototype.transitionToActive_ = function() 
{
	// Stop waiting for remote video.
	getEle(Ui.remoteVideo).oncanplay = undefined;
	var connectTime = window.performance.now();

	this.logPeerState();

	trace('Call setup time: ' + (connectTime - this.call_.startTime).toFixed(0) + 'ms.');
	trace('attachMediaStream: ' + getEle(Ui.localVideo).src);

	// Transition opacity from 0 to 1 for the remote and mini videos.
	this.activate_(getEle(Ui.remoteVideo));
	// Transition opacity from 1 to 0 for the local video.
	// Rotate the div containing the videos 180 deg with a CSS transform.
	this.activate_(getEle(Ui.videosDiv));
	this.show_(getEle(Ui.cmdHangup));
	this.displayStatus_('');
};

//****************************************************************************************************
VidCtrl.prototype.transitionToWaiting_ = function() 
{
	getEle(Ui.remoteVideo).oncanplay = undefined;

	this.hide_(getEle(Ui.cmdHangup));
	this.deactivate_(getEle(Ui.videosDiv));

	if (!this.remoteVideoResetTimer_) 
	{
		this.remoteVideoResetTimer_ = setTimeout(function() 
		{
			this.remoteVideoResetTimer_ = null;
			trace('Resetting remoteVideo src after transitioning to waiting.');
			getEle(Ui.remoteVideo).src = '';
		}.bind(this), 800);
	}

	this.activate_(getEle(Ui.localVideo));
	this.deactivate_(getEle(Ui.remoteVideo));
};

//****************************************************************************************************
VidCtrl.prototype.transitionToDone_ = function() 
{
	getEle(Ui.remoteVideo).oncanplay = undefined;
	this.deactivate_(getEle(Ui.localVideo));
	this.deactivate_(getEle(Ui.remoteVideo));
	this.hide_(getEle(Ui.cmdHangup));
	this.displayStatus_('');
};

//****************************************************************************************************
VidCtrl.prototype.connect = function(sTo, sSesId)
{
	console.log("session_id = " + sSesId);

	this.setTo(sTo);
	this.setSesId(sSesId);

	this.createCall_();
	this.finishCallSetup_(sSesId);

	if (this.localStream_) 
	{
		this.attachLocalStream_();
	}
};

//****************************************************************************************************
VidCtrl.prototype.login = function(sUsr, sPwd, fncOk, fncErr)
{
	if (this.getSocket() == null)
	{
		console.log("api gateway url = " + Cfg.getUrlApi());

		var pDeviceInfo = {imei:"chrome_imei", model:"chrome model", rooted:false}; 

		var pSkt = new Socket(Cfg.getUrlApi(), sUsr, sPwd, pDeviceInfo, function(pRet)
		{
			if (pRet.ret == "true")	
			{
				console.log("login success");
				this.setCallback();
				fncOk(pRet.grp, pRet.type);
			}
			else
			{
				console.log("login failed [" + JSON.stringify(pRet)+ "]");
				fncErr("login failed [" + pRet.msg + "]");
			}
		}.bind(this));
		this.setSocket(pSkt);
	}
	else
	{
		this.getSocket().login(sUsr, sPwd);	
	}
};

//****************************************************************************************************
VidCtrl.prototype.register = function(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel, fncRet)
{
	var pSkt = this.getSocket();
	if (pSkt == null)
	{
		pSkt = new Socket(Cfg.getUrlApi(), null, null, null, function(uiId, bRet, sMsg)
		{
			console.log("register id = " + uiId + " ret = " + bRet + " msg = " + sMsg);
			fncRet(uiId, bRet, sMsg);
		});
		this.setSocket(pSkt);
	}

	var fncAddUsr = function()
	{
		pSkt.addUsr(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel);
	}.bind(this);

	if (pSkt.isConnected())
		fncAddUsr();
	
	pSkt.setOnConnect(function()
	{
		fncAddUsr();
	}.bind(this));
};

//****************************************************************************************************
VidCtrl.prototype.setLUsr = function(uiId, sUsr, sLUsr, bAdd)
{
	this.p_Socket.setLUsr(uiId, sUsr, sLUsr, bAdd);
};

//****************************************************************************************************
VidCtrl.prototype.initUi = function()
{
	console.log("initUi");
	this.cmdHangup = getEle(Ui.cmdHangup);
	this.cmdRecord = getEle(Ui.cmdRecord);
	this.imgCmdRecord = getEle(Ui.imgCmdRecord);
	this.callIcons = getEle(Ui.callIcons);
	this.localVideo = getEle(Ui.localVideo);
	this.remoteVideo = getEle(Ui.remoteVideo);
	this.videosDiv = getEle(Ui.videosDiv);
};

//****************************************************************************************************
VidCtrl.prototype.setCallback = function()
{
	trace('Opening signaling channel.');

	this.p_Socket.setOnMsg(function(pMsg)
	{
		console.log("onMessage(" + JSON.stringify(pMsg) + ")");		
	});

	this.p_Socket.setOnSendMsgRet(function(uiId, bState, sData)
	{
		console.log("onMsgRet id = " + uiId + " state = " + bState + " data = " + sData);		
	});

	this.p_Socket.setOnSig(this.onSig.bind(this));

	this.p_Socket.setOnSendSigRet(function(uiId, bState, sData)
	{
		console.log("onSigRet id = " + uiId + " state = " + bState + " data = " + sData);
	});

	this.p_Socket.setOnReq(this.onReq.bind(this));

	this.p_Socket.setOnSendReqRet(function(uiId, sData)
	{
		console.log("onSendRequestRet id = " + uiId + " data = " + sData);
	});

	this.p_Socket.setOnRes(this.onRes.bind(this))

	this.p_Socket.setOnSendResRet(function(uiId, sData)
	{
		console.log("onSendResRet id = " + uiId + " data = " + sData);
	});

	this.p_Socket.setOnCmd(function(sFrom, sCmd)
	{
		console.log("on command from " + sFrom + " cmd = " + sCmd);
	});

	this.p_Socket.setOnSendCmdRet(function(uiId, sRet)
	{
		console.log("on send command ret id = " + uiId + " ret = " + sRet);
	});

	this.p_Socket.setOnCmdRes(this.onCmdRes.bind(this));	

	this.p_Socket.setOnGetRes(this.onGetRes.bind(this));

	this.p_Socket.setOnSendCmdResRet(function(uiId, sRet)
	{
		console.log("on send command response ret id = " + uiId + " ret = " + sRet);

	});

	this.p_Socket.setOnCfgStartRet(this.onCfgStartRet.bind(this));
	
	this.p_Socket.setOnCfgClsRet(this.onCfgClsRet.bind(this));

	this.p_Socket.setOnCfgTurnRet(this.onCfgTurnRet.bind(this));

	this.p_Socket.setOnUsrInfo(this.onUsrInfo.bind(this));

	this.p_Socket.setOnFileStart
	(
		function(sFrom, uiId, uiLen, uiPktSize, sMsg)
		{
			console.log("start from = " + sFrom + " file id = " + uiId + " len = " + uiLen + 
					" pkt_size = " + uiPktSize + " msg  = " + sMsg);	

			if (this.fnc_onFileStart != null)
				this.fnc_onFileStart(sFrom, uiId, uiLen, sMsg);
			else
				console.log("no on image start callback");
		}.bind(this)
	);

	this.p_Socket.setOnFileProgress
	(
		function(sFrom, uiId, uiProgress, sMsg)
		{
			console.log("file receive from = " + sFrom + " progresss " + uiProgress + " msg = " + sMsg);	

			if (this.fnc_onFileProgress != null)
				this.fnc_onFileProgress(sFrom, uiId, uiProgress, sMsg);
		}.bind(this)
	);

	this.p_Socket.setOnFile
	(
		function(sFrom, uiId, sData, sMsg)
		{
			console.log("file data length = " + sData.length);

			this.a_File.push(sData);

			console.log("on file id = " + uiId + " message = (" + sMsg+ ")");
			//var sFile = 'data:image/jpeg;base64,' + btoa(sData);

			if (this.fnc_onFile != null)
				this.fnc_onFile(sFrom, uiId, sData, sMsg);
			else
				console.log("no img callback");
		}.bind(this)
	);

	this.p_Socket.setOnLstApk(function(pData)
	{
		if (this.fnc_onLstApk != null)
			this.fnc_onLstApk(pData);
		else
			console.log("no on lst apk callback");
	}.bind(this));

	this.p_Socket.setOnHttp(function(pData)
	{
		if (this.fnc_onHttp != null)
			this.fnc_onHttp(pData);
		else
			console.log("no on http callback");
	}.bind(this));

}

//****************************************************************************************************
VidCtrl.prototype.onSig = function(sSig)
{
	if (this.call_ != null)
		this.call_.sig(sSig);
	else
		console.log("receive signal after when call_ == null");
};

//****************************************************************************************************
VidCtrl.prototype.onReq = function(iId, sFrom, sReq)
{
	console.log("id = " + iId + " from = " + sFrom + " request = " + sReq);

	if (this.fnc_onReq != null)
		this.fnc_onReq(iId, sFrom, sReq);
	else
		console.log("no on request callback");
};

//****************************************************************************************************
VidCtrl.prototype.onRes = function(iId, sFrom, sRes)
{
	console.log("on res id = " + iId + " from = " + sFrom + " response = " + sRes);

	if (this.fnc_onRes != null)
		this.fnc_onRes(iId, sFrom, sRes);
	else
		console.log("no reqponse callback");
};

//****************************************************************************************************
VidCtrl.prototype.onCmdRes = function(iId, sFrom, sRes)
{
	console.log("onCmdRes id = " + iId + " from = " + sFrom + " res = " + sRes);

	if (this.fnc_onCmdRes != null)
		this.fnc_onCmdRes(iId, sFrom, sRes);
	else
		console.log("no command response callback");
};

//****************************************************************************************************
VidCtrl.prototype.onGetRes = function(iId, sFrom, sRes)
{
	console.log("onGetRes id = " + iId + " from = " + sFrom + " res = " + sRes);

	if (this.fnc_onGetRes != null)
		this.fnc_onGetRes(iId, sFrom, sRes);
	else
		console.log("no get response callback");

};

//****************************************************************************************************
VidCtrl.prototype.onUsrInfo = function(uiId, pMsg)
{
	console.log("onUsrInfo id = " + uiId + " msg = " + JSON.stringify(pMsg));

	if (this.fnc_onUsrInfo != null)
		this.fnc_onUsrInfo(uiId, pMsg);
	else
		console.log("no user info callback");
};

//****************************************************************************************************
VidCtrl.prototype.onCfgStartRet = function(uiId, oCfgParam)
{
	console.log("on cfg start id = " + uiId + " ret " + JSON.stringify(oCfgParam));

	if (this.call_ != null)
		this.call_.cfg(oCfgParam.params);
	else
		console.log("calll_ == null");
};

//****************************************************************************************************
VidCtrl.prototype.onCfgClsRet = function(uiId, oRet)
{
	console.log("onCfgClsRet id = " + uiId + " ret = " + JSON.stringify(oRet));
};

//****************************************************************************************************
VidCtrl.prototype.onCfgTurnRet = function(uiId, pTurn)
{
	console.log("onCfgTurn id = " + uiId + " turn = " + JSON.stringify(pTurn));

	if (this.call_ != null)
		this.call_.turn(pTurn.iceServers);
	else
		console.log("call_ == null");
};

//****************************************************************************************************
VidCtrl.prototype.onCallStart = function(sSesId) 
{
	console.log("displaySharingInfo ses_id = " + sSesId + "\n");
};

//****************************************************************************************************
VidCtrl.prototype.displayStatus_ = function(sStatus) 
{
	console.log("status = (" + sStatus + ")");
};

//****************************************************************************************************
VidCtrl.prototype.displayError_ = function(error) 
{
	trace(error);
	console.log("ERROR VidCtrl : " + error);
};

//****************************************************************************************************
VidCtrl.prototype.toggleAudioMute_ = function() 
{
	this.call_.toggleAudioMute();
};

//****************************************************************************************************
VidCtrl.prototype.toggleVideoMute_ = function() 
{
	this.call_.toggleVideoMute();
};

//****************************************************************************************************
VidCtrl.prototype.hide_ = function(element) 
{
	if (isSet(element) && isSet(element.classList))
		element.classList.add('hidden');
	else
		console.log("invalid element");
};

//****************************************************************************************************
VidCtrl.prototype.show_ = function(element) 
{ 
	if (isSet(element) && isSet(element.classList))
		element.classList.remove('hidden');
	else
		console.log("invalid element");
};

//****************************************************************************************************
VidCtrl.prototype.activate_ = function(element) 
{
	if (isSet(element) && isSet(element.classList))
		element.classList.add('active');
	else 
		console.log("invalid element");
};

//****************************************************************************************************
VidCtrl.prototype.deactivate_ = function(element) 
{
	if (isSet(element) && isSet(element.classList))
		element.classList.remove('active');
	else 
		console.log("invalid element");
};

//****************************************************************************************************
VidCtrl.prototype.showCtrl = function() 
{
	if (!getEle(Ui.callIcons).classList.contains('active')) 
		this.activate_(getEle(Ui.callIcons));
};

//****************************************************************************************************
VidCtrl.prototype.loadUrlParams_ = function() 
{
	var DEFAULT_VIDEO_CODEC = 'VP9';
	var urlParams = queryStringToDictionary(window.location.search);
	this.loadingParams_.audioSendBitrate = urlParams['asbr'];
	this.loadingParams_.audioSendCodec = urlParams['asc'];
	this.loadingParams_.audioRecvBitrate = urlParams['arbr'];
	this.loadingParams_.audioRecvCodec = urlParams['arc'];
	this.loadingParams_.opusMaxPbr = urlParams['opusmaxpbr'];
	this.loadingParams_.opusFec = urlParams['opusfec'];
	this.loadingParams_.opusDtx = urlParams['opusdtx'];
	this.loadingParams_.opusStereo = urlParams['stereo'];
	this.loadingParams_.videoSendBitrate = urlParams['vsbr'];
	this.loadingParams_.videoSendInitialBitrate = urlParams['vsibr'];
	this.loadingParams_.videoSendCodec = urlParams['vsc'];
	this.loadingParams_.videoRecvBitrate = urlParams['vrbr'];
	this.loadingParams_.videoRecvCodec = DEFAULT_VIDEO_CODEC;
};

//****************************************************************************************************
VidCtrl.prototype.sendReq = function(iId, sSesId, sTo, sReq)
{
	this.getSocket().sendReq(iId, sSesId, sTo, sReq, 30);
};

//****************************************************************************************************
VidCtrl.prototype.sendRes = function(iId, sSesId, sTo, sRes)
{
	this.getSocket().sendRes(iId, sSesId, sTo, sRes, 30);
};

//****************************************************************************************************
VidCtrl.prototype.sendCmd = function(iId, sSesId, sTo, sCmd)
{
	this.getSocket().sendCmd(iId, sSesId == null ? this.getSesId() : sSesId, sTo, sCmd, 30);
};

//****************************************************************************************************
VidCtrl.prototype.sendCmdRes = function(iId, sTo, sRes)
{
	this.getSocket().sendCmdRes(iId, this.getSesId(), sTo, sRes, 30);
};

//****************************************************************************************************
VidCtrl.prototype.sendGet = function(uiId, sSesId, sTo, sMsg)
{
	this.getSocket().sendGet(uiId, sSesId, sTo, sMsg, 30);
};

//****************************************************************************************************
VidCtrl.prototype.sendFile = function(sTo, sSesId, pFile, sMsg, fncProgress)
{
	this.getSocket().sendFile(sTo, sSesId, pFile, sMsg, fncProgress);
};

//****************************************************************************************************
VidCtrl.prototype.addApk = function(uiId, sSesId, pFile, sDesc, fncProgress)
{
	this.getSocket().addApk(uiId, sSesId, pFile, sDesc, fncProgress);
};

//****************************************************************************************************
VidCtrl.prototype.getLstApk = function(uiId, sSesId)
{
	this.getSocket().getLstApk(uiId, sSesId);
}

//****************************************************************************************************
VidCtrl.prototype.installApk = function(uiId, sSesId, sTo, uiExpireTime, sApk)
{
	this.getSocket().installApk(uiId, sSesId, sTo, uiExpireTime, sApk);
};

//****************************************************************************************************
VidCtrl.prototype.downloadFile = function()
{
	var pZip = new JSZip();

	for (var iIdx = 0; iIdx < this.a_File.length; iIdx ++)
	{
		var sFile = this.a_File[iIdx];
		var sFile = "img_" + iIdx + ".jpg";
		console.log("img_file = " + sFile + " length = " + sFile.length);
		pZip.file(sFile, sFile, {binary:true});			
	}

	console.log("generating zip file ...");
	pZip.generateAsync({type:"blob"}).then(function(pBlob)
	{
		console.log("zip file has been generated");
		download(pBlob, "receaved_images.zip");
	}.bind(this));
};

//****************************************************************************************************
VidCtrl.prototype.saveRecording = function()
{
	console.log("save recording ");
	this.p_Recorder.getDataURL(function(sUrl)
	{
		console.log("url has been received");
		console.log("url = " + sUrl.length);

		download(this.p_Recorder.getBlob(), "ReceivedVideo.webm", "image/gif");

	}.bind(this));
};

//****************************************************************************************************
VidCtrl.prototype.http = function(uiId, sSrv, sPath, pData)
{
	this.getSocket().http(uiId, sSrv, sPath, pData);
};

//****************************************************************************************************
VidCtrl.prototype.logoff = function()
{
	this.getSocket().logoff();
};

//****************************************************************************************************
VidCtrl.prototype.logPeerState = function()
{
	this.call_.getPeerConnectionStats(function(oState) 
	{
		console.log("peer connection status " + JSON.stringify(oState));
	});
};

//****************************************************************************************************
VidCtrl.IconSet_ = function(iconSelector) 
{
	this.iconElement = document.querySelector(iconSelector);
};

//****************************************************************************************************
VidCtrl.prototype.setSocket = function(pSocket)
{
	this.p_Socket = pSocket;
};

//****************************************************************************************************
VidCtrl.prototype.getSocket = function()
{
	return this.p_Socket;
};

//****************************************************************************************************
VidCtrl.prototype.setSesId = function(sSesId)
{
	console.log("setSesId(" + sSesId + ")");
	this.s_SesId = sSesId; 
};

//****************************************************************************************************
VidCtrl.prototype.getSesId = function()
{
	return this.s_SesId; 
};

//****************************************************************************************************
VidCtrl.prototype.setTo = function(sTo)
{
	this.s_To = sTo;
};

//****************************************************************************************************
VidCtrl.prototype.getTo = function()
{
	return this.s_To;
};

//****************************************************************************************************
VidCtrl.prototype.setOnReq = function(fncOnReq)
{
	this.fnc_onReq = fncOnReq;
};

//****************************************************************************************************
VidCtrl.prototype.setOnRes = function(fncOnRes)
{
	this.fnc_onRes = fncOnRes;
};

//****************************************************************************************************
VidCtrl.prototype.setOnCmdRes = function(fncOnRes)
{
	this.fnc_onCmdRes = fncOnRes;
};

//****************************************************************************************************
VidCtrl.prototype.setOnGetRes = function(fncOnGetRes)
{
	this.fnc_onGetRes = fncOnGetRes;
};

//****************************************************************************************************
VidCtrl.prototype.setOnUsrInfo = function(fncOnUsrInfo)
{
	this.fnc_onUsrInfo = fncOnUsrInfo;
};

//****************************************************************************************************
VidCtrl.prototype.setOnFileStart = function(fncOnFileStart)
{
	this.fnc_onFileStart = fncOnFileStart;
};

//****************************************************************************************************
VidCtrl.prototype.setOnFileProgress = function(fncOnFileProgress) 
{
	this.fnc_onFileProgress = fncOnFileProgress;
};

//****************************************************************************************************
VidCtrl.prototype.setOnFile = function(fncOnFile)
{
	this.fnc_onFile = fncOnFile;
};

//****************************************************************************************************
VidCtrl.prototype.setOnLocalHangup = function(fncOnHangup)
{
	this.fnc_onLocalHangup = fncOnHangup;
};

//****************************************************************************************************
VidCtrl.prototype.setOnRemoteHangup = function(fncOnHangup)
{
	this.fnc_onRemoteHangup = fncOnHangup;
};

//****************************************************************************************************
VidCtrl.prototype.setOnLstApk = function(fncOnLstApk)
{
	this.fnc_onLstApk = fncOnLstApk;
};

//****************************************************************************************************
VidCtrl.prototype.setOnHttp = function(fncOnHttp)
{
	this.fnc_onHttp = fncOnHttp;
};
