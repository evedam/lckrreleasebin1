
function Socket(sUrl, sUsr, sPwd, pDeviceInfo, fncRet)
{
	//****************************************************************************************************
	var State = 
	{
		Init:1,
		Connected:2,
		Logged:3,
		Err:4
	};

	//****************************************************************************************************
	function Msg(sAct, pData)
	{
		this.setAct 	= function(sAct) 	{ this.s_Act = sAct;		};		
		this.getAct 	= function()		{ return this.s_Act;		};
		this.setData	= function(aData)	{ this.a_Data = aData;		};
		this.getData	= function()		{ return this.a_Data;		};
		this.getCntData = function()		{ return this.a_Data.length;	};
		this.getData	= function(iIdx)	{ return this.a_Data[iIdx];	};

		this.setAct(sAct);
		this.setData(pData); 
	}

	//****************************************************************************************************
	function LocalFile(sTo, sSesId, sData, sMsg, fnOnProgress)
	{
		this.setTo = function(sTo)			{ this.s_To = sTo; 			}	
		this.getTo = function()				{ return this.s_To;			}
		this.setSesId = function(sSesId)		{ this.s_SesId = sSesId;		}
		this.getSesId = function()			{ return this.s_SesId;			}
		this.setData = function(sData)			{ this.s_Data = sData; 			} 
		this.getData = function() 			{ return this.s_Data;			}
		this.getLen = function()			{ return this.s_Data.length;		}
		this.setMsg = function(sMsg)			{ this.s_Msg = sMsg; 			}
		this.getMsg = function()			{ return this.s_Msg; 			}
		this.setOnProgress = function(fnOnProgress)	{ this.fn_OnProgress = fnOnProgress;	}
		this.getOnProgress = function()			{ return this.fn_OnProgress;		}
		this.setPktSize = function(uiPktSize)		{ this.ui_PktSize = uiPktSize;		}
		this.getPktSize = function()			{ return this.ui_PktSize;		}

		this.setTo(sTo);
		this.setSesId(sSesId);
		this.setData(sData);
		this.setMsg(sMsg);
		this.setOnProgress(fnOnProgress);
		this.setPktSize(sData.length < 1024 ? sData.length : 1024);		
	}

	//****************************************************************************************************
	function RmtFile(sFrom, uiId, uiLen, uiPktSize, sMsg)
	{
		this.isDone = function()
		{
			return this.getProgress() == 100;
		};

		this.setData = function(uiIdx, sData)
		{
			this.ui_Cnt ++;
			this.a_Data[uiIdx] = sData;	
		};

		this.getData = function()
		{
			var sData = "";
			var uiLen = this.a_Data.length;
			for (var uiIdx = 0; uiIdx < uiLen; uiIdx ++)
				sData += this.a_Data[uiIdx];	
			return sData;
		};

		this.getProgress = function()
		{
			return ((this.ui_Cnt / (((this.ui_Len + this.ui_PktSize - 1) / this.ui_PktSize) | 0)) * 100) | 0; 
		};

		this.setFrom = function(sFrom)		{ this.s_From = sFrom;		};
		this.getFrom = function()		{ return this.s_From;		};
		this.setId = function(uiId)		{ this.ui_Id = uiId;		};
		this.getId = function()			{ return this.ui_Id;		};
		this.setLen = function(uiLen)		{ this.ui_Len = uiLen;		};
		this.getLen = function()		{ return this.ui_Len;		};
		this.setPktSize = function(uiSize)	{ this.ui_PktSize = uiSize;	};
		this.getPktSize = function()		{ return this.ui_PktSize;	};
		this.setMsg = function(sMsg)		{ this.s_Msg = sMsg;		};
		this.getMsg = function()		{ return this.s_Msg;		};

		this.setFrom(sFrom);
		this.setId(uiId);
		this.setLen(uiLen);
		this.setPktSize(uiPktSize);
		this.setMsg(sMsg);

		this.a_Data = [];
		this.ui_Cnt = 0;
	}

	//****************************************************************************************************
	var s_UserName = sUsr;
	var s_Password = sPwd;
	var p_DeviceInfo = pDeviceInfo;
	var s_Url = sUrl;
	var p_Socket = io(s_Url);	
	var e_State = State.Init;
	var q_Msg = [];
	var a_LocalFile = [];
	var m_RmtFile = {};

	var fn_OnConnect = null;
	var fn_OnMsg = null;
	var fn_OnSendMsgRet = null;
	var fn_OnSig = null;
	var fn_OnSendSigRet = null;
	var fn_OnReq = null;
	var fn_OnSendReqRet = null;
	var fn_OnRes = null;
	var fn_OnSendResRet = null;
	var fn_OnCmd = null;
	var fn_OnSendCmdRet = null;
	var fn_OnCmdRes = null;
	var fn_OnSendCmdResRet = null;
	var fn_OnSendGetRet = null;
	var fn_OnGetRes = null;
	var fn_OnCfgStartRet = null;
	var fn_OnCfgClsRet = null;
	var fn_OnCfgTurnRet = null;
	var fn_OnUsrInfo = null;
	var fn_OnFileStart = null;
	var fn_OnFileProgress = null;
	var fn_OnFile = null;

	var fn_OnLstApk = null;
	
	var fn_OnHttp = null;

	//****************************************************************************************************
	this.addUsr = function(uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel)
	{
		console.log("Socket::addUsr add user id = " + uiId + " usr = " + sUsr + " pwd = " + sPwd + " grp = " + sGrp + 
			" type = " + sType + " name = " + sName + " email = " + sEmail + " tel = " + sTel);
		p_Socket.emit("usr_add", uiId, sUsr, sPwd, sGrp, sType, sName, sEmail, sTel);
	};

	//****************************************************************************************************
	this.setLUsr = function(uiId, sUsr, sLUsr, bAdd)
	{
		console.log("setLUsr id = " + uiId + " usr = " + sUsr + " lusr = " + sLUsr + " add = " + bAdd);
		p_Socket.emit("usr_set_lusr", uiId, sUsr, sLUsr, bAdd);
	};

	//****************************************************************************************************
	this.addSes = function(sSesId)
	{
		console.log("add session (" + sSesId + ")");
		send("add_ses", [sSesId]);
	};

	//****************************************************************************************************
	this.login = function(sUsr, sPwd)
	{
		s_UserName = sUsr;
		s_Password = sPwd;
		login();
	};

	//****************************************************************************************************
	this.logoff = function()
	{
		send("logoff");
	};

	//****************************************************************************************************
	this.sendMsg = function(uiId, sSesId, sTo, pMsg)
	{
		console.log("sending message id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + " msg = " + JSON.stringify(pMsg));
		send("send_msg", [uiId, sSesId, sTo, pMsg]);
	};

	//****************************************************************************************************
	this.sendSig = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending signal id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_sig", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.sendReq = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending request id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_req", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.sendRes = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending response id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_res", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.sendCmd = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending command id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_cmd", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.sendCmdRes = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending command response id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_cmd_res", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.sendGet = function(uiId, sSesId, sTo, pMsg, uiExpireTime)
	{
		console.log("sending get id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
			" msg = " + JSON.stringify(pMsg) + " expire_time = " + uiExpireTime);
		send("send_get", [uiId, sSesId, sTo, pMsg, uiExpireTime]);
	};

	//****************************************************************************************************
	this.cfgStart = function(uiId, sSesId, uiExpireTime)
	{
		console.log("sending cfg start id = " + uiId + " ses_id = " + sSesId + " expire_time = " + uiExpireTime);
		send("cfg_start", [uiId, sSesId, uiExpireTime]);
	};

	//****************************************************************************************************
	this.cfgCls = function(uiId, sSesId, uiExpireTime)
	{
		console.log("sending cfg cls id = " + uiId + " ses_id = " + sSesId + " expire_time = " + uiExpireTime);
		send("cfg_cls", [uiId, sSesId, uiExpireTime]); 
	};

	//****************************************************************************************************
	this.cfgTurn = function(uiId, uiExpireTime)
	{
		console.log("sending cfg turn id = " + uiId + " expire_time = " + uiExpireTime);
		send("cfg_turn", [uiId, uiExpireTime]); 
	};

	//****************************************************************************************************
	this.usrGetLst = function(uiId, sType, sState, uiIdx, uiCnt, bSubscripe)
	{
		console.log("get user list id = " + uiId + " subscripe = " + bSubscripe);
		send("usr_get_lst", [uiId, sType, sState, uiIdx, uiCnt, bSubscripe, false]);
	};

	//****************************************************************************************************
	this.isConnected = function()
	{
		return e_State == State.Connected;
	};

	//****************************************************************************************************
	this.setOnConnect = function(fnOnConnect)
	{
		fn_OnConnect = fnOnConnect;
	};

	//****************************************************************************************************
	this.setOnMsg = function(fnOnMsg)
	{
		fn_OnMsg = fnOnMsg;
	};

	//****************************************************************************************************
	this.setOnSendMsgRet = function(fnOnSendMsgRet)
	{
		fn_OnSendMsgRet = fnOnSendMsgRet;	
	};

	//****************************************************************************************************
	this.setOnSig = function(fnOnSig)
	{
		fn_OnSig = fnOnSig;
	};

	//****************************************************************************************************
	this.setOnSendSigRet = function(fnOnSendSigRet)
	{
		fn_OnSendSigRet = fnOnSendSigRet;
	}; 
	
	//****************************************************************************************************
	this.setOnReq = function(fnOnReq)
	{
		fn_OnReq = fnOnReq;
	};

	//****************************************************************************************************
	this.setOnSendReqRet = function(fnOnSendReqRet)
	{
		fn_OnSendReqRet = fnOnSendReqRet;
	};

	//****************************************************************************************************
	this.setOnRes = function(fnOnRes)
	{
		fn_OnRes = fnOnRes;
	};

	//****************************************************************************************************
	this.setOnSendResRet = function(fnOnSendResRet)
	{
		fn_OnSendResRet = fnOnSendResRet;
	};

	//****************************************************************************************************
	this.setOnCmd = function(fnOnCmd)
	{
		fn_OnCmd = fnOnCmd;
	};

	//****************************************************************************************************
	this.setOnSendCmdRet = function(fnOnSendCmdRet)
	{
		fn_OnSendCmdRet = fnOnSendCmdRet;
	};

	//****************************************************************************************************
	this.setOnCmdRes = function(fncOnCmdRes)
	{
		fn_OnCmdRes = fncOnCmdRes;
	};

	//****************************************************************************************************
	this.setOnSendCmdResRet = function(fncOnCmdResRet)
	{
		fn_OnSendCmdResRet = fncOnCmdResRet;
	};

	//****************************************************************************************************
	this.setOnSendGetRet = function(fncOnSendGetRet)
	{
		fn_OnSendGetRet = fncOnSendGetRet;
	};

	//****************************************************************************************************
	this.setOnGetRes = function(fncOnGetRes)
	{
		fn_OnGetRes = fncOnGetRes;
	};

	//****************************************************************************************************
	this.setOnCfgStartRet = function(fncOnCfgStartRet)
	{
		fn_OnCfgStartRet = fncOnCfgStartRet;
	};

	//****************************************************************************************************
	this.setOnCfgClsRet = function(fncOnCfgClsRet)
	{
		fn_OnCfgClsRet = fncOnCfgClsRet;
	};

	//****************************************************************************************************
	this.setOnCfgTurnRet = function(fncOnCfgTurnRet)
	{
		fn_OnCfgTurnRet = fncOnCfgTurnRet;
	};

	//****************************************************************************************************
	this.setOnUsrInfo = function(fncOnUsrInfo)
	{
		fn_OnUsrInfo = fncOnUsrInfo;
	};

	//****************************************************************************************************
	function sendMsgQueue()
	{
		while (q_Msg.length > 0 && e_State == State.Logged)	
		{
			var pMsg = q_Msg.shift();	

			switch (pMsg.getCntData())
			{
			case 0:
				p_Socket.emit(pMsg.getAct());
				break;
			case 1:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0));
				break;
			case 2:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1)); break;
			case 3:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2));
				break;
			case 4:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3));
				break;
			case 5:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4));
				break;
			case 6:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5));
				break;
			case 7:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6));
				break;
			case 8:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7));
				break;
			case 9:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8));
				break;
			case 10:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(4),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9));
				break;
			case 11:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10));
				break;
			case 12:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10),
					pMsg.getData(11));
				break;
			case 13:
				p_Socket.emit(pMsg.getAct(), pMsg.getData(0), pMsg.getData(1), pMsg.getData(2), pMsg.getData(3), pMsg.getData(5),
					pMsg.getData(5), pMsg.getData(6), pMsg.getData(7), pMsg.getData(8), pMsg.getData(9), pMsg.getData(10),
					pMsg.getData(11), pMsg.getData(12));
				break;
			}
		}
	}

	//****************************************************************************************************
	function queue(pMsg)
	{
		q_Msg.push(pMsg);	
		sendMsgQueue();
	}
	
	//****************************************************************************************************
	function send(sAct, aData)
	{
		queue(new Msg(sAct, aData == undefined ? [] : aData));
	}

	//****************************************************************************************************
	function login()
	{
		p_Socket.emit("login", s_UserName, s_Password, p_DeviceInfo);
	}

	//****************************************************************************************************
	p_Socket.on("connect", function()
	{
		console.log("on connect ");
		console.log("Stete = " + State.Connected);
		
		e_State = State.Connected;

		if (fn_OnConnect != null)
			fn_OnConnect();
		else
			console.log("no connect callback ");

		if (s_UserName != "" && s_UserName != null)
			login();
	});

	//****************************************************************************************************
	p_Socket.on("disconnect", function()
	{
		console.log("on disconnect");
		e_State = State.Init;
	});

	//****************************************************************************************************
	p_Socket.on("usr_add_ret", function(pRet)
	{
		console.log("ret = " + JSON.stringify(pRet));
		fncRet(pRet.id, pRet.ret == "true", pRet.msg);
	});

	//****************************************************************************************************
	p_Socket.on("login_ret", function(pRet)
	{
		console.log("login response (" + JSON.stringify(pRet) + ")");	

		if (pRet.ret == "true")
		{
			e_State = State.Logged;
			sendMsgQueue();
		}

		fncRet(pRet);
	});

	//****************************************************************************************************
	p_Socket.on("msg", function(pMsg)
	{
		console.log("on message " + JSON.stringify(pMsg));

		if (fn_OnMsg != null)
			fn_OnMsg(pMsg.p_Msg);
		else
			console.log("no messsage callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_msg_ret", function(pMsg)
	{
		console.log("on send msg ret " + JSON.stringify(pMsg));

		if (fn_OnSendMsgRet != null)
			fn_OnSendMsgRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send message ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("sig", function(pMsg)
	{
		console.log("on signal " + JSON.stringify(pMsg));

		if (fn_OnSig != null)
			fn_OnSig(pMsg.p_Msg);
		else
			console.log("no signal callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_sig_ret", function(pMsg)
	{
		console.log("on send signal ret " + JSON.stringify(pMsg));

		if (fn_OnSendSigRet != null)
			fn_OnSendSigRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send signal ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("req", function(pMsg)
	{
		console.log("on request " + JSON.stringify(pMsg));

		if (fn_OnReq != null)
			fn_OnReq(pMsg.i_Id, pMsg.s_From, pMsg.p_Msg);
		else
			console.log("no request callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_req_ret", function(pMsg)
	{
		console.log("on send request ret " + JSON.stringify(pMsg));

		if (fn_OnSendReqRet != null)
			fn_OnSendReqRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send request ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("res", function(pMsg)
	{
		console.log("on response " + JSON.stringify(pMsg));

		if (fn_OnRes != null)
			fn_OnRes(pMsg.i_Id, pMsg.s_From, pMsg.p_Msg);
		else
			console.log("no response callback");
	});
	
	//****************************************************************************************************
	p_Socket.on("send_res_ret", function(pMsg)
	{
		console.log("on send response ret " + JSON.stringify(pMsg));		
	
		if (fn_OnSendResRet != null)
			fn_OnSendResRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send response ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("cmd", function(pMsg)
	{
		console.log("on command " + JSON.stringify(pMsg));

		if (fn_OnCmd != null)
			fn_OnCmd(pMsg.s_From, pMsg.p_Msg);
		else
			console.log("no command callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_cmd_ret", function(pMsg)
	{
		console.log("on send command ret " + JSON.stringify(pMsg));

		if (fn_OnSendCmdRet != null)
			fn_OnSendCmdRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send command ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("cmd_res", function(pMsg)
	{
		console.log("on command response " + JSON.stringify(pMsg));

		if (fn_OnCmdRes != null)
			fn_OnCmdRes(pMsg.i_Id, pMsg.s_From, pMsg.p_Msg);
		else
			console.log("no command response callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_cmd_res_ret", function(pMsg)
	{
		console.log("on command response ret " + JSON.stringify(pMsg));	

		if (fn_OnSendCmdResRet != null)
			fn_OnSendCmdResRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no send command response ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("send_get_ret", function(pMsg)
	{
		console.log("on get ret " + JSON.stringify(pMsg));

		if (fn_OnSendGetRet != null)
			fn_OnSendGetRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no get ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("get_res", function(pMsg)
	{
		console.log("on get res " + JSON.stringify(pMsg));

		if (fn_OnGetRes != null)
			fn_OnGetRes(pMsg.i_Id, pMsg.s_From, pMsg.p_Msg);
		else
			console.log("no get res callback");
	});
		
	//****************************************************************************************************
	p_Socket.on("cfg_start_ret", function(pMsg)
	{
		console.log("on cfg start ret " + JSON.stringify(pMsg));

		if (fn_OnCfgStartRet != null)
			fn_OnCfgStartRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no cfg start ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("cfg_cls_ret", function(pMsg)
	{
		console.log("on cfg start ret " + JSON.stringify(pMsg));

		if (fn_OnCfgClsRet != null)
			fn_OnCfgClsRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no cfg cls ret callback");
	});

	//****************************************************************************************************
	p_Socket.on("cfg_turn_ret", function(pMsg)
	{
		console.log("on cfg turn ret " + JSON.stringify(pMsg));
	
		if (fn_OnCfgTurnRet != null)
			fn_OnCfgTurnRet(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no cfg turn callback");
	});

	//****************************************************************************************************
	p_Socket.on("usr_info", function(pMsg)
	{
		console.log("on user info " + JSON.stringify(pMsg));

		if (fn_OnUsrInfo != null)
			fn_OnUsrInfo(pMsg.i_Id, pMsg.p_Msg);
		else
			console.log("no usr info callback");
	});

	//****************************************************************************************************
	p_Socket.on("usr_set_lusr_ret", function(pMsg)
	{
		console.log("usr_set_lusr_ret " + JSON.stringify(pMsg));	
	});

	//****************************************************************************************************
	p_Socket.on("error", function(sMsg)
	{
		console.log("on error [" + sMsg + "]");
		e_State = State.Err;
	});

	//****************************************************************************************************
	p_Socket.on("err", function(sMsg)
	{
		console.log("on error [" + sMsg + "]");	
	});

	//Send file

	//****************************************************************************************************
	p_Socket.on("send_start_file_ret", function(pData)
	{
		var pMsg = pData.p_Msg;
		console.log("json = " + JSON.stringify(pData));
		console.log("start send file bin id = " + pMsg.id + " usr_data = " + pMsg.usr_data);	
		
		if (pMsg.id >= 0)
			continueSendFile(pMsg.id, pMsg.s_SesId, pMsg.usr_data);
		else
			console.log("unable to start send file [" + pData.err + "]");

	});

	// Apk

	//****************************************************************************************************
	p_Socket.on("apk_file_start_ret", function(pData)
	{
		console.log("apk_file_start_ret = " + JSON.stringify(pData));
		var pMsg = pData.p_Msg;
		continueAddApk(pData.i_Id, pData.s_SesId, pMsg.usr_data);
	});

	//****************************************************************************************************
	p_Socket.on("apk_get_lst_ret", function(iState, pData)
	{
		console.log("apk_get_lst_ret state = " + iState + " date = " + JSON.stringify(pData));
	});

	//****************************************************************************************************
	p_Socket.on("apk_lst", function(pData)
	{
		console.log("apk_lst " + JSON.stringify(pData));

		if (fn_OnLstApk != null)
			fn_OnLstApk(JSON.parse(pData.p_Msg));
		else
			console.log("no lst apk callback");
	});

	// Http

	//****************************************************************************************************
	p_Socket.on("http", function(pData)
	{
		console.log("http response " + JSON.stringify(pData));

		if (fn_OnHttp != null)
			fn_OnHttp(pData);
		else
			console.log("no http callback");
		
	}.bind(this));	

	//****************************************************************************************************
	function sendFileBinary(sTo, uiFileId, sSesId, iIdx, sData, uiLen)
	{
		send("send_file_bin", [sTo, uiFileId, sSesId, iIdx, sData, uiLen, 60]);

		console.log("sending binary to (" + sTo + ")" + iIdx + " length = " + sData.length);
	}

	//****************************************************************************************************
	function startSendFile(pFileData, uiIdx)
	{
		send("send_start_file", [uiIdx, pFileData.getTo(), pFileData.getSesId(), pFileData.getLen(), pFileData.getPktSize(), 
			pFileData.getMsg(), uiIdx]);
		console.log("start sending file");
	}

	//****************************************************************************************************
	this.sendFile = function(sTo, sSesId, pFile, sMsg, fnOnProgress)
	{
		if (!isSet(sTo))
			throw "invalid to user name";
		
		if (!isSet(pFile))
			throw "invalid pFile";	

		if (!isSet(sMsg))
			sMsg = "";

		if (!isSet(fnOnProgress))
			throw "invalid onProgress function";

		var pReader = new FileReader();
		pReader.onload = function(pE)
		{
			var sContent = pE.target.result;
			var pData = new LocalFile(sTo, sSesId, sContent, sMsg, fnOnProgress);
			a_LocalFile.push(pData);	
			startSendFile(pData, a_LocalFile.length - 1); 
		}

		pReader.readAsBinaryString(pFile);
	};

	//****************************************************************************************************
	function continueSendFile(uiFileId, sSesId, iIdxFileData)
	{
		var pFileData = a_LocalFile[iIdxFileData];
		var sFileContent = pFileData.getData();
		var fnOnProgress = pFileData.getOnProgress();
		var uiLen = sFileContent.length;
		var uiPktSize = pFileData.getPktSize();
		var sTo = pFileData.getTo();

		for (var iIdx = 0, iCnt = 0; iIdx < uiLen; iIdx += uiPktSize, iCnt++)
		{
			var sData = sFileContent.substring(iIdx, iIdx + uiPktSize);
			sendFileBinary(sTo, uiFileId, sSesId, iCnt, sData, sData.length);
		
			iProgress = 100 * (((iCnt + 1) * uiPktSize) / uiLen);
			fnOnProgress(iProgress | 0);
		} 
	}

	// apk 

	//****************************************************************************************************
	function addApkBinary(uiFileId, sSesId, iIdx, sData, uiLen)
	{
		send("apk_file_bin", [uiFileId, sSesId, iIdx, sData, uiLen, 60]);

		console.log("adding apk bin idx = " + iIdx + " length = " + sData.length);
	}

	//****************************************************************************************************
	function startAddApkFile(uiId, pFileData, uiIdx)
	{
		send("apk_file_start", [uiId, pFileData.getSesId(), pFileData.getLen(), pFileData.getPktSize(), pFileData.getMsg(), uiIdx]);
		console.log("start apk file");
	}

	//****************************************************************************************************
	this.addApk = function(uiId, sSesId, pFile, sDesc, fnOnProgress)
	{
		console.log("id = " + uiId + " ses_id = " + sSesId + " file = " + pFile + " desc = " + sDesc);

		if (!isSet(pFile))
			throw "please enter valid file";	

		if (!isSet(sDesc))
			throw "please enter valid description";

		if (!isSet(fnOnProgress))
			throw "please enter valid progress callback";

		var pReader = new FileReader();
		pReader.onload = function(pE)
		{
			var sContent = pE.target.result;
			var pData = new LocalFile("", sSesId, sContent, sDesc, fnOnProgress);
			a_LocalFile.push(pData);	
			startAddApkFile(uiId, pData, a_LocalFile.length - 1); 
		}

		pReader.readAsBinaryString(pFile);
	};

	//****************************************************************************************************
	function continueAddApk(uiId, sSesId, iIdxFileData)
	{
		console.log("id = " + uiId + " ses_id = " + sSesId + " idx_file_data = " + iIdxFileData);

		var pFileData = a_LocalFile[iIdxFileData];
		var sFileContent = pFileData.getData();
		var fnOnProgress = pFileData.getOnProgress();
		var uiLen = sFileContent.length;
		var uiPktSize = pFileData.getPktSize();

		var iIdx = 0;
		var iCnt = 0;
		var pInterval = setInterval(function()
		{
			for (var i = 0; iIdx < uiLen && i < 100; i++)
			{
				var sData = sFileContent.substring(iIdx, iIdx + uiPktSize);
				addApkBinary(uiId, sSesId, iCnt, sData, sData.length);
		
				iProgress = 100 * (((iCnt + 1) * uiPktSize) / uiLen);
				fnOnProgress(iProgress | 0);

				iIdx += uiPktSize;
				iCnt++;

				if (iIdx == uiLen - 1)
					cleanInterval(pInterval);
			}

		}.bind(this), 100);
	}

	//****************************************************************************************************
	this.installApk = function(uiId, sSesId, sTo, uiExpireTime, sApk)
	{
		console.log("apk_install id = " + uiId + " ses_id = " + sSesId + " to = " + sTo + 
				" expire_time = " + uiExpireTime + " apk = " + sApk);
		send("apk_install", [uiId, sSesId, sTo, uiExpireTime, sApk]);	
	};

	//****************************************************************************************************
	this.getLstApk = function(uiId, sSesId)
	{
		console.log("getLstApk " + uiId + " ses_id = " + sSesId);

		send("apk_get_lst", [uiId, sSesId]);
	};

	// Http 

	//****************************************************************************************************
	this.http = function(uiId, sSrv, sPath, pData)
	{
		if (!isSet(uiId))
		{
			console.log("please enter valid id");
			return;
		}

		if (!isSet(sSrv))
		{
			console.log("please enter valid srvive name");
			return;
		}

		if (!isSet(sPath))
		{
			console.log("please enter valid path");
			return;
		}

		if (!isSet(pData))
		{
			console.log("please enter valid data");
			return;
		}

		console.log("http id = " + uiId + " srv = " + sSrv + " path = " + sPath + " data = " + JSON.stringify(pData));

		send("http", [uiId, sSrv, sPath, pData]);
	};

	// Receive file

	//****************************************************************************************************
	p_Socket.on("start_file", function(pMsg)
	{
		console.log("start file " + pMsg + " str = (" + JSON.stringify(pMsg) + ")");

		var sFrom = pMsg.s_From;
		var pFileData = pMsg.p_Msg;
		var uiFileId = pFileData.id;
		var uiLen = pFileData.len;
		var uiPktSize = pFileData.pkt_size;
		var sMsg = pFileData.msg;

		m_RmtFile[uiFileId] = new RmtFile(sFrom, uiFileId, uiLen, uiPktSize, sMsg);	
		
		fn_OnFileStart(sFrom, uiFileId, uiLen, uiPktSize, sMsg);

		console.log("start file from = " + sFrom +" id = " + uiFileId + " len = " + uiLen + 
			" pkt_size = " + uiPktSize + " message = " + sMsg);
	});

	//****************************************************************************************************
	p_Socket.on("file_bin", function(pMsg)
	{
		var sFrom = pMsg.s_From;
		var pData = pMsg.p_Msg;
		var uiFileId = pData.id;
		var uiIdx = pData.idx;
		var uiLen = pData.len;

		var pRmtFile = m_RmtFile[uiFileId];	
		if (!isSet(pRmtFile))
		{
			console.log("on::file_bin invalid file id " + uiFileId);
			return;
		}

		pRmtFile.setData(uiIdx, pData.data);
		var sMsg = pRmtFile.getMsg();

		console.log("rmt file len = " + pRmtFile.getLen() + " pkt_size = " + pRmtFile.getPktSize());

		var iProgress = pRmtFile.getProgress(); 
		fn_OnFileProgress(sFrom, uiFileId, iProgress, sMsg);

		console.log("on file bin from = " + sFrom + " fild_id = " + uiFileId + " idx = " + uiIdx + 
			" len = " + uiLen + " progress = " + iProgress); 

		if (pRmtFile.isDone())
		{
			console.log("file receive complete id = " + pRmtFile.getId());
			fn_OnFile(sFrom, pRmtFile.getId(), pRmtFile.getData(), sMsg);		
		}
	});

	// Apk
	//****************************************************************************************************
	p_Socket.on("install_apk_ret", function(uiId, pMsg)
	{
		console.log("install_apk ret id = " + uiId + " msg = " + JSON.stringify(pMsg));
	});

	//****************************************************************************************************
	this.setOnFileStart = function(fnOnFileStart)
	{
		fn_OnFileStart = fnOnFileStart;	
	};

	//****************************************************************************************************
	this.setOnFileProgress = function(fnOnFileProgress)
	{
		fn_OnFileProgress = fnOnFileProgress;
	};

	//****************************************************************************************************
	this.setOnFile = function(fnOnFile)
	{
		fn_OnFile = fnOnFile;
	};

	//****************************************************************************************************
	this.setOnLstApk = function(fnOnLstApk)
	{
		fn_OnLstApk = fnOnLstApk;
	};

	//****************************************************************************************************
	this.setOnHttp = function(fnOnHttp)
	{
		fn_OnHttp = fnOnHttp;
	};
}
