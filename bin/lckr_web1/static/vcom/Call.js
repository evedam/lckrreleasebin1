'use strict';

//****************************************************************************************************
var Call = function(params, sTo, fncSendSig, fncAddSes, fncCfgStart, fncCfgCls, fncCfgTurn) 
{
	this.params_ = params;

	this.s_To = sTo;

	this.pcClient_ = null;
	this.localStream_ = null;

	this.startTime = null;

	// Public callbacks. Keep it sorted.
	this.oncallerstarted = null;
	this.onerror = null;
	this.oniceconnectionstatechange = null;
	this.onlocalstreamadded = null;
	this.onnewicecandidate = null;
	this.onremotehangup = null;
	this.onremotesdpset = null;
	this.onremotestreamadded = null;
	this.onsignalingstatechange = null;
	this.onstatusmessage = null;

	this.getMediaPromise = null;
	this.getIceServersPromise = null;

	this.fnc_sendSig = fncSendSig;
	this.fnc_addSes = fncAddSes;
	this.fnc_cfgStart = fncCfgStart;
	this.fnc_cfgCls = fncCfgCls;
	this.fnc_cfgTurn = fncCfgTurn;

	this.requestMediaAndIceServers();
};

//****************************************************************************************************
Call.prototype.requestMediaAndIceServers = function() 
{
	this.getMediaPromise = this.maybeGetMedia_();
	this.fnc_cfgTurn();
};

//****************************************************************************************************
Call.prototype.isInitiator = function() 
{
	return this.params_.isInitiator;
};

//****************************************************************************************************
Call.prototype.start = function(sSesId) 
{
	this.connect(sSesId);
};

//****************************************************************************************************
Call.prototype.hangup = function(async) 
{
	this.startTime = null;

	if (this.localStream_) 
	{
		if (typeof this.localStream_.getTracks === 'undefined') 
		{
			this.localStream_.stop();
		} 
		else 
		{
			this.localStream_.getTracks().forEach(function(track) 
			{
				track.stop();
			});
		}

		this.localStream_ = null;
	}

	if (!this.getSesId()) 
		return;

	if (this.pcClient_) 
	{
		this.pcClient_.close();
		this.pcClient_ = null;
	}

	var steps = [];
	steps.push(
	{
		step: function() 
		{
			this.fnc_cfgCls(this.getSesId());
		}.bind(this),
		errorString: 'Error sending /leave:'
	});

	steps.push(
	{
		step: function() 
		{
			this.fnc_sendSig(13, this.s_To, JSON.stringify({type: 'session_done'}));

		}.bind(this),
		errorString: 'Error sending bye:'
	});

	steps.push(
	{
		step: function() 
		{
			this.setSesId(null);
		}.bind(this),
		errorString: 'Error setting params:'
	});

	if (async) 
	{
		var errorHandler = function(errorString, error) 		
		{
			trace(errorString + ' ' + error.message);
		};

		var promise = Promise.resolve();
		for (var i = 0; i < steps.length; ++i) 
		{
			promise = promise.then(steps[i].step).catch(errorHandler.bind(this, steps[i].errorString));
		}

		return promise;
	} 
	else 
	{
		// Execute the cleanup steps.
		var executeStep = function(executor, errorString) 
		{
			try 
			{
				executor();
			}
			catch (ex) 
			{
				trace(errorString + ' ' + ex);
			}
		};

		for (var j = 0; j < steps.length; ++j) 
		{
			executeStep(steps[j].step, steps[j].errorString);
		}

		if (this.getSesId() !== null) 
			trace('ERROR: sync cleanup tasks did not complete successfully.');
		else 
			trace('Cleanup completed.');

		return Promise.resolve();
	}
};

//****************************************************************************************************
Call.prototype.onRemoteHangup = function() 
{
	this.startTime = null;

	// On remote hangup this client becomes the new initiator.
	this.params_.isInitiator = true;

	if (this.pcClient_) 
	{
		this.pcClient_.close();
		this.pcClient_ = null;
	}

	this.startSignaling_();
};

//****************************************************************************************************
Call.prototype.getPeerConnectionStates = function() 
{
	if (!this.pcClient_) 
	{
		return null;
	}

	return this.pcClient_.getPeerConnectionStates();
};

//****************************************************************************************************
Call.prototype.getPeerConnectionStats = function(callback) 
{
	if (!this.pcClient_) 
	{
		return;
	}

	this.pcClient_.getPeerConnectionStats(callback);
};

//****************************************************************************************************
Call.prototype.toggleVideoMute = function() 
{
	var videoTracks = this.localStream_.getVideoTracks();

	if (videoTracks.length === 0) 
	{
		trace('No local video available.');
		return;
	}

	trace('Toggling video mute state.');
	for (var i = 0; i < videoTracks.length; ++i) 
	{
		videoTracks[i].enabled = !videoTracks[i].enabled;
	}

	trace('Video ' + (videoTracks[0].enabled ? 'unmuted.' : 'muted.'));
};

//****************************************************************************************************
Call.prototype.toggleAudioMute = function() 
{
	var audioTracks = this.localStream_.getAudioTracks();

	if (audioTracks.length === 0) 
	{
		trace('No local audio available.');
		return;
	}

	trace('Toggling audio mute state.');

	for (var i = 0; i < audioTracks.length; ++i) 
	{
		audioTracks[i].enabled = !audioTracks[i].enabled;
	}

	trace('Audio ' + (audioTracks[0].enabled ? 'unmuted.' : 'muted.'));
};

//****************************************************************************************************
Call.prototype.connect = function(sSesId) 
{
	console.log("connecting to ses_id = " + sSesId);
	this.setSesId(sSesId);

	console.log("open called in Call.js");

	this.fnc_cfgStart(sSesId);	
};

//****************************************************************************************************
// Asynchronously request user media if needed.
Call.prototype.maybeGetMedia_ = function() 
{
	// mediaConstraints.audio and mediaConstraints.video could be objects, so
	// check '!=== false' instead of '=== true'.

	var needStream = (this.params_.mediaConstraints.audio !== false || this.params_.mediaConstraints.video !== false);
	var mediaPromise = null;

	if (needStream) 
	{
		var mediaConstraints = this.params_.mediaConstraints;

		mediaPromise = requestUserMedia(mediaConstraints).then(function(stream) 
		{
			trace('Got access to local media with mediaConstraints:\n' + '  \'' + JSON.stringify(mediaConstraints) + '\''); 
			this.onUserMediaSuccess_(stream);
		}.bind(this)).catch(function(error) 
		{
			this.onError_('Error getting user media: ' + error.message);
			this.onUserMediaError_(error);
		}.bind(this));
	} 
	else 
	{
		mediaPromise = Promise.resolve();
	}

	return mediaPromise;
};

//****************************************************************************************************
Call.prototype.onUserMediaSuccess_ = function(stream) 
{
	this.localStream_ = stream;

	if (this.onlocalstreamadded) 
	{
		this.onlocalstreamadded(stream);
	}
};

//****************************************************************************************************
Call.prototype.onUserMediaError_ = function(error) 
{
	var errorMessage = 'Failed to get access to local media. Error name was ' +
		error.name + '. Continuing without sending a stream.';
		this.onError_('getUserMedia error: ' + errorMessage);

	alert(errorMessage);
};

//****************************************************************************************************
Call.prototype.maybeCreatePcClientAsync_ = function() 
{
	return new Promise(function(resolve, reject) 
	{
		if (this.pcClient_ != null) 
		{
			trace("this.pcClient_ != null");
			resolve();
			return;
		}

		if (typeof RTCPeerConnection.generateCertificate === 'function') 
		{
			var certParams = {name: 'ECDSA', namedCurve: 'P-256'};
			RTCPeerConnection.generateCertificate(certParams)
			.then(function(cert) 
			{
				trace('ECDSA certificate generated successfully.');
				this.params_.peerConnectionConfig.certificates = [cert];
				this.createPcClient_();
				resolve();
			}.bind(this))
			.catch(function(error) 
			{
				trace('ECDSA certificate generation failed.');
				reject(error);
			});
		} 
		else 
		{
			this.createPcClient_();
			resolve();
		}
	}.bind(this));
};

//****************************************************************************************************
Call.prototype.createPcClient_ = function() 
{
	this.pcClient_ = new PeerConnectionClient(this.params_, this.startTime);
	this.pcClient_.onsignalingmessage = this.sendSignalingMessage_.bind(this);
	this.pcClient_.onremotehangup = this.onremotehangup;
	this.pcClient_.onremotesdpset = this.onremotesdpset;
	this.pcClient_.onremotestreamadded = this.onremotestreamadded;
	this.pcClient_.onsignalingstatechange = this.onsignalingstatechange;
	this.pcClient_.oniceconnectionstatechange = this.oniceconnectionstatechange;
	this.pcClient_.onnewicecandidate = this.onnewicecandidate;
	this.pcClient_.onerror = this.onerror;

	trace('Created PeerConnectionClient');

	this.fnc_addSes();
};

//****************************************************************************************************
Call.prototype.startSignaling_ = function() 
{
	trace('Starting signaling. initiator = ' + this.isInitiator());

	if (this.isInitiator() && this.oncallerstarted) 
		this.oncallerstarted(this.getSesId());

	this.startTime = window.performance.now();

	this.maybeCreatePcClientAsync_().then(function() 
	{
		if (this.localStream_) 
		{
			trace('Adding local stream.');
			this.pcClient_.addStream(this.localStream_);
		}

		if (this.params_.isInitiator) 
		{
			this.pcClient_.startAsCaller(this.params_.offerOptions);
		} 
		else 
		{
			this.pcClient_.startAsCallee(this.params_.messages);
		}
	}.bind(this))
	.catch(function(e) 
	{
		this.onError_('Create PeerConnection exception: ' + e.message);
		alert('Cannot create RTCPeerConnection: ' + e.message);
}.bind(this));
};
    
//****************************************************************************************************
Call.prototype.sig = function(msg) 
{
	this.maybeCreatePcClientAsync_().then(this.pcClient_.receiveSignalingMessage(msg));
};

//****************************************************************************************************
Call.prototype.cfg = function(oCfgParam)
{
	this.setSesId(oCfgParam.ses_id);
	this.params_.isInitiator = oCfgParam.is_initiator;

	console.log("is_initiator " + oCfgParam.is_initiator);

	this.params_.messages = oCfgParam.messages;

	Promise.all([this.getIceServersPromise, this.getMediaPromise]).then(function() 
	{
		this.startSignaling_();
	}.bind(this)).catch(function(error) 
	{
		this.onError_('Failed to start signaling: ' + error.message);
	}.bind(this));
};

//****************************************************************************************************
Call.prototype.turn = function(pTurn)
{
	var servers = this.params_.peerConnectionConfig.iceServers;
	this.params_.peerConnectionConfig.iceServers = servers.concat(pTurn); 
	this.getIceServersPromise = Promise.resolve();
};

//****************************************************************************************************
Call.prototype.sendSignalingMessage_ = function(message) 
{ 
	console.log("sending signaling message " + JSON.stringify(message));

	var msgString = JSON.stringify(message); 
	this.fnc_sendSig(13, this.s_To, msgString);
};

//****************************************************************************************************
Call.prototype.onError_ = function(message) 
{
	if (this.onerror) 
	{
		this.onerror(message);
	}
};

//****************************************************************************************************
Call.prototype.setSesId = function(sSesId)
{
	this.s_SesId = sSesId;
};

//****************************************************************************************************
Call.prototype.getSesId = function()
{
	return this.s_SesId;
};
