
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { UserService } from '../../user.service.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_users',
	templateUrl: 'users.component.html'
})
export class UseresComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private userService: UserService,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	getDevices(fromServer): void 
	{
		this.userService.getDevices(fromServer).then(users => this.users = users);
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.getDevices(false);
	}

	//****************************************************************************************************
	onSelect(usr: User): void 
	{
		this.cfg.setUsrSel(usr);
	}

	//****************************************************************************************************
	gotoDetail(): void 
	{
		this.router.navigate(['/detail', this.cfg.getUsrSel().getId()]);
	}
	
	//****************************************************************************************************
	refresh() : void
	{
		this.processing = true;
		setTimeout(function()
		{
			this.processing = false;
		}.bind(this), 2000);

		this.getDevices(true);
	}

	users: User[];
	processing: boolean = false;
}

