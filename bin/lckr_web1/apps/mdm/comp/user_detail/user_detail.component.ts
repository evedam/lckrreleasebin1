import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { UserService } from '../../user.service.ts';
import { User } from '../../user.ts';
import 'rxjs/add/operator/switchMap';

@Component
({
	moduleId: module.id,
	selector: 'comp_user_detail',
	templateUrl: 'user_detail.component.html',
})

export class UserDetailComponent implements OnInit 
{
	//****************************************************************************************************
	constructor
	(
		private userService: UserService,
		private route: ActivatedRoute,
		private location: Location
	) 
	{
	
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.route.params.switchMap((params: Params) => this.userService.getUser(params['id'])).subscribe(user => this.user = user);
	}

	//****************************************************************************************************
	goBack(): void 
	{
		this.location.back();
	}

	@Input()
	user: User;
}
