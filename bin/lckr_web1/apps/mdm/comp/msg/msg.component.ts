
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_msg',
	templateUrl: 'msg.component.html',
	styleUrls: [ 'msg.component.css' ]
})
export class MsgComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
	}
}

