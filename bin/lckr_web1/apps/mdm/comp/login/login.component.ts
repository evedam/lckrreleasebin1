
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { UserService } from '../../user.service';
import { CfgService } from '../../cfg.service';
import { CmdService } from '../../cmd.service';
import { FileService } from '../../file.service';
import { ScreenShareService } from '../../screen_share.service';
import { ScriptService } from '../../script.service';
import { DeviceStateService } from '../../device_state.service';
import { LocService } from '../../loc.service';

@Component
({
	moduleId: module.id,
	selector: 'sec_login',
	templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit
{
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private usrService: UserService,
		private cfg: CfgService,
		private cmdService: CmdService,
		private fileService: FileService,
		private screenShareService: ScreenShareService,
		private scriptService: ScriptService,
		private deviceStateService: DeviceStateService,
		private locService: LocService, 
	)
	{
		this.usr  = {id: "admin1", pwd:"admin"};
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{

	}

	//****************************************************************************************************
	login() : void
	{
		this.tried = true;
		console.log("login id = " + this.usr.id + " pwd = " + this.usr.pwd);

		if (!this.isUsrOk())
		{
			console.log("please enter valid user name");
			return;
		}

		if (!this.isPwdOk())
		{
			console.log("please enter valid password");
			return;
		}

		var sUsr = this.usr.id; 
		var sPwd = this.usr.pwd; 
		
		this.processing = true;
		p_VidCtrl.login
		(
			sUsr, sPwd, 
			function(sGrp, sType)
			{
				console.log("login success grp = " + sGrp + " type = " + sType);	

				if (sType != "admin")
				{
					console.log("login should be a admin user.");
					//alert("Please login from a admin user account.");
					//TestCode
					//return;
				}
				
				var pUsr = new User();
				pUsr.setId(sUsr);
				pUsr.setGrp(sGrp);
				pUsr.setType(sType);
				this.cfg.setUsr(pUsr);

				this.router.navigate(['/admin']);

				initMsg();
				initDefaultMsgCallback();
	
				this.cmdService.init();
				this.fileService.init();
				this.screenShareService.init();
				this.scriptService.init();
				this.deviceStateService.init();
				this.locService.init();

				this.processing = false;

			}.bind(this), 
			function(sErr)
			{
				console.log("login failed [" + sErr + "]");	
				this.processing = false;
			
				alert("Please enter valid user name and password.");
			}.bind(this)
		);

	}

	//****************************************************************************************************
	onKey(code) : void
	{
		switch (code)
		{
		case 13:
			this.login();
			break;
		}
	}

	//****************************************************************************************************
	isUsrOk() : boolean
	{
		return isSet(this.usr.id) && isAlNum(this.usr.id);
	}

	//****************************************************************************************************
	isPwdOk() : boolean
	{
		return isSet(this.usr.pwd); 
	}

	//****************************************************************************************************
	usr: User;
	tried: boolean;
	processing: boolean;
} 
