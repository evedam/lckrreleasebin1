
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { State } from '../../../state';
import { DeviceStateService } from '../../../device_state.service';
import { CfgService } from '../../../cfg.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_send_device_state',
	templateUrl: './send_device_state.component.html'
})
export class SendDeviceStateComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private deviceStateService: DeviceStateService,
		private cfg: CfgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.getLState(false);
	}

	//****************************************************************************************************
	onSelState(id: string) : void	
	{
		console.log("id = " + id);
		this.state = id;	
		console.log("selected state = " + JSON.stringify(this.getState()));
	}

	//****************************************************************************************************
	ngOnDestroy() : void
	{
	}

	//****************************************************************************************************
	getLState(fromServer: boolean) : void
	{
		this.deviceStateService.getLState(fromServer);
	}	

	//****************************************************************************************************
	private sendState() : void 
	{
		console.log("send state to usr " + (this.cfg.getUsrSel() ? this.cfg.getUsrSel().getId() : "") + " state = " + state);

		var state = this.state;
		if (!isSet(state))
		{
			console.log("please select a state");
			alert("please select a state");
			return;
		}

		if (this.cfg.getCntUsrSel() == 0)
		{
			console.log("please select a user");
			alert("Please select a user.");
			return;
		}

		this.processing = true;

		var pMUsrSel = this.cfg.getMUsrSel();
		for (var usr in pMUsrSel)
			this.deviceStateService.sendState(pMUsrSel[usr].getId(), state);	

		setTimeout(function()
		{
			this.processing = false;
		}.bind(this),
		4000);
	}

	private getState() : string
	{
		return this.state;
	}
	
	private state: string;
}

