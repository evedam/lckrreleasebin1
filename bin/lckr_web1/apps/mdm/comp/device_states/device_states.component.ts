
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { Pkg } from '../../pkg';
import { State } from '../../state';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { DeviceStateService } from '../../device_state.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_device_states',
	templateUrl: './device_states.component.html'
})
export class DeviceStatesComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private deviceStateService: DeviceStateService 
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
	}
	
	//****************************************************************************************************
	ngOnDestroy() : void
	{
	}

	//****************************************************************************************************
	setState() : void
	{
		var state = new State();
		state.setId("state1");
		state.setDesc("new state description 1");
		state.setSt("active");
		
		for (var idx = 0; idx < 10; idx++)
		{
			var pkg = new Pkg();
			pkg.setApk("apk1" + idx);			
			pkg.setPkg("pkg1" + idx);
			pkg.setVersionCode("1" + idx);
			pkg.setVersionName("1.2.2_" + idx);
			state.addPkg(pkg);
		}

		for (var idx = 0; idx < 3; idx++)
		{
			var pkg = new Pkg();
			pkg.setApk("apk2" + idx);
			pkg.setPkg("pkg2" + idx);	
			pkg.setVersionCode("2" + idx);
			pkg.setVersionName("1.2.2_" + idx);
			state.addPkgRm(pkg);
		}

		this.deviceStateService.setState(state.getData(), function(pData)
		{
			console.log("ret = " + JSON.stringify(pData));
		}.bind(this));
	}

	//****************************************************************************************************
	getLState() : void
	{
		this.deviceStateService.getLState();
	}	

	//****************************************************************************************************
	rmState(id: string) : void
	{
		this.deviceStateService.rmState(id, function(pData)
		{
			console.log("data = " + JSON.stringify(pData));
		}.bind(this));	
	}

	//****************************************************************************************************
	addDevice() : void	
	{
		if (!isSet(this.cfg.getUsrSel()))
		{
			console.log("please select user");
			return;
		}

		this.deviceStateService.addDevice(this.cfg.getUsrSel().getId(), ["state1", "state2"], function(pRet)
		{
			console.log("ret = " + JSON.stringify(pRet));
		}.bind(this));
	}

	//****************************************************************************************************
	setDevice() : void	
	{
		if (!isSet(this.cfg.getUsrSel()))
		{
			console.log("please select user");
			return;
		}

		this.deviceStateService.setDevice(this.cfg.getUsrSel().getId(), ["state1", "state2"], function(pRet)
		{
			console.log("ret = " + JSON.stringify(pRet));
		}.bind(this));
	}

	//****************************************************************************************************
	getLDevice() : void
	{
		this.deviceStateService.getLDevice();
	}

	//****************************************************************************************************
	rmDevice() : void	
	{
		if (!isSet(this.cfg.getUsrSel()))
		{
			console.log("please select user");
			return;
		}

		this.deviceStateService.rmDevice(this.cfg.getUsrSel().getId(), function(pRet)
		{
			console.log("ret = " + JSON.stringify(pRet));
		}.bind(this));
	}

	//****************************************************************************************************
	showAdd(show:boolean)	
	{
		this.showAll(false);
		this.isAdd = show;
	}

	//****************************************************************************************************
	isShowAdd()
	{
		return this.isAdd;
	}

	//****************************************************************************************************
	showEdit(show:boolean)	
	{
		this.showAll(false);
		this.isEdit = show;
	}

	//****************************************************************************************************
	isShowEdit()
	{
		return this.isEdit;
	}


	//****************************************************************************************************
	showApply(show:boolean)	
	{
		this.showAll(false);
		this.isApply = show;
	}

	//****************************************************************************************************
	isShowApply()
	{
		return this.isApply;
	}

	//****************************************************************************************************
	showSetDevice(show:boolean)	
	{
		this.showAll(false);
		this.isSetDevice = show;
	}

	//****************************************************************************************************
	isShowSetDevice()
	{
		return this.isSetDevice;
	}

	//****************************************************************************************************
	showAll(show:boolean)
	{
		this.isAdd = show;
		this.isEdit = show;
		this.isApply = show;
		this.isSetDevice = show;
	}

	isAdd: boolean = false;
	isEdit: boolean = false;
	isApply: boolean = false;
	isSetDevice: boolean = false;
}

