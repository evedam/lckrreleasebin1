
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';
import { ApkService } from '../../apk.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_add_apk',
	templateUrl: 'add_apk.component.html'
})
export class AddApkComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private apkService: ApkService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.progress = 0;
		initWindowButtons();
	}

	//****************************************************************************************************
	onFile(event) : void 
	{
		this.file = event.srcElement.files[0];
		console.log("select file = " + this.file);	
	}

	//****************************************************************************************************
	addApk() : void 
	{
		this.tried = true;

		console.log("files = " + this.file + " description = " + this.description); 

		if (!this.isDescriptionOk())
		{
			console.log("plase enter valid description");
			return;
		}
		
		if (!this.isFileOk())
		{
			console.log("plase select valid file");
			return;
		}

		this.apkService.addApk(this.file, this.description, function(progress)
		{
			console.log("add apk progress " + progress);
		
			if (progress == 100)
			{
				console.log("adding apk file done");
				setTimeout(function()	
				{
					this.progress = 0;
					this.onDone();
				}.bind(this), 1000);
			}

			this.progress = progress; 
		}.bind(this));
	}

	//****************************************************************************************************
	onDone() : void
	{
		showSuccessNotification("Uploading apk file success.");
	}

	//****************************************************************************************************
	isDescriptionWarning() : boolean
	{
		return this.tried && (!isSet(this.description) || !isAlNumSpace(this.description));  
	}

	//****************************************************************************************************
	isDescriptionOk() : boolean
	{
		return this.tried && isAlNumSpace(this.description) 
	}

	//****************************************************************************************************
	isFileWarning() : boolean
	{
		return this.tried && this.file == null;
	} 

	//****************************************************************************************************
	isFileOk() : boolean
	{
		return this.tried && this.file != null;
	}

	description: string;
	file: File;
	tried: boolean;
}

