
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Apk } from '../../apk';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { ApkService } from '../../apk.service';
import { PkgService } from '../../pkg.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_install_apk',
	templateUrl: 'install_apk.component.html'
})
export class InstallApkComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private apkService: ApkService,
		private pkgService: PkgService
	) 
	{ 
		this.selectedApk = null;
		this.processing = false;
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		initWindowButtons();

		this.apkService.getApks();
	}

	//****************************************************************************************************
	install() : void
	{
		console.log("install to usr " + (this.cfg.getUsrSel() ? this.cfg.getUsrSel().getId() : "") + " apk = " + this.selectedApk);

		if (this.cfg.getCntUsrSel() == 0)
		{
			console.log("please select a user");
			alert("Please select a user.");
			return;
		}

		if (this.selectedApk  == null)
		{
			console.log("please select a apk");
			alert("Please select a apk file to be installed.");
			return;
		}

		this.processing = true;

		var pMUsrSel = this.cfg.getMUsrSel();
		for (var usr in pMUsrSel)
			p_Msg.installApk(124, "", pMUsrSel[usr].getId(), 60, this.selectedApk, this.open);	

		setTimeout(function()
		{
			showSuccessNotification("Install apk has been queued successfully.");
			this.processing = false;

			for (var usr in pMUsrSel)
				this.pkgService.getPkgs(pMUsrSel[usr].getId(), true);
		}.bind(this), 	
		2000);
	}

	//****************************************************************************************************
	selectedApk: string;
	processing: boolean;
}

