import { Component } from "@angular/core";
import { Http } from "@angular/http";

import { CfgService } from '../../../cfg.service';
import { UserService } from '../../../user.service'
import { User } from '../../../user.ts';
import { Device } from '../../../device.ts';

@Component(
{
	selector: 'comp_report_device_list',
	templateUrl: './mdm/comp/report/device_list/device_list.component.html'
})
export class ReportDeviceListComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor(
			private cfg: CfgService, 
			private userService: UserService) 
	{
	}

	//****************************************************************************************************
	ngOnInit() : void 
	{
		this.getDevices(false);
	}

	//****************************************************************************************************
	onKeyDown(key: number) : void
	{
		console.log("keydown key code = " + key);

		switch (key)
		{
		case 17:
			this.multiSel = true;
			break;
		}
	}

	//****************************************************************************************************
	onKeyUp(key: number) : void
	{
		console.log("keydown key code = " + key);
		switch (key)
		{
		case 17:
			this.multiSel = false;
			break;
		}

	}

	//****************************************************************************************************
	edit(usr: Usr) : void
	{
		console.log("edit uid" + JSON.stringify(usr));

		this.editIds[usr.getId()] = true;
	}

	//****************************************************************************************************
	save(usr: Usr) : void
	{
		console.log("save uid" + JSON.stringify(usr));

		this.userService.setUid(usr.getId(), usr.getUid());
		this.userService.setName(usr.getId(), usr.getName());
		
		this.editIds[usr.getId()] = false;
	} 

	//****************************************************************************************************
	cancel(usr: Usr) : void
	{
		console.log("cancel " + JSON.stringify(usr));

		this.editIds[usr.getId()] = false;
	} 

	//****************************************************************************************************
	isEdit(usr: Usr) : boolean
	{
		if (!isSet(this.editIds[usr.getId()]))
			return false;	
	
		return this.editIds[usr.getId()] == true;
	}

	//****************************************************************************************************
	getDevices(bFromServer: boolean) : void
	{
		this.userService.getDevices(bFromServer).then(users => this.users = users);
	}

	//****************************************************************************************************
	public toInt(num: string) 
	{
		return +num;
	}

	//****************************************************************************************************
	public sortByWordLength = (a: any) => 
	{
		return a.state.length;
	}

	//****************************************************************************************************
	public getFilterUser() : User
	{
		var usr = new User(this.filterId, this.filterUid, this.filterName, "", "", this.filterState);
		var device = new Device();
		device.setImei(this.filterImei);
		device.setModel(this.filterModel);
		usr.setDevice(device);
		return usr;
	}

	//****************************************************************************************************
	public selUsr(usr: User) : User
	{
		if (this.multiSel)
			this.cfg.addUsrSel(usr);
		else
			this.cfg.setUsrSel(usr);
	}

	//****************************************************************************************************
	isSel(usr) : void
	{
		return this.cfg.isSelUsr(usr);
	}

	//****************************************************************************************************
	public secToDate(seconds)
	{
		if (isEmpty(seconds))
			return "";

		return secToDateStr(seconds);
	}
	
	//****************************************************************************************************
	refresh() : void
	{
		this.processing = true;
		setTimeout(function()
		{
			this.processing = false;
		}.bind(this), 2000);

		this.getDevices(true);
	}

	//****************************************************************************************************
	downloadCsv() : void
	{
		this.processing = true;
		setTimeout(function()
		{
			this.processing = false;
		}.bind(this), 2000);

		let users = this.userService.getDeviceArray();
		let csv = "Id, Name, Device Model, Last Connect\n";
		let len = users.length;
		for (let i = 0; i < len; i++)
		{
			let usr = users[i];
			console.log(JSON.stringify(usr));
		
			let line = usr.getId() + ", "; 
			line += usr.getName() + ", ";
			line += (usr.getDevice() ? usr.getDevice().getModel() : "") + ", ";
			line += usr.getConnectedTime();

			csv += line + "\n";
		}

		stringDownload(csv, "device_list.csv");	
	}

	//****************************************************************************************************
	downloadAppInfo() : void
	{
		this.processing = true;
		setTimeout(function()
		{
			this.processing = false;
		}.bind(this), 2000);

		if (this.selectedPkg == null)
		{
			console.log("plese select a pakcage");
			return;
		}

		let users = this.userService.getDeviceArray();
		let csv = "Id, Name, Device Model, " + this.selectedPkg + " Version Code, " + this.selectedPkg + " Version Name, Last Connect\n";
		let len = users.length;
		for (let i = 0; i < len; i++)
		{
			let usr = users[i];
			console.log(JSON.stringify(usr));
		
			let line = usr.getId() + ", "; 
			line += usr.getName() + ", ";
			line += (usr.getDevice() ? usr.getDevice().getModel() : "") + ", ";

			if (usr.getDevice() && usr.getDevice().mPkg[this.selectedPkg])
			{
				let pkg = usr.getDevice().mPkg[this.selectedPkg];	

				line += pkg.getVersionCode() + ", ";
				line += pkg.getVersionName() + ", ";
			}
			else 
				line += ",,";

			line += usr.getConnectedTime();

			csv += line + "\n";
		}

		stringDownload(csv, "app_info.csv");	
	}

	//****************************************************************************************************
	getState(usr: User) : boolean
	{
		if (usr.getState() == "disconnected" && ((new Date().getTime() / 1000) - usr.getDisconnectedTime()) < 12)
			return "connecting";

		if (usr.getState() == "init")
			return "disconnected";

		return usr.getState(); 
	}

	public filterId: string = "";
	public filterUid: string = "";
	public filterName: string = "";
	public filterState: string = "";
	public filterImei: string = "";
	public filterModel: string = "";
	public rowsOnPage:number = 5;
	public sortBy: string = "type";
	public sortOrder: string = "asc";
	public processing: boolean = false;
	public multiSel: boolean = false;
	private editIds: {} = {};
	private selectedPkg: string = null;
}
