
import { Component, OnInit } from '@angular/core';

import { Apk } from '../../apk.ts';
import { CfgService } from '../../cfg.service.ts';
import { ApkService } from '../../apk.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_view_apks',
	templateUrl: 'view_apks.component.html',
	styleUrls: [ 'view_apks.component.css' ]
})
export class ViewApksComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private cfg: CfgService,
		private apkService: ApkService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.getLstApk(false);
		initWindowButtons();
	}

	//****************************************************************************************************
	onSelectApk(apk) : void
	{
		console.log("select apk " + JSON.stringify(apk));
		this.cfg.setApkSel(apk);
	}

	//****************************************************************************************************
	getLstApk(fromServer: boolean) : void
	{
		this.apkService.getApks(fromServer).then(apks => this.apks = apks);
	}

	//****************************************************************************************************
	refresh() : void
	{
		this.getLstApk(true);
	}

	//****************************************************************************************************
	apks: Apks[];
}

