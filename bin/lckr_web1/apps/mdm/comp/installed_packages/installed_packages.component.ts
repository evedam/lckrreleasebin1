
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Pkg} from '../../pkg.ts';
import { ViewService } from '../../view.service.ts';
import { CfgService } from '../../cfg.service.ts';
import { PkgService } from '../../pkg.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_installed_packages',
	templateUrl: 'installed_packages.component.html'
})
export class InstalledPackagesComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private pkgService: PkgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit() : void 
	{
		this.getPkgs(false);
		this.pkgService.incCntUsr();

		initWindowButtons();
	}

	//****************************************************************************************************
	ngOnDestroy() : void
	{
		console.log("on destroy");
		this.pkgService.decCntUsr();
	}

	//****************************************************************************************************
	getPkgs(fromServer) : void
	{
		if (this.cfg.getUsrSel() == null)
		{
			console.log("please select a user to get pakcage info");
			return;
		}

		this.pkgService.getPkgs(this.cfg.getUsrSel().getId(), fromServer);
	}

	//****************************************************************************************************
	refresh() : void
	{
		this.getPkgs(true);	
	}

	//****************************************************************************************************
	selectPkg(pkg: Pkg) : void
	{
		this.selectedPkg = pkg;
		this.cfg.setPkgSel(pkg);
		
		console.log("selected package = " + JSON.stringify(pkg));
	}

	//****************************************************************************************************
	selectedPkg: Pkg;
}

