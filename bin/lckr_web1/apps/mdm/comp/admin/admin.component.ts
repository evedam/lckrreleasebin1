
import { Component, OnInit } from '@angular/core';

import { User } from '../../user.ts';
import { UserService } from '../../user.service.ts';
import { ViewService } from '../../view.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'comp_admin',
	templateUrl: 'admin.component.html',
	styleUrls: [ 'admin.component.css' ]
})
export class AdminComponent implements OnInit 
{

	//****************************************************************************************************
	constructor
	(
		private userService: UserService, 
		private viewService: ViewService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.userService.getDevices(false);
	}

	//****************************************************************************************************
	toggleSidebar() : void
	{
		console.log("toggle sidebar");
		this.activeSidebar = !this.activeSidebar; 
	}

	//****************************************************************************************************
	activeSidebar: boolean;
}

