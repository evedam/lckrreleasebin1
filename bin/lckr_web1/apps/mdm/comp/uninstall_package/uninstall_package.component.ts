
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../user';
import { ViewService } from '../../view.service';
import { CfgService } from '../../cfg.service';
import { PkgService } from '../../pkg.service';

@Component
({
	moduleId: module.id,
	selector: 'comp_uninstall_package',
	templateUrl: 'uninstall_package.component.html'
})
export class UninstallPackageComponent implements OnInit 
{ 
	//****************************************************************************************************
	constructor
	(
		private router: Router,
		private viewService: ViewService,
		private cfg: CfgService,
		private pkgService: PkgService
	) 
	{ 
	}

	//****************************************************************************************************
	ngOnInit(): void 
	{
		this.pkgService.incCntUsr();

		initWindowButtons();
	}

	//****************************************************************************************************
	rm() : void
	{
		if (this.cfg.getCntUsrSel() == 0)
		{
			console.log("please select a user to remove the package");
			alert("Plase select a user.");
			return;
		}
		
		if (this.selectedPkg == null)
		{
			console.log("plase select a package to be uninstalled");
			alert("Please select a package.");
			return;
		}

		var pMUsrSel = this.cfg.getMUsrSel();
		for (var usr in pMUsrSel)
			p_Msg.apkRm(126, "", pMUsrSel[usr].getId(), 0, this.selectedPkg);	
	}

	//****************************************************************************************************
	refresh() : void
	{
		this.pkgService.getPkgs(this.cfg.getUsrSel().getId(), true);
	}

	//****************************************************************************************************
	selectedPkg: string;
}

