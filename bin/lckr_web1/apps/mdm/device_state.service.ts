
import { Injectable } from '@angular/core';
import { State } from './state';

@Injectable()
export class DeviceStateService
{
	//****************************************************************************************************
	init() : void
	{
		p_Msg.getModDeviceState().setOnLState(function(pData)
		{
			console.log("data = " + JSON.stringify(pData));	
			if (pData.ret == "true" || pData.ret == true)
				this.loadLState(pData.data.lstate);
			else
				console.log("invalid response");
		}.bind(this));

		p_Msg.getModDeviceState().setOnLDevice(function(pData)
		{
			console.log("data = " + JSON.stringify(pData));	

			if (pData.ret == "true" || pData.ret == true)
				this.loadLDevice(pData.data.ldevice);
			else
				console.log("invalid response");
		}.bind(this));
	}

	//****************************************************************************************************
	public getState(id: string) : void
	{
		return this.aState[this.states[id]];
	}

	//****************************************************************************************************
	private loadLState(states) : viod
	{
		this.states = {};
		this.aState = [];

		if (states == null)
		{
			console.log("loadLState() states == null");
			return;
		}

		var len = states.length;
		for (var idx = 0; idx < len; idx++)				
		{
			var state = new State();
			state.load(states[idx]);				

			this.aState.push(state);
			this.states[state.getId()] = idx;
		}		

		console.log("loaded states " + JSON.stringify(this.aState));
	}

	//****************************************************************************************************
	private loadLDevice(devices) : void
	{
		this.devices = {};
		this.aDevice = [];

		var len = devices.length; 
		for (var idx = 0; idx < len; idx++)
		{
			var device = devices[idx];
			this.devices[device.id] = idx;	
		}

		this.aDevice = devices;
		console.log("loaded devices " + JSON.stringify(this.aDevice));
	}

	//****************************************************************************************************
	addState(state, fnRet) : void
	{
		p_Msg.getModDeviceState().addState(state, fnRet);
	}

	//****************************************************************************************************
	setState(state, fnRet) : void
	{
		p_Msg.getModDeviceState().setState(state, fnRet);
	}

	//****************************************************************************************************
	getLState(fromServer: boolean) : void
	{
		p_Msg.getModDeviceState().getLState();
	}

	//****************************************************************************************************
	rmState(id: string, fnRet) : void
	{
		p_Msg.getModDeviceState().rmState(id, fnRet);
	}

	//****************************************************************************************************
	addDevice(id, states, fnRet) : void
	{
		p_Msg.getModDeviceState().addDevice(id, states, fnRet);
	}

	//****************************************************************************************************
	setDevice(id, states, fnRet) : void
	{
		p_Msg.getModDeviceState().setDevice(id, states, fnRet);
	}
	
	//****************************************************************************************************
	getLDevice() : void
	{
		p_Msg.getModDeviceState().getLDevice();
	}

	//****************************************************************************************************
	rmDevice(id: string, fnRet) : void
	{
		p_Msg.getModDeviceState().rmDevice(id, fnRet);
	}

	//****************************************************************************************************
	sendState(state: string, to: string)
	{
		p_Msg.getModDeviceState().sendState(state, to);	
	}

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
	}

	states: {} = {};
	aState: [] = [];
	devices: {} = {};
	aDevice: [] = [];
}
