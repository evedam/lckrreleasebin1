
export class File
{
	//****************************************************************************************************
	constructor(path: string, type: Type, size: number, hidden: boolean)
	{
		console.log("loading file path = " + path + " type = " + type + " size = " + size + " hidden = " + hidden);
		this.setPath(path);
		this.setType(type);
		this.setSize(size);
		this.setHidden(hidden);
	}	

	//****************************************************************************************************
	static toType(type: string) : File.Type
	{
		console.log("to type");
		
		switch (type)
		{
		case "file":
			return File.Type.File;
		case "dir":
			return File.Type.Dir;
		case "lnk":
			return File.Type.Lnk;
		default:
			return File.Type.None;
		}
	}

	//****************************************************************************************************
	getName() : string
	{
		var path = this.getPath();
		if (!isSet(path))
			return "";

		return getFileName(path);
	}

	setPath(path: string) : void
	{
		this.path = path;
	}

	getPath() : string
	{
		return this.path;
	}

	setType(type: File.Type) : void
	{
		this.type = type;
	}

	getType() : File.Type
	{
		return this.type;
	}

	setSize(size: number) : void
	{
		this.size = size;
	}	

	getSize() : number
	{
		return this.size;
	}

	setHidden(hidden: boolean) : void
	{
		this.hidden = hidden;
	}

	isHidden() : boolean
	{
		return this.hidden;
	}
	
	path: string;
	type: Type;
	size: number;
	hidden: boolean;
}

module File 
{
	export const enum Type
	{
		File,
		Dir,
		nk,
		None
	};

}
