import { Pkg } from './pkg';

export class Device 
{
	constructor()
	{
		this.setImei("");
		this.setModel("");
		this.setRooted(false);
	}

	load(device) : void
	{
		this.setImei(device.imei);
		this.setModel(device.model);
		this.setRooted(device.rooted);

		console.log("lpkg = " + device.lpkg);
		
		this.lpkg = [];
		if (isSet(device) && isSet(device.lpkg))
		{
			console.log("lpkg 2 = " + device.lpkg);
			let pkgs = JSON.parse(device.lpkg);
			if (pkgs != null)
			{ 
				for (let i = 0; i < pkgs.length; i++)
				{
					let data = pkgs[i];
					let pkg = new Pkg();
					pkg.load(data);
					this.lpkg.push(pkg);
					this.mPkg[pkg.pkg] = pkg;

					console.log("loaded pkg = " + JSON.stringify(pkg));
				}
			}
		} 
	} 

	setImei(imei: string) : void
	{
		this.imei = imei;
	}

	getImei() : string
	{
		return this.imei;
	}

	setModel(model: string) : void
	{
		this.model = model;
	} 

	getModel() : string
	{
		return this.model;
	}

	setRooted(rooted: boolean) : void
	{
		this.rooted = rooted;
	}

	isRooted() : boolean
	{
		return this.rooted;
	}

	getPkg(pkg)
	{
		return this.mPkg[pkg];
	}

	imei: string;
	model: string;
	rooted: boolean;
	lpkg: Pkg[];
	mPkg = {};
}
