import { Injectable } from '@angular/core';

@Injectable()
export class ViewService 
{
	showCall(call: boolean) : void
	{
		this.refresh();
		this.call = call;
	}

	isCall() : boolean
	{
		return this.call;	
	}

	showUsers(show: boolean) : void
	{
		this.refresh();
		this.users = show;
	}

	isUsers() : boolean
	{
		return this.users;
	}

	showGroups(show: boolean) : void
	{
		this.refresh();
		this.groups = show;	
	}

	isGroups() : boolean
	{
		return this.groups;
	}

	showAddUser(show: boolean) : void
	{
		this.refresh();
		this.addUser = show;
	}

	isAddUser() : boolean
	{
		return this.addUser;
	}

	showAddApk(show: boolean) : void
	{
		this.refresh();
		this.addApk = show;
	}

	isAddApk() : boolean
	{
		return this.addApk;
	}

	showViewApks(show: boolean) : void
	{
		this.refresh();
		this.viewApks = show;
	}

	isViewApks() : boolean
	{
		return this.viewApks;
	}

	showInstallApk(show: boolean) : void
	{
		this.refresh();
		this.installApk = show;
	}

	isInstallApk() : boolean
	{
		return this.installApk;
	}

	showInstalledPackages(show: boolean) : boolean
	{
		this.refresh();
		this.installedPackages = show;
	}

	isInstalledPackages() : boolean
	{
		return this.installedPackages;
	}

	showUninstallPackage(show: boolean) : void
	{
		this.refresh();
		this.uninstallPackage = show;
	}

	isUninstallPackage() : boolean
	{
		return this.uninstallPackage;
	}
	
	showMsg(show: boolean) : void
	{
		this.refresh();
		this.msg = show;
	}

	isMsg() : boolean
	{
		return this.msg;
	}

	showFiles(show: boolean) : void
	{
		this.refresh();
		this.files = show;
	}

	isFiles() : boolean
	{
		return this.files;
	}

	showScreenShare(show: boolean) : void
	{
		this.refresh();
		this.screenShare = show;
	}

	isScreenShare() : boolean
	{
		return this.screenShare;
	}

	showDeviceMan(show: boolean) : void
	{
		this.refresh();
		this.deviceMan = show;
	}

	isDeviceMan() : boolean
	{
		return this.deviceMan;	
	}

	showScript(show: boolean) : void
	{
		this.refresh();
		this.script = show;
	}

	isScript() : boolean	
	{
		return this.script;
	}

	showDeviceStates(show: boolean) : void
	{
		this.refresh();
		this.deviceStates = show;
	}

	isDeviceStates() : boolean	
	{
		return this.deviceStates;
	}

	showTopMenu(show: boolean) : void
	{
		this.topMenu = show;
	}

	isTopMenu() : boolean
	{
		return this.topMenu;
	}

	showReportDeviceList(show: boolean) : void
	{
		this.refresh();
		this.reportDeviceList = show;
	}

	isReportDeviceList() : boolean
	{
		return this.reportDeviceList;
	}

	showLoc(show: boolean) : void
	{
		this.refresh();
		this.loc = show;
	}

	isLoc() : boolean
	{
		return this.loc;
	}

	setPreserveView(preserve) : void
	{
		this.preserveView = preserve;
	}

	isPreserveView() : boolean
	{
		return this.preserveView;
	}

	showAll(show: boolean) : void
	{
		this.call = show;
		this.users = show;
		this.groups = show;
		this.addUser = show;
		this.addApk = show;
		this.viewApks = show;
		this.installApk = show;
		this.installedPackages = show;
		this.uninstallPackage = show;
		this.msg = show;
		this.screenShare = show;
		this.files = show;
		this.deviceMan = show;
		this.script = show;
		this.deviceStates = show;
		this.reportDeviceList = show;
		this.loc = show;
	}
	
	refresh() : void
	{
		if (!this.isPreserveView()) 
			this.showAll(false);	
	}

	call: boolean = false;
	users: boolean = true;
	groups: boolean = false;
	addUser: boolean = false;
	addApk: boolean = false;
	viewApks: boolean = false;
	installApk: boolean = false;
	installedPackages: boolean = false;
	uninstallPackage: boolean = false;
	msg: boolean = false;
	screenShare: boolean = false;
	files: boolean = false;
	deviceMan: boolean = false;
	script: bolean = false;
	deviceStates: boolean = false;
	topMenu: boolean = false;
	reportDeviceList: boolean = false;
	loc: boolean = false;
	preserveView: boolean = false;
}
