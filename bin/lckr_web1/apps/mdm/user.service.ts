import { Injectable } from '@angular/core';

import { User } from './user';
import { Device } from './device';
import { MockUsers } from './mock_users';

@Injectable()
export class UserService
{
	//****************************************************************************************************
	constructor()
	{
		this.users = {};
	}

	//****************************************************************************************************
	setUid(id: string, uid: string) : void
	{
		console.log("set id = " + id + " uid = " + uid);

		p_Msg.getModUsr().setUid(id, uid, function(ret)
		{
			console.log("data = " + JSON.stringify(ret));
		}.bind(this));	
	}

	//****************************************************************************************************
	setName(id: string, name: string)
	{
		console.log("set id = " + id + " name = " + name);
		
		p_Msg.getModUsr().setName(id, name, function(ret)
		{
			console.log("data = " + JSON.stringify(ret));
		}.bind(this));
	}

	//****************************************************************************************************
	getUser(id: string): string 
	{
		console.log("get a user id = (" + id  + ")");
		return this.users["device"][id];
	}

	//****************************************************************************************************
	getDeviceSync(id: string) : User
	{
		console.log("getDeviceSync id = " + id);

		var usr = null;

		if (isSet(this.getDeviceMap()))	
			usr = this.getDeviceMap()[id];

		console.log("getDeviceSync user = " + JSON.stringify(usr));

		return usr;
	}

	//****************************************************************************************************
	getAllUsers(sType: string, fromServer: boolean): Promise<User[]>
	{
		console.log("get users");
		
		if (p_Msg == null)
		{
			console.log("p_Msg == null");
			return new Promise();
		}
		
		if (isEmpty(this.users[sType]) || fromServer)
		{
			p_Msg.usrGetLst(6000, sType, "", 0, 0, true);
			this.users[sType] = null;

			if (sType == "device")
				this.aDevice = []; 
			else if (sType == "grp")
				this.aGrp = [];

			return new Promise
			(
				resolve => 
				{ 
					p_Msg.setOnUsrLst(function(uiId, mUsr, sUsrType)
					{
						console.log("on lst users id = " + uiId + " lst_usr = " + JSON.stringify(mUsr));

						this.users[sType] = {};
			
						if (sType == "device")
							this.aDevice = [];
						else if (sType == "grp")
							this.aGrp = [];

						for (var id in mUsr)
						{
							this.addUsr(mUsr[id], false);	
						}

						resolve(this.users);
							
					}.bind(this));
	
					p_Msg.setOnUsrAdd(function(usr)
					{
						console.log("add user id = " + JSON.stringify(usr));	
						this.addUsr(usr, true);
						this.setUsr(usr, UsrState.Init);
					}.bind(this));

					p_Msg.setOnUsrConnect(function(usr)
					{
						console.log("on usr connected " + JSON.stringify(usr));
						this.setUsr(usr, UsrState.Connect);
					}.bind(this));

					p_Msg.setOnUsrDisconnect(function(usr)
					{
						console.log("on usr disconnect " + JSON.stringify(usr));	
						this.setUsr(usr, UsrState.Disconnect);
					}.bind(this));

					p_Msg.setOnUsrLogout(function(usr)
					{
						console.log("on usr remove " + JSON.stringify(usr));
						this.rmUsr(usr.id);
					}.bind(this));
				}
			); 

		}
		else
			return this.retUsers();
	} 

	//****************************************************************************************************
	getDevices(fromServer: boolean) : Promise<User[]>
	{
		console.log("getDevices " + fromServer);
		return this.getAllUsers("device", fromServer);
	}

	//****************************************************************************************************
	getGroups(fromServer: boolean) : Promise<User[]>
	{
		return this.getAllUsers("grp", fromServer);
	}

	//****************************************************************************************************
	addUsr(usrData, front: boolean) : void
	{ 
		if (isEmpty(usrData.type))
		{
			console.log("please enter valid user type to add " + JSON.stringify(usrData));
			return;
		}

		if (!isSet(this.users[usrData.type]))
		{
			console.log("unknown user type ") + sUsrType;
			return;
		}

		var sUsrType = usrData.type;
		var usr = new User(); 
		usr.load(usrData);
		this.users[sUsrType][usr.getId()] = usr;

		if (isSet(usr.device) && isSet(usr.device.lpkg))
		{
			for (let i = 0; i < usr.device.lpkg.length; i++)
			{
				let pkg = usr.device.lpkg[i];
				if (!isSet(this.mPkg[pkg.pkg]))
				{
					this.mPkg[pkg.pkg] = pkg;
					this.aPkg.push(pkg);
				}
			}
		}

		if (sUsrType == "device")
		{
			if (front)
				this.aDevice.unshift(usr);
			else
				this.aDevice.push(usr);
		}
		else if (sUsrType == "grp")
		{
			this.aGrp.push(usr);
		}
		else
			console.log("unknown user type " + sUsrType);

	}

	//****************************************************************************************************
	setUsr(usr, state) : void
	{
		if (isEmpty(this.getDeviceMap()))
		{
			console.log("this.users == null");
			return;
		}

		console.log("set user state usr = " + JSON.stringify(usr));

		if (isEmpty(this.getDeviceMap()[usr.id]))
		{
			console.log("no such device to set user state");
			return;
		}

		this.getDeviceMap()[usr.id].setState(UsrState.toStr(state));
		if (isSet(usr.device))
		{
			console.log("device = " + JSON.stringify(usr.device));
			var device = new Device();
			device.load(usr.device);
			this.getDeviceMap()[usr.id].setDevice(device);
		}
		else
			console.log("no device info");

		var iTime = new Date().getTime() / 1000;

		switch (state)
		{
		case UsrState.Connect:
			this.getDeviceMap()[usr.id].setConnectedTime(iTime);
			break;
		case UsrState.Disconnect:
			this.getDeviceMap()[usr.id].setDisconnectedTime(iTime);
			break;
		}
	}

	//****************************************************************************************************
	setLUsr(id:number, usr: string, lstUsr, add: boolean) : void
	{
		console.log("setLUsr id = " + id + " usr = " + usr + " lst_usr = " + lstUsr + " add = " + add);
		p_Msg.setLUsr(id, usr, lstUsr, add);
	}

	//****************************************************************************************************
	rmUsr(id)
	{
		if (isEmpty(this.getDeviceMap()))
		{
			console.log("this.users == null");
			return;
		}

		console.log("remove a user " + id);
	}

	//****************************************************************************************************
	retUsers() : Promise<{}> 
	{
		return new Promise
		(
			resolve => 
			{
				resolve(this.users);
			}
		); 

	}

	//****************************************************************************************************
	keys() : string[]
	{
		if (isEmpty(this.getDeviceMap()))
		{
			console.log("this.users == null");
			return [];
		}

		return Object.keys(this.getDeviceMap());
	}

	//****************************************************************************************************
	size() : number
	{
		return this.keys().length;
	}

	//****************************************************************************************************
	getDeviceArray() : User[]
	{
		return this.aDevice;
	}

	//****************************************************************************************************
	getGrpArray() : Usr[]
	{
		return this.aGrp;
	}

	//****************************************************************************************************
	getDeviceMap() 
	{
		return this.users["device"];
	}

	//****************************************************************************************************
	public users: {} 
	public aDevice: [];
	public aGrp: [];
	public mPkg = {};
	public aPkg = [];
}
