import { User } from './user.ts';

export const MockUsers: User[] = 
[
	new User( "dileepamobile", "pickme",  'item 11'),
	new User( "dileepa", "pickme",  'item 12'),
	new User( "13", "pickme",  'item 13'),
	new User( "14", "pickme",  'item 14'),
	new User( "15", "pickme",  'item 15'),
	new User( "16", "pickme",  'item 16'),
	new User( "17", "pickme",  'item 17'),
	new User( "18", "pickme",  'item 18'),
	new User( "19", "pickme",  'item 19'),
	new User( "20", "pickme",  'item 20')
];
