
import { Device } from './device';

export class User 
{
	constructor(id: string, uid: string, name: string, grp: string, type: string, state: string)
	{
		this.setId(id);
		this.setUid(uid);
		this.setName(name);
		this.setGrp(grp);
		this.setType(type);
		this.setState(state);
	}

	load(usr) : void
	{
		this.setId(usr.id);
		this.setUid(usr.uid);
		this.setName(usr.name);
		this.setGrp(usr.grp);
		this.setType(usr.type);
		this.setState(usr.state);
		this.setConnectedTime(usr.connected_time);
		this.setDisconnectedTime(usr.disconnected_time);

		var device = new Device();
		if (isSet(usr.device))
			device.load(usr.device);
		this.setDevice(device);
	} 

	setId(sId : string) : void
	{
		this.id = sId;
	}

	getId() : string
	{
		return this.id;
	}

	setUid(uid: string) : void
	{
		this.uid = uid;
	}

	getUid() : string
	{
		return this.uid;
	}

	setName(name: string) : void
	{
		this.name = name;
	}

	getName() : string
	{
		return this.name;
	}

	setGrp(sGrp: string) : void
	{
		this.grp = sGrp;
	}

	getGrp() : string
	{
		return this.grp;
	}

	setType(sType: string) : void
	{
		this.type = sType;
	}

	getType() : string
	{
		return this.type;
	}

	setState(state: string) : void
	{
		this.state = state;
	}

	getState() : string
	{
		return this.state;
	}

	setConnectedTime(time: number) : void
	{
		this.connectedTime = time;
	}

	getConnectedTime() : number
	{
		return this.connectedTime;
	}

	setDisconnectedTime(time: number) : void
	{
		this.disconnectedTime = time;
	}

	getDisconnectedTime() : number
	{
		return this.disconnectedTime;
	}

	setDevice(device: Device) : void
	{
		this.device = device;
	}

	getDevice() : Device	
	{
		return this.device;
	}

	id: string;
	uid: string;
	name: string;
	grp: string;
	type: string;
	state: string;
	connectedTime: number;
	disconnectedTime: number;
	device: Device;
}
