import { Injectable } from '@angular/core';

import { Apk } from './apk.ts';

@Injectable()
export class ApkService
{
	//****************************************************************************************************
	getApk(id: string): Promise<Apk> 
	{
		console.log("get a apk id = (" + id  + ")");

		return this.getApks().then(apks => apks.find(apk => apk.id == id));
	}

	//****************************************************************************************************
	addApk(file, description, fncProgress) : void
	{
		console.log("file = " + file + " adding apk description " + description);

		if (description == null || description == "")
		{
			console.log("please enter valid description");
			return;
		}

		if (file == null)
		{
			console.log("please enter valid file");
			return;
		} 

		p_Msg.addApk(123, "", file, description, function(progress)
		{
			fncProgress(progress);
			
			if (progress == 100)
				this.getApks(true);
		}.bind(this)); 
	}

	//****************************************************************************************************
	getApks(fromServer: boolean): Promise<Apk[]>
	{
		console.log("get apks");
		
		if (this.apks == null || fromServer)
		{
			p_Msg.getLstApk(125, "");
			this.apks= [];

			return new Promise
			(
				resolve => 
				{ 
					p_Msg.setOnLstApk(function(apks)
					{
						if (apks == null)
						{
							console.log("empty apk list");
							return;
						}

						console.log("on lst apk " + JSON.stringify(apks));
						var len = apks.length;
						for (var idx = 0; idx < len; idx++)
						{
							var apk = new Apk();
							apk.load(apks[idx]);
							this.apks.push(apk);
						}

						console.log("apks = " + JSON.stringify(this.apks));

						resolve(this.apks);

					}.bind(this));
				}
			); 

		}
		else
			return this.retApks();
	} 

	//****************************************************************************************************
	retApks() : Promise<Apk[]> 
	{
		return new Promise
		(
			resolve => 
			{
				resolve(this.apks);
			}
		); 

	}

	//****************************************************************************************************
	apks: Apk[];
}
