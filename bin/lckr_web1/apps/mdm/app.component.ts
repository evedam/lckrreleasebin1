
import { Component } from '@angular/core';

import { CfgService } from './cfg.service.ts';
import { ViewService } from './view.service.ts';

@Component
({
	moduleId: module.id,
	selector: 'mdm-app',
	templateUrl: 'app.component.html'
})
export class AppComponent 
{
	constructor
	(
		private cfg: CfgService,
		private viewService: ViewService
	) 
	{
		this.title = "Main Content";		
	}

	title = 'VCom';
}
