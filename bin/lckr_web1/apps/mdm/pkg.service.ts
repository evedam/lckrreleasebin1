import { Injectable } from '@angular/core';

import { User } from './User.ts';
import { Pkg } from './pkg.ts';

@Injectable()
export class PkgService
{
	//****************************************************************************************************
	getPkg(usrId: string, id: string): Promise<Pkg> 
	{
		console.log("get a pkgs usrId = (" + usrId + ") id = (" + id  + ")");

		return this.getPkgs(usrId).then(pkgs => pkgs.find(pkg => pkg.id == id));
	}

	//****************************************************************************************************
	getPkgs(usr: string, fromServer: boolean): Promise<Pkg[]>
	{
		console.log("get pkgs usr = " + usr + " from_server = " + fromServer);

		if (usr == null || usr == undefined)
		{
			console.log("please enter valid user name to get packages");
			return;
		}
		
		if (this.usrPkgs[usr] == null || fromServer)
		{
			p_Msg.sendGetApkLst(125, "", usr, 60);	
			this.usrPkgs[usr] = null; 

			return new Promise
			(
				resolve => 
				{ 
					p_Msg.setOnLstApkInstalled(function(id, from, pkgs)
					{
						console.log("on lst pkg installed id = " + id + " from = " + usr+ 
							" pkgs = " + JSON.stringify(pkgs));

						var aPkg = [];
						var len = pkgs.length;
						for (var idx = 0; idx < len; idx++)
						{
							var pkg = new Pkg(); 
							pkg.load(pkgs[idx]);
							aPkg.push(pkg);
						}
						this.usrPkgs[usr] = aPkg;

						resolve(this.usrPkgs[usr]);
					}.bind(this));
				}
			); 

		}
		else
			return this.retPkgs(usr);
	} 

	//****************************************************************************************************
	retPkgs(usr: string) : Promise<Pkgs[]> 
	{
		return new Promise
		(
			resolve => 
			{
				resolve(this.usrPkgs[usr]);
			}
		); 

	}

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
		console.log("on user selected " + JSON.stringify(usr));
		
		if (this.getCntUsr() > 0)
		{
			console.log("requesting packages from " + JSON.stringify(usr));
			this.getPkgs(usr.getId(), false);
		}
	}

	//****************************************************************************************************
	incCntUsr() : void
	{
		this.cntUsr++;
	}

	//****************************************************************************************************
	decCntUsr() : void
	{
		this.cntUsr--;
	}

	//****************************************************************************************************
	getCntUsr() : number
	{
		return this.cntUsr;
	}

	//****************************************************************************************************
	usrPkgs = {}; 
	cntUsr: number = 0;
}
