
import { Injectable } from '@angular/core';

@Injectable()
export class LocService
{
	//****************************************************************************************************
	init() : void
	{
		console.log("initializion loc service");
		p_Msg.getModLoc().setOnCurrentL(function(pData)
		{
			console.log("data = " + JSON.stringify(pData));	
			if (Array.isArray(pData))
				this.loadLLoc(pData);
			else
				console.log("invalid response");
		}.bind(this)); 
	}

	//****************************************************************************************************
	private loadLLoc(locs) : viod
	{
		this.locs.splice(0, locs.length);
		console.log("loaded locations " + JSON.stringify(this.locs));

		for (let i = 0; i < locs.length; i++)
		{
			let loc = locs[i];
			console.log("loc = " + JSON.stringify(loc));

			this.locs.push
			(
				{ 
					lat: loc.lat,
					lng: loc.lng,
					imei: loc.imei
				}
			);

			console.log("locs = " + JSON.stringify(this.locs));
		}
	}

	//****************************************************************************************************
	getCurrentL(fromServer: boolean) : void
	{
		p_Msg.getModLoc().getCurrentL();
	}

	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
	}
	
	locs:[] = 
	[
		{
			lat: 6.9076835,
			lng: 79.9223699,
			imei: 'A',
			draggable: true
		}
	]; 
}
