import { Injectable } from '@angular/core';

import { User } from './User.ts';

@Injectable()
export class CmdService
{
	//****************************************************************************************************
	sendCmd(id: number, sesId: string, to: string, cmd) : void
	{
		console.log("sendCmd id = " + id + " sesId = " + sesId + " to = " + to + " cmd = " + JSON.stringify(cmd));

		if (!isSet(id))
		{
			console.log("please enter valid id");
			return;
		}

		if (!isSet(to))
		{
			console.log("please enter valid to");
			return;
		}

		if (!isSet(cmd))
		{
			console.log("please enter valid cmd");
			return;
		}

		if (p_Msg == null)
		{
			console.log("no p_Msg");
			return;
		}

		p_Msg.sendCmd(id, sesId, to, JSON.stringify(cmd));
	} 

	//****************************************************************************************************
	startScreenShare(to: string) : void
	{
		p_Msg.shareScreen(2000, to);		
	}

	//****************************************************************************************************
	stopScreenShare(to: string) : void
	{
		p_Msg.stopShareScreen(2001, to);		
	}

	//****************************************************************************************************
	init() : void
	{
		p_Msg.setOnCmdRes(function(id, from, res)
		{
			console.log("onCmdRes id = " + id + " from = " + from + " res = " + res);

			var oRes = JSON.parse(res);
			if (oRes.type == "install")
			{
				if (oRes.state == "downloading_started")	
				{
					console.log("apk downloading started");
					showSuccessNotification("Downloading started by " + from);
				}
				else if (oRes.state == "downloading_progress")
				{
					var progress = oRes.progress;
					var time = new Date().getTime() / 1000;
					if (time - this.noticedTime > 10)
					{
						this.noticedTime = time;
						//showSuccessNotification("Download apk in progress " + progress + " for user " + from);
					}
					console.log("apk downloading progress " + progress);
				}
				else if (oRes.state == "started")
				{
					console.log("apk installed to install");
					showSuccessNotification("Installing process started for user " + from);
				}
			}
			else
				console.log("invalid type " + oRes.type);
			
		}.bind(this));
	}
	

	//****************************************************************************************************
	// service callbacks

	//****************************************************************************************************
	onSetUsrSel(usr: User) : void
	{
	}

	//****************************************************************************************************
	callbacks = [];
	noticedTime: number = 0;
}
