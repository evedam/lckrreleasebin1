
export class Script
{
	//****************************************************************************************************
	constructor()
	{

	}

	//****************************************************************************************************
	public load(data) : void
	{
		this.setBash(data.bash);
		this.setRoot(data.root);
	}

	//****************************************************************************************************
	public getData() 
	{
		var data = {};
		data.bash = this.getBash();
		data.root = this.isRoot();
		return data;
	}

	public setBash(cmd) : void
	{
		this.bash = cmd;
	}

	public getBash() : string
	{
		return this.bash;
	}

	public setRoot(root: boolean) : void	
	{
		this.root = root;
	}

	public isRoot() : boolean
	{
		return this.root;
	}

	bash: string = "";
	root: boolean = false;
}

