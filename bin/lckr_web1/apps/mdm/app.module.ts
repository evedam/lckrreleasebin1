import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';

import { AuthGuard }		from './auth_guard.service';
import { AppComponent }         from './app.component';
import { LoginComponent} 	from './comp/login/login.component';
import { AdminComponent }   	from './comp/admin/admin.component';
import { UserDetailComponent }  from './comp/user_detail/user_detail.component';
import { UseresComponent }      from './comp/users/users.component';
import { CallComponent }	from './comp/call/call.component';
import { TopbarComponent }	from './comp/topbar/topbar.component';
import { MenuComponent }	from './comp/menu/menu.component';
import { SidebarComponent }	from './comp/sidebar/sidebar.component';
import { AddApkComponent }	from './comp/add_apk/add_apk.component';
import { ViewApksComponent } 	from './comp/view_apks/view_apks.component';
import { InstallApkComponent }	from './comp/install_apk/install_apk.component';
import { InstalledPackagesComponent } from './comp/installed_packages/installed_packages.component';
import { UninstallPackageComponent } from './comp/uninstall_package/uninstall_package.component';
import { DeviceManComponent } 	from './comp/device_man/device_man.component';
import { FilesComponent } 	from './comp/files/files.component';
import { ScreenShareComponent } from './comp/screen_share/screen_share.component';
import { ScriptComponent } 	from './comp/script/script.component';
import { GroupsComponent }	from './comp/groups/groups.component';
import { LocComponent }		from './comp/loc/loc.component';
import { UserService }          from './user.service';
import { ApkService }           from './apk.service';
import { PkgService }           from './pkg.service';
import { CmdService }           from './cmd.service';
import { ScreenShareService }   from './screen_share.service';
import { FileService }        	from './file.service';
import { ScriptService }	from './script.service';
import { DeviceStateService }	from './device_state.service';
import { LocService }		from './loc.service';
import { CfgService }		from './cfg.service';
import { ViewService } 		from './view.service';
import { AppRoutingModule }     from './app_routing.module';

import { ReversePipe } 		from './pipe/reverse.pipe';

import { DataTableModule }	from 'angular2-datatable'; 
import { TooltipModule } 	from "ngx-tooltip";
import { AgmCoreModule } 	from 'angular2-google-maps'; 

import { UsersTableModule }	from './comp/users_table/users_table.module';
import { DeviceStatesModule } 	from './comp/device_states/device_states.module';
import { ReportDeviceListModule }from './comp/report/device_list/device_list.module';

@NgModule
(
{
	imports: 
	[
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		DataTableModule,
		TooltipModule,
		CommonModule,
		UsersTableModule,
		DeviceStatesModule,
		ReportDeviceListModule,
		AgmCoreModule.forRoot({ apiKey: "AIzaSyCQBfOuf37egeTjaAscnSlDrLNnpSwjqYM" })
	],
	declarations: 
	[
		AppComponent,
		LoginComponent,
		AdminComponent,
		UserDetailComponent,
		UseresComponent,
		CallComponent,
		TopbarComponent,
		MenuComponent,	
		SidebarComponent,	
		AddApkComponent,
		ViewApksComponent,
		InstallApkComponent,
		InstalledPackagesComponent,
		UninstallPackageComponent,
		DeviceManComponent,
		FilesComponent,
		ScreenShareComponent,
		ScriptComponent,
		GroupsComponent,
		LocComponent,
	
		ReversePipe
	],
	providers: 
	[ 
		AuthGuard,
		UserService, 
		ApkService, 
		PkgService, 
		CmdService, 
		ScreenShareService, 
		FileService, 
		ScriptService, 
		DeviceStateService, 
		LocService, 
		CfgService,
		ViewService 
	],
	bootstrap: 
	[ 
		AppComponent 
	]
})
export class AppModule 
{ 

}

