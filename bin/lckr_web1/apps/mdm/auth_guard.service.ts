
import { Injectable } from '@angular/core';
import	{ 
		CanActivate, 
		Router,  
		ActivatedRouteSnapshot, 
		RouterStateSnapshot 
	} from '@angular/router';

import { CfgService } from "./cfg.service.ts";

@Injectable()
export class AuthGuard implements CanActivate 
{
	//****************************************************************************************************	
	constructor(private cfg : CfgService, private router: Router)
	{
	
	}

	//****************************************************************************************************	
	canActivate(pRoute: ActivatedRouteSnapshot, pState: RouterStateSnapshot) 
	{
		console.log('AuthGuard#canActivate called');
		return this.hasAccess(pState.url); 
	}
	
	//****************************************************************************************************	
	hasAccess(sUrl: string) : boolean
	{
		console.log("has access " + sUrl);
		var bHasAccess = false;
		if (this.cfg.isLogged())
		{
			bHasAccess = this.cfg.getUsr().getType() == "admin";
		} 
		
		if (!bHasAccess)
		{
			this.router.navigate(['/login']);
			return bHasAccess 
		}

		return bHasAccess;
	}
}
