
export default class Util
{
	static isSet(val)
	{
		return val != undefined && typeof val != undefined && val != "" && !(Object.keys(val).length === 0 && val.constructor === Object);
	}

	static isDef(val)
	{ 
		return val != undefined && typeof val != undefined;
	}

	static isInt(val)
	{
		return !isNaN(val);
	}
	
	static isValidEmail(email)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email); 
	}

	static isValidInt(val)
	{
		return Number.isInteger(val);
	}

	static isValidTime(time)
	{
		return Number.isInteger(time);
	}

	static isValidAlphabet(val)
	{
		return true;
	}
}
