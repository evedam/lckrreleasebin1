import * as sourceMaps from 'source-map-support'
import FileUtils from './utils/fileUtils'
import * as serverConfigs from './utils/serverConfigs'
import logger from './utils/logger'
import * as constants from './app/helpers/constants'

const platformConfig = FileUtils.loadConfigs(serverConfigs.CONFIGS.PLATFORM)

/**
 * All daemon services that should be instantiated before starting the https server, or that can be
 * initiated asynchronously will be initiated/instantiated here
 */
export default class Daemon {
    /**
     * Instantiate daemon service proxy
     */
    constructor() {
        // Do nothing
    }

    /**
     * Instantiate and/or initiate daemon services that are required before starting the https server
     * @param {function} callback - callback
     * @returns {function} callback
     */
    static initSyncServices(callback) {
        Daemon.modifyErrorPrototype();
        return callback();
    }

    /**
     * Instantiate and/or initiate daemon services that run independent of the https server
     */
    static initAsyncServices() {
        Daemon.enableSourceMaps();
    }

    /**
     * Enable Source-map support in dev environments
     */
    static enableSourceMaps() {
        if (platformConfig[serverConfigs.SOURCE_MAPS_SUPPORT]) {
            sourceMaps.install();
        }
    }

    /**
     * Appends additional details to the standard error object
     * @param {string} [pathOverrider] - String to override the error propagation path
     * @param {string} [causesOverrider] - String to override the error causes list
     */
    static modifyErrorPrototype(pathOverrider, causesOverrider) {
        /**
         * @param {string} className - Name of the module in which the error was detected
         * @param {string} method - Name of the module-method in which the error was detected
         * @param {string} cause - More details about the cause of the error
         */
        Error.prototype.appendDetails = function (className = "*NULL*", method = "*NULL*", cause = "*NULL*") {
            this.path = pathOverrider || (this.path || "#") + ` -> [${className}]|(${method})`;
            this.causes = causesOverrider || (this.causes || "#") + ` -> (${method})|${cause}`;
        };
    }

}
