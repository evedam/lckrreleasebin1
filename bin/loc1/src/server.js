/**
 * All configurations related to the https server implementation should go here
 */

import https from 'https';
import http from 'http';
import * as sourceMaps from 'source-map-support';

import app from './app';
import Daemon from './daemon';
import FileUtils from './utils/fileUtils';
import * as serverConfigs from './utils/serverConfigs';
import logger from './utils/logger';
import * as constants from './app/helpers/constants';

sourceMaps.install();
const platformConfig = FileUtils.loadConfigs(serverConfigs.CONFIGS.PLATFORM);

const credentials = {
    key: FileUtils.readCertificateSync("KEY"),
    cert: FileUtils.readCertificateSync("CERT"),
    ca: FileUtils.readCertificateSync("CA"),
    requestCert: true,
    rejectUnauthorized: false
};

const PORT = process.env.PORT || platformConfig[serverConfigs.HTTPS_PORT];
const HOST = process.env.HOST || "localhost";
const secure = false;

// Create HTTPS server and listen on dedicated port
const server = secure ? https.createServer(credentials, app) : http.createServer(app);

// Instantiate daemon service
new Daemon();

// Init HTTPS server dependencies and start listening
Daemon.initSyncServices(function () {
    server.listen(PORT, function () {
        logger.info(`Listening on port ${PORT}`);
    });
});

// Init asynchronous services
Daemon.initAsyncServices();

//Stop process killing on exceptions
process.on('uncaughtException', function (err) {
    logger.fatal('UncaughtException : %s', err.stack ? err.stack : err);
});

server.on('uncaughtException', function (req, res, next, err) {
    logger.error('UncaughtException : %s', err.stack ? err.stack : err);
    return res.status(constants.INTERNAL_ERROR).send(err.message);
});

server.on('error', function (err) {
    logger.fatal('Error : %s', err.stack ? err.stack : err);
    switch (err.code) {
        case 'EACCES':
            process.exit(1);
            break;
        case 'EADDRINUSE':
            process.exit(1);
            break;
    }
});
