import Conn from './conn';
import Mod from '../helpers/mod';
import Accelerometer from '../ds/accelerometer';
import * as mongo from 'mongodb';
import logger from '../../utils/logger';
import configs from '../../utils/configs';
import util from '../../utils/util';

class AccelerometerMod
{
	/**
	 * 
	 * Get constuctor of Accelerometer
	 * 
	 */
	constructor()
	{
		
	}
	
	/**
	 * 
	 *  Get collection for Accelerometer
	 * 
	 * @param { function } fnc return callback to get collection
	 * 
	 */
	getCol(ret)
	{
		logger.info("mod::Accelerometer::getCol");
		
		Conn.connect(function(db)
		{
			logger.info("mod::accelerometer::connected");
			
			ret(db.collection(configs.getTblAccelerometer()));
		}.bind(this))
	}
	
	/**
	 * 
	 * Add data for Accelerometer
	 * 
	 * @param { Accelerometer } accelerometer data object
	 * @param { function } ret return callback
	 * 
	 */
	add(accelerometer, ret)
	{
		logger.info("mod::Accelerometer::add data = " + JSON.stringify(accelerometer));
		
		let data = new Accelerometer();
		data.load(accelerometer);
		data.setCreatedTime(new Date().getTime());
		
		let vdtMsg = this.validate(data, true);
		if (util.isSet(vdtMsg))
			return ret(false, vdtMsg);
		
		this.getCol(function(col)
		{
			let unique = [];
			let uniqueFilter = {}
			
			if (util.isSet(data.getId()))
				unique.push({'_id':data.getId()});
			
			uniqueFilter["$or"] = unique;
			
			col.findOne(uniqueFilter, function(err, doc)
			{
				if (!doc)
				{
					col.insertOne(data, function(err, result)
					{
						if (!err)
						{
							logger.info("user inserting successful ret = (" + JSON.stringify(result) + ")");
							ret(true, result.insertedId);
						}
						else
						{
							logger.info("inserting error [" + err + "]");
							ret(false, err);
						}
					}.bind(this));
				}
				else
				{
					let vdtMsg = {};
					
					if (util.isSet(data._id) && doc._id == data._id)
					{
						if (!util.isSet(vdtMsg._id))
							vdtMsg._id = [];
						
						vdtMsg._id.push("_id already exits");
					}
					
					vdtMsg.type = "duplicate keys";
					logger.error("ERROR : " + JSON.stringify(vdtMsg));
					ret(false, vdtMsg);
				}
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Set data for Accelerometer
	 * 
	 * @param { Accelerometer.Filter } filter filter for select data to be update
	 * @param { Accelerometer } accelerometer  object to update
	 * @param { function } ret return callback
	 * 
	 */
	set(filter, accelerometer, ret)
	{
		logger.info("mod::Accelerometer::set filter = " + JSON.stringify(filter) + " accelerometer = " + JSON.stringify(accelerometer));
		
		filter = new Filter(filter);
		let data = new Accelerometer();
		data.load(accelerometer);
		
		let vdtMsg = this.validate(data, false);
		if (util.isSet(vdtMsg))
			return ret(false, vdtMsg);
		
		this.getCol(function(col)
		{
			let unique = [];
			let uniqueFilter = {}
			
			if (util.isSet(data.getId()))
				unique.push({'_id':data.getId()});
			
			if (util.isSet(filter.getId()))
				uniqueFilter._id = {$ne:filter.getId()};
			
			uniqueFilter["$or"] = unique;
			
			col.findOne(uniqueFilter, function(err, doc)
			{
				if (!doc)
				{
					col.update(filter, {$set:data}, function(err, result)
					{
						if (!err)
						{
							logger.info("mod::Accelerometer::set update success");
							ret(true, null);
						}
						else
						{
							logger.info("mod::Accelerometer::set error " + err);
							ret(false, err);
						}
					}.bind(this));
				}
				else
				{
					let vdtMsg = {};
					
					if (util.isSet(data._id) && doc._id == data._id)
					{
						if (!util.isSet(vdtMsg._id))
							vdtMsg._id = [];
						
						vdtMsg._id.push("_id already exits");
					}
					
					vdtMsg.type = "duplicate keys";
					logger.error("ERROR : " + JSON.stringify(vdtMsg));
					ret(false, vdtMsg);
				}
			}.bind(this));
		}.bind(this))
	}
	
	/**
	 * 
	 *  Set data from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { Accelerometer } accelerometer object to update
	 * @param { function } ret return callback
	 * 
	 */
	setFromId(_id, accelerometer, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		_id = new mongo.ObjectID(_id);
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, accelerometer, ret);
	}
	
	/**
	 * 
	 * Set _id for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { id } _id _id to update
	 * @param { function } ret return callback
	 * 
	 */
	setId(id, _id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isDef(_id))
			return ret(false, "please enter valid _id [undefined]");
		
		col.findOne({_id:_id}, function(err, doc)
		{
			if (!doc)
			{
				let filter = new Filter();
				filter.setId(new mongo.ObjectID(id));
				this.set(filter, {_id:_id}, ret);
			}
			else
			{
				let msg = "(_id already exits)";
				logger.error("duplicate keys (" + msg + ")");
				ret(false, "duplicate keys (" + msg + ") ");
			}
		}.bind(this));
	}
	
	/**
	 * 
	 * Set imei for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { string } imei imei to update
	 * @param { function } ret return callback
	 * 
	 */
	setImei(id, imei, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {imei:imei}, ret);
	}
	
	/**
	 * 
	 * Set x for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { string } x x to update
	 * @param { function } ret return callback
	 * 
	 */
	setX(id, x, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(x))
			return ret(false, "please enter valid x");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {x:x}, ret);
	}
	
	/**
	 * 
	 * Set y for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { string } y y to update
	 * @param { function } ret return callback
	 * 
	 */
	setY(id, y, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(y))
			return ret(false, "please enter valid y");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {y:y}, ret);
	}
	
	/**
	 * 
	 * Set z for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { string } z z to update
	 * @param { function } ret return callback
	 * 
	 */
	setZ(id, z, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(z))
			return ret(false, "please enter valid z");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {z:z}, ret);
	}
	
	/**
	 * 
	 * Set time for Accelerometer
	 * 
	 * @param { id } id id of the field
	 * @param { string } time time to update
	 * @param { function } ret return callback
	 * 
	 */
	setTime(id, time, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(time))
			return ret(false, "please enter valid time");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {time:time}, ret);
	}
	
	/**
	 * 
	 * Set imei from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } imei imei to update
	 * @param { function } ret return callback
	 * 
	 */
	setImeiFromId(_id, imei, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {imei:imei}, ret);
	}
	
	/**
	 * 
	 * Set x from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } x x to update
	 * @param { function } ret return callback
	 * 
	 */
	setXFromId(_id, x, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(x))
			return ret(false, "please enter valid x");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {x:x}, ret);
	}
	
	/**
	 * 
	 * Set y from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } y y to update
	 * @param { function } ret return callback
	 * 
	 */
	setYFromId(_id, y, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(y))
			return ret(false, "please enter valid y");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {y:y}, ret);
	}
	
	/**
	 * 
	 * Set z from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } z z to update
	 * @param { function } ret return callback
	 * 
	 */
	setZFromId(_id, z, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(z))
			return ret(false, "please enter valid z");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {z:z}, ret);
	}
	
	/**
	 * 
	 * Set time from _id for Accelerometer
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } time time to update
	 * @param { function } ret return callback
	 * 
	 */
	setTimeFromId(_id, time, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(time))
			return ret(false, "please enter valid time");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {time:time}, ret);
	}
	
	/**
	 * 
	 * Get one row of Accelerometer
	 * 
	 * @param { Accelerometer.Filter } filter filter to select values
	 * @param { function } ret return callback
	 * 
	 */
	getOne(filter, opt, ret)
	{
		opt = new Opt(opt);
		logger.info("filter = " + JSON.stringify(filter));
		let o = {limit:opt.getLimit(), skip:opt.getSkip(), sort:opt.getSort()};
		
		this.getCol(function(col)
		{
			logger.info("mod::Accelerometer::getOne filter = " + JSON.stringify(filter));
			
			col.findOne(filter, o, function(err, doc)
			{
				if (!err && util.isSet(doc))
				{
					let val = new Accelerometer();
					val.load(doc);
					logger.info("mod::Accelerometer::getOne data = " + JSON.stringify(val));
					ret(true, val);
				}
				else
				{
					logger.error("ERROR : " + err);
					ret(false, null);
				}
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 *  Get one object of Accelerometer from _id
	 * 
	 * @param { id } id id to select values
	 * @param { function } ret return callback
	 * 
	 */
	get(id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		this.getOne({_id:new mongo.ObjectID(id)}, ret);
	}
	
	/**
	 * Get one object of Accelerometer from _id
	 * 
	 * @param { id } _id _id to select values
	 * @param { function } ret return callback
	 * 
	 */
	getFromId(_id, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		_id = mongo.ObjectID(_id);
		
		this.getOne(new Filter({_id:_id}), ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer
	 * 
	 * @param { Accelerometer.Filter } ft filter object to select values
	 * @param { function } ret return callback
	 * 
	 */
	getL(aFt, opt, ret)
	{
		if (!Array.isArray(aFt))
			aFt = [aFt];
		
		for (let i = 0; i < aFt.length; i++)
			aFt[i] = new Filter(aFt[i]);
		
		opt = new Opt(opt);
		logger.info("aFt = " + JSON.stringify(aFt));
		let o = {limit:opt.getLimit(), skip:opt.getSkip(), sort:opt.getSort()};
		let filter = [];
		
		for (let iFt = 0; iFt < aFt.length; iFt++)
		{
			let filterEle = {};
			
			let ft = aFt[iFt];
			
			if (ft.getId() != undefined)
			{
				filterEle._id = ft.getId();
			}
			
			if (ft.getImei() != undefined)
			{
				filterEle.imei = ft.getImei();
			}
			
			if (ft.getX() != undefined)
			{
				filterEle.x = ft.getX();
			}
			
			if (ft.getY() != undefined)
			{
				filterEle.y = ft.getY();
			}
			
			if (ft.getZ() != undefined)
			{
				filterEle.z = ft.getZ();
			}
			
			if (ft.getTime() != undefined)
			{
				filterEle.time = ft.getTime();
			}
			
			if (ft.getCreatedTime() != undefined)
			{
				filterEle.createdTime = ft.getCreatedTime();
			}
			
			filter.push(filterEle);
		}
		
		if (filter.length == 1)
		{
			filter = filter[0];
		}
		else
		{
			let ft = {}
			ft["$or"] = filter;
			filter = ft;
		}
		
		logger.info("filter = " + JSON.stringify(filter) + "");
		
		this.getCol(function(col)
		{
			col.find(filter, o).toArray().then(function(vals)
			{
				let retArray = [];
				let len = vals.length;
				
				for (let idx = 0; idx < len; idx++)
				{
					let doc = vals[idx];
					let val = new Accelerometer();
					val.load(doc);
					logger.info("mod::Accelerometer::getL loaded = " + doc._id);
					retArray.push(val);
				}
				
				ret(true, retArray);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from imei
	 * 
	 * @param { string } imei imei to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromImei(imei, skip, limit, ret)
	{
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setImei(imei);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from x
	 * 
	 * @param { string } x x to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromX(x, skip, limit, ret)
	{
		if (!util.isSet(x))
			return ret(false, "please enter valid x");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setX(x);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from y
	 * 
	 * @param { string } y y to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromY(y, skip, limit, ret)
	{
		if (!util.isSet(y))
			return ret(false, "please enter valid y");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setY(y);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from z
	 * 
	 * @param { string } z z to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromZ(z, skip, limit, ret)
	{
		if (!util.isSet(z))
			return ret(false, "please enter valid z");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setZ(z);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from time
	 * 
	 * @param { string } time time to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromTime(time, skip, limit, ret)
	{
		if (!util.isSet(time))
			return ret(false, "please enter valid time");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setTime(time);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Accelerometer from createdTime
	 * 
	 * @param { string } createdTime createdTime to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromCreatedTime(createdTime, skip, limit, ret)
	{
		if (!util.isSet(createdTime))
			return ret(false, "please enter valid createdTime");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setCreatedTime(createdTime);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get set of field
	 * 
	 * @param { field } field field name
	 * @param { function } ret return callback
	 * 
	 */
	getSet(field, ret)
	{
		this.getCol(function(col)
		{
			col.distinct(field, function(err, docs)
			{
				console.log(JSON.stringify(docs));
				ret(true, docs);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Search for list of objects of Accelerometer
	 * 
	 * @param { Accelerometer.Filter } ft filter object to select values
	 * @param { function } ret return callback
	 * 
	 */
	searchL(aFt, opt, ret)
	{
		if (!Array.isArray(aFt))
			aFt = [aFt];
		
		for (let i = 0; i < aFt.length; i++)
			aFt[i] = new Filter(aFt[i]);
		
		for (let iFt = 0; iFt < aFt.length; iFt++)
		{
			let ft = aFt[iFt];
			
			if (util.isSet(ft.imei))
				ft.setImei({$regex:new RegExp("^.*" + ft.imei + ".*$")});
			
			if (util.isSet(ft.x))
				ft.setX({$regex:new RegExp("^.*" + ft.x + ".*$")});
			
			if (util.isSet(ft.y))
				ft.setY({$regex:new RegExp("^.*" + ft.y + ".*$")});
			
			if (util.isSet(ft.z))
				ft.setZ({$regex:new RegExp("^.*" + ft.z + ".*$")});
			
			if (util.isSet(ft.time))
				ft.setTime({$regex:new RegExp("^.*" + ft.time + ".*$")});
			
			if (util.isSet(ft.createdTime))
				ft.setCreatedTime({$regex:new RegExp("^.*" + ft.createdTime + ".*$")});
			
		}
		
		logger.info("filter = " + JSON.stringify(aFt));
		
		this.getL(aFt, opt, ret);
	}
	/**
	 * Remove one row of Accelerometer
	 * 
	 * @param { Accelerometer.Filter } filter filter to select values
	 * @param { function } ret return callback
	 * 
	 */
	rmOne(filter, ret)
	{
		this.getCol(function(col)
		{
			logger.info("mod::Accelerometer::rmOne filter = " + JSON.stringify(filter));
			
			col.deleteOne(filter, function(err, doc)
			{
				if (!err)
					return ret(true);
				else
					ret(false, err);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * Remove one object of Accelerometer from _id
	 * 
	 * @param { id } id id to select values
	 * @param { function } ret return callback
	 * 
	 */
	rm(id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		this.rmOne({_id:new mongo.ObjectID(id)}, ret);
	}
	
	/**
	 * Remove one object of Accelerometer from _id
	 * 
	 * @param { id } _id _id to select values
	 * @param { function } ret return callback
	 * 
	 */
	rmFromId(_id, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		this.rmOne(new Filter({_id:_id}), ret);
	}
	
	/**
	 * 
	 * validate Accelerometer object
	 * 
	 * @param { Object } accelerometer input object
	 * @param { boolean } isNew to indicate validate new object or existing object
	 * 
	 */
	validate(accelerometer, isNew)
	{
		let vdtMsg = {};
		if (this.validateId(accelerometer.getId(), isNew).length > 0)
			vdtMsg._id = this.validateId(accelerometer.getId(), isNew);
		
		if (this.validateImei(accelerometer.getImei(), isNew).length > 0)
			vdtMsg.imei = this.validateImei(accelerometer.getImei(), isNew);
		
		if (this.validateX(accelerometer.getX(), isNew).length > 0)
			vdtMsg.x = this.validateX(accelerometer.getX(), isNew);
		
		if (this.validateY(accelerometer.getY(), isNew).length > 0)
			vdtMsg.y = this.validateY(accelerometer.getY(), isNew);
		
		if (this.validateZ(accelerometer.getZ(), isNew).length > 0)
			vdtMsg.z = this.validateZ(accelerometer.getZ(), isNew);
		
		if (this.validateTime(accelerometer.getTime(), isNew).length > 0)
			vdtMsg.time = this.validateTime(accelerometer.getTime(), isNew);
		
		if (this.validateCreatedTime(accelerometer.getCreatedTime(), isNew).length > 0)
			vdtMsg.createdTime = this.validateCreatedTime(accelerometer.getCreatedTime(), isNew);
		
		return vdtMsg;
	}
	
	/**
	 * 
	 * validate  _id
	 * 
	 * @param { id } _id validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateId(_id, isNew)
	{
		let ret = [];
		
		if (util.isSet(_id) && !util.isValidAlphabet(_id))
		{
			ret.push("please enter valid _id [alphabet error] (" + _id + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  imei
	 * 
	 * @param { string } imei validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateImei(imei, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(imei))
		{
			ret.push("please enter valid imei [empty value] (" + imei + ") ");
		}
		
		if (util.isSet(imei) && !util.isValidAlphabet(imei))
		{
			ret.push("please enter valid imei [alphabet error] (" + imei + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  x
	 * 
	 * @param { string } x validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateX(x, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(x))
		{
			ret.push("please enter valid x [empty value] (" + x + ") ");
		}
		
		if (util.isSet(x) && !util.isValidAlphabet(x))
		{
			ret.push("please enter valid x [alphabet error] (" + x + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  y
	 * 
	 * @param { string } y validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateY(y, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(y))
		{
			ret.push("please enter valid y [empty value] (" + y + ") ");
		}
		
		if (util.isSet(y) && !util.isValidAlphabet(y))
		{
			ret.push("please enter valid y [alphabet error] (" + y + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  z
	 * 
	 * @param { string } z validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateZ(z, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(z))
		{
			ret.push("please enter valid z [empty value] (" + z + ") ");
		}
		
		if (util.isSet(z) && !util.isValidAlphabet(z))
		{
			ret.push("please enter valid z [alphabet error] (" + z + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  time
	 * 
	 * @param { string } time validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateTime(time, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(time))
		{
			ret.push("please enter valid time [empty value] (" + time + ") ");
		}
		
		if (util.isSet(time) && !util.isValidAlphabet(time))
		{
			ret.push("please enter valid time [alphabet error] (" + time + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  createdTime
	 * 
	 * @param { string } createdTime validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateCreatedTime(createdTime, isNew)
	{
		let ret = [];
		
		if (util.isSet(createdTime) && !util.isValidTime(createdTime))
		{
			ret.push("please enter valid createdTime [syntax error] (" + createdTime + ") ");
		}
		
		if (util.isSet(createdTime) && !util.isValidAlphabet(createdTime))
		{
			ret.push("please enter valid createdTime [alphabet error] (" + createdTime + ") ");
		}
		
		return ret;
	}
	
}

export default new AccelerometerMod();

export class Opt
{
	constructor(data)
	{
		
		if (util.isSet(data))
			this.load(data);
	}
	
	/**
	 * 
	 * Load data from for options
	 * 
	 * @param { Object } data input object
	 * 
	 */
	load(data)
	{
		if (util.isSet(data.skip))
			this.setSkip(parseInt(data.skip));
		
		if (util.isSet(data.limit))
			this.setLimit(parseInt(data.limit))
		
		if (util.isSet(data.sort))
			this.setSort(data.sort);
		
	}
	
	setSkip(skip)
	{
		this.skip = util.isSet(skip) ? skip : 0;
	}
	
	getSkip()
	{
		return this.skip || 0;
	}
	
	setLimit(limit)
	{
		this.limit = util.isSet(limit) ? limit : 100;
	}
	
	getLimit()
	{
		return this.limit || 100;
	}
	
	setSort(sort)
	{
		this.sort = sort;
	}
	
	getSort()
	{
		return this.sort;
	}
	
}

export class Filter
{
	constructor(data)
	{
		
		if (util.isSet(data))
			this.load(data);
	}
	
	/**
	 * 
	 * Load data from a object of Accelerometer
	 * 
	 * @param { Object } data input object
	 * 
	 */
	load(data)
	{
		if (util.isDef(data._id))
			this.setId(data._id);
		
		if (util.isDef(data.imei))
			this.setImei(data.imei);
		
		if (util.isDef(data.x))
			this.setX(data.x);
		
		if (util.isDef(data.y))
			this.setY(data.y);
		
		if (util.isDef(data.z))
			this.setZ(data.z);
		
		if (util.isDef(data.time))
			this.setTime(data.time);
		
		if (util.isDef(data.createdTime))
			this.setCreatedTime(data.createdTime);
		
	}
	
	setId(_id)
	{
		this._id = mongo.ObjectId(_id);
	}
	
	getId()
	{
		return this._id;
	}
	
	setImei(imei)
	{
		this.imei = imei;
	}
	
	getImei()
	{
		return this.imei;
	}
	
	setX(x)
	{
		this.x = x;
	}
	
	getX()
	{
		return this.x;
	}
	
	setY(y)
	{
		this.y = y;
	}
	
	getY()
	{
		return this.y;
	}
	
	setZ(z)
	{
		this.z = z;
	}
	
	getZ()
	{
		return this.z;
	}
	
	setTime(time)
	{
		this.time = time;
	}
	
	getTime()
	{
		return this.time;
	}
	
	setCreatedTime(createdTime)
	{
		this.createdTime = createdTime;
	}
	
	getCreatedTime()
	{
		return this.createdTime;
	}
	
}
