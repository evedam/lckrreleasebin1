import Conn from './conn';
import Mod from '../helpers/mod';
import Location from '../ds/location';
import * as mongo from 'mongodb';
import logger from '../../utils/logger';
import configs from '../../utils/configs';
import util from '../../utils/util';

class LocationMod
{
	/**
	 * 
	 * Get constuctor of Location
	 * 
	 */
	constructor()
	{
		
	}
	
	/**
	 * 
	 *  Get collection for Location
	 * 
	 * @param { function } fnc return callback to get collection
	 * 
	 */
	getCol(ret)
	{
		logger.info("mod::Location::getCol");
		
		Conn.connect(function(db)
		{
			logger.info("mod::location::connected");
			
			ret(db.collection(configs.getTblLocation()));
		}.bind(this))
	}
	
	/**
	 * 
	 * Add data for Location
	 * 
	 * @param { Location } location data object
	 * @param { function } ret return callback
	 * 
	 */
	add(location, ret)
	{
		logger.info("mod::Location::add data = " + JSON.stringify(location));
		
		let data = new Location();
		data.load(location);
		data.setCreatedTime(new Date().getTime());
		
		let vdtMsg = this.validate(data, true);
		if (util.isSet(vdtMsg))
			return ret(false, vdtMsg);
		
		this.getCol(function(col)
		{
			let unique = [];
			let uniqueFilter = {}
			
			if (util.isSet(data.getId()))
				unique.push({'_id':data.getId()});
			
			uniqueFilter["$or"] = unique;
			
			col.findOne(uniqueFilter, function(err, doc)
			{
				if (!doc)
				{
					col.insertOne(data, function(err, result)
					{
						if (!err)
						{
							logger.info("user inserting successful ret = (" + JSON.stringify(result) + ")");
							ret(true, result.insertedId);
						}
						else
						{
							logger.info("inserting error [" + err + "]");
							ret(false, err);
						}
					}.bind(this));
				}
				else
				{
					let vdtMsg = {};
					
					if (util.isSet(data._id) && doc._id == data._id)
					{
						if (!util.isSet(vdtMsg._id))
							vdtMsg._id = [];
						
						vdtMsg._id.push("_id already exits");
					}
					
					vdtMsg.type = "duplicate keys";
					logger.error("ERROR : " + JSON.stringify(vdtMsg));
					ret(false, vdtMsg);
				}
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Set data for Location
	 * 
	 * @param { Location.Filter } filter filter for select data to be update
	 * @param { Location } location  object to update
	 * @param { function } ret return callback
	 * 
	 */
	set(filter, location, ret)
	{
		logger.info("mod::Location::set filter = " + JSON.stringify(filter) + " location = " + JSON.stringify(location));
		
		filter = new Filter(filter);
		let data = new Location();
		data.load(location);
		
		let vdtMsg = this.validate(data, false);
		if (util.isSet(vdtMsg))
			return ret(false, vdtMsg);
		
		this.getCol(function(col)
		{
			let unique = [];
			let uniqueFilter = {}
			
			if (util.isSet(data.getId()))
				unique.push({'_id':data.getId()});
			
			if (util.isSet(filter.getId()))
				uniqueFilter._id = {$ne:filter.getId()};
			
			uniqueFilter["$or"] = unique;
			
			col.findOne(uniqueFilter, function(err, doc)
			{
				if (!doc)
				{
					col.update(filter, {$set:data}, function(err, result)
					{
						if (!err)
						{
							logger.info("mod::Location::set update success");
							ret(true, null);
						}
						else
						{
							logger.info("mod::Location::set error " + err);
							ret(false, err);
						}
					}.bind(this));
				}
				else
				{
					let vdtMsg = {};
					
					if (util.isSet(data._id) && doc._id == data._id)
					{
						if (!util.isSet(vdtMsg._id))
							vdtMsg._id = [];
						
						vdtMsg._id.push("_id already exits");
					}
					
					vdtMsg.type = "duplicate keys";
					logger.error("ERROR : " + JSON.stringify(vdtMsg));
					ret(false, vdtMsg);
				}
			}.bind(this));
		}.bind(this))
	}
	
	/**
	 * 
	 *  Set data from _id for Location
	 * 
	 * @param { id } _id _id of the field
	 * @param { Location } location object to update
	 * @param { function } ret return callback
	 * 
	 */
	setFromId(_id, location, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		_id = new mongo.ObjectID(_id);
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, location, ret);
	}
	
	/**
	 * 
	 * Set _id for Location
	 * 
	 * @param { id } id id of the field
	 * @param { id } _id _id to update
	 * @param { function } ret return callback
	 * 
	 */
	setId(id, _id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isDef(_id))
			return ret(false, "please enter valid _id [undefined]");
		
		col.findOne({_id:_id}, function(err, doc)
		{
			if (!doc)
			{
				let filter = new Filter();
				filter.setId(new mongo.ObjectID(id));
				this.set(filter, {_id:_id}, ret);
			}
			else
			{
				let msg = "(_id already exits)";
				logger.error("duplicate keys (" + msg + ")");
				ret(false, "duplicate keys (" + msg + ") ");
			}
		}.bind(this));
	}
	
	/**
	 * 
	 * Set imei for Location
	 * 
	 * @param { id } id id of the field
	 * @param { string } imei imei to update
	 * @param { function } ret return callback
	 * 
	 */
	setImei(id, imei, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {imei:imei}, ret);
	}
	
	/**
	 * 
	 * Set lng for Location
	 * 
	 * @param { id } id id of the field
	 * @param { string } lng lng to update
	 * @param { function } ret return callback
	 * 
	 */
	setLng(id, lng, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(lng))
			return ret(false, "please enter valid lng");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {lng:lng}, ret);
	}
	
	/**
	 * 
	 * Set lat for Location
	 * 
	 * @param { id } id id of the field
	 * @param { string } lat lat to update
	 * @param { function } ret return callback
	 * 
	 */
	setLat(id, lat, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		if (!util.isSet(lat))
			return ret(false, "please enter valid lat");
		
		let filter = new Filter();
		filter.setId(new mongo.ObjectID(id));
		this.set(filter, {lat:lat}, ret);
	}
	
	/**
	 * 
	 * Set imei from _id for Location
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } imei imei to update
	 * @param { function } ret return callback
	 * 
	 */
	setImeiFromId(_id, imei, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {imei:imei}, ret);
	}
	
	/**
	 * 
	 * Set lng from _id for Location
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } lng lng to update
	 * @param { function } ret return callback
	 * 
	 */
	setLngFromId(_id, lng, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(lng))
			return ret(false, "please enter valid lng");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {lng:lng}, ret);
	}
	
	/**
	 * 
	 * Set lat from _id for Location
	 * 
	 * @param { id } _id _id of the field
	 * @param { string } lat lat to update
	 * @param { function } ret return callback
	 * 
	 */
	setLatFromId(_id, lat, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		if (!util.isSet(lat))
			return ret(false, "please enter valid lat");
		
		let filter = new Filter();
		filter.setId(_id);
		this.set(filter, {lat:lat}, ret);
	}
	
	/**
	 * 
	 * Get one row of Location
	 * 
	 * @param { Location.Filter } filter filter to select values
	 * @param { function } ret return callback
	 * 
	 */
	getOne(filter, opt, ret)
	{
		opt = new Opt(opt);
		logger.info("filter = " + JSON.stringify(filter));
		let o = {limit:opt.getLimit(), skip:opt.getSkip(), sort:opt.getSort()};
		
		this.getCol(function(col)
		{
			logger.info("mod::Location::getOne filter = " + JSON.stringify(filter));
			
			col.findOne(filter, o, function(err, doc)
			{
				if (!err && util.isSet(doc))
				{
					let val = new Location();
					val.load(doc);
					logger.info("mod::Location::getOne data = " + JSON.stringify(val));
					ret(true, val);
				}
				else
				{
					logger.error("ERROR : " + err);
					ret(false, null);
				}
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 *  Get one object of Location from _id
	 * 
	 * @param { id } id id to select values
	 * @param { function } ret return callback
	 * 
	 */
	get(id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		this.getOne({_id:new mongo.ObjectID(id)}, ret);
	}
	
	/**
	 * Get one object of Location from _id
	 * 
	 * @param { id } _id _id to select values
	 * @param { function } ret return callback
	 * 
	 */
	getFromId(_id, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		_id = mongo.ObjectID(_id);
		
		this.getOne(new Filter({_id:_id}), ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Location
	 * 
	 * @param { Location.Filter } ft filter object to select values
	 * @param { function } ret return callback
	 * 
	 */
	getL(aFt, opt, ret)
	{
		if (!Array.isArray(aFt))
			aFt = [aFt];
		
		for (let i = 0; i < aFt.length; i++)
			aFt[i] = new Filter(aFt[i]);
		
		opt = new Opt(opt);
		logger.info("aFt = " + JSON.stringify(aFt));
		let o = {limit:opt.getLimit(), skip:opt.getSkip(), sort:opt.getSort()};
		let filter = [];
		
		for (let iFt = 0; iFt < aFt.length; iFt++)
		{
			let filterEle = {};
			
			let ft = aFt[iFt];
			
			if (ft.getId() != undefined)
			{
				filterEle._id = ft.getId();
			}
			
			if (ft.getImei() != undefined)
			{
				filterEle.imei = ft.getImei();
			}
			
			if (ft.getLng() != undefined)
			{
				filterEle.lng = ft.getLng();
			}
			
			if (ft.getLat() != undefined)
			{
				filterEle.lat = ft.getLat();
			}
			
			if (ft.getCreatedTime() != undefined)
			{
				filterEle.createdTime = ft.getCreatedTime();
			}
			
			filter.push(filterEle);
		}
		
		if (filter.length == 1)
		{
			filter = filter[0];
		}
		else
		{
			let ft = {}
			ft["$or"] = filter;
			filter = ft;
		}
		
		logger.info("filter = " + JSON.stringify(filter) + "");
		
		this.getCol(function(col)
		{
			col.find(filter, o).toArray().then(function(vals)
			{
				let retArray = [];
				let len = vals.length;
				
				for (let idx = 0; idx < len; idx++)
				{
					let doc = vals[idx];
					let val = new Location();
					val.load(doc);
					logger.info("mod::Location::getL loaded = " + doc._id);
					retArray.push(val);
				}
				
				ret(true, retArray);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Get one list of objects of Location from imei
	 * 
	 * @param { string } imei imei to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromImei(imei, skip, limit, ret)
	{
		if (!util.isSet(imei))
			return ret(false, "please enter valid imei");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setImei(imei);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Location from lng
	 * 
	 * @param { string } lng lng to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromLng(lng, skip, limit, ret)
	{
		if (!util.isSet(lng))
			return ret(false, "please enter valid lng");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setLng(lng);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Location from lat
	 * 
	 * @param { string } lat lat to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromLat(lat, skip, limit, ret)
	{
		if (!util.isSet(lat))
			return ret(false, "please enter valid lat");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setLat(lat);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get one list of objects of Location from createdTime
	 * 
	 * @param { string } createdTime createdTime to select values
	 * @param { function } ret return callback
	 * 
	 */
	getLFromCreatedTime(createdTime, skip, limit, ret)
	{
		if (!util.isSet(createdTime))
			return ret(false, "please enter valid createdTime");
		
		if (!util.isInt(skip))
			return ret(false, "please enter valid skip count");
		
		if (!util.isInt(limit))
			return ret(false, "please enter valid limit");
		
		let opt = new Opt();
		opt.setSkip(skip);
		opt.setLimit(limit);
		
		let filter = new Filter();
		filter.setCreatedTime(createdTime);
		
		this.getL(filter, opt, ret);
	}
	
	/**
	 * 
	 * Get set of field
	 * 
	 * @param { field } field field name
	 * @param { function } ret return callback
	 * 
	 */
	getSet(field, ret)
	{
		this.getCol(function(col)
		{
			col.distinct(field, function(err, docs)
			{
				console.log(JSON.stringify(docs));
				ret(true, docs);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * 
	 * Search for list of objects of Location
	 * 
	 * @param { Location.Filter } ft filter object to select values
	 * @param { function } ret return callback
	 * 
	 */
	searchL(aFt, opt, ret)
	{
		if (!Array.isArray(aFt))
			aFt = [aFt];
		
		for (let i = 0; i < aFt.length; i++)
			aFt[i] = new Filter(aFt[i]);
		
		for (let iFt = 0; iFt < aFt.length; iFt++)
		{
			let ft = aFt[iFt];
			
			if (util.isSet(ft.imei))
				ft.setImei({$regex:new RegExp("^.*" + ft.imei + ".*$")});
			
			if (util.isSet(ft.lng))
				ft.setLng({$regex:new RegExp("^.*" + ft.lng + ".*$")});
			
			if (util.isSet(ft.lat))
				ft.setLat({$regex:new RegExp("^.*" + ft.lat + ".*$")});
			
			if (util.isSet(ft.createdTime))
				ft.setCreatedTime({$regex:new RegExp("^.*" + ft.createdTime + ".*$")});
			
		}
		
		logger.info("filter = " + JSON.stringify(aFt));
		
		this.getL(aFt, opt, ret);
	}
	/**
	 * Remove one row of Location
	 * 
	 * @param { Location.Filter } filter filter to select values
	 * @param { function } ret return callback
	 * 
	 */
	rmOne(filter, ret)
	{
		this.getCol(function(col)
		{
			logger.info("mod::Location::rmOne filter = " + JSON.stringify(filter));
			
			col.deleteOne(filter, function(err, doc)
			{
				if (!err)
					return ret(true);
				else
					ret(false, err);
			}.bind(this));
		}.bind(this));
	}
	
	/**
	 * Remove one object of Location from _id
	 * 
	 * @param { id } id id to select values
	 * @param { function } ret return callback
	 * 
	 */
	rm(id, ret)
	{
		if (!util.isSet(id))
			return ret(false, "please enter valid id");
		
		this.rmOne({_id:new mongo.ObjectID(id)}, ret);
	}
	
	/**
	 * Remove one object of Location from _id
	 * 
	 * @param { id } _id _id to select values
	 * @param { function } ret return callback
	 * 
	 */
	rmFromId(_id, ret)
	{
		if (!util.isSet(_id))
			return ret(false, "please enter valid _id");
		
		this.rmOne(new Filter({_id:_id}), ret);
	}
	
	/**
	 * 
	 * validate Location object
	 * 
	 * @param { Object } location input object
	 * @param { boolean } isNew to indicate validate new object or existing object
	 * 
	 */
	validate(location, isNew)
	{
		let vdtMsg = {};
		if (this.validateId(location.getId(), isNew).length > 0)
			vdtMsg._id = this.validateId(location.getId(), isNew);
		
		if (this.validateImei(location.getImei(), isNew).length > 0)
			vdtMsg.imei = this.validateImei(location.getImei(), isNew);
		
		if (this.validateLng(location.getLng(), isNew).length > 0)
			vdtMsg.lng = this.validateLng(location.getLng(), isNew);
		
		if (this.validateLat(location.getLat(), isNew).length > 0)
			vdtMsg.lat = this.validateLat(location.getLat(), isNew);
		
		if (this.validateCreatedTime(location.getCreatedTime(), isNew).length > 0)
			vdtMsg.createdTime = this.validateCreatedTime(location.getCreatedTime(), isNew);
		
		return vdtMsg;
	}
	
	/**
	 * 
	 * validate  _id
	 * 
	 * @param { id } _id validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateId(_id, isNew)
	{
		let ret = [];
		
		if (util.isSet(_id) && !util.isValidAlphabet(_id))
		{
			ret.push("please enter valid _id [alphabet error] (" + _id + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  imei
	 * 
	 * @param { string } imei validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateImei(imei, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(imei))
		{
			ret.push("please enter valid imei [empty value] (" + imei + ") ");
		}
		
		if (util.isSet(imei) && !util.isValidAlphabet(imei))
		{
			ret.push("please enter valid imei [alphabet error] (" + imei + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  lng
	 * 
	 * @param { string } lng validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateLng(lng, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(lng))
		{
			ret.push("please enter valid lng [empty value] (" + lng + ") ");
		}
		
		if (util.isSet(lng) && !util.isValidAlphabet(lng))
		{
			ret.push("please enter valid lng [alphabet error] (" + lng + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  lat
	 * 
	 * @param { string } lat validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateLat(lat, isNew)
	{
		let ret = [];
		
		if (isNew == true && !util.isSet(lat))
		{
			ret.push("please enter valid lat [empty value] (" + lat + ") ");
		}
		
		if (util.isSet(lat) && !util.isValidAlphabet(lat))
		{
			ret.push("please enter valid lat [alphabet error] (" + lat + ") ");
		}
		
		return ret;
	}
	
	/**
	 * 
	 * validate  createdTime
	 * 
	 * @param { string } createdTime validation result decsciption. undefined if everything is ok.
	 * 
	 * @returns { [string] } validation result decsciption. undefined if everything is ok.
	 * 
	 */
	validateCreatedTime(createdTime, isNew)
	{
		let ret = [];
		
		if (util.isSet(createdTime) && !util.isValidTime(createdTime))
		{
			ret.push("please enter valid createdTime [syntax error] (" + createdTime + ") ");
		}
		
		if (util.isSet(createdTime) && !util.isValidAlphabet(createdTime))
		{
			ret.push("please enter valid createdTime [alphabet error] (" + createdTime + ") ");
		}
		
		return ret;
	}
	
}

export default new LocationMod();

export class Opt
{
	constructor(data)
	{
		
		if (util.isSet(data))
			this.load(data);
	}
	
	/**
	 * 
	 * Load data from for options
	 * 
	 * @param { Object } data input object
	 * 
	 */
	load(data)
	{
		if (util.isSet(data.skip))
			this.setSkip(parseInt(data.skip));
		
		if (util.isSet(data.limit))
			this.setLimit(parseInt(data.limit))
		
		if (util.isSet(data.sort))
			this.setSort(data.sort);
		
	}
	
	setSkip(skip)
	{
		this.skip = util.isSet(skip) ? skip : 0;
	}
	
	getSkip()
	{
		return this.skip || 0;
	}
	
	setLimit(limit)
	{
		this.limit = util.isSet(limit) ? limit : 100;
	}
	
	getLimit()
	{
		return this.limit || 100;
	}
	
	setSort(sort)
	{
		this.sort = sort;
	}
	
	getSort()
	{
		return this.sort;
	}
	
}

export class Filter
{
	constructor(data)
	{
		
		if (util.isSet(data))
			this.load(data);
	}
	
	/**
	 * 
	 * Load data from a object of Location
	 * 
	 * @param { Object } data input object
	 * 
	 */
	load(data)
	{
		if (util.isDef(data._id))
			this.setId(data._id);
		
		if (util.isDef(data.imei))
			this.setImei(data.imei);
		
		if (util.isDef(data.lng))
			this.setLng(data.lng);
		
		if (util.isDef(data.lat))
			this.setLat(data.lat);
		
		if (util.isDef(data.createdTime))
			this.setCreatedTime(data.createdTime);
		
	}
	
	setId(_id)
	{
		this._id = mongo.ObjectId(_id);
	}
	
	getId()
	{
		return this._id;
	}
	
	setImei(imei)
	{
		this.imei = imei;
	}
	
	getImei()
	{
		return this.imei;
	}
	
	setLng(lng)
	{
		this.lng = lng;
	}
	
	getLng()
	{
		return this.lng;
	}
	
	setLat(lat)
	{
		this.lat = lat;
	}
	
	getLat()
	{
		return this.lat;
	}
	
	setCreatedTime(createdTime)
	{
		this.createdTime = createdTime;
	}
	
	getCreatedTime()
	{
		return this.createdTime;
	}
	
}
