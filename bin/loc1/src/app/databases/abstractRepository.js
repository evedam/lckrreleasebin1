import * as errMsg from '../helpers/errorMessages';

export default class AbstractRepository {

    constructor(){
        this.abstract()
    }

    abstract() {
        if (this.constructor === AbstractRepository) {
            throw new TypeError(errMsg.ABSTRACT_CLASS_INSTENTIATE_ERROR);
        }

        if (this.save === undefined) {
            throw new TypeError(errMsg.METHOD_NOT_IMPLEMENTED_ERROR("save"));
        }

        if (this.update === undefined) {
            throw new TypeError(errMsg.METHOD_NOT_IMPLEMENTED_ERROR("update"));
        }

        if (this.delete === undefined) {
            throw new TypeError(errMsg.METHOD_NOT_IMPLEMENTED_ERROR("delete"));
        }

        if (this.get === undefined) {
            throw new TypeError(errMsg.METHOD_NOT_IMPLEMENTED_ERROR("get"));
        }
    }

}