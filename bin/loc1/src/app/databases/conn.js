import MongoClient from 'mongodb';
import * as serverConfigs from '../../utils/serverConfigs';
import FileUtils from '../../utils/fileUtils';
import * as constants from '../helpers/constants';
import ConnectionPool from './connectionPool';

class Conn
{
	constructor()
	{
		
		let config = FileUtils.loadConfigs(serverConfigs.CONFIGS.MONGO);
		let HOST = process.env.DB_HOST || config.host;
		let PORT = process.env.DB_PORT || config.port;
		let DB = process.env.DB_NAME || config.db;
		let USER = process.env.MONGO_USER || config.username;
		let PWD = process.env.MONGO_PWD || config.password;
		
		let mongoURL = constants.MONGO_URL(USER, PWD, HOST, PORT, DB);
		
		this.connectionPool = new ConnectionPool(mongoURL, 5);
	}
	
	/**
	 * 
	 * Connect to database
	 * @param { function } fnc return callback
	 * 
	 */
	connect(fnc)
	{
		this.connectionPool.connect().then(function(db)
		{
			fnc(db);
		}.bind(this));
	}
}

export default new Conn();

