import MongoClient from "mongodb";
import logger from "../../utils/logger";

export default class ConnectionPool 
{
	/*********************************************************************************************************/
	constructor(mongoURL, connectionCount)
	{ 
		this.connections = [];
		this.index = 0;

		logger.info("constructor");
		for (let i = 0; i < connectionCount; i++)
		{
			logger.info("connection started " + i);
			MongoClient.connect(mongoURL, function(err, db)
			{
				if (!err)
				{
					logger.info("connection pool add " + i);
					this.connections.push(db);	
				}
				else
					logger.info("error while connection [" + err + "]");
			}.bind(this)); 
		}
	}
	
	
	/*********************************************************************************************************/
	/*  Connect to database                                                                                  */
	/*                                                                                                       */
	/* return:                                                                                             	 */
	/*  	Promise to get database connection								 */	
	/*                                                                                                       */
	/*********************************************************************************************************/
	connect()
	{
		return new Promise(function(resolve, reject)		
		{
			this.index = (this.index + 1) % this.connections.length;
			logger.info("connection count = " + this.connections.length);
			logger.info("use connection " + this.index);
			
			resolve(this.connections[this.index]);

		}.bind(this));
	} 
}
