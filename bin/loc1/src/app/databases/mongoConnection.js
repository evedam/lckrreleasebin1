import FileUtils from '../../utils/fileUtils';
import * as serverConfigs from '../../utils/serverConfigs'
import logger from '../../utils/logger'
import * as constants from '../helpers/constants';
import {MongoClient} from 'mongodb';

let _this;

class MongoConnection {

    constructor() {
        this.db = null;
        _this = this;
    }

    _createDB() {
        let config = FileUtils.loadConfigs(serverConfigs.CONFIGS.MONGO);
        let HOST = process.env.DB_HOST || config.host;
        let PORT = process.env.DB_PORT || config.port;
        let DB = process.env.DB_NAME || config.db;
        let USER = process.env.MONGO_USER || config.username;
        let PWD = process.env.MONGO_PWD || config.password;

        let mongoURL = constants.MONGO_URL(USER, PWD, HOST, PORT, DB);

        return MongoClient.connect(mongoURL);
    }

    getConnection() {
        return new Promise((resolve, reject)=>{
            if(_this.db) {
                return resolve(_this.db);
            } else {
                return _this._createDB()
                    .then(resolve)
                    .catch(reject);
            }
        });
    }

}

export default new MongoConnection();