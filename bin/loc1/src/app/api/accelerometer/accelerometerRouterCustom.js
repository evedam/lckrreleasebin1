
import express from 'express';
import accelerometerModule from './accelerometerModule';
import * as accelerometerRouter from './accelerometerRouter';
import * as serverConfigs from '../../../utils/serverConfigs';
import util from '../../../utils/util';
import logger from '../../../utils/logger';
import Rtr from '../../helpers/rtr';

let _this;

class AccelerometerRouterCustom extends accelerometerRouter.AccelerometerRouter
{
	constructor()
	{
		super()
		_this = this;
		
		logger.info("rtr::Accelerometer::constructor initializing router");

		_this.router.post('/get_by_id_list', _this.getLByIdList);
		
		return _this.router;
	}	

	getLByIdList(req, res, next)
	{
		let arr = req.body;
		let filter = []; 
		let opt = {}; 

		if (!util.isSet(arr) || !Array.isArray(arr))
			return _this.err(res, {ret:"filed", msg:"please enter valid id array"});
	
		for (let i = 0, len = arr.length; i < len; i++)
		{
			filter.push({_id:arr[i]});
		}
		
		accelerometerModule.getL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	} 
}

export default new AccelerometerRouterCustom();

