import Module from '../../helpers/module';
import accelerometerMod from '../../databases/accelerometerMod';

let _this = this;

class AccelerometerModule extends Module
{
	constructor()
	{
		
		super()
		_this = this;
	}
	
	add(data, ret)
	{
		accelerometerMod.add(data, ret);
	}
	
	set(filter, val, ret)
	{
		accelerometerMod.set(filter, val, ret);
	}
	
	setFromId(_id, val, ret)
	{
		accelerometerMod.setFromId(_id, val, ret);
	}
	
	setId(_id, val, ret)
	{
		accelerometerMod.setId(_id, val, ret);
	}
	
	setImei(imei, val, ret)
	{
		accelerometerMod.setImei(imei, val, ret);
	}
	
	setX(x, val, ret)
	{
		accelerometerMod.setX(x, val, ret);
	}
	
	setY(y, val, ret)
	{
		accelerometerMod.setY(y, val, ret);
	}
	
	setZ(z, val, ret)
	{
		accelerometerMod.setZ(z, val, ret);
	}
	
	setTime(time, val, ret)
	{
		accelerometerMod.setTime(time, val, ret);
	}
	
	setCreatedTime(createdTime, val, ret)
	{
		accelerometerMod.setCreatedTime(createdTime, val, ret);
	}
	
	setImeiFromId(_id, val, ret)
	{
		accelerometerMod.setImeiFromId(_id, val, ret);
	}
	
	setXFromId(_id, val, ret)
	{
		accelerometerMod.setXFromId(_id, val, ret);
	}
	
	setYFromId(_id, val, ret)
	{
		accelerometerMod.setYFromId(_id, val, ret);
	}
	
	setZFromId(_id, val, ret)
	{
		accelerometerMod.setZFromId(_id, val, ret);
	}
	
	setTimeFromId(_id, val, ret)
	{
		accelerometerMod.setTimeFromId(_id, val, ret);
	}
	
	setCreatedTimeFromId(_id, val, ret)
	{
		accelerometerMod.setCreatedTimeFromId(_id, val, ret);
	}
	
	getOne(filter, opt, ret)
	{
		accelerometerMod.getOne(filter, opt, ret);
	}
	
	get(id, ret)
	{
		accelerometerMod.get(id, ret);
	}
	
	getFromId(_id, ret)
	{
		accelerometerMod.getFromId(_id, ret);
	}
	
	getL(ft, opt, ret)
	{
		accelerometerMod.getL(ft, opt, ret);
	}
	
	getLFromImei(imei, skip, limit, ret)
	{
		accelerometerMod.getLFromImei(imei, skip, limit, ret);
	}
	
	getLFromX(x, skip, limit, ret)
	{
		accelerometerMod.getLFromX(x, skip, limit, ret);
	}
	
	getLFromY(y, skip, limit, ret)
	{
		accelerometerMod.getLFromY(y, skip, limit, ret);
	}
	
	getLFromZ(z, skip, limit, ret)
	{
		accelerometerMod.getLFromZ(z, skip, limit, ret);
	}
	
	getLFromTime(time, skip, limit, ret)
	{
		accelerometerMod.getLFromTime(time, skip, limit, ret);
	}
	
	getLFromCreatedTime(createdTime, skip, limit, ret)
	{
		accelerometerMod.getLFromCreatedTime(createdTime, skip, limit, ret);
	}
	
	getSet(field, ret)
	{
		accelerometerMod.getSet(field, ret);
	}
	
	searchL(ft, opt, ret)
	{
		accelerometerMod.searchL(ft, opt, ret);
	}
	
	rmOne(filter, ret)
	{
		accelerometerMod.rmOne(filter, ret);
	}
	
	rm(id, ret)
	{
		accelerometerMod.rm(id, ret);
	}
	
	rmFromId(_id, ret)
	{
		accelerometerMod.rmFromId(_id, ret);
	}
	
}

export default new AccelerometerModule();

