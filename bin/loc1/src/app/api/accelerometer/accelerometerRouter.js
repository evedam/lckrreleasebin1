import express from 'express';
import Rtr from '../../helpers/rtr';
import accelerometerModule from './accelerometerModule';
import * as serverConfigs from '../../../utils/serverConfigs';
import util from '../../../utils/util';
import logger from '../../../utils/logger';

let _this, router = new express.Router();

export class AccelerometerRouter extends Rtr
{
	constructor()
	{
		
		super()
		_this = this;
		
		logger.info("rtr::Accelerometer::constructor initializing router");
		
		//insert new item
		router.post('/add', _this.add);
		
		// update existing items
		router.post('/set', _this.set);
		router.post('/set_from_id', _this.setFromId);
		router.post('/set_id', _this.setId);
		router.post('/set_imei', _this.setImei);
		router.post('/set_x', _this.setX);
		router.post('/set_y', _this.setY);
		router.post('/set_z', _this.setZ);
		router.post('/set_time', _this.setTime);
		router.post('/set_created_time', _this.setCreatedTime);
		router.post('/set_imei_from_id', _this.setImeiFromId);
		router.post('/set_x_from_id', _this.setXFromId);
		router.post('/set_y_from_id', _this.setYFromId);
		router.post('/set_z_from_id', _this.setZFromId);
		router.post('/set_time_from_id', _this.setTimeFromId);
		router.post('/set_created_time_from_id', _this.setCreatedTimeFromId);
		
		// append to existing item
		
		// retrive data
		router.post('/get_one', _this.getOne);
		router.get('/get/:_id', _this.get);
		router.get('/get_from_id/:_id', _this.getFromId);
		router.post('/get_l', _this.getL);
		router.post('/get_l_from_imei', _this.getLFromImei);
		router.post('/get_l_from_x', _this.getLFromX);
		router.post('/get_l_from_y', _this.getLFromY);
		router.post('/get_l_from_z', _this.getLFromZ);
		router.post('/get_l_from_time', _this.getLFromTime);
		router.post('/get_l_from_created_time', _this.getLFromCreatedTime);
		
		router.post('/get_set', _this.getSet);
		
		// search 
		router.post('/search_l', _this.searchL);
		
		
		// remove items
		router.post('/rm_one', _this.rmOne);
		router.delete('/rm/:id', _this.rm);
		router.delete('/rm_from_id/:_id', _this.rmFromId);
		
		_this.router = router;
	}
	
	add(req, res, next)
	{
		let data = req.body;
		
		accelerometerModule.add(data, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		});
	}
	
	set(req, res, next)
	{
		accelerometerModule.set(req.body.filter, req.body.val, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		});
	}
	
	setFromId(req, res, next)
	{
		accelerometerModule.setFromId(req.body._id, req.body.val, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setId(req, res, next)
	{
		let id = req.body._id;
		let _id = req.body._id;
		
		accelerometerModule.setId(id, _id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setImei(req, res, next)
	{
		let id = req.body._id;
		let imei = req.body.imei;
		
		accelerometerModule.setImei(id, imei, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setX(req, res, next)
	{
		let id = req.body._id;
		let x = req.body.x;
		
		accelerometerModule.setX(id, x, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setY(req, res, next)
	{
		let id = req.body._id;
		let y = req.body.y;
		
		accelerometerModule.setY(id, y, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setZ(req, res, next)
	{
		let id = req.body._id;
		let z = req.body.z;
		
		accelerometerModule.setZ(id, z, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setTime(req, res, next)
	{
		let id = req.body._id;
		let time = req.body.time;
		
		accelerometerModule.setTime(id, time, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setCreatedTime(req, res, next)
	{
		let id = req.body._id;
		let createdTime = req.body.createdTime;
		
		accelerometerModule.setCreatedTime(id, createdTime, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setImeiFromId(req, res, next)
	{
		accelerometerModule.setImeiFromId(req.body._id, req.body.imei, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setXFromId(req, res, next)
	{
		accelerometerModule.setXFromId(req.body._id, req.body.x, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setYFromId(req, res, next)
	{
		accelerometerModule.setYFromId(req.body._id, req.body.y, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setZFromId(req, res, next)
	{
		accelerometerModule.setZFromId(req.body._id, req.body.z, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setTimeFromId(req, res, next)
	{
		accelerometerModule.setTimeFromId(req.body._id, req.body.time, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setCreatedTimeFromId(req, res, next)
	{
		accelerometerModule.setCreatedTimeFromId(req.body._id, req.body.createdTime, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getOne(req, res, next)
	{
		accelerometerModule.getOne(req.body.filter, req.body.opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	get(req, res, next)
	{
		let id = req.params["_id"];
		
		accelerometerModule.get(id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getFromId(req, res, next)
	{
		accelerometerModule.getFromId(req.params["_id"], function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getL(req, res, next)
	{
		let filter = req.body.filter;
		let opt = req.body.opt;
		
		accelerometerModule.getL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromImei(req, res, next)
	{
		let imei = req.body.imei;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromImei(imei, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromX(req, res, next)
	{
		let x = req.body.x;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromX(x, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromY(req, res, next)
	{
		let y = req.body.y;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromY(y, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromZ(req, res, next)
	{
		let z = req.body.z;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromZ(z, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromTime(req, res, next)
	{
		let time = req.body.time;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromTime(time, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromCreatedTime(req, res, next)
	{
		let createdTime = req.body.createdTime;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		accelerometerModule.getLFromCreatedTime(createdTime, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getSet(req, res, next)
	{
		let field = req.body.field;
		
		accelerometerModule.getSet(field, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	searchL(req, res, next)
	{
		let filter = req.body.filter;
		let opt = req.body.opt;
		
		accelerometerModule.searchL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rmOne(req, res, next)
	{
		accelerometerModule.rmOne(req.body.filter, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rm(req, res, next)
	{
		let id = req.params["id"];
		
		accelerometerModule.rm(id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success"});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rmFromId(req, res, next)
	{
		let _id = req.params["_id"];
		
		accelerometerModule.rmFromId(_id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success"});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
}

export default new AccelerometerRouter();

