import express from 'express';
import Rtr from '../../helpers/rtr';
import locationModule from './locationModule';
import * as serverConfigs from '../../../utils/serverConfigs';
import util from '../../../utils/util';
import logger from '../../../utils/logger';

let _this, router = new express.Router();

export class LocationRouter extends Rtr
{
	constructor()
	{
		
		super()
		_this = this;
		
		logger.info("rtr::Location::constructor initializing router");
		
		//insert new item
		router.post('/add', _this.add);
		
		// update existing items
		router.post('/set', _this.set);
		router.post('/set_from_id', _this.setFromId);
		router.post('/set_id', _this.setId);
		router.post('/set_imei', _this.setImei);
		router.post('/set_lng', _this.setLng);
		router.post('/set_lat', _this.setLat);
		router.post('/set_created_time', _this.setCreatedTime);
		router.post('/set_imei_from_id', _this.setImeiFromId);
		router.post('/set_lng_from_id', _this.setLngFromId);
		router.post('/set_lat_from_id', _this.setLatFromId);
		router.post('/set_created_time_from_id', _this.setCreatedTimeFromId);
		
		// append to existing item
		
		// retrive data
		router.post('/get_one', _this.getOne);
		router.get('/get/:_id', _this.get);
		router.get('/get_from_id/:_id', _this.getFromId);
		router.post('/get_l', _this.getL);
		router.post('/get_l_from_imei', _this.getLFromImei);
		router.post('/get_l_from_lng', _this.getLFromLng);
		router.post('/get_l_from_lat', _this.getLFromLat);
		router.post('/get_l_from_created_time', _this.getLFromCreatedTime);
		
		router.post('/get_set', _this.getSet);
		
		// search 
		router.post('/search_l', _this.searchL);
		
		
		// remove items
		router.post('/rm_one', _this.rmOne);
		router.delete('/rm/:id', _this.rm);
		router.delete('/rm_from_id/:_id', _this.rmFromId);
		
		_this.router = router;
	}
	
	add(req, res, next)
	{
		let data = req.body;
		
		locationModule.add(data, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		});
	}
	
	set(req, res, next)
	{
		locationModule.set(req.body.filter, req.body.val, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		});
	}
	
	setFromId(req, res, next)
	{
		locationModule.setFromId(req.body._id, req.body.val, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setId(req, res, next)
	{
		let id = req.body._id;
		let _id = req.body._id;
		
		locationModule.setId(id, _id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setImei(req, res, next)
	{
		let id = req.body._id;
		let imei = req.body.imei;
		
		locationModule.setImei(id, imei, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setLng(req, res, next)
	{
		let id = req.body._id;
		let lng = req.body.lng;
		
		locationModule.setLng(id, lng, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setLat(req, res, next)
	{
		let id = req.body._id;
		let lat = req.body.lat;
		
		locationModule.setLat(id, lat, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setCreatedTime(req, res, next)
	{
		let id = req.body._id;
		let createdTime = req.body.createdTime;
		
		locationModule.setCreatedTime(id, createdTime, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setImeiFromId(req, res, next)
	{
		locationModule.setImeiFromId(req.body._id, req.body.imei, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setLngFromId(req, res, next)
	{
		locationModule.setLngFromId(req.body._id, req.body.lng, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setLatFromId(req, res, next)
	{
		locationModule.setLatFromId(req.body._id, req.body.lat, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	setCreatedTimeFromId(req, res, next)
	{
		locationModule.setCreatedTimeFromId(req.body._id, req.body.createdTime, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getOne(req, res, next)
	{
		locationModule.getOne(req.body.filter, req.body.opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success", id:msg});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	get(req, res, next)
	{
		let id = req.params["_id"];
		
		locationModule.get(id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getFromId(req, res, next)
	{
		locationModule.getFromId(req.params["_id"], function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getL(req, res, next)
	{
		let filter = req.body.filter;
		let opt = req.body.opt;
		
		locationModule.getL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromImei(req, res, next)
	{
		let imei = req.body.imei;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		locationModule.getLFromImei(imei, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromLng(req, res, next)
	{
		let lng = req.body.lng;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		locationModule.getLFromLng(lng, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromLat(req, res, next)
	{
		let lat = req.body.lat;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		locationModule.getLFromLat(lat, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getLFromCreatedTime(req, res, next)
	{
		let createdTime = req.body.createdTime;
		let skip = req.body.skip;
		let limit = req.body.limit;
		
		locationModule.getLFromCreatedTime(createdTime, skip, limit, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	getSet(req, res, next)
	{
		let field = req.body.field;
		
		locationModule.getSet(field, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	searchL(req, res, next)
	{
		let filter = req.body.filter;
		let opt = req.body.opt;
		
		locationModule.searchL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rmOne(req, res, next)
	{
		locationModule.rmOne(req.body.filter, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rm(req, res, next)
	{
		let id = req.params["id"];
		
		locationModule.rm(id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success"});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
	rmFromId(req, res, next)
	{
		let _id = req.params["_id"];
		
		locationModule.rmFromId(_id, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, {ret:"success"});
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}
	
}

export default new LocationRouter();

