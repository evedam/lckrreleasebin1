import Module from '../../helpers/module';
import locationMod from '../../databases/locationMod';

let _this = this;

class LocationModule extends Module
{
	constructor()
	{
		
		super()
		_this = this;
	}
	
	add(data, ret)
	{
		locationMod.add(data, ret);
	}
	
	set(filter, val, ret)
	{
		locationMod.set(filter, val, ret);
	}
	
	setFromId(_id, val, ret)
	{
		locationMod.setFromId(_id, val, ret);
	}
	
	setId(_id, val, ret)
	{
		locationMod.setId(_id, val, ret);
	}
	
	setImei(imei, val, ret)
	{
		locationMod.setImei(imei, val, ret);
	}
	
	setLng(lng, val, ret)
	{
		locationMod.setLng(lng, val, ret);
	}
	
	setLat(lat, val, ret)
	{
		locationMod.setLat(lat, val, ret);
	}
	
	setCreatedTime(createdTime, val, ret)
	{
		locationMod.setCreatedTime(createdTime, val, ret);
	}
	
	setImeiFromId(_id, val, ret)
	{
		locationMod.setImeiFromId(_id, val, ret);
	}
	
	setLngFromId(_id, val, ret)
	{
		locationMod.setLngFromId(_id, val, ret);
	}
	
	setLatFromId(_id, val, ret)
	{
		locationMod.setLatFromId(_id, val, ret);
	}
	
	setCreatedTimeFromId(_id, val, ret)
	{
		locationMod.setCreatedTimeFromId(_id, val, ret);
	}
	
	getOne(filter, opt, ret)
	{
		locationMod.getOne(filter, opt, ret);
	}
	
	get(id, ret)
	{
		locationMod.get(id, ret);
	}
	
	getFromId(_id, ret)
	{
		locationMod.getFromId(_id, ret);
	}
	
	getL(ft, opt, ret)
	{
		locationMod.getL(ft, opt, ret);
	}
	
	getLFromImei(imei, skip, limit, ret)
	{
		locationMod.getLFromImei(imei, skip, limit, ret);
	}
	
	getLFromLng(lng, skip, limit, ret)
	{
		locationMod.getLFromLng(lng, skip, limit, ret);
	}
	
	getLFromLat(lat, skip, limit, ret)
	{
		locationMod.getLFromLat(lat, skip, limit, ret);
	}
	
	getLFromCreatedTime(createdTime, skip, limit, ret)
	{
		locationMod.getLFromCreatedTime(createdTime, skip, limit, ret);
	}
	
	getSet(field, ret)
	{
		locationMod.getSet(field, ret);
	}
	
	searchL(ft, opt, ret)
	{
		locationMod.searchL(ft, opt, ret);
	}
	
	rmOne(filter, ret)
	{
		locationMod.rmOne(filter, ret);
	}
	
	rm(id, ret)
	{
		locationMod.rm(id, ret);
	}
	
	rmFromId(_id, ret)
	{
		locationMod.rmFromId(_id, ret);
	}
	
}

export default new LocationModule();

