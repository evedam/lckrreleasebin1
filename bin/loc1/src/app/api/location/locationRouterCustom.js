
import express from 'express';
import locationModule from './locationModule';
import * as locationRouter from './locationRouter';
import * as serverConfigs from '../../../utils/serverConfigs';
import util from '../../../utils/util';
import logger from '../../../utils/logger';
import Rtr from '../../helpers/rtr';

let _this;

class LocationRouterCustom extends locationRouter.LocationRouter
{
	constructor()
	{
		super()
		_this = this;
		
		logger.info("rtr::Location::constructor initializing router");

		_this.router.post('/get_by_id_list', _this.getLByIdList);
		_this.router.post('/get_current_l', _this.getCurrentL);
		
		return _this.router;
	}	

	getLByIdList(req, res, next)
	{
		let arr = req.body;
		let filter = []; 
		let opt = {}; 

		if (!util.isSet(arr) || !Array.isArray(arr))
			return _this.err(res, {ret:"filed", msg:"please enter valid id array"});
	
		for (let i = 0, len = arr.length; i < len; i++)
		{
			filter.push({_id:arr[i]});
		}
		
		locationModule.getL(filter, opt, function(ret, msg)
		{
			if (ret == true)
			{
				return _this.ret(res, msg);
			}
			else
			{
				return _this.err(res, {ret:"failed", msg:msg});
			}
		}.bind(this));
	}

	getCurrentL(req, res, next)
	{
		let filter = {}; 
		let opt = {}; 
		let locations = [];

		locationModule.getSet("imei", (ret, imeis) =>
		{ 
			let len = imeis.length;
			for (let i = 0; i < len; i++)
			{
				let imei = imeis[i];

				locationModule.getOne({imei:imei}, {sort:{createdTime:-1}}, (ret, loc) =>
				{
					locations.push(loc);
					console.log("push location " + JSON.stringify(loc));

					if (i == len - 1)
					{
						return _this.ret(res, locations);
					}
				}); 
			} 
		}); 
	}

}

export default new LocationRouterCustom();

