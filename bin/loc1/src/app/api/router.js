// Import core modules
import express from 'express'
import * as constants from '../helpers/constants';

// Routers
import LocationRouterCustom from './location/locationRouterCustom';
import AccelerometerRouterCustom from './accelerometer/accelerometerRouterCustom';

const router = new express.Router();
const routerPath = `/${constants.VER}`;

router.use(`${routerPath}/${constants.LOCATION_API}`, LocationRouterCustom); 
router.use(`${routerPath}/${constants.ACCELEROMETER_API}`, AccelerometerRouterCustom); 

export default router;
