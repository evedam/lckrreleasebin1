/**
 * Created by sahanm on 4/20/17.
 */
export const ABSTRACT_CLASS_INSTENTIATE_ERROR = "Abstract classes can not instantiate";
export const METHOD_NOT_IMPLEMENTED_ERROR = (methodName)=>`"Method ${methodName} not implemented"`;