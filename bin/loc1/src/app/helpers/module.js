import logger from '../../utils/logger'

/**
 * Module class with reusable utils embedded to be extended by all other modules
 */
export default class Module{

    /**
     * @returns {object | null} - Module instance
     * @constructor
     */
    constructor() {
        let _this = this;
        _this.logger = logger;
        return _this;
    }
}
