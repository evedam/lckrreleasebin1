// Mysql constants
export const TRANSACTION_QUERY_COUNT_THRESHOLD = 5;

// REST constants
export const VER = "v1";
export const LOCATION_API = "loc";
export const ACCELEROMETER_API = "accelerometer";
export const CONTENT_TYPE = 'content-type';
export const X_KEY = 'x-key';
export const X_ACCESS_TOKEN = 'x-access-token';

export const APPLICATION_JSON = 'application/json';

export const RESOURCE_ID = "id";
export const RESOURCE_FILE_NAME = "fileName";
export const RESOURCE_AWS_FILE_NAME = "awsId";

// HTTP status codes
export const SUCCESS = 200;
export const INTERNAL_ERROR = 500;
export const BAD_REQUEST = 400;
export const NOT_FOUND = 404;

// Miscellaneous constants
export const ONE_SECOND = 1000;

//export const MONGO_URL = (user, password, host, port, db)=>`mongodb://${user}:${password}@${host}:${port}/${db}`;
export const MONGO_URL = (user, password, host, port, db)=>`mongodb://${host}:${port}/${db}`;
export const RESOURCE_COLLECTION = "Resource";
