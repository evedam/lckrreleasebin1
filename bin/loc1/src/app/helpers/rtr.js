
import Decorator from './decorator'
import * as constants from './constants' 
import logger from '../../utils/logger'

export default class Rtr extends Decorator  
{
	constructor() 
	{
		super();
		return this;
	}

	ret(res, data)
	{
		data.__dbg = {service:"loc1", ret:"success"};
		return res.status(constants.SUCCESS).json(data)
	}

	err(res, err)
	{
		this.logger.error(err);
		err.__dbg = {service:"loc1", ret:"error"};
		return res.status(constants.BAD_REQUEST).json(err);
	} 
}
