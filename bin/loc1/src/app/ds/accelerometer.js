import util from '../../utils/util';

export default class Accelerometer
{
	constructor()
	{
		
	}
	
	load(data)
	{
		if (util.isDef(data._id))
			this.setId(data._id)
		
		if (util.isDef(data.imei))
			this.setImei(data.imei)
		
		if (util.isDef(data.x))
			this.setX(data.x)
		
		if (util.isDef(data.y))
			this.setY(data.y)
		
		if (util.isDef(data.z))
			this.setZ(data.z)
		
		if (util.isDef(data.time))
			this.setTime(data.time)
		
		if (util.isDef(data.createdTime))
			this.setCreatedTime(data.createdTime)
		
	}
	
	setId(_id)
	{
		this._id = _id;
	}
	
	getId()
	{
		return this._id;
	}
	
	setImei(imei)
	{
		this.imei = imei;
	}
	
	getImei()
	{
		return this.imei;
	}
	
	setX(x)
	{
		this.x = x;
	}
	
	getX()
	{
		return this.x;
	}
	
	setY(y)
	{
		this.y = y;
	}
	
	getY()
	{
		return this.y;
	}
	
	setZ(z)
	{
		this.z = z;
	}
	
	getZ()
	{
		return this.z;
	}
	
	setTime(time)
	{
		this.time = time;
	}
	
	getTime()
	{
		return this.time;
	}
	
	setCreatedTime(createdTime)
	{
		this.createdTime = createdTime;
	}
	
	getCreatedTime()
	{
		return this.createdTime;
	}
	
}
