import util from '../../utils/util';

export default class Location
{
	constructor()
	{
		
	}
	
	load(data)
	{
		if (util.isDef(data._id))
			this.setId(data._id)
		
		if (util.isDef(data.imei))
			this.setImei(data.imei)
		
		if (util.isDef(data.lng))
			this.setLng(data.lng)
		
		if (util.isDef(data.lat))
			this.setLat(data.lat)
		
		if (util.isDef(data.createdTime))
			this.setCreatedTime(data.createdTime)
		
	}
	
	setId(_id)
	{
		this._id = _id;
	}
	
	getId()
	{
		return this._id;
	}
	
	setImei(imei)
	{
		this.imei = imei;
	}
	
	getImei()
	{
		return this.imei;
	}
	
	setLng(lng)
	{
		this.lng = lng;
	}
	
	getLng()
	{
		return this.lng;
	}
	
	setLat(lat)
	{
		this.lat = lat;
	}
	
	getLat()
	{
		return this.lat;
	}
	
	setCreatedTime(createdTime)
	{
		this.createdTime = createdTime;
	}
	
	getCreatedTime()
	{
		return this.createdTime;
	}
	
}
