import express from 'express'
import bodyParser from 'body-parser'
import router from './app/api/router'
import * as constants from './app/helpers/constants'
import cors from 'cors';

let app = express();

// Use middleware as required
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Base router
app.use('/', router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log("404 path = " + req.path); 
    let err = new Error('Not Found');
    err.status = constants.NOT_FOUND;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.status(err.status || constants.INTERNAL_ERROR);
    res.json({
        error: err.message
    });
    return next();
});

export default app;
