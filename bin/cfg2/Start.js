
const Msg = require("./Msg");
const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");
const Config = require("./Config");

module.exports = 
{
	process: function(pReq, pRes)
	{
		var sId = pReq.params.id; 
		var bInitiator = false;
		
		if (this.m_IdRoom[sId] == undefined)
		{
			Msg.addRoom(sId);
			bInitiator = true;	

			var oRoom = {};
			this.m_IdRoom[sId] = oRoom;
		}
		
		Log.d("id = " + sId + " initiator = " + bInitiator);

		var asMsg = Msg.getMsg(sId);
		var oRes= this.getResponse(sId, asMsg, bInitiator);

		Util.writeJsonRes(pRes, oRes);

		Log.d("join response = (" + JSON.stringify(oRes) + ")");
	},

	getResponse: function(sId, asMsg, bInitiator)
	{
		var sMediaConstrains = "";
		if (bInitiator)
			sMediaConstrains = "{\"audio\": true, \"video\": {\"mandatory\": {}, \"optional\": [{\"minWidth\": \"1280\"}, {\"minHeight\": \"720\"}]}}";
		else
			sMediaConstrains = "{\"audio\": true, \"video\": true}";

		var oRet = 
		{  
			"params":
			{  
				"is_initiator":bInitiator,
				"messages":asMsg,
				"pc_config":"{\"bundlePolicy\": \"max-bundle\", \"iceServers\": [], \"rtcpMuxPolicy\": \"require\"}",
				"ses_id":sId,
				"ice_server_url":Config.getUrlApi() + "/v1/cfg/turn",
				"ice_server_url_and":"https://computeengineondemand.appspot.com/turn?username=55013077&key=4080218913"
			},
			"result":"SUCCESS"
		};

		return oRet;
	},

	rmRoom: function(sId)
	{
		Log.d("remove room Join");
		delete this.m_IdRoom[sId];
	},

	m_IdRoom: {}
};
