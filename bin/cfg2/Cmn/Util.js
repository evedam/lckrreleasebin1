
const RestClient = require("node-rest-client").Client;
const Log = require("./Log");

var p_RestClient = new RestClient();

module.exports = 
{
	//****************************************************************************************************
	isEmpty: function(sStr)
	{
		return sStr == null || sStr == undefined || sStr == "";
	},

	//****************************************************************************************************
	isSet: function(pVar)
	{
		return typeof pVar != 'undefined' && pVar != null;
	},

	//****************************************************************************************************
	post: function(sUrl, pMsg, fnRet)
	{
		try
		{
			Log.d("cmn::Util::post url = " + sUrl + " msg = " + pMsg);
			getRestClient().post(sUrl, pMsg, function(pData, pRes)
			{
				Log.d("cmn::Util::post responst data (" + sUrl + ") = " + JSON.stringify(pData));
				fnRet(pData);
			});
		}
		catch (pExe)
		{
			Log.e("cmn::Util::post exception [" + pExe + "]");
			throw "cmn::Util::post Msg post exception [" + pExe + "]";
		}
	},

	//****************************************************************************************************
	writeJsonRes: function(pRes, oRes)
	{
		pRes.writeHead(200, {"Content-Type": "application/json"});
		pRes.end(JSON.stringify(oRes));
	},
	
	//****************************************************************************************************
	writeJsonResErr: function(pRes, oRes)
	{
		pRes.writeHead(200, {"Content-Type": "application/json"});
		pRes.status(400).end(JSON.stringify(oRes));
	}
};

//****************************************************************************************************
function getRestClient()
{
	return p_RestClient;
}
