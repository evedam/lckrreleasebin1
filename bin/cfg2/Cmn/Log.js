module.exports = 
{
	//****************************************************************************************************
	rv: function(sVal)
	{
		var e = new Error('dummy');
		var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
		.replace(/^\s+at\s+/gm, '')
		.replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
		.split('\n');

		process.stdout.write("\n");
		process.stdout.write("call stack = " + stack + "\n");
		process.stdout.write("(" + sVal + ")\n");
	},	

	//****************************************************************************************************
	r: function(sVal)
	{
		process.stdout.write(sVal + "\n");
	},

	//****************************************************************************************************
	dv: function(sVal)
	{
		this.rv(sVal);
	},

	//****************************************************************************************************
	d: function(sVal)
	{
		this.r(sVal);
	},

	//****************************************************************************************************
	e: function(sVal)
	{
		this.r("ERROR : " + sVal);
	},

	//****************************************************************************************************
	t: function(iTab, sVal)
	{
		var sTab = "";
		for (var iIdx = 0; iIdx < iTab; iIdx ++)
			sTab += "\t";
		this.d(sTab + sVal);
	}
};
