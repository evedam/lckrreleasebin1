
const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");

module.exports =
{
	process: function(pReq, pRes)
	{
		var Msg = this;
		var asBody = [];
		var sRoomId = pReq.params.id; 
		var iClientId = asTok[3];

		Log.d("url = " + pReq.url + " room_id = " + sRoomId + " client_id = " + iClientId);
		
		pReq.on('data', function(pBuf)
		{
			asBody.push(pBuf);
		}).on('end', function()
		{
			var sBody = Buffer.concat(asBody).toString();
			process.stdout.write("message request room_id = " + sRoomId + " body = " + sBody);

			Log.d("get Room " + JSON.stringify(Msg.m_IdMsg));

			if (Msg.m_IdMsg[sRoomId] != undefined)
				Msg.m_IdMsg[sRoomId].push(sBody);
			else
				Log.d("unable to send message room_id = " + sRoomId + " msg = " + sBody);
		});

		Util.writeJsonRes(pRes, {result:"SUCCESS"});
	},

	addRoom: function(sId)
	{
		Log.d("add room id = " + sId);
		this.m_IdMsg[sId] = new Array();

		Log.d("add Room " + JSON.stringify(this.m_IdMsg[sId]));
	},

	rmRoom: function(sId)
	{
		Log.d("remove room " + sId);
		delete this.m_IdMsg[sId];
	},

	getMsg: function(sRoomId)
	{
		Log.d("get messag " + JSON.stringify(this.m_IdMsg));

		var asMsg = this.m_IdMsg[sRoomId];		
		return asMsg;
	},

	m_IdMsg: {}
};
