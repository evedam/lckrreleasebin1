
const Msg = require("./Msg");
const Start = require("./Start");
const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");

module.exports = 
{
	process: function(pReq, pRes)
	{
		var sId = pReq.params.id;
		Log.d("Cls:: id = " + sId);

		Msg.rmRoom(sId);
		Start.rmRoom(sId);

		Util.writeJsonRes(pRes, {result:"SUCCESS"});
	}
};
