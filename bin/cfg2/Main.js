
const bSecure = false;
var Http = null;

if (bSecure)
	Http = require("https");
else	
	Http = require("http");

const Express = require("express");
const Logger = require("morgan");
const Path = require("path");
const Fs = require("fs");

const Msg = require("./Msg");
const Turn = require("./Turn");
const Start = require("./Start");
const Cls = require("./Cls");
const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");
const Config = require("./Config");

App = Express();

App.use(Logger("dev"));

Config.init();
var iPort = Config.getPort(); 

//****************************************************************************************************
App.post("/v1/message/:id", function(pReq, pRes)
{
	message(pReq, pRes);
});

//****************************************************************************************************
App.post("/v1/start/:id", function(pReq, pRes)
{
	Log.d("get " + pReq.url + " id = " + pReq.params.id);
	start(pReq, pRes);
});

//****************************************************************************************************
App.post('/v1/cls/:id', function(pReq, pRes)
{
	cls(pReq, pRes);
});

//****************************************************************************************************
App.get("/v1/turn", function(pReq, pRes)
{
	turn(pReq, pRes);
});

//****************************************************************************************************
App.all("*", function(pReq, pRes)
{
	Log.d("invalid url " + pReq.url);
	Util.writeJsonResErr(pRes, {state:"false", msg:"please do request with valid url", url:pReq.url});
});

if (bSecure)
{
	Http.createServer
		(
			{
				key:Fs.readFileSync("/etc/apache2/ssl/key.pem"),
				cert:Fs.readFileSync("/etc/apache2/ssl/cert.pem")
			},
			App
		).listen(iPort);
}
else
{
	Http.createServer(App).listen(iPort);
}

Log.d("listen " + iPort + " ... ");

//****************************************************************************************************
function message(pRequest, pResponse)
{
	Msg.process(pRequest, pResponse);
}

//****************************************************************************************************
function start(pRequest, pResponse)
{
	Start.process(pRequest, pResponse);
}

//****************************************************************************************************
function cls(pRequest, pResponse)
{
	Cls.process(pRequest, pResponse);
}

//****************************************************************************************************
function turn(pRequest, pResponse) 
{
	Turn.process(pRequest, pResponse);
}

