
const Log = require("./Cmn/Log");
const Util = require("./Cmn/Util");

module.exports = 
{
	process: function(pReq, pRes)
	{
		var oRes = 
			{
				lifetimeDuration: "43200.000s",
				iceServers:
					[
						{
							urls: ["turn:numb.viagenie.ca"],
							username: "webrtc@live.com",
							credential: "muazkh"
						},
						{
							urls: ["stun:stun01.sipphone.com", "stun:stun.ekiga.net", "stun:stun.l.google.com:19302"]
						}
					]
			};

		Log.d("response = " + JSON.stringify(oRes));
		Util.writeJsonRes(pRes, oRes);
	}
};
