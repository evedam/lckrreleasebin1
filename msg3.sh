
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/msg3

echo starting msg3 ...

cd $HOME && ./msg3 --port=3002 --url-api=http://127.0.0.1:3001 --url-usr=http://127.0.0.1:3005 --dir-data=Data --db=msg3_1 --db-usr=msg3_usr_1 --save-files &> $CWD/log/msg3_log.txt &

