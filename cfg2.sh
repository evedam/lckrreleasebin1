
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/cfg2

echo starting cfg2 ...

cd $HOME && node Main.js --port=3003 --url-api=http://127.0.0.1:3001 &> $CWD/log/cfg2_log.txt &

