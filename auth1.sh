
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/auth1

echo starting auth1 ...

cd $HOME && ./auth1 --port=3006 --url-usr=http://127.0.0.1:3005 --db=auth1_1 &> $CWD/log/auth_log.txt &
