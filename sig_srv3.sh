
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/sig_srv3

echo starting sig_srv3 ...


cd $HOME && node Main.js --secure=false --port=80 --port-http=3001 --socket-ping-timeout=10000 --socket-ping-interval=10000 --url-msg=http://127.0.0.1:3002 --url-cfg=http://127.0.0.1:3003 --url-usr=http://127.0.0.1:3005 --url-web=http://127.0.0.1:3004 --url-auth=http://127.0.0.1:3006 --url-apk=http://127.0.0.1:3007 --url-device=http://127.0.0.1:3008 --url-loc=http://127.0.0.1:3009 --queue-msg --reconnect-with-ping=false &> $CWD/log/sig_srv3_log.txt &

cd $HOME/static && rm -fv apk  
cd $HOME/static && ln -sv ../../apk1/Data/Apks apk  
