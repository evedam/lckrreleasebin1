
#!/bin/bash

CWD=`pwd`

echo importing binaries ...

rm -rf bin
mkdir -p bin

mkdir -pv bin/apk1
cp ../../apk1/bin/apk1 bin/apk1 -v 

mkdir -pv bin/auth1
cp ../../auth1/bin/auth1 bin/auth1 -v 

mkdir -pv bin/device1
cp ../../device1/bin/device1 bin/device1 -v 

mkdir bin/msg3
cp ../../msg3/bin/msg3 bin/msg3 -v 

mkdir bin/usr1
cp ../../usr1/bin/usr1 bin/usr1 -v 

echo importing sigsrv3 
cp -rf ../../sig_srv3 bin 
rm -rf bin/sig_srv3/.git

echo importing cfg2
cp -rf ../../cfg2 bin
rm -rf bin/cfg/.git

echo importing web2
cp -rf ../../web2 bin
rm -rf bin/web2/.git

echo importing lckr_web1
cp -rf ../../lckr_web1 bin
rm -rf bin/lckr_web1/.git

echo importing loc1 
cp -rf ../../loc1 bin 
rm -rf bin/loc1/.git

./add_release_note.sh
