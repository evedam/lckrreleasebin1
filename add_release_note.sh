
#!/bin/bash

CWD=`pwd`
relFile="releaseNote.txt";
date=`date +%Y-%m-%d:%H:%M:%S`

echo adding release note 

repoNames=(apk1 auth1 cfg2 device1 lckr_web1 loc1 msg3 sig_srv3 usr1 web2)

echo "" >> $relFile
echo "****************************************************************************************************" >> $relFile
echo "* DATE "$date >> $relFile
echo "* branch `git branch` commit `git rev-parse HEAD`" >> $relFile
echo "****************************************************************************************************" >> $relFile
echo "name|commit" >> $relFile
for repoName in "${repoNames[@]}" 
do
	:
	echo adding reselase note for $repoName
	echo $repoName "|" `git --git-dir=../../$repoName/.git --work-tree=../../$repoName/ rev-parse HEAD` >> $relFile
done

