
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/apk1

echo starting apk1 ...

cd $HOME && ./apk1 --port=3007 --url-api=http://127.0.0.1:3001 --url-msg=http://127.0.0.1:3002 --url-usr=http://127.0.0.1:3005 --dir-data=Data --db=apk1_1 --db-usr=apk1_usr &> $CWD/log/apk1_log.txt &

