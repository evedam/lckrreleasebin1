
#!/bin/bash

CWD=`pwd`
HOME=$CWD/bin/device1

echo starting device1 ...

cd $HOME && ./device1 --port=3008 --url-api=http://127.0.0.1:3001 --url-usr=http://127.0.0.1:3005 --db=device1_1 --dir-data=Data  &> $CWD/log/device_log.txt &
